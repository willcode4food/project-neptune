var gulp = require('gulp'),
    minifycss = require('gulp-minify-css'),  
    order = require('gulp-order'),
    gutil = require('gulp-util'),
    uglify = require('gulp-uglify'),
    del = require('del'),
    wait = require('gulp-wait'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    deployAzure = require('gulp-deploy-azure-cdn'),
    notify = require('gulp-notify'),
    browserSync = require("browser-sync").create(),
    fatalLevel = require('yargs').argv.fatal,
    preprocess = require('gulp-preprocess'),
    hashCreator = require('gulp-hash-creator'),
    replace = require('gulp-replace'),
    hash = hashCreator({
        content:Date()
    }),
    ERROR_LEVELS = ['error','warning'],
    baseFileNameEngine = 'playcraft-dist',
    baseFileNameGame = 'pn-dist',
    baseFileNameStyles = 'game';
 
/********   Error Handling *************/

// Return true if the given level is equal to or more severe than
// the configured fatality error level.
// If the fatalLevel is 'off', then this will always return false.
// Defaults the fatalLevel to 'error'.
function isFatal(level) {
   return ERROR_LEVELS.indexOf(level) <= ERROR_LEVELS.indexOf(fatalLevel || 'error');
}
// Handle an error based on its severity level.
// Log all levels, and exit the process for fatal levels.
function handleError(level, error) {

   gutil.log(gutil.colors.red(error.message));
   

   if (isFatal(level)) {
      process.exit(1);
   }
  // this.emit('end');
}

// helper callback methods to the on() stream method
function onError(error) { handleError.call(this, 'error', error);}
function onWarning(error) { handleError.call(this, 'warning', error);}
/*************************************/

gulp.task('styles', ['build-prod-clean'],function() {
  	return gulp.src('game/styles/*.css')
    .pipe(wait(1000))
    .pipe(rename({basename: baseFileNameStyles, suffix: '-' + hash}))
    .pipe(minifycss()).on('error', notify.onError(function(error){
            onWarning(error);
            return "Warning! Problem in Sass Task";

         }))  
    .pipe(gulp.dest('game/dist/styles'))    
    .pipe(notify({ message: 'Styles Minified' }));
});
gulp.task('engine-deps',['build-prod-clean'],function() {
    return gulp.src('./bower_components/objects.js/gamecore.js')
            .pipe(gulp.dest('./playcraftjs/lib/ext'));
});
gulp.task('engine',['engine-deps','build-prod-clean'],function(){
    return gulp.src(['playcraftjs/lib/**/*.js',
     '!playcraftjs/lib/**/grunt.js',
     '!playcraftjs/lib/**/*.min.js', 
     '!playcraftjs/lib/ext/gamecore.js/gamecore.js'
     ])  
        .pipe(order([
            // externals
            'packed.js', // included only when packing
            // gamecore
            'ext/gamecore.js',
            'ext/box2dweb.2.1a-pc.js',
            // playcraft engine
            'playcraft.js',
            'boot.js', // <--- must be first after playcraft.js for engine scripts (sets up some translations)
            'input.js',
            'ext/base64.js',
            'hashmap.js',
            'tools.js',
            'color.js',
            'debug.js',
            'device.js',
            'sound.js',
            'layer.js',
            'entitylayer.js',
            'tileset.js',
            'tilemap.js',
            'tilelayer.js',
            'entity.js',
            'sprite.js',
            'spritesheet.js',
            'math.js',
            'image.js',
            'scene.js',
            'game.js',
            'loader.js',
            'dataresource.js',
            'components/component.js',
            'components/circle.js',
            'components/poly.js',
            'components/scale.js',
            'components/spin.js',
            'components/physics.js',
            'components/alpha.js',
            'components/joint.js',
            'components/expiry.js',
            'components/originshifter.js',
            'components/debuginfo.js',
            'components/spatial.js',
            'components/overlay.js',
            'components/clip.js',
            'components/activator.js',
            'components/input.js',
            'components/fade.js',
            'components/rect.js',
            'components/text.js',
            'components/sprite.js',
            'components/layout.js',
            'components/particleemitter.js',
            'es/entitymanager.js',
            'es/systemmanager.js',
            'systems/system.js',
            'systems/entitysystem.js',
            'systems/physics.js',
            'systems/effects.js',
            'systems/particles.js',
            'systems/input.js',
            'systems/expiry.js',
            'systems/activation.js',
            'systems/render.js',
            'systems/layout.js',
            // custom modules for the project neptune application
            'custom/systems/forminput.js',                        
            'custom/localstore.js',
            'custom/formlayer.js',
            'custom/textboxentity.js',
            'custom/buttonentity.js',
            'custom/tilelayerscroller.js',
            'custom/httpclient.js',
            'custom/entitylayerscroller.js',
            'custom/entitydicelayer.js',
            'custom/spritesheetscroller.js',
            'ext/zynga-scroller/Animate.js',
            'ext/zynga-scroller/Scroller.js',
            'ext/sjcl/sjcl.js',            
            'custom/components/textinput.js',
            'custom/components/buttoninput.js'
        ]))
        .pipe(concat(baseFileNameEngine+'.js'))
        .pipe(gulp.dest('game/dist/js'))   
        .pipe(rename({suffix: '-' + hash}))
        .pipe(uglify()).on('error',notify.onError(function(error){
            onError(error);
            return "Error in Engine Build Task";

         }))    
        .pipe(gulp.dest('game/dist/js'))    
        .pipe(notify({     
           message: 'Playcraft Engine Scripts built' 
    }));
 });

gulp.task('game', ['build-prod-clean','preprocess-game-js-prod'], function() {
    return gulp.src(['game/js/**/*.js', '!game/authComplete.js'])    
     .pipe(order([
            'hubscene.js',
            'loginscene.js',
            'menuscene.js',
            'player.js',
            'playerloader.js',
            'playerui.js',
            'zone.js',
            'shipzone.js',
            'tntilemap.js',
            'util.js',
            'background.js',
            'backgroundsarfieldlayer.js',
            'board.js',
            'boardtile.js',
            'boardtilemap.js',
            'cardtextlayoutfactory.js',
            'classificationzone.js',
            'constants.js',
            'deviceconfigfactory.js',
            'devicedetection.js',
            'dicezone.js',
            'pnhttpclient.js',
            'entitydicelayer.js',
            'game.js',
            'gamescene.js',
            'gamestateloader.js',
            'holdingzone.js',
            'components/carddata.js',
            'components/dicedata.js',
            'systems/boardphysics.js',
            'systems/cardcontrols.js'
     ]))  
     .pipe(concat(baseFileNameGame + '.js'))
     .pipe(gulp.dest('game/dist/js'))    
     .pipe(rename({suffix: '-' + hash}))
     .pipe(uglify()).on('error',notify.onError(function(error){
            onError(error);
            return "Error in Game Build Task";

         }))        
     .pipe(gulp.dest('game/dist/js'))       
     .pipe(notify({
         message: 'Game Scripts Built' }));
 });

gulp.task('preprocess-html-local', function(){
 return gulp.src('game/preprocess/html/index-preprocess.html')
    .pipe(preprocess({
        context:{
            NODE_ENV: 'local',
            DEBUG: true
        }

    }))
    // .pipe(replace(baseFileNameStyles + '.css', baseFileNameStyles + '-' + hash + '.css'))
    .pipe(rename(function(path){
         path.basename = path.basename.replace('-preprocess','');
    }))
    .pipe(gulp.dest('./'))    
    .pipe(notify({message:'Preprocess HTML Succesful'}));

});

gulp.task('preprocess-html-prod', function(){
 return gulp.src('game/preprocess/html/index-preprocess.html')

    .pipe(preprocess({
        context:{
            NODE_ENV: 'prod',
            DEBUG: false
        }

    }))
    .pipe(replace(baseFileNameGame, baseFileNameGame + '-' + hash))
    .pipe(replace(baseFileNameEngine, baseFileNameEngine + '-' + hash))
    .pipe(replace(baseFileNameStyles + '.css', baseFileNameStyles + '-' + hash + '.css'))
    .pipe(rename(function(path){

         path.basename = path.basename.replace('-preprocess','');
    }))

    
    .pipe(gulp.dest('game/dist'))    
    .pipe(notify({message:'Preprocess HTML Succesful'}));

});
gulp.task('preprocess-game-js-local', function(){
 return gulp.src('game/preprocess/js/*.js')
    .pipe(preprocess({
        context:{
            NODE_ENV: 'local',
            DEBUG: true
        }

    }))
    .pipe(rename(function(path){
         path.basename = path.basename.replace('-preprocess','');
    }))
    .pipe(gulp.dest('game/js'))    
    .pipe(notify({message:'Preprocess JS Succesful'}));

});
gulp.task('preprocess-game-js-prod', function(){
 return gulp.src('game/preprocess/js/**/*')
    .pipe(preprocess({
        context:{
            NODE_ENV: 'prod',
            DEBUG: true
        }

    }))
    .pipe(rename(function(path){
         path.basename = path.basename.replace('-preprocess','');
    }))
    .pipe(gulp.dest('game/js'))    
    .pipe(notify({message:'Preprocess JS Succesful'}));

});
gulp.task('deploy-security', function (){
    return gulp.src(['./authComplete.js','./authComplete.html'])
           .pipe(gulp.dest('./game/dist'));

});
gulp.task('build-prod-clean', function(){
    return del(['game/dist/**/*'], function(err, paths){
        notify({message:'Deleted Distribution Folder'});
    });
});
gulp.task('build-prod',['build-prod-clean','deploy-security','styles', 'game','engine','engine-deps','preprocess-html-prod'], function(){
    return del(['game/dist/js/pn-dist.js','game/dist/js/playcraft-dist.js'], function(err, paths){
        notify({message:'Deleted Non-Minified Files'});
    });
});
gulp.task('deploy-prod', function(){
     return gulp.src('game/dist/**',['*.js','*.css','*.html'], {
        base: 'game/dist' // the base directory in which the file is located. The relative path of file to this directory is used as the destination path
        })
     //.pipe(wait(4000))
     .pipe(deployAzure({
        containerName: 'games', // container name 
        serviceOptions: ['projectneptune', 'RAgAzeGZ8nvI++j/3bx4jr94kfptwYyYtJCcezax+h7vVkPXGQJMaN2DmuBSUnmXuoWlCHkUMmUirwiPZcYMYw=='], // custom arguments to azure.createBlobService
        folder: 'pn', // path within container
        zip: true, // gzip files if they become smaller after zipping, content-encoding header will change if file is zipped
        deleteExistingBlobs: true, // true means recursively deleting anything under folder
        concurrentUploadThreads: 10, // number of concurrent uploads, choose best for your network condition
        metadata: {
            cacheControl: 'public, max-age=31530000', // cache in browser
            cacheControlHeader: 'public, max-age=31530000' // cache in azure CDN. As this data does not change, we set it to 1 year
        },
        testRun: false // test run - means no blobs will be actually deleted or uploaded, see log messages for details
    })).on('error',notify.onError(function(error){
            onError(error);
            return "Error Azure Deploy Task";

    }))
    .pipe(notify({onLast: true, message:'Production Deploy Successful'}));

});

gulp.task('build-prod-test',['build-prod-clean','styles', 'game','engine','engine-deps','preprocess-html-prod'], function(){
    browserSync.init({
        //proxy: "http://local.eddiegames.com:1120/game",
        // open: 'external'
        server: {
                baseDir: 'game/dist',
                index:'index.html',

            },
        port: 1120
    });
    gulp.watch('game/styles/**/*.{css,sass,scss}', ['styles']);
    gulp.watch('game/js/**/*.js', ['game']);
    gulp.watch('playcraftjs/lib/**/*.js', ['engine']);
    gulp.watch('game/*.html').on('change', browserSync.reload);
    gulp.watch('game/js/**/*.js').on('change', browserSync.reload);
    gulp.watch('playcraftjs/lib/**/*.js').on('change', browserSync.reload);

})
gulp.task('default', ['preprocess-html-local','build-prod-clean','engine-deps','preprocess-game-js-local'], function() {
    browserSync.init({
        server: {
                baseDir: './',
                index:'index.html',

            },
        port: 1120
    });
    notify({message:'Dev Watch Ready'});
    gulp.watch('game/styles/**/*.{css,sass,scss}', ['styles']);
    gulp.watch('game/*.html').on('change', browserSync.reload);
    gulp.watch('game/preprocess/**/*.js',['preprocess-game-js-local']);
    gulp.watch('game/js/**/*.js').on('change', browserSync.reload);
    gulp.watch('playcraftjs/lib/**/*.js').on('change', browserSync.reload);

});
