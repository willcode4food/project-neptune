//
// Playcraft Engine Simpler Server
// This is a simple example server to run games locally, it's not intended for production use
//
var port = 1120;
var requirejs = require('requirejs');
var express = require('express');
var logger  = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var app = express();
var path = require('path');
var webRoot = path.resolve(process.env.PLAYCRAFT_WEBROOT || '.');
var playcraftHome = path.resolve(process.env.PLAYCRAFT_HOME || '..');

app.set('port', process.env.PORT || port);
app.use(logger('dev'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(methodOverride());

app.set('view engine', 'jade');
app.set('view options', { doctype:'html', pretty:true, layout:false });
['playcraftjs/lib', 'playcraftjs/dist', 'game'].forEach(function(p) {
  app.use('/' + p, express.static(path.join(playcraftHome + '/' + p)));    
});

//app.get('/playcraftjs/lib',  express.static(path.resolve(playcraftHome + '/playcraftjs/lib')));
app.get(webRoot);
app.engine('html', require('ejs').renderFile);


app.use(app.router);

// Routes
app.get('/', function(req, res)
{
  app.set('views', webRoot);
  res.render('index.html');
});
app.get('/game', function (req, res)
{
    app.set('views', playcraftHome+'/game');
    res.render('index.html');
    res.render('authComplete.html');
});

//////////////////////////////////////////////////////////////////////////////
//
// Fire it all up!
//
//////////////////////////////////////////////////////////////////////////////


// Start the app server

app.listen(port, function ()
{
  console.log("Playcraft Engine is running from "+webRoot+" with game in "+playcraftHome);
  console.log("Connect using http://localhost:"+port+" or http://127.0.0.1:"+port);
});



