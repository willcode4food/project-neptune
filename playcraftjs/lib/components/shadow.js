pc.components.Shadow = pc.components.Component.extend('pc.components.Shadow',
{
		create:function (options)
        {
            var n = this._super();
            n.config(options);
            return n;
        }
},
{
	offSetX: null,
	offSetY: null,
	blur: null,
	color:null,
	init: function(){
		this._super('shadow');
	},
	config: function(options){
		this.offSetY = options.offSetY;
		this.offSetX = options.offSetX;
		this.blur = options.blur;
		if (options.color)
        {
            if (this.color == null)
                this.color = pc.Color.create(options.color);
            else
                this.color.set(options.color);
        } else
              this.color = null;
	}
});