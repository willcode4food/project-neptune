/**
 * Playcraft Engine - (C)2012 Playcraft Labs, Inc.
 * See licence.txt for details
 */

/**
 * @class pc.components.TextLayout
 * @description
 * [Extends <a href='pc.components.Component'>pc.components.Component</a>]<BR>
 * [Used in <a href='pc.systems.Render'>pc.systems.Render</a>]
 * <p>
 * Adds display text to an entity.
 */
pc.components.TextLayout = pc.components.Component.extend('pc.components.TextLayout',
    /** @lends pc.components.TextLayout */
    {
       
        create: function(options)
        {
            var n = this._super();
            n.config(options);
            return n;
        }
    },
    /** @lends pc.components.Text.prototype */
    {
        /** pc.Color representing fill color */
        layout: null,
       
        /**
         * Constructs a new component. See create method for options
         * @param {Object} options Options
         */
        init: function(options)
        {
            this._super('textLayout');
            this.layout = [];
            if (pc.valid(options))
                this.config(options);
        },

        /**
         * Configures the component. See create method for options
         * @param {Object} options Options
         */
        config: function(options)
        {
            
            this.layout = pc.checked(options.layout,null);
        }

    });


