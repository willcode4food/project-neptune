﻿/**
 * Playcraft Engine - (C)2012 Playcraft Labs, Inc.
 * See licence.txt for details
 */

/**
 * @class pc.Ajax
 * @description
 * [Extends <a href='pc.Base'>pc.Base</a>]
 * <p>
 * A static class that facilitates ajax requests 
 */


pc.Ajax = pc.Base.extend('pc.Ajax',
 {},
 {

        _caller:null,

        init: function(caller)
        {
            this._caller = caller;
        },
        get: function(url) {
            // Return a new promise.
            return new Promise(function(resolve, reject) {
                // Do the usual XHR stuff
                var req = new XMLHttpRequest();
                req.open('GET', url);

                req.onload = function() {
                    // This is called even on 404 etc
                    // so check the status
                    if (req.status == 200) {
                        // Resolve the promise with the response text
                        resolve(req.responseText, this._caller);
                    }
                    else {
                        // Otherwise reject with the status text
                        // which will hopefully be a meaningful error
                        reject(Error(req.statusText));
                    }
                };

                // Handle network errors
                req.onerror = function() {
                    reject(Error("Network Error"));
                };

                // Make the request
                req.send();
            });
        }
       
    });