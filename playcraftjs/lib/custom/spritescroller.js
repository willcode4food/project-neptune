﻿pc.SpriteScroller = pc.Sprite.extend('pc.SpriteScroller',
{},
{
    create: function (spriteSheet) {
        var n = this._super(spriteSheet);
        n.config(spriteSheet);
        return n;
    },
    init:function(spriteSheet)
    {
        this._super(spriteSheet);
    },
});