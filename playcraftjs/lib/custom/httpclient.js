﻿/**
 *   Project Neptune - (C)2015 Eddie Games Inc.
 *   Author: Marc D. Arbesman
 */

/**
 *  Class: pc.HttpClient
 *  Extends: pc.Base
 *  
 *  Description: A client to send asynchronous XML HTTP Requests
 *  
 */

pc.HttpClient = pc.Base.extend('pc.HttpClient',
{},
{
    method: null,
    url: null,
    cache: null,
    mimeType: null,
    responseType: null,
    onComplete: null,
    onSuccess: null,
    onError: null,
    onLoading: null,
    onUnauthorize: null,
    method: null,
    MAX_XHR_WAITING_TIME: null,
    formData: null,
    callingObject: null,
    _onBeforeSend: null,
    _error: null,
    _success: null,
    _complete: null,
    _serialize: null,
    _unauthorize: null,
    _xhr: null,
    

    init: function (options) {
        this.config(options);
    },
    config: function (options) {
        // if (!pc.valid(options.callingObject)) {
        //     throw 'No Scene specified to call back to ';
        // }
        this.callingObject = pc.checked(options.callingObject,{});
        this.cache = pc.checked(options.cache, true);
        this.MAX_XHR_WAITING_TIME = 5000;
        this.onComplete = pc.checked(options.onComplete, this._complete);
        this.onSuccess = pc.checked(options.onSuccess, this._success)
        this.onError = pc.checked(options.onError, this._error);
        this.onLoading = pc.checked(options.onLoading, this._loading);
        this.onUnauthorize = pc.checked(options.onUnauthorize, this._unauthorize);
        this._onBeforeSend = pc.checked(options.onBeforeSend, this._onBeforeSend);
        this._xhr = new XMLHttpRequest();
    },
    postFormAsync: function (options) {
        /// <summary>
        /// Handles POST requests
        /// </summary>
        /// <param name="options"></param>
        this.postSetup(options);
        this.formData = this._serialize(options.formData);
        this._sendAjax();
       
    },
    postAsync: function (options) {
        /// <summary>
        /// Handles POST requests
        /// </summary>
        /// <param name="options"></param>
        this._xhr.headersToSet = options.headers;
        this.postSetup(options);
        this.formData = options.formData;
        this._sendAjax();

    },
    postSetup: function(options){
        if (!pc.valid(options.url)) {
            throw 'No URL specified';
        }
        // if (!pc.valid(options.formData)) {
        //     throw 'No form data was sent';
        // }
        // if (options.formData.length < 1) {
        //     throw 'Form data not present';
        // }
        this.url = options.url;
        this.method = 'POST';
        return;
    },
  
    getAsync: function (options) {
        /// <summary>
        /// Handles GET requests
        /// </summary>
        /// <param name="options"></param>
        if (!pc.valid(options.url)) {
            throw 'No URL specified';
        }
        if (!pc.valid(options.headers)) {
            throw 'Http Headers not found';
        }
        // set all headers
        // we can assume we'll have at least one for the bearer token that must be set 
        // upon every request.
        this._xhr.headersToSet = options.headers;
        this.method = 'GET';
        this.url = options.url;
        this._sendAjax();

    },
    delAsync: function (options) {
        if (!pc.valid(options.url)) {
            throw 'No URL specified';
        }
        if (!pc.valid(options.headers)) {
            throw 'Http Headers not found';
        }
        // set all headers
        // we can assume we'll have at least one for the bearer token that must be set 
        // upon every request.
        this._xhr.headersToSet = options.headers;
        this.method = 'DELETE';
        this.url = options.url;
        this._sendAjax();
    },
    _sendAjax: function () {
        /// <summary>
        /// Initiates the XMLHTTPRequest 
        /// handles 200, 400 and 401 responses
        /// </summary>
        var url = this.url;

        this._xhr.self = this;
        
        this._xhr.onreadystatechange = function () {
            if (this.readyState === 1) {
                this.self.onLoading(this.self);
            }
            if (this.readyState === 4) {
                //clearTimeout(timer);
                if (this.status === 200) {
                    this.self.onSuccess && this.self.onSuccess(JSON.parse(this.responseText), this.self);
                    this.self.onComplete && this.self.onComplete(this.self);
                } else if (this.status === 400) {
                    this.self.onError &&
                    this.self.onError(JSON.parse(this.responseText), this.self);
                } else if (this.status === 401) {
                    // unauthorized
                    this.self.onUnauthorize(this.self);
                }
                else {
                    this.self.onError &&
                    this.self.onError(this.responseText, this.self);
                    this.self.onComplete && this.self.onComplete(this.self);
                }
            }
        };
        
        this._xhr.open(this.method, this.url);
        this._xhr.ontimeout = function () { this.self.onError('Response timed out') };
        this._xhr.timeout = 0;
        this._onBeforeSend && this._onBeforeSend(this._xhr);
        this._xhr.send(this.formData);
    },
    _complete: function () {
        console.log('Complete');
    },
    _error: function () {
        console.log('There was an error with the request');
    },
    _loading: function(){
        console.log('Loading');
    },
    _onBeforeSend:function(){
        return;
    },
    _serialize: function (obj) {
        /// <summary>
        /// serializes form data into query string data
        /// </summary>
        /// <param name="obj"></param>
        var str = [];
        for (var p in obj)
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        return str.join("&");
    },
    _unauthorize: function(){
        console.log('Authorization has been denied for this request.');
    }
    
});