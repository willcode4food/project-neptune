﻿TileLayerScroller = pc.TileLayer.extend('TileLayerScroller',
{},
{
       
    
    scroller: null,
    container: null,   
    top: 0,
    left: 0,
    zoom: 1,
    zoomRows: 0,
    zoomCols: 0,
    zoomLastCols: 0,
    zoomLastRows: 0,
    startLeft: null,
    startTop: null, 
    lastZoom: 1,
    lastLeft: 0,
    arrOverFlowTop: null,    
    shiftedUp: 0,
    shiftedLeft: 0,
    contentWidth: null,
    contentHeight: null,
    settings: null,

    init: function (name, usePrerendering, tileMap, tileSets, contentWidth, contentHeight) {
        this._super(name, usePrerendering, tileMap, tileSets);
        this.contentWidth = contentWidth;
        this.contentHeight = contentHeight;
        this.settings = pc.device.game.gameDeviceSettings;

        // create the Scoller object from Zynga
        // using the tilelayer's inherent draw() function as the callback
        this.scroller = new Scroller(this.draw, {
            zooming: true,
            parent: this,
            minZoom: this.settings.getGameZoomMin(),
            maxZoom: this.settings.getGameZoomMax(),
            bouncing: false,
            animation: true,
            animationDuration: 100,
            locking: true,
            scrollingX: true,
            scrollingY: true            

        });
        // The setDimensions() call is necessary to fully setup the scroller  object
        this.scroller.setDimensions(            
          pc.device.canvas.width,
          pc.device.canvas.height,            
          this.contentWidth,
          this.contentHeight
       );        
       // the overflow array when the tile map is moved towards the top of the screen.
       this.arrOverFlowTop = [];
    },
    draw: function (left, top, zoom, that) {
        // the that parameter passed into this function is a reference of the instatiated object of this class
        // in order to support the modified version of class.js that PlayCraft supports, we have to provide the Scroller from Zynga with a reference
        // of this object.  We return the object as the word that.  

        //checking if the default paramter, left which is returned by the Scroller object's callback to see if the draw() function was called 
        // by scroller or by PlayCraft.  
        if (!pc.valid(left)) {
            // draw() function has been called from playcraft.  So setting the local parameter to the value that was saved when it 
            // was called back by the Scroller object.
            left = this.left;
        }
        else if (pc.valid(that)) {
            // callback from Scroller object.  set reference of left data so it can be utilized during the draw call from PlayCraft
            that.left = left;                      
        }
        if (!pc.valid(top)) {
            top = this.top;
        }
        else if (pc.valid(that)) {
            that.top = top;
        }
        if (!pc.valid(zoom)) {
            zoom = this.zoom;
        }
        else if (pc.valid(that)) {        
            that.lastZoom = that.zoom;
            that.zoom = zoom;
        }
        // if scene is instantiated we know draw() was called from PlayCraft
        if (pc.valid(this.scene)) {
            var tileWidth = Math.floor(this.tileMap.tileWidth * this.zoom),
            tileHeight = Math.floor(this.tileMap.tileHeight * this.zoom),
            // the properties currCanvasWidth and currCanvasHeight are custom to my game.
            // This will need to be changed to pc.device.game.canvasWidth and pc.device.game.canvasHeight
            // adding support for resizing the browser.
            //contentWidth = pc.device.canvas.width,
            clientWidth = pc.device.game.gameDeviceSettings.getBaseDeviceWidth(),
            clientHeight = pc.device.game.gameDeviceSettings.getBaseDeviceHeight(),
            maxRows = this.tileMap.tilesHigh,
            maxCols = this.tileMap.tilesWide;
      
            // Compute starting rows/columns and support out of range scroll positions
            this.startRow = Math.max(Math.floor(top / tileHeight), 0);
            this.startCol = Math.max(Math.floor(left / tileWidth), 0);
            // Compute maximum rows/columns to render for content size      
            

            // Compute initial render offsets
            // 1. Positive scroll position: We match the starting rows/tile first so we
            //    just need to take care that the half-visible tile is fully rendered
            //    and placed partly outside.
            // 2. Negative scroll position: We shift the whole render context
            //    (ignoring the tile dimensions) and effectively reduce the render
            //    dimensions by the scroll amount.
            this.startTop = 0;
            this.startLeft = 0;
            if (this.zoom > this.scroller.options['minZoom']) {
                this.startTop = this.top >= 0 ? -this.top % tileHeight : -this.top;
                this.startLeft = this.left >= 0 ? -this.left % tileWidth : -this.left;
            
            }
            if (this.zoom > this.scroller.options['minZoom']) {
                this.zoomLastCols = this.zoomCols;
                this.zoomLastRows = this.zoomRows;
            }

            if (this.debugTileMap) {
                pc.device.ctx.fillText('Zoom Rows: ' + this.zoomRows + ' Last Zoom: ' + this.zoomLastRows, 50, 510);
                pc.device.ctx.fillText('Zoom Cols: ' + this.zoomCols + ' Last Zoom: ' + this.zoomLastCols, 50, 520);
                pc.device.ctx.fillText('start top: ' + this.startTop, 50, 530);
                pc.device.ctx.fillText('start left: ' + this.startLeft, 50, 540);
                pc.device.ctx.fillText('left: ' + this.left, 50, 550);
                pc.device.ctx.fillText('top: ' + this.top, 50, 560);
                pc.device.ctx.fillText('zoom: ' + this.zoom, 50, 570);
                pc.device.ctx.fillText('shifted up: ' + this.shiftedUp, 50, 580);
                pc.device.ctx.fillText('shifted left: ' + this.shiftedLeft, 50, 590);
            }

            // Compute number of rows to render            
            this.zoomRows = this.tileMap.tilesHigh;

            if ((this.top % tileHeight) > 0) {
                this.zoomRows += 1;
            }

            if ((this.startTop + (this.zoomRows * tileHeight)) < clientHeight) {
                this.zoomRows += 1;
            }

            // Compute number of columns to render            
            this.zoomCols = this.tileMap.tilesWide;

            if ((this.left % tileWidth) > 0) {
                this.zoomCols += 1;
            }

            if ((this.startLeft + (this.zoomCols * tileWidth)) < clientWidth) {
                this.zoomCols += 1;
            }

            // Limit rows/columns to maximum numbers
            this.zoomRows = Math.min(this.zoomRows, maxRows - this.startRow);
            this.zoomCols = Math.min(this.zoomCols, maxCols - this.startCol);

            if ((this.zoomCols < this.zoomLastCols) && (this.zoom > this.scroller.options['minZoom'])){
                this.shiftTileMapLeft(1);
            }
            if ((this.zoomCols > this.zoomLastCols && this.zoomLastCols !== 0) && (this.zoom > this.scroller.options['minZoom'])) {
                this.shiftTileMapRight(1);
            }
       
            if ((this.zoomRows < this.zoomLastRows) && (this.zoom > this.scroller.options['minZoom'])) {
                this.shiftTileMapUp(1);
            }
            if ((this.zoomRows > this.zoomLastRows) && (this.zoom > this.scroller.options['minZoom'])) {
                this.shiftTileMapDown(1);
               
            }

            if (this.debugTileMap) {
                this.drawMapDebug();
            }
            // this is the original tile map generation code from within playcraft
            // 
            var tx = Math.floor((this.origin.x)/ tileWidth);
            if (tx < 0) tx = 0;
            var ty = Math.floor((this.origin.y) / tileHeight);
            if (ty < 0) ty = 0;
            
            var tw = (Math.ceil((this.origin.x + this.scene.viewPort.w) / tileWidth) - tx) + 2;
            if (tx + tw >= this.tileMap.tilesWide - 1) tw = this.tileMap.tilesWide - 1 - tx;
            
            var th = (Math.ceil((this.origin.y + this.scene.viewPort.h) / tileHeight) - ty) + 2;
            if (ty + th >= this.tileMap.tilesHigh - 1) th = this.tileMap.tilesHigh - 1 - ty;

            for (var y = ty, c = ty + th; y < c + 1; y++) {

                // modified a little to include the starTop propertie
                // will shift layer when drawn according to data sent from the Scroller object
                var ypos = this.screenY(y * tileHeight + this.startTop);

                for (var x = tx, d = tx + tw; x < d; x++) {                    
                    // scaling the sprite sheet 
                    // will have to be performed to all the sprite sheets if more than one.
                    // currently assumes only one sprite sheet is 
                    //console.log('x: ' + x + 'xpos: ' + this.screenX(x * tileWidth + this.startLeft));
                    this.tileMap.tileSets[0].tileSpriteSheet.scaleX = this.zoom;
                    this.tileMap.tileSets[0].tileSpriteSheet.scaleY = this.zoom;                    
                    this.tileMap.drawTileTo(
                        pc.device.ctx, x, y,
                        this.screenX(x * tileWidth + this.startLeft), ypos);
                    if (this.debugShowGrid) {
                        pc.device.ctx.save();
                        pc.device.ctx.strokeStyle = '#222222';
                        pc.device.ctx.strokeRect(this.screenX(x * tileWidth + this.startLeft), this.screenY(y * tileHeight + this.startTop),
                            tileWidth, tileHeight);
                        pc.device.ctx.restore();
                    }
                }
            }
            if ((this.zoom === 1) && (this.shiftedLeft !== 0 || this.shiftedUp !== 0)) {
                //this.resetTileMap();
            }
        }
    },
    shiftTileMapLeft: function (shiftLeft) {
        for (var i = 0; i < this.tileMap.tiles.length; i++) {           
            this.tileMap.tiles[i] = this.tileMap.tiles[i].concat(this.tileMap.tiles[i].splice(0, shiftLeft));
        }
        if (this.arrOverFlowTop.length > 0) {
            for (var i = 0; i < this.arrOverFlowTop.length; i++) {
                this.arrOverFlowTop[i] = this.arrOverFlowTop[i].concat(this.arrOverFlowTop[i].splice(0, shiftLeft));
            }
        }
        this.shiftedLeft += shiftLeft;
    },
    shiftTileMapRight: function (shiftRight) {
        for (var i = 0; i < this.tileMap.tiles.length; i++) {
            this.tileMap.tiles[i] = this.tileMap.tiles[i].concat(this.tileMap.tiles[i].splice(0, this.tileMap.tiles[i].length - shiftRight));
        }
        if (this.arrOverFlowTop.length > 0) {
            for (var i = 0; i < this.arrOverFlowTop.length; i++) {
                this.arrOverFlowTop[i] = this.arrOverFlowTop[i].concat(this.arrOverFlowTop[i].splice(0, this.arrOverFlowTop[i].length - shiftRight));
            }
        }
        this.shiftedLeft -= shiftRight;

    },
    shiftTileMapUp: function (shiftUp) {   
        this.arrOverFlowTop = this.arrOverFlowTop.concat(this.tileMap.tiles.splice(0, shiftUp));        
   
            for (i = 0; i < shiftUp; i++) {
                this.tileMap.tiles.push(this.createBlankTileRow(this.tileMap.tiles));
                this.shiftedUp += 1;
            }
    },
    shiftTileMapDown: function (shiftDown) {

        if (this.arrOverFlowTop.length > 0) {
            for (i = 0; i < shiftDown; i++) {
                this.tileMap.tiles.unshift(this.arrOverFlowTop.pop());
                this.tileMap.tiles.pop();
                this.shiftedUp -= 1;
            }
        }
    },
    createBlankTileRow: function (tileArray) {
         

        //find lenght of tile map row
        var len = tileArray[0].length;
        var arrBlanks = [];
        for (i = 0; i < len; i++) {
            arrBlanks.push(-1);
        }
        return arrBlanks;
    },
    drawMapDebug: function () {
        var overflowX = 0, overflowY = 0;
        if (this.arrOverFlowTop.length > 0) {
            overflowX = this.arrOverFlowTop[0].length;
            overflowY = this.arrOverFlowTop.length;
            for (var i = 0; i < this.arrOverFlowTop.length; i++) {
                for (var j = 0; j < this.arrOverFlowTop[i].length; j++) {
                    var text = this.arrOverFlowTop[i][j];
                    if (text === 0) {
                        pc.device.ctx.fillStyle = 'red';
                    }
                    else {
                        pc.device.ctx.fillStyle = 'yellow';
                    }
                    pc.device.ctx.fillText(this.arrOverFlowTop[i][j], j * 20, (i + 1) * 20);
                }
            }
        }

        for (var i = 0; i < this.tileMap.tiles.length; i++) {
            for (var j = 0; j < this.tileMap.tiles[i].length; j++) {
                var text = this.tileMap.tiles[i][j];
                if (text === 0) {
                    pc.device.ctx.fillStyle = 'white';
                }
                else {
                    pc.device.ctx.fillStyle = 'green';
                }
                pc.device.ctx.fillText(this.tileMap.tiles[i][j], j * 20, (i + overflowY + 1) * 20);
            }
        }

    },
    resetTileMap: function () {
        if (this.shiftedLeft > 0) {
            this.shiftTileMapRight(this.shiftedLeft);
        }
        if (this.shiftedUp > 0) {
            this.shiftTileMapDown(this.shiftedUp);
        }
        if (this.shiftedLeft < 0) {
            this.shiftTileMapLeft((this.shiftedLeft * -1));
        }
        if (this.shiftedUp < 0) {
            this.shiftTileMapUp((this.shiftedUp * -1));
        }
        this.left = 0;
        this.top = 0;
        this.zoomLastCols = 0;
        this.zoomLastRows = 0;
        this.lastZoom = 0;
        this.shiftedLeft = 0;
        this.shiftedUp = 0;
        this.scroller = null;
        this.scroller = new Scroller(this.draw, {
            zooming: true,
            parent: this,
            minZoom: this.settings.getGameZoomMin(),
            maxZoom: this.settings.getGameZoomMax(),
            bouncing: false,
            animation: true,
            locking: true,
            scrollingX: true,
            scrollingY: true,

        });
        // The setDimensions() call is necessary to fully setup the scroller  object
        this.scroller.setDimensions(
            pc.device.canvas.width,
            pc.device.canvas.height,
            this.contentWidth,
            this.contentHeight
        );
    },
    process: function () {
    }
});

