﻿        // add callbacks for ajax responses
/**
 * pc.FormLayer
 * 
 * Provides a system to create forms.  Takes in an array of objects that dictate the styling and other meta data
 * about the form elements.  The second object passed in dicates the action and button styles.
 * 
 * Current we only support text boxes but you can also send in hidden values.  Just set the hidden attribute to true 
 * populate the label and value fields (see below) 
 *
 * 
 */


pc.FormLayer = pc.EntityLayer.extend('pc.FormLayer', {
    
},
{
    inputItems: null,
    url: null,
    method: null,   
    formData: null,
    onComplete: null,
    onSuccess: null,
    onError: null,
    onLoading: null,
    onUnauthorize:null,
    errorMsgX: null,
    errorMsgY: null,
    errMsgEntity: null,
    buttonEntity: null,
    hiddenFields: null,
    init: function (name, worldSizeX, worldSizeY, entityFactory) {
        this._super(name, worldSizeX, worldSizeY, entityFactory);

        
    },
    setInactive: function () {
        // default all form elements to their original label text
        var mgr = this.getEntityManager(),
        node = mgr.entities.first;
        while (node) {
            if (node.obj.hasTag('textinput')) {
                // text boxes have the text input field 
                // first remove the text component
                node.obj.updateLabel(node.obj.label);
                if (node.obj.hasTag('textboxfocused')) {
                    node.obj.removeTag('textboxfocused');
                }
                if (node.obj.hasTag('textinput') && (!node.obj.hidden)) {
                    node.obj.value = '';
                }
            }

            node = node.next();
        }
        this.formData = {};
        this._super();
    },
    config: function (formTextInput, action, onSuccess, onComplete, onLoading, onError) {
        //set all necessary systems
        // render system to draw text etc
        this.addSystem(new pc.systems.Render());
        // we use the scale effect to show which item is highlighted
        this.addSystem(new pc.systems.Effects());
        // and the layout system to automatically arrange the title items on the side of the screen
        this.addSystem(new pc.systems.Layout());    
        this.formData = {};
        // handle input
        this.addSystem(new pc.systems.FormInput());
        this.inputItems = [];
        this.hiddenFields = [];
        // handles tabbing through form elements.  
        pc.device.input.bindAction(this, 'cycleformelements', 'TAB');
        pc.device.input.bindAction(this, 'submit', 'ENTER');
        
        // load all the inputs 
        for (var i = 0; i < formTextInput.length; i++) {
            // hidden field so only store the id and value on the object.
            // do not make a form field    
              // construct the text box looking entity
            entity = pc.TextBoxEntity.create(this);
            entity.initTextBox(formTextInput[i]);
            // handle our mouse and touch inputs
            pc.device.input.bindAction(this, 'blur', 'MOUSE_BUTTON_LEFT_DOWN', entity.getComponent('spatial'));
            pc.device.input.bindAction(this, 'blur', 'TOUCH', entity.getComponent('spatial'));
            if (!entity.hidden) {
                this.inputItems.push(entity);
            }
            else {
                this.hiddenFields.push(entity);
            }
        }
    
        // set AJAX parameters
        this.method = pc.checked(action.method, 'GET');
        this.url = pc.checked(action.url, 'URL');
        this.buttonEntity = pc.ButtonEntity.create(this);
        this.buttonEntity.initButton({
            posX: action.posX,
            posY: action.posY,
            buttonVAlign: pc.checked(action.buttonVAlign, 'middle'),
            buttonHAlign: pc.checked(action.buttonHAlign, 'left'),
            buttonWidth: pc.checked(action.buttonWidth, 175),
            buttonFontHeight: pc.checked(action.buttonFontHeight, 30),
            labelOffset: pc.checked(action.labelOffset, { x: 5, y: -10 }),
            buttonHeight: pc.checked(action.buttonHeight, 40),
            label: pc.checked(action.label, 'Submit'),
            margin: pc.checked(action.margin, {})
        });
        // need extra event handler (aside from events handled in the forminput system)
        pc.device.input.bindAction(this, 'submit', 'MOUSE_BUTTON_LEFT_DOWN', this.buttonEntity.getComponent('spatial'));
        pc.device.input.bindAction(this, 'submit', 'TOUCH', this.buttonEntity.getComponent('spatial'));
        this.onSuccess = onSuccess;
        this.onComplete = onComplete;
        this.onLoading = onLoading;
        this.onError = onError;
    },
    onComplete: function(){
        this.buttonEntity.removeTag('submitting');
    },
    onError: function(){
        this.buttonEntity.removeTag('submitting');
    },
    onAction: function (actionName, event, pos, uiTarget) {
        // text input event handlers
        if (actionName === 'blur') {
            this._clearErrorMsg();
            this.onBlur();            
        }
        if (actionName === 'enter') {
            this._submitForm();
        }
        if (actionName === 'cycleformelements') {
            this._cycleFormElement()
        }
        if (actionName === 'submit') {
            if (!this.buttonEntity.hasTag('submitting')) {
                this._clearErrorMsg();
                this.onBlur();
                this._submitForm();
            }
        }
       
    },
    onBlur: function () {
        /// <summary>
        /// blurs all form elements (focusing will be handled on the individual entity)
        /// </summary>
        // onBlur all other entities
        var node = this.getEntityManager().entities.first;

        while (node) {
            if (pc.valid(node.obj.blurEntity)) {
                node.obj.blurEntity();
            }
            node = node.next();
        }
        
    },
    _cycleFormElement: function () {
        /// <summary>
        /// facilitates the tabbing through the form and focusing and bluring appropriate inputs 
        /// </summary>
        
        //get focused Entity
        var node = this.getEntityManager().entities.first, focusedEntity, index;
        while (node) {
            if (node.obj.hasTag('textboxfocused')) {
                focusedEntity = node.obj;
                focusedEntity.blurEntity();
            }            
            node = node.next();
        }
        
        var formElementIndex = this.inputItems.indexOf(focusedEntity),i = formElementIndex;
        if (formElementIndex === this.inputItems.length - 1) {
            i--;
        }
        else {
            i++;
        }       
        //assumes all elements have been onBlured
        this.inputItems[i].focusEntity();            
        
    },  
    _submitForm: function () {
        this.buttonEntity.addTag('submitting');
        this.onBlur();
        //submit form
        var node = this.getEntityManager().entities.first;
        
        while(node){
            if(node.obj.hasTag('textinput')){
                this.formData[node.obj.id]  = node.obj.value;
            }
            node = node.next();
        }
  
        var client = new pc.HttpClient({
            cache: true,
            onError: this.onError,
            onComplete: this.onComplete,
            onSuccess: this.onSuccess,
            onLoading: this.onLoading,
            onUnauthorized: this.onUnauthorize,
            onBeforeSend: function(self){
                self.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                self.setRequestHeader('Accept', 'application/json');                               
            },
            callingObject: this              
        });
       
        client.postFormAsync({
            url: this.url,
            formData: this.formData
        });
    },
    _clearErrorMsg: function(){
        if (pc.valid(this.errMsgEntity)) {
            if (this.errMsgEntity.hasComponentOfType('text')) {
                this.errMsgEntity.removeComponentByType('text');
            }
        }
    }
});