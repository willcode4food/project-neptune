pc.FormListViewLayer = pc.EntityLayerScroller.extend('pc.FormListViewLayer', 
{},
{
    _itemHeight: null,
    _itemWidth: null,
    _itemStartX: null,
    _itemStartY: null,
    _itemPadding: null,
    _itemColor: null,
    _itemBorderColor: null,
    _itemBorderWidth: null,
    _settings: null,
    _isScrolling: false,
    _cumulativeScroll: {x:0,y:0},
    _scrollWheelEndDebouncer : null,
    _listViewItems:null,

	init: function(name, worldSizeX, worldSizeY, scrollOptions, entityFactory){

        this._super(name, worldSizeX, worldSizeY, scrollOptions, entityFactory);
		    this.addSystem(new pc.systems.Render());
        this.addSystem(new pc.systems.Effects());
        this.addSystem(new pc.systems.Layout());
        this.addSystem(new pc.systems.FormInput());

        pc.device.input.bindAction(this, 'draggrab', 'MOUSE_BUTTON_LEFT_DOWN');
        pc.device.input.bindAction(this, 'dragmove', 'MOUSE_MOVE_DRAGDROP');
        pc.device.input.bindAction(this, 'dragletgo', 'MOUSE_BUTTON_LEFT_UP');
        pc.device.input.bindAction(this, 'touchstart', 'TOUCH');
        pc.device.input.bindAction(this, 'touchmove', 'TOUCH_MOVE');
        pc.device.input.bindAction(this, 'touchend', 'TOUCH_END_NOPOS');
        pc.device.input.bindAction(this, 'scrollwheel', 'MOUSE_WHEEL_DOWN');
        pc.device.input.bindAction(this, 'scrollwheel', 'MOUSE_WHEEL_UP');
        this._settings = pc.device.game.gameDeviceSettings;
    },
    create: function(options){
         this._itemHeight = pc.checked(options.itemHeight, 180);
         this._itemWidth = pc.checked(options.itemWidth, 1180);
         this._itemStartX = pc.checked(options.itemStartX, 60);
         this._itemStartY = pc.checked(options.itemStartY, 120);
         this._itemPadding = pc.checked(options.itemPadding, 20);
         this._itemColor = pc.checked(options.itemColor,'#e7e2cd');
         this._itemBorderColor = pc.checked(options.itemBorderColor,'#e7e2cd');
         this._itemBorderWidth = pc.checked(options.itemBorderWidth,0);

         this._listViewItems = pc.checked(options.listViewItems, []);

         var scrollOptions = {
            zooming: pc.checked(options.scrollOptions.zooming, true),
            parent: this,
            minZoom: pc.checked(options.scrollOptions.minZoom,1),
            maxZoom: pc.checked(options.scrollOptions.maxZoom,2),
            bouncing: pc.checked(options.scrollOptions.bouncing, false),
            animating: pc.checked(options.scrollOptions.animating, false),
            animationDuration: pc.checked(options.scrollOptions.animationDuration, 500),
            locking: pc.checked(options.scrollOptions.locking, true),
            scrollingX: pc.checked(options.scrollOptions.scrollingX, true),
            scrollingY: pc.checked(options.scrollOptions.scrollingY, true),
            snapping: pc.checked(options.scrollOptions.snapping, false),
            paging: pc.checked(options.scrollOptions.paging, false)
         };


         
         this._initScroller(scrollOptions);
         var startYItr = this._itemStartY
         for(var x in options.listViewItems){

                var sessionEntity = pc.Entity.create(this),                    
                    action = pc.checked(options.listViewItems[x].action,{});

                var buttonOptions = {
                    visible: pc.checked(action.visible,true),
                    showShadow: pc.checked(action.showShadow, true),
                    shadowColor: pc.checked(action.shadowColor,'#000000'),
                    shadowBlur: pc.checked(action.shadowBlur,4),
                    shadowOffSetY: pc.checked(action.shadowOffSetY,3),
                    shadowOffSetX: pc.checked(action.shadowOffSetX,2.5),
                    valign: pc.checked(action.buttonVAlign, 'middle'),
                    halign: pc.checked(action.buttonHAlign, 'left'),
                    width: pc.checked(action.buttonWidth, 200),
                    posX: pc.checked(action.posX,null),
                    posY: pc.checked(action.posY, null),
                    fontHeight: pc.checked(action.buttonFontHeight, 30),
                    labelOffset: pc.checked(action.labelOffset, { x: 0, y: -10 }),
                    height: pc.checked(action.buttonHeight, 40),
                    label: pc.checked(action.label, 'Submit'),
                    fontColor: pc.checked(action.buttonFontColor, '#ffffFF'),
                    font: pc.checked(action.font, 'Government Agent BB'),
                    color: pc.checked(action.buttonColor, '#21355a'),
                    borderColor: pc.checked(action.buttonBorderColor, '#21355a'),
                    borderWidth: pc.checked(action.buttonBorderWidth, 0),
                    mouseOverColor: pc.checked(action.mouseOverColor, '#21355a'),
                    mouseOverFontColor: pc.checked(action.mouseOverFontColor, '#4CC8FB'),
                    mouseOverBorderColor: pc.checked(action.mouseOverBorderColor, '#ed9590'),
                    mouseClickFontColor: pc.checked(action.mouseClickFontColor, '#4CC8FB'),
                    mouseClickColor: pc.checked(action.mouseClickColor, '#21355a'),
                    mouseClickBorderColor: pc.checked(action.mouseClickBorderColor, '#ed9590'),
                    margin: pc.checked(action.margin, {top:0,right:0,bottom:20, left:40}),
                    clickEvent: pc.checked(action.clickEvent, function () { return; }),
                    unClickEvent: pc.checked(action.unClickEvent, function () { return; }),
                    touchEvent: pc.checked(action.touchEvent, function(){return;}),
                    unTouchEvent: pc.checked(action.unTouchEvent, this.unClickEvent),
                      animation: pc.checked(action.animation,{   // move button 
                       onMouseButtonDown: function(callingObject){
                        var self = callingObject;
                        self.getComponent('spatial').pos.x += 2;
                        self.getComponent('spatial').pos.y += 2;
                        self.getComponent('shadow').offSetX -= 1.5;
                        self.getComponent('shadow').offSetY -= 2;
                       },
                       onMouseButtonUp: function(callingObject) {
                           var self = callingObject;
                           self.getComponent('spatial').pos.x -= 2;
                           self.getComponent('spatial').pos.y -= 2;
                           self.getComponent('shadow').offSetX += 1.5;
                           self.getComponent('shadow').offSetY += 2;
                       },
                       onTouchStart: function(callingObject){
                        var self = callingObject;
                        self.getComponent('spatial').pos.x += 2;
                        self.getComponent('spatial').pos.y += 2;
                        self.getComponent('shadow').offSetX -= 1.5;
                        self.getComponent('shadow').offSetY -= 2;
                       },
                       onTouchEnd: function(callingObject) {
                           var self = callingObject;
                           self.getComponent('spatial').pos.x -= 2;
                           self.getComponent('spatial').pos.y -= 2;
                           self.getComponent('shadow').offSetX += 1.5;
                           self.getComponent('shadow').offSetY += 2;
                       }
                       // onMouseButtonOut: function(callingObject) {
                       //     var self = callingObject;
                       //     self.getComponent('spatial').pos.x -= 2;
                       //     self.getComponent('spatial').pos.y -= 2;
                       //     self.getComponent('shadow').offSetX += 1.5;
                       //     self.getComponent('shadow').offSetY += 2;
                       // }
                       // onMouseButtonOver: function(callingObject){
                       //  var self = callingObject;
                       //  self.onMouseButtonOver();
                                   //  return;
                                   // },
                })                  
               };
               sessionEntity.addComponent(pc.components.Spatial.create({
                        x: this._itemStartX,
                        y: startYItr,
                        w: this._itemWidth,
                        h: this._itemHeight
               }));
               sessionEntity.addComponent(pc.components.Rect.create({
                    color: this._itemColor,
                    lineColor: this._itemBorderColor,
                    lineWidth: this._itemBorderWidth

               }));
               sessionEntity.addComponent(pc.components.Shadow.create({
                  offSetY: 1.0,
                  offSetX: 0.5,
                  blur: 2.5,
                  color: '#000000'
               }));
              
                sessionEntity.addComponent(pc.components.TextLayout.create({
                        layout: options.listViewItems[x].textLayouts
                }));
                if(buttonOptions.visible){
                  var buttonEntity = pc.ButtonEntity.create(this);
                  buttonEntity.initButton({
                      label:buttonOptions.label,
                      buttonWidth: buttonOptions.width,
                      buttonHeight: buttonOptions.height,
                      buttonFontHeight:buttonOptions.fontHeight,
                      buttonFontColor: buttonOptions.fontColor,
                      buttonColor: buttonOptions.color,
                      posX: this._itemStartX + (this._itemWidth - buttonOptions.posX),
                      posY: startYItr + (this._itemHeight - buttonOptions.posY),               
                      labelOffset: { x: buttonOptions.labelOffset.x, y: buttonOptions.labelOffset.y }
              
                  });                  
                }
                

               startYItr += this._itemHeight + this._itemPadding;
            }

    },
    _initScroller: function(scrollOptions){
            this.scroller = new Scroller(this.setScrollerData, {
            zooming: pc.checked(scrollOptions.zooming, true),
            parent: this,
            minZoom: pc.checked(scrollOptions.minZoom,1),
            maxZoom: pc.checked(scrollOptions.maxZoom,2),
            bouncing: pc.checked(scrollOptions.bouncing, false),
            animating: pc.checked(scrollOptions.animating, false),
            animationDuration: pc.checked(scrollOptions.animationDuration, 500),
            locking: pc.checked(scrollOptions.locking, false),
            scrollingX: pc.checked(scrollOptions.scrollingX, true),
            scrollingY: pc.checked(scrollOptions.scrollingY, true),
            snapping: pc.checked(scrollOptions.snapping, false),
            paging: pc.checked(scrollOptions.paging, false)

        });
        // The setDimensions() call is necessary to fully setup the scroller  object
        this.scroller.setDimensions(
          pc.device.canvas.width,
          pc.device.canvas.height,
          this._settings.getBaseDeviceWidth(),
          this._getScrollerHeight()
       );
    },
    _getScrollerHeight: function(){
        return this._settings.getBaseDeviceHeight() + ((((this._itemHeight + (this._itemPadding * 2)) * this._listViewItems.length) + this._itemStartY) - this._settings.getBaseDeviceHeight());
    },
	setInactive: function(){
        // The list view will be refreshed with new entities.  So upon deactivation, need
        // to remove all entities overriding layer base method
            var node = this.getEntityManager().entities.first;                

            while (node) {
                var inputComponent = node.obj.getComponent('input');             
                this.getEntityManager().remove(node.obj);            
                node = node.next();
            }
        
        this._super();
    },
    onAction: function (actionName, event, pos, uiTarget) {      
        var pageX = event.pageX, pageY = event.pageY, timeStamp = event.timeStamp, detail = event.detail, wheelDelta = event.wheelDelta, touches = event.touches, scale = event.scale;
         if (actionName === 'draggrab') {
                if (event.target.tagName.match(/input|textarea|select/i)) {
                    return;
                }    
                this.doTouchStart([{
                    pageX: pageX,
                    pageY: pageY
                }], timeStamp);
                
                this.mouseDown = true;              
        }
        if(actionName === 'scrollwheel'){
            var scrollDeltaX = 0, scrollDeltaY = 0;
            if(!this._isScrolling){
                this._isScrolling = true;
                this.doTouchStart([event], timeStamp);
                return;
            }
            // Convert the scrollwheel values to a scroll value
            if (event.wheelDelta) {
              if (event.wheelDeltaX) {
                scrollDeltaX = event.wheelDeltaX / 2;
                scrollDeltaY = event.wheelDeltaY / 2;
              } else {
                scrollDeltaX = 0;
                scrollDeltaY = event.wheelDelta / 2;
              }
            } else {
              if (event.axis && event.axis === event.HORIZONTAL_AXIS) {
                scrollDeltaX = event.detail * -10;
                scrollDeltaY = 0;
              } else {
                scrollDeltaX = 0;
                scrollDeltaY = event.detail * -10;
              }
            }
         
            
            this._cumulativeScroll.x = Math.round(this._cumulativeScroll.x + scrollDeltaX);
            this._cumulativeScroll.y = Math.round(this._cumulativeScroll.y + scrollDeltaY);
            
             this.doTouchMove([{ 
                pageX: pageX + this._cumulativeScroll.x, 
                pageY: (pageY + this._cumulativeScroll.y)
             }], timeStamp);
             
            if(this._scrollWheelEndDebouncer  !== null){
                clearTimeout(this._scrollWheelEndDebouncer );
            }
            var self = this;

            this._scrollWheelEndDebouncer = setTimeout(function(){ 
                self.doTouchEnd(timeStamp);
                self._isScrolling = false;
            },200);
        }
        // handle mouse move
        if (actionName === 'dragmove') {
            if (!this.mouseDown) {
                return;
            }
            this.doTouchMove([{
                pageX: pageX,
                pageY: pageY
            }], timeStamp);
            this.mouseDown = true;
        }
      // handle mouse drop
        if (actionName === 'dragletgo') {
            if (!this.mouseDown) {
                return;
            }
            this.doTouchEnd(timeStamp);
            this.mouseDown = false;
        }
        if (actionName === 'touchstart') {
            if (touches[0] && touches[0].target && touches[0].target.tagName.match(/input|textarea|select/i)) {
                return;
            }
            this.doTouchStart(touches, timeStamp);
        }
        if (actionName === 'touchmove') {
                   this.doTouchMove(touches, timeStamp, scale);

                }
        if (actionName === 'touchend') {
                    this.doTouchEnd(timeStamp);                          
        }
    },
    process: function () {
        this._super();

        // set global to track whether the game is zoomed in or not
        // the zooming function is intended for "read only" so we want         
        // to be able to detect in other areas of the game to disable features

        if (this.zoom !== 1) {
            pc.device.game.isGameZoomed = true;
        }
        else {
            pc.device.game.isGameZoomed = false;
        }
    } 
});