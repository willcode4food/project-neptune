/**
 *   Playcraft Engine
 *   Author: Marc D. Arbesman
 */

/**
 * Class: EntityLayerScroller
 * Extendds: pc.EntityLayer
 * Description: handles input for scroller api
 */
pc.EntityLayerScroller = pc.EntityLayer.extend('EntityLayerScroller',
{},
{
    scroller: null,
    left: null,
    top: null,
    zoom: 1,
    scaleX: null,
    scaleY: null,
    init: function (name, worldSizeX, worldSizeY, scrollOptions, entityFactory) {
        this._super(name, worldSizeX, worldSizeY, entityFactory);
        if(!pc.valid(scrollOptions)){
            // initialize the layer, but not the scroller if no scroll options are passed
            return;
        }
        // create the Scoller object  (from Zynga)
        // using the tilelayer's inherent draw() function as the callback
        this.scroller = new Scroller(this.setScrollerData, {
            zooming: pc.checked(scrollOptions.zooming, true),
            parent: this,
            minZoom: pc.checked(scrollOptions.minZoom,1),
            maxZoom: pc.checked(scrollOptions.maxZoom,2),
            bouncing: pc.checked(scrollOptions.bouncing, false),
            animating: pc.checked(scrollOptions.animating, false),
            animationDuration: pc.checked(scrollOptions.animationDuration, 500),
            locking: pc.checked(scrollOptions.locking, true),
            scrollingX: pc.checked(scrollOptions.scrollingX, true),
            scrollingY: pc.checked(scrollOptions.scrollingY, true),
            snapping: pc.checked(scrollOptions.snapping, false),
            paging: pc.checked(scrollOptions.paging, false)

        });
        // The setDimensions() call is necessary to fully setup the scroller  object
        this.scroller.setDimensions(
          pc.device.canvas.width,
          pc.device.canvas.height,
          pc.checked(scrollOptions.deviceWidth, 1280),
          pc.checked(scrollOptions.deviceHeight, 720)
       );
    },
    setScrollerData: function (left, top, zoom, that) {
        if (pc.valid(that)){
            that.zoom = zoom;
            var node = that.entityManager.entities.first;
            while (node) {
                if (node.obj.hasComponentOfType('spatial')) {
                    if (!node.obj.hasTag('nozoom')) {
                        var spatial = node.obj.getComponent('spatial');
                        spatial.top = top;
                        spatial.left = left;
                        spatial.zoom = zoom;
                    }
                    
                }
                node = node.next();
            }    
        }
        
    },
    doMouseZoom: function (wheelDelta, timeStamp, pageX, pageY) {
        this.scroller.doMouseZoom(wheelDelta, timeStamp, pageX, pageY);       
    },
    doTouchStart: function (touches, timeStamp) {
        this.scroller.doTouchStart(touches, timeStamp)        
    },
    doTouchMove: function (touches, timeStamp, scale) {
        this.scroller.doTouchMove(touches, timeStamp, scale);        
    },
    doTouchEnd: function (timeStamp) {
        this.scroller.doTouchEnd(timeStamp);
    },
    process: function () {
        this._super();

        // set global to track whether the game is zoomed in or not
        // the zooming function is intended for "read only" so we want         
        // to be able to detect in other areas of the game to disable features

        if (this.zoom !== 1) {
            pc.device.game.isGameZoomed = true;
        }
        else {
            pc.device.game.isGameZoomed = false;
        }
    }
});