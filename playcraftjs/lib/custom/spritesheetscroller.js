﻿pc.SpriteSheetScroller = pc.SpriteSheet.extend('pc.SpriteSheetScroller',
{},
{
    scroller: null,
    left: null,
    top: null,
    zoom: null,
    originX: null,
    originY: null,
    lastZoom: null,
    contentWidth: null,
    contentHeight: null,
    startTop: null,    
    origin: null,
    settings: null,
    init: function (options) {
        this._super(options);
        this.contentHeight = options.contentHeight;
        this.contentWidth = options.contentWidth;
        this.settings = pc.device.game.gameDeviceSettings;
        this.originX = options.originX;
        this.originY = options.originY;

        // create the Scoller object  (from Zynga)
        // using the tilelayer's inherent draw() function as the callback
        this.scroller = new Scroller(this.setScrollerData, {
            zooming: true,
            parent: this,
            minZoom: this.settings.getGameZoomMin(),
            maxZoom: this.settings.getGameZoomMax(),
            bouncing: false,
            animation: true,
            animationDuration: 500,
            locking: true,
            scrollingX: true,
            scrollingY: true

        });
        // The setDimensions() call is necessary to fully setup the scroller  object
        this.scroller.setDimensions(
          pc.device.canvas.width,
          pc.device.canvas.height,
          this.contentWidth,
          this.contentHeight
       );
    },
    setScrollerData: function (left, top, zoom, that) {
        that.left = left;
        that.top = top;
        that.lastZoom = that.zoom;
        that.zoom = zoom;
    },
    draw: function (ctx, state, x, y, dir) {
        var frame,
            offsetX,
            offsetY,
            scaleX = this.scaleX + (this.zoom - 1),
            scaleY = this.scaleY + (this.zoom - 1),
            drawX,
            drawY;

        if (state.currentAnim == null) {
            frame = this.frames[state.currentFrame];
            offsetX = offsetY = 0;
        }
        else {
            var fx = state.currentAnim.frames[state.currentFrame][0];
            var fy = state.currentAnim.frames[state.currentFrame][1];
            offsetX = state.currentAnim.offsetX;
            offsetY = state.currentAnim.offsetY;
            scaleX *= state.currentAnim.scaleX;
            scaleY *= state.currentAnim.scaleY;
            frame = this.frames[fx + fy * this.framesWide];
        }
        if (!pc.valid(frame))
            throw new Error('Frame out of bounds: ' + state.currentFrame);
    
        var frameSourceX = frame[0];
        var frameSourceY = frame[1];
        var frameWidth = frame[2];
        var frameHeight = frame[3];      
        var frameImage = frame[4];
        var frameRegX = frame[5];
        var frameRegY = frame[6];
   

        //pc.device.ctx.fillText('zoom:  ' + this.zoom, 50, 530);        
        if (!frameImage.loaded || state == null || !state.active) return;

        if (scaleX != 1 || scaleY != 1)
            frameImage.setScale(scaleX, scaleY);

        if (state.alpha != 1)
            frameImage.alpha = state.alpha;

        if (this.compositeOperation != null)
            frameImage.setCompositeOperation(this.compositeOperation);
        if (this.zoom > this.scroller.options['minZoom']) {
            drawX = Math.floor((Math.ceil(x - frameRegX + this.originX) * this.zoom - this.left - this.originX));
            drawY = Math.floor((Math.ceil(y - frameRegY + this.originY) * this.zoom - this.top - this.originY));
        }
        else {
            drawX = Math.floor((Math.ceil(x - frameRegX  + this.originX) - this.originX));
            drawY = Math.floor((Math.ceil(y - frameRegY  + this.originY) - this.originY));
           
        }

        frameImage.draw(ctx,
          (this.sourceX + frameSourceX),
          (this.sourceY + frameSourceY),          
          drawX,
          drawY,
          frameWidth, frameHeight,
          this.useRotation ? dir : 0);
  
        // restore scaling (as images can be used amongst spritesheets, we need to be nice)
        if (scaleX != 1 || scaleY != 1)
            frameImage.setScale(1, 1);

        // set the alpha back to normal
        if (state.alpha != 1)
            frameImage.alpha = 1;

        if (this.compositeOperation != null)
            frameImage.setCompositeOperation('source-over');

    }
    

});