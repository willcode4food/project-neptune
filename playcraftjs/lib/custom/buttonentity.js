﻿pc.ButtonEntity = pc.Entity.extend("pc.ButtonEntity",
{},
{
    label: null,
    width: null,
    height: null,
    valign: null,
    halign: null,
    posX: null,
    posY: null,
    borderWidth: null,
    borderColor: null,
    mouseOverBorderColor: null,
    mouseClickBorderColor: null,
    fontHeight: null,
    fontColor: null,
    font: null,
    mouseOverFontColor: null,
    mouseClickFontColor: null,
    color: null,
    mouseOverColor: null,
    mouseClickColor: null,
    labelOffset: null,
    clickAnimation: null,
    clickEvent:null,
    unClickEvent: null,
    touchEvent: null,
    unTouchEvent: null,
    shadowOffSetX: null,
    shadowOffSetY: null,
    shadowBlur: null,
    shadowColor: null,
    animation: null,
    gameDeviceSettings: null,
    init: function (layer) {
        this._super(layer);
        this.gameDeviceSettings = new pn.DeviceConfigFactory();
    },
    create: function(layer){
        this._super(layer);
    },
    initButton: function (options) {
       
        //construct button entity
        this.showShadow = pc.checked(options.showShadow, true);
        this.shadowColor = pc.checked(options.shadowColor,'#000000');
        this.shadowBlur = pc.checked(options.shadowBlur,4);
        this.shadowOffSetY = pc.checked(options.shadowOffSetY,3);
        this.shadowOffSetX = pc.checked(options.shadowOffSetX,2.5);
        this.valign = pc.checked(options.buttonVAlign, 'middle');
        this.halign = pc.checked(options.buttonHAlign, 'left');
        this.width = pc.checked(options.buttonWidth, this.gameDeviceSettings.getButtonDefaultWidth());
        this.posX = pc.checked(options.posX,null);
        this.posY = pc.checked(options.posY, null);
        this.fontHeight = pc.checked(options.buttonFontHeight, 30);
        this.labelOffset = pc.checked(options.labelOffset, { x: 0, y: -10 });
        this.height = pc.checked(options.buttonHeight, this.gameDeviceSettings.getButtonDefaultHeight());
        this.label = pc.checked(options.label, 'Submit');
        this.fontColor = pc.checked(options.buttonFontColor, '#ffffFF');
        this.font = pc.checked(options.font, 'Government Agent BB');
        this.color = pc.checked(options.buttonColor, '#21355a');
        this.borderColor = pc.checked(options.buttonBorderColor, '#21355a');
        this.borderWidth = pc.checked(options.buttonBorderWidth, 0);
        this.mouseOverColor = pc.checked(options.mouseOverColor, '#21355a');
        this.mouseOverFontColor = pc.checked(options.mouseOverFontColor, '#4CC8FB');
        this.mouseOverBorderColor = pc.checked(options.mouseOverBorderColor, '#ed9590');
        this.mouseClickFontColor = pc.checked(options.mouseClickFontColor, '#4CC8FB');
        this.mouseClickColor = pc.checked(options.mouseClickColor, '#21355a');
        this.mouseClickBorderColor = pc.checked(options.mouseClickBorderColor, '#ed9590');
        this.margin = pc.checked(options.margin, {top:0,right:0,bottom:20, left:40});
        this.clickEvent = pc.checked(options.clickEvent, function () { return; });
        this.unClickEvent = pc.checked(options.unClickEvent, function () { return; });
        this.touchEvent = pc.checked(options.touchEvent, function(){return;});
        this.unTouchEvent = pc.checked(options.unTouchEvent, this.unClickEvent);
        this.animation = pc.checked(options.animation,{   // move button 
           onMouseButtonDown: function(callingObject){
            var self = callingObject;
            self.getComponent('spatial').pos.x += 2;
            self.getComponent('spatial').pos.y += 2;
            self.getComponent('shadow').offSetX -= 1.5;
            self.getComponent('shadow').offSetY -= 2;
           },
           onMouseButtonUp: function(callingObject) {
               var self = callingObject;
               self.getComponent('spatial').pos.x = self.posX;
               self.getComponent('spatial').pos.y = self.posY;
               self.getComponent('shadow').offSetX = self.shadowOffSetX;
               self.getComponent('shadow').offSetY = self.shadowOffSetY;
           },
           onTouchStart: function(callingObject){
            var self = callingObject;
            self.getComponent('spatial').pos.x += 2;
            self.getComponent('spatial').pos.y += 2;
            self.getComponent('shadow').offSetX -= 1.5;
            self.getComponent('shadow').offSetY -= 2;
           },
           onTouchEnd: function(callingObject) {
               var self = callingObject;
               self.getComponent('spatial').pos.x = self.posX;
               self.getComponent('spatial').pos.y = self.posY;
               self.getComponent('shadow').offSetX = self.shadowOffSetX;
               self.getComponent('shadow').offSetY = self.shadowOffSetY;
           }
           // onMouseButtonOut: function(callingObject) {
           //     var self = callingObject;
           //     self.getComponent('spatial').pos.x -= 2;
           //     self.getComponent('spatial').pos.y -= 2;
           //     self.getComponent('shadow').offSetX += 1.5;
           //     self.getComponent('shadow').offSetY += 2;
           // }
           // onMouseButtonOver: function(callingObject){
           //  var self = callingObject;
           //  self.onMouseButtonOver();
           //  return;
           // },           
        });
        this.addComponent(pc.components.Spatial.create({ w: this.width, h: this.height }));
        if (pc.valid(options.imageResource)) {
            var buttonSprite, buttonSpriteSheet;
            //assumes resource has already been loaded            
            buttonSpriteSheet = new pc.SpriteSheet({
                        image: options.imageResource.resource,
                        frameWidth: pc.checked(options.imageResource.resource.width,150),
                        frameHeight: pc.checked(options.imageResource.resource.height,50),
                        framesWide: pc.checked(options.imageResource.framesWide, 1),
                        framesHigh: pc.checked(options.imageResource.framesHigh,1)

                    });
         
            buttonSprite = new pc.Sprite(buttonSpriteSheet);
            this.addComponent(pc.components.Sprite.create(buttonSprite));          
        }
        else {
            this.addComponent(pc.components.Rect.create({
                color: pc.checked(this.color,'#037E8C'),
                lineColor: pc.checked(this.borderColor,'#037E8C'),
                lineWidth: pc.checked(this.borderWidth,0)
            }));
        }
        if(!pc.valid(this.posX) || !pc.valid(this.posY)){
            // use a layout if the absolute positioning has not been specified
            this.addComponent(pc.components.Layout.create({
                vertical: pc.checked(this.valign,'middle'),
                horizontal: pc.checked(this.halign,'center'),
                margin: pc.checked(this.margin,{})
            }));    
        }
        else{
            // absolute position if the coordinants are valid
            this.getComponent('spatial').pos.x = this.posX;
            this.getComponent('spatial').pos.y = this.posY;
        }
        if (pc.valid(options.label)) {
            // if label offset X value set to 0, automatically center text
            if (this.labelOffset.x === 0){
                this.labelOffset.x = this.getCenterStartX()
            }
            if(this.labelOffset.y === 0){
                this.labelOffset.y = this.getCenterStartY();
            }
            this.addComponent(pc.components.Text.create({
                fontHeight: pc.checked(this.fontHeight, 12),
                color: pc.checked(this.fontColor, '#FFFFFF'),
                font:this.font,
                text: [pc.checked(this.label, 'Button')],
                offset: pc.checked(this.labelOffset, {})
            }));
        }
        if(this.showShadow){
              this.addComponent(pc.components.Shadow.create({
              offSetY: this.shadowOffSetY,
              offSetX: this.shadowOffSetX,
              blur: this.shadowBlur,
              color: this.shadowColor
            }));  
        }
        this._bindActions();
    },
    updateButtonText: function (textUpdate) {
        if(this.hasComponent('text')){
            // if label offset X value set to 0, automatically center text
            if (this.labelOffset.x === 0){
                this.labelOffset.x = this.getCenterStartX();
            }
            if(this.labelOffset.y === 0){
                this.labelOffset.y = this.getCenterStartY();
            }
            this.removeComponent('text');
            this.addComponent(pc.components.Text.create({
                fontHeight: this.fontHeight,
                font:this.font,
                color: this.fontColor,
                text: [textUpdate],
                offset: this.labelOffset
            }));
        }
    },
    getCenterStartX: function(){
         pc.device.ctx.font = this.fontHeight + 'px ' + this.font;
         return (this.width / 2) - (pc.device.ctx.measureText(this.label).width/2);

    },
    getCenterStartY: function(){

         pc.device.ctx.font = this.fontHeight + 'px ' + this.font;
        return -(this.height / 2) + (this.fontHeight/2) -5;
    },
    // onClickAnimate: function(){

    // },
    _bindActions: function () {
        if(this.hasComponentOfType('input')){
            this.removeComponent('input');
        }
        this.addComponent(pc.components.Input.create(
            {
                states: [
                     ['off button', ['MOUSE_OUT']],
                     ['click button', ['MOUSE_BUTTON_LEFT_DOWN'],false]
                ],
                actions: [
                       ['click button', ['MOUSE_BUTTON_LEFT_DOWN']],
                       ['release button', ['MOUSE_BUTTON_LEFT_UP']],
                       ['touch button', ['TOUCH']],
                       ['touch end button', ['TOUCH_END']],
                       ['over button', ['MOUSE_OVER']],
                       ['off button', ['MOUSE_OUT']]

                ]
            }));
        // because components are pooled, we need to flip the ._bound to false so the system picks it back up.
        this.getComponent('input')._bound = false;
    },
   /******************************
    *
    *  Button Event handlers
    * 
    *******************************/
    onMouseButtonOver: function () {
        if (!this.hasComponentOfType('sprite')) {
            this.getComponent('rect').color = new pc.Color(this.mouseOverColor);
            this.getComponent('rect').lineColor = new pc.Color(this.mouseOverBorderColor);
            this.getComponent('text').color = new pc.Color(this.mouseOverFontColor);
        }
        if(this.animation && this.animation.onMouseButtonOver){
            this.animation.onMouseButtonOver(this);
        }
    },
    onMouseButtonOut: function () {
        if (!this.hasComponentOfType('sprite')) {

            this.getComponent('rect').color = new pc.Color(this.color);
            this.getComponent('rect').lineColor = new pc.Color(this.borderColor);
            this.getComponent('text').color = new pc.Color(this.fontColor);
        }
        if(this.animation && this.animation.onMouseButtonOut){
            this.animation.onMouseButtonOut(this);
        }
    },
    onMouseButtonDown: function () {
        if (!this.hasComponentOfType('sprite')) {
            this.getComponent('rect').color = new pc.Color(this.mouseClickColor);
            this.getComponent('rect').lineColor = new pc.Color(this.mouseClickBorderColor);
            this.getComponent('text').color = new pc.Color(this.mouseClickFontColor);
        }
        if (this.hasComponentOfType('sprite') && (this.getComponent('sprite').sprite.spriteSheet.framesWide > 1 || this.getComponent('sprite').sprite.spriteSheet.framesHigh > 1)) {
            this.getComponent('sprite').sprite.currentFrame = 1;
        }
        if(this.animation && this.animation.onMouseButtonDown){
            this.animation.onMouseButtonDown(this);
        }
        

        this.clickEvent();
    },
    onMouseButtonUp: function () {
        if (!this.hasComponentOfType('sprite')) {
            this.getComponent('rect').color = new pc.Color(this.color);
            this.getComponent('rect').lineColor = new pc.Color(this.borderColor);
            this.getComponent('text').color = new pc.Color(this.fontColor);
        }
        if (this.hasComponentOfType('sprite') && (this.getComponent('sprite').sprite.spriteSheet.framesWide > 1 || this.getComponent('sprite').sprite.spriteSheet.framesHigh > 1)) {
            this.getComponent('sprite').sprite.currentFrame = 0;
        }
        if(this.animation && this.animation.onMouseButtonUp){
            this.animation.onMouseButtonUp(this);
        }
        this.unClickEvent();
    },
    onTouchStart: function(){
        if (!this.hasComponentOfType('sprite')) {
            this.getComponent('rect').color = new pc.Color(this.mouseClickColor);
            this.getComponent('rect').lineColor = new pc.Color(this.mouseClickBorderColor);
            this.getComponent('text').color = new pc.Color(this.mouseClickFontColor);
        }
        if (this.hasComponentOfType('sprite') && (this.getComponent('sprite').sprite.spriteSheet.framesWide > 1 || this.getComponent('sprite').sprite.spriteSheet.framesHigh > 1)) {
            this.getComponent('sprite').sprite.currentFrame = 1;
        }
        if(this.animation && this.animation.onTouchStart){
            this.animation.onTouchStart(this);
        }
        this.addTag('buttonpress');
        this.touchEvent();
    },
    onTouchEnd: function(){
        if(this.hasTag('buttonpress')){
            if (!this.hasComponentOfType('sprite')) {
            this.getComponent('rect').color = new pc.Color(this.color);
            this.getComponent('rect').lineColor = new pc.Color(this.borderColor);
            this.getComponent('text').color = new pc.Color(this.fontColor);
            }
            if (this.hasComponentOfType('sprite') && (this.getComponent('sprite').sprite.spriteSheet.framesWide > 1 || this.getComponent('sprite').sprite.spriteSheet.framesHigh > 1)) {
                this.getComponent('sprite').sprite.currentFrame = 0;
            }
            if(this.animation && this.animation.onTouchEnd){
                this.animation.onTouchEnd(this);
            }
            this.removeTag('buttonpress');  
            this.unTouchEvent();        
        }
      
    }

});