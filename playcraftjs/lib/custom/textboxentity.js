  pc.TextBoxEntity = pc.Entity.extend('pc.TextBoxEntity',
{},
{
    width: null,
    height: null,
    color: null,
    fontColor:null,
    fontHeight: null,
    borderColor: null,
    borderWidth: null,
    focusBgColor: null,
    focusFontColor: null,
    focusFontHeight: null,
    focusBorderColor: null,
    focusBorderWidth: null,
    blurBgColor: null,
    blurBorderColor: null,
    blueFontColor: null,
    blurFontHeight: null,
    blurBorderWidth: null,
    isSecure: null,
    posX: null,
    poxY: null,
    vAlign: null,
    hAlign: null,
    margin: null,
    hidden: null,
    id: null,
    value: null,
    label: null,
    maxChar: null,
    tabOrder: null,
    shadowOffSetX: null,
    shadowOffSetY: null,
    shadowBlur: null,
    shadowColor: null,
    init: function (layer) {
        this._super(layer);
    },
    initTextBox: function (options) {
        this.showShadow = pc.checked(options.showShadow, true);
        this.shadowColor = pc.checked(options.shadowColor,'#000000');
        this.shadowBlur = pc.checked(options.shadowBlur,4);
        this.shadowOffSetY = pc.checked(options.shadowOffSetY,1);
        this.shadowOffSetX = pc.checked(options.shadowOffSetX,0.5);      
        this.width = pc.checked(options.width,300);
        this.height = pc.checked(options.height,40);
        this.color = pc.checked(options.color, '#183360');
        this.fontColor = pc.checked(options.fontColor,'#275cb4');
        this.fontHeight = pc.checked(options.fontHeight,20);
        this.borderColor = pc.checked(options.borderColor,'#183360');
        this.borderWidth = pc.checked(options.borderWidth,0);
        this.focusBgColor = pc.checked(options.focusBgColor,'#183360');
        this.focusFontColor = pc.checked(options.focusFontColor,'#4CC8FB');
        this.focusFontHeight = pc.checked(options.focusFontHeight,20);
        this.focusBorderColor = pc.checked(options.focusBorderColor,'#183360');
        this.focusBorderWidth = pc.checked(options.focusBorderWidth,0);
        this.blurBgColor = pc.checked(options.blurBgColor,'#183360');
        this.blurBorderColor = pc.checked(options.blurBorderColor,'#183360');
        this.blurFontColor = pc.checked(options.blurFontColor,'#275cb4');
        this.blurBorderWidth = pc.checked(options.blurBorderWidth,0)
        this.blurFontHeight = pc.checked(options.blurFontHeight,20);
        this.vAlign = pc.checked(options.vAlign,'middle');
        this.hAlign = pc.checked(options.hAlign,'left');
        this.margin = pc.checked(options.margin,{
                left: 40,
                bottom: 30,
                top: 0,
                right: 0
        });
        this.posX = pc.checked(options.posX,null);
        this.posY = pc.checked(options.posY,null);
        this.hidden = pc.checked(options.hidden,true);
        this.id = pc.checked(options.id, '');
        this.value = pc.checked(options.value,'');
        this.isSecure = pc.checked(options.isSecure, false);
        this.label = pc.checked(options.label, ''); 
        this.maxChar = pc.checked(options.maxChar, 10);
        this.hidden = pc.checked(options.hidden, false);
        this.tabOrder = pc.checked(options.tabOrder, 0);
        this.addTag('textinput');
        
        if (!options.hidden) {
            // hidden text boxes don't need event handling
            this._buildTextBox(options);
            this._bindActions();
        }       
        else{
          this._buildTextBoxEntity(options);
        }
    },
    updateLabel: function (textUpdate) {
        if (this.hasComponentOfType('text')) {
            var fontHeight = this.getComponent('text').fontHeight,
                color = this.getComponent('text').color,
                text = this.getComponent('text').text,
                offSet = this.getComponent('text').offset;

            this.removeComponentByType('text');
            this.addComponent(pc.components.Text.create({
                fontHeight: fontHeight,
                color: color.rgb,
                text: [textUpdate]            
            }));
        }
    },

    _buildTextBox: function (options) {        
       this._buildTextBoxEntity(options);
       this.addComponent(pc.components.Rect.create({ 
          color: this.color, 
          lineColor: this.borderColor, 
          lineWidth: this.borderWidth }));
        if(!pc.valid(this.posY) || !pc.valid(this.posX)){
          this.addComponent(pc.components.Layout.create({ 
          vertical: this.vAlign, 
          horizontal: this.hAlign, 
          margin: this.margin })); 
        }
        this.addComponent(pc.components.Text.create({ 
          fontHeight: this.fontHeight, 
          color: this.fontColor,
          text: [options.label], 
          offset: options.offset })); 
        if(this.showShadow){
          this.addComponent(pc.components.Shadow.create({
          offSetY: this.shadowOffSetY,
          offSetX: this.shadowOffSetX,
          blur: this.shadowBlur,
          color: this.shadowColor
        }));  
        }
          
    },
    _buildTextBoxEntity: function(options){
        this.addComponent(pc.components.Spatial.create({ w: this.width, h: this.height, x: options.posX, y: options.posY }));
    },
    blurEntity: function () {
        // only blur visible fields
        if (this.hasComponentOfType('text')) {
            if (!this.hidden) {
                var text = this.getComponent('text').text[0],
                textComp = this.getComponent('text');
                if (text === '|') {
                    //    // no entry so default 
                    text = this.label;

                }
                text = text.replace('|', '');
                // reset the entity to unfocused styles
                this.removeComponentByType('rect');
                this.addComponent(pc.components.Rect.create({ color: this.blurBgColor, lineColor: this.blurBorderColor, lineWidth: this.blurBorderWidth }));
                this.removeComponentByType('text');
                this.addComponent(pc.components.Text.create({ fontHeight: this.blurFontHeight, color: this.blurFontColor, text: [text], offset: { x: textComp.offset.x, y: textComp.offset.y } }));
                if (this.hasTag('textboxfocused')) {
                    this.removeTag('textboxfocused');
                }
            }
        }        
    },
    focusEntity: function () {
        //when we focus we want to simulate a cursor. A '|' in this case
        //we want to change the text color of the focused element and change the style of the text box        
        if (this.hasComponentOfType('text')) {
            var text = this.getComponent('text').text[0],
                textComp = this.getComponent('text');
            text = text.replace('|', '');
            this.removeComponentByType('rect');
            this.addComponent(pc.components.Rect.create({ color: this.focusBgColor, lineColor: this.focusBorderColor, lineWidth: this.focusBorderWidth }));
            this.removeComponentByType('text');
            //maintaining the label within the textbox
            if (text === this.label) {
                // input entity is defaulted so show cursor            
                this.addComponent(pc.components.Text.create({ fontHeight: this.fontHeight, color: this.focusFontColor, text: ['|'], offset: { x: textComp.offset.x, y: textComp.offset.y } }));
            }
            else {
                // has user entered text
                this.addComponent(pc.components.Text.create({ fontHeight: this.fontHeight, color: this.focusFontColor, text: [text + '|'], offset: { x: textComp.offset.x, y: textComp.offset.y } }));

            }
            //maintain state of which entity is focused
            this.focusedEntity = this;
            this.addTag('textboxfocused');
        }
        else {
            throw 'components for the textboxentity class have not been added';
        }

    },
    _bindActions: function () {
        this.addComponent(pc.components.Input.create(
            {
                // bind all the alphabet and numbers for input (easy way to prevent special characters too)
                states: [
                     ['shiftHeld', ['SHIFT']]                     
                ],
                actions: [
                       ['characterkey', ['A']],
                       ['characterkey', ['B']],
                       ['characterkey', ['C']],
                       ['characterkey', ['D']],
                       ['characterkey', ['E']],
                       ['characterkey', ['F']],
                       ['characterkey', ['G']],
                       ['characterkey', ['H']],
                       ['characterkey', ['I']],
                       ['characterkey', ['J']],
                       ['characterkey', ['K']],
                       ['characterkey', ['L']],
                       ['characterkey', ['M']],
                       ['characterkey', ['N']],
                       ['characterkey', ['O']],
                       ['characterkey', ['P']],
                       ['characterkey', ['Q']],
                       ['characterkey', ['R']],
                       ['characterkey', ['S']],
                       ['characterkey', ['T']],
                       ['characterkey', ['U']],
                       ['characterkey', ['V']],
                       ['characterkey', ['W']],
                       ['characterkey', ['X']],
                       ['characterkey', ['Y']],
                       ['characterkey', ['Z']],
                       ['characterkey', ['1']],
                       ['characterkey', ['2']],
                       ['characterkey', ['3']],
                       ['characterkey', ['4']],
                       ['characterkey', ['5']],
                       ['characterkey', ['6']],
                       ['characterkey', ['7']],
                       ['characterkey', ['8']],
                       ['characterkey', ['9']],
                       ['characterkey', ['0']],
                       ['characterkey', ['@']],
                       ['characterkey', ['.']],
                       // functional keys too
                       ['enter', ['ENTER']],
                       ['backspace', ['BACKSPACE'],true],                       
                       ['shiftHeld', ['SHIFT']],
                       ['focus', ['MOUSE_BUTTON_LEFT_DOWN']],
                       ['focus', ['TOUCH']]                

                ]
            }));
    },
    /**********************************
     *
     * Text Box Event Handlers
     * 
     **********************************/
    onTextBoxType: function (inputChar) {
        //handling character key input
        if (this.hasTag('textboxfocused')) {
            if (this.hasComponentOfType('text')) {
                var text = this.getComponent('text').text[0],
                isSecure = this.isSecure,
                textInput = this.getComponent('text'),
                label = this.label;
                id = this.id;
                text = text.replace('|', '');
                // secured fields only show *
                if (isSecure) {
                    text += '*|';
                }
                else {
                    text += inputChar + '|';
                }
                // if a secure field we only want to show * but we need to maintain the value in the textInput component
                this.value += inputChar;
                //max char is 15..to do: make this configurable per form
                if (text.length < this.maxChar) {
                    this.removeComponentByType('text');
                    this.addComponent(pc.components.Text.create(
                        {
                            fontHeight: textInput.fontHeight,
                            color: textInput.color.rgb,
                            text: [text],
                            offset:
                                {
                                    x: textInput.offset.x,
                                    y: textInput.offset.y
                                }
                        }));
                }
            }
            else {
                throw 'components for the textboxthis class have not been added';
            }
        }
    },
    onTextBoxBackspace: function () {
        // removes the pipe from the end of the text input value;
        if (this.hasTag('textboxfocused')) {
            if (this.hasComponentOfType('text')) {
                var text = this.getComponent('text').text[0],
               textInput = this.getComponent('text')
               textValue = this.value;

                text = text.replace('|', '');
                text = text.substring(0, text.length - 1);
                text = text + '|';
                textValue = textValue.substring(0, textValue.length - 1);
                this.removeComponentByType('text');
                this.addComponent(pc.components.Text.create({ fontHeight: textInput.fontHeight, color: textInput.color.rgb, text: [text], offset: { x: textInput.offset.x, y: textInput.offset.y } }));
                this.value = textValue;
            }
            else {
                throw 'components for the textboxthis class have not been added';
            }

        }
    }

});