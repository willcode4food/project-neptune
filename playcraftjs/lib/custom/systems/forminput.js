﻿pc.systems.FormInput = pc.systems.Input.extend('pc.systems.FormInput',
 {},
 {    
     formData: null,
     init: function () {
         this._super(['input'], 60);
         this.constants = pc.device.game.__constants;
         this.settings = pc.device.game.gameDeviceSettings;
         this.formData = {};

     },
     onAction: function (actionName, event, pos, uiTarget) {
         var entity = uiTarget.getEntity();

         // focus on the text box
         if (actionName === 'focus') {
             if (pc.valid(entity.focusEntity)) {
                 entity.focusEntity();
             }                 
         }
         // user is typing
         if (pc.valid(entity) && entity.hasTag('textboxfocused')) {
             if (actionName === 'characterkey') {
                 //apparently we have to map key codes to ascii characters
                 if (this.isInputState(entity, 'shiftHeld')) {
                     // handle caps 
                     entity.onTextBoxType(pc.device.game.util.keyMapToUpper(event.keyCode));
                 }
                 else {
                     entity.onTextBoxType(pc.device.game.util.keyMap[event.keyCode]);
                 }
             }             
             if (actionName === 'backspace') {
                 event.preventDefault();
                 entity.onTextBoxBackspace();
                 return;

             }
         }
         // button event handlers
         if (actionName === 'click button') {
            if(pc.valid(entity.onMouseButtonDown)){
                entity.onMouseButtonDown();
            }
            
         }
         if (actionName === 'touch button') {
            if(pc.valid(entity.onTouchStart)){
                entity.onTouchStart();
            }
         }
         if (actionName === 'touch end button') {
            if(pc.valid(entity.onTouchEnd)){
                entity.onTouchEnd();
            }
         }
         if (actionName === 'release button') {
            if(pc.valid(entity.onMouseButtonUp)){
                entity.onMouseButtonUp();
            }
         }
         if (actionName === 'over button' && !this.isInputState(entity,'click button')) {
            if(pc.valid(entity.onMouseButtonOver)){
                entity.onMouseButtonOver();
            }
         }
         if (actionName === 'off button') {
            if(pc.valid(entity.onMouseButtonOut)){
                entity.onMouseButtonOut();
            }
         }
     }
 });