﻿/**
 *   Project Neptune - (C)2015 Eddie Games Inc.
 *   Author: Marc D. Arbesman
 */


pc.LocalStore = pc.Base.extend('pc.LocalStore',
{
    loadJSONObject: function (options) {
        // load JSON into localStorage Dynamically
        this.checkIfCompatible();
        localStorage.setItem(options.storeName, this.encrypt(options.storeName,JSON.stringify(options.obj)));
    },
    getJSONObject: function (storeName) {
        this.checkIfCompatible();
        return JSON.parse(this.decrypt(storeName,localStorage.getItem(storeName)));
    },
    removeObject: function(storeName){
        localStorage.removeItem(storeName);
    },
    checkIfCompatible: function () {
        if (typeof (Storage) === undefined) {
            throw ('Local Storage Not Supported');

        }
    },
    exists: function(key){
        if (localStorage.getItem(key) === null) {
            return false;
        }
            return true;

    },
    encrypt: function (key, data) {
        return sjcl.encrypt(key, data);
    },
    decrypt: function (key, data) {
        return sjcl.decrypt(key,data);
    }
    
},
{});