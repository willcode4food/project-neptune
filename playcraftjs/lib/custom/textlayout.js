﻿pc.TextLayout = pc.Base('pc.TextLayout',
 {},
 {
     text: null,
     lineWidth: null,
     color: null,
     strokeColor: null,
     lineWidth: null,
     fontHeight: null,
     font: null,
     offset: null,

     init: function(options)
     {
         this._super('text');
         this.color = pc.Color.create('#ffffff');
         this.strokeColor = pc.Color.create('#888888');
         this.text = [];
         this.font = 'Calibri';
         this.fontHeight = 20;
         this.offset = pc.Dim.create(0,0);
         this._fontCache = '';
         if (pc.valid(options))
             this.config(options);
     },
     config: function (options) {

         for (var obj in options) {
             this.textLayout.push(obj);
         }
         this.color.set(pc.checked(options.color, '#ffffff'));
         this.strokeColor.set(pc.checked(options.strokeColor, '#888888'));
         this.lineWidth = pc.checked(options.lineWidth, 0);
         this.text = pc.checked(options.text, ['']);
         this.font = pc.checked(options.font, 'Arial');
         this.fontHeight = pc.checked(options.fontHeight, 20);
         if (pc.valid(options.offset)) {
             this.offset.x = pc.checked(options.offset.x);
             this.offset.y = pc.checked(options.offset.y);
         }
       
     },
});