﻿UIPortraitMenu = pc.Pooled.extend('UIPortraitMenu',
    {},
    {
        canvas: null,
        context: null,
        stage: null,
        listening: null,
        mousePos: null,
        mouseClick: null,
        mouseOver: null,
        mouseMove: null,
        currentRegion: null,
        regionIndex: null,
        lastRegionIndex: null,
        mouseOverRegionIndex: null,
        hasShadow: null,
        target: null,
        alwaysVisible: null,
        hitSegment: null,
        startAngle: null,
        endAngle: null,
        segments: null,
        radius: null,
        arcRadius: null,
        rotation: null,
        scale: null,
        tiles: [],
        images: [],
        currentRegionEvent: {},
        anchor: null,
        isbeingClosed: null,
        shadowColor: null,
        backColor: null,
        mouseOverColor: null,
        width: null,
        height: null,
        cX: null,
        cY: null,
        ctxFactor: null,
        gradient: null,
        create: function () {
            var newUIPortraitMenu = this._super;
            return newUIPortraitMenu;
        },
        config: function () {
            this.canvas = pc.device.canvas;
            this.context = pc.device.ctx;
            this.stage = undefined;
            this.listening = false;
            this.mousePos = null;
            this.mouseClick = false
            this.mouseOver = false;
            this.mouseMove = false;
            this.regionIndex = -1;
            this.lastRegionIndex = -1;
            this.mouseOverRegionIndex = -1;
            this.hasShadow = true;
            this.target = document.getElementById(targetElement);
            this.alwaysVisible = false;
            this.hitSegment = -1;
            this.startAngle = 130.0;
            this.endAngle = 50.0;
            this.segments = 3;
            this.radius = 150;
            this.arcRadius = 50;
            this.rotation = 0;
            this.scale = 1;
            this.tiles = [];
            this.images = [];
            this.currentRegionEvent = {};
            this.anchor = "top";
            this.isbeingClosed = false;
            this.shadowColor = 'rgba(0,0,0,0.6)';
            this.backColor = "rgba(0, 0, 0, 0.5)";
            this.mouseOverColor = "rgba(128, 143, 255, 0.44)";
            this.width = this.canvas.width;
            this.height = this.canvas.height;
            this.cX = this.width / 2;
            this.cY = this.height / 2;
            this.ctxFactor = (((this.startAngle - this.endAngle) / this.segments) * 11.5) / 100; //roundness
            this.gradient = null;

            this.target.onmouseover = function () {
                that.isbeingClosed = false;
                SetPosition(that);
                that.refresh();
            };

            // Update with Playcraft way for handling user input events
            //document.addEventListener("click", function (evt) {
            //    if (that.alwaysVisible) return;
            //    that.isbeingClosed = true;
            //    fadeOut(that);
            //}, false);

            //window.addEventListener("resize", function (evt) {
            //    that.refresh();
            //}, false);

            //if (window.addEventListener) {
            //    window.addEventListener('load', function (evt) {
            //        that.refresh();
            //    }, false);
            //} else if (window.attachEvent) {
            //    window.attachEvent('onload', function (evt) {
            //        that.refresh();
            //    }, false);
            //}
            this.setStage(function () {
                if ((this.isbeingClosed == false && this.canvas.style.display == 'block') || this.alwaysVisible) {
                    SetPosition(this);
                    this.clearCvs();
                    // Move registration point to the center of the canvas
                    this.context.save();
                    this.context.translate(this.cX, this.cY);
                    // Rotate  
                    this.context.rotate(this.rotation * TO_RADIANS);
                    // Move registration point back to the top left corner of canvas
                    this.context.translate(-this.cX, -this.cY);
                    this.context.scale(this.scale, this.scale);
                    this.drawCanvas();
                    this.context.restore();
                    this.target.style.zIndex = (this.canvas.style.zIndex) + 1;
                }
            });
        },
        drawSegment: function (instance, segmentIndex) {
            var radius = instance.radius;
            var arcRadius = instance.arcRadius;
            var startAngle = GetAngle(instance, segmentIndex, 1);
            var endAngle = GetAngle(instance, segmentIndex, 0);
            var xCenter = instance.cX;
            var yCenter = instance.cY / .5
            var fol = parseInt(instance.segments / 2) == segmentIndex;
            var context = instance.context;
            var middleAngle = (startAngle + endAngle) / 2;
            var outterSX = getX(startAngle, radius, xCenter);
            var outterSY = getY(startAngle, radius, yCenter);
            var innerSX = getX(startAngle, radius - arcRadius, xCenter);
            var innerSY = getY(startAngle, radius - arcRadius, yCenter);
            var outterEX = getX(endAngle, radius, xCenter);
            var outterEY = getY(endAngle, radius, yCenter);
            var innerEX = getX(endAngle, radius - arcRadius, xCenter);
            var innerEY = getY(endAngle, radius - arcRadius, yCenter);
            var outterMX = getX(middleAngle, radius + instance.ctxFactor, xCenter);
            var outterMY = getY(middleAngle, radius + instance.ctxFactor, yCenter);
            var innerMX = getX(middleAngle, radius - arcRadius + instance.ctxFactor, xCenter);
            var innerMBX = getX(middleAngle + 4, radius - arcRadius, xCenter);
            var innerMPX = getX(middleAngle - 4, radius - arcRadius, xCenter);
            var innerMBY = getY(middleAngle + 4, radius - arcRadius, yCenter);
            var innerMPY = getY(middleAngle - 4, radius - arcRadius, yCenter);
            var innerMCX = getX(middleAngle, radius - arcRadius + instance.ctxFactor, xCenter);
            var innerMCY = getY(middleAngle, radius - arcRadius + instance.ctxFactor, yCenter + 8);
            var innerMY = getY(middleAngle, radius - arcRadius + instance.ctxFactor, yCenter);
            context.moveTo(outterSX, outterSY);
            context.quadraticCurveTo(outterSX, outterSY, innerSX, innerSY);
            context.moveTo(innerSX, innerSY);
            if (fol) {
                context.quadraticCurveTo(innerSX, innerSY, innerMPX, innerMPY);
                context.lineTo(innerMCX, innerMCY);
                context.lineTo(innerMBX, innerMBY);
                context.quadraticCurveTo(innerEX, innerEY, innerEX, innerEY);
            } else
                context.quadraticCurveTo(innerMX, innerMY, innerEX, innerEY);
            context.quadraticCurveTo(innerEX, innerEY, outterEX, outterEY);
            context.quadraticCurveTo(outterMX, outterMY, outterSX, outterSY);
        },
    });