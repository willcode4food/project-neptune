﻿/**
 *   Project Neptune - (C)2015 Eddie Games Inc.
 *   Author: Marc D. Arbesman
 */

/**
 * Class: pn.Scene
 * Extendds: pc.Scene
 * Description: sets up the layout and objects for the scene.  This is the main scene for game play.
 */
pn.scene.GameScene = pc.Scene.extend('GameScene',
    {},
    {
        background: null,
        gameLayer: null,
        playerUI: null,
        board: null,
        __constants: null,
        settings: null,
        playerShipZone: null,
        opponentShipZone: null,
        playerHandZone: null,
        opponentHandZone: null,
        playerClassificationZone: null,
        diceZone: null,
        init: function () {
            this._super();
            this.background = new Background(this);
            this.settings = pc.device.game.gameDeviceSettings;
            this.constants = pc.device.game.__constants;
            // dealing with the layer addition from the game scene to the encapsilated game board class.  
            // This time, we are using a Tile layer
            // And instead, the tileLayer object is rendered in the board class itself.
            this.gameLayer = new pc.EntityLayerScroller('Cards In Play layer');                      
            this.gameLayer.zIndex = 5;
            var userPlayer = pc.device.game.getUserPlayer(),
                opponentPlayer = pc.device.game.getOpponentPlayer(),
                userPlayerShipZone = pc.device.game.getZoneByName(userPlayer, 'Ship'),
                userPlayerHoldingZone = pc.device.game.getZoneByName(userPlayer, 'Holding'),
                opponentPlayerHoldingZone = pc.device.game.getZoneByName(opponentPlayer, 'Holding'),
                opponentPlayerShipZone = pc.device.game.getZoneByName(opponentPlayer, 'Ship'),
                playerClassZoneJSON = pc.device.game.getZoneByName(userPlayer, 'Classification');

                

            this.board = new Board(this);
            //initialize the game UI

            this.playerUI = new PlayerUI(this);
            
            this.playerShipZone = new ShipZone(userPlayer, userPlayerShipZone);
            this.playerShipZone.load(
                this.board.boardShipMap.boardTiles,
                this.gameLayer,
                200,
                570,
                this.constants.get('DECK_ORIENT_LANDSCAPE')
            );
            this.opponentShipZone = new ShipZone(opponentPlayer, opponentPlayerShipZone);
            this.opponentShipZone.load(
                this.board.boardShipMap.boardTiles,
                this.gameLayer,
                200,
                200,
                this.constants.get('DECK_ORIENT_LANDSCAPE')
              );

            this.playerHandZone = new HoldingZone(userPlayer, userPlayerHoldingZone);
            this.playerHandZone.load(
             this.board.boardShipMap.boardTiles,
                this.gameLayer,
                this.settings.getUIPlayerHandStartX(),
                this.settings.getUIPlayerHandStartY(),
                this.settings.getUIPlayerHandMoveX(),
                this.settings.getUIPlayerHandMoveY(),
                this.settings.getUIPlayerHandPadding(),
                this.constants.get('DECK_ORIENT_PORTRAIT')
              );
          
            this.opponentHandZone = new HoldingZone(opponentPlayer, opponentPlayerHoldingZone);
            this.opponentHandZone.load(
              this.board.boardShipMap.boardTiles,
                this.gameLayer,
                this.settings.getUIOpponentHandStartX(),
                this.settings.getUIOpponentHandStartY(),
                this.settings.getUIOpponentHandMoveX(),
                this.settings.getUIOpponentHandMoveY(),
                this.settings.getUIOpponentHandPadding(),
                this.constants.get('DECK_ORIENT_PORTRAIT')
              );

            this.diceZone = new DiceZone({
                scene: this,                
               
            });
            this.playerClassificationZone = new ClassificationZone(userPlayer, playerClassZoneJSON);
            this.playerClassificationZone.load(
                this.board.boardShipMap.boardTiles,
                this.gameLayer,
                this.settings.getUIPlayerAttributeStartX(),
                this.settings.getUIPlayerAttributeStartY(),
                this.constants.get('DECK_ORIENT_PORTRAIT')
                );
            

            this.addLayer(this.gameLayer);

            this.playerShipZone.dealCards();
            this.opponentShipZone.dealCards();            

    // setting up event handlers for zooming of the entire game scene
            pc.device.input.bindAction(this, 'draggrab', 'MOUSE_BUTTON_LEFT_DOWN');
            pc.device.input.bindAction(this, 'dragmove', 'MOUSE_MOVE_DRAGDROP');
            pc.device.input.bindAction(this, 'dragletgo', 'MOUSE_BUTTON_LEFT_UP');
            pc.device.input.bindAction(this, 'zoomin', 'MOUSE_WHEEL_DOWN');
            pc.device.input.bindAction(this, 'zoomout', 'MOUSE_WHEEL_UP');
            pc.device.input.bindAction(this, 'zoomtouchstart', 'TOUCH');
            pc.device.input.bindAction(this, 'zoomtouchmove', 'TOUCH_MOVE');
            pc.device.input.bindAction(this, 'zoomtouchend', 'TOUCH_END_NOPOS');
            pc.device.input.bindAction(this, 'increment', 'UP');
            pc.device.input.bindAction(this, 'decrement', 'DOWN');

        },
        onAction: function (actionName, event, pos) {
            var pageX = event.pageX, pageY = event.pageY, timeStamp = event.timeStamp, detail = event.detail, wheelDelta = event.wheelDelta, touches = event.touches, scale = event.scale;
            // handle mouse wheel zoom in
            if (actionName === 'increment') {
                this.playerUI.updatePlayerLeadership(1);
                this.playerUI.updatePlayerCrew(1);
                this.playerUI.updatePlayerPower(1);

                this.playerUI.updateOpponentLeadership(1);
                this.playerUI.updateOpponentCrew(1);
                this.playerUI.updateOpponentPower(1);
                this.diceZone.roll({
                    frames: 30,
                    numDice:3
                });
            }
            if (actionName === 'decrement') {
                this.playerUI.updatePlayerLeadership(-1);
                this.playerUI.updatePlayerCrew(-1);
                this.playerUI.updatePlayerPower(-1);

                this.playerUI.updateOpponentLeadership(-1);
                this.playerUI.updateOpponentCrew(-1);
                this.playerUI.updateOpponentPower(-1);
                this.diceZone.clear();
            }
      
                if (actionName === 'zoomout' || actionName === 'zoomin') {
                    //var delta = Math.floor(wheelDelta ? wheelDelta / 40 : detail ? -detail : 0);

                    
                    var pageX = event.pageX, pageY = event.pageY, timeStamp = event.timeStamp, detail = event.detail, wheelDelta = event.wheelDelta, touches = event.touches, scale = event.scale;
                    this.board.boardShipTileLayer.scroller.doMouseZoom(
                         detail ? (detail * -120) : wheelDelta,
                         timeStamp,
                         pageX + this.board.boardShipTileLayer.origin.x,
                         pageY + this.board.boardShipTileLayer.origin.y
                    );
                    this.board.boardPlayfieldTileLayer.scroller.doMouseZoom(
                        detail ? (detail * -120) : wheelDelta,
                        timeStamp,
                        pageX + this.board.boardPlayfieldTileLayer.origin.x,
                        pageY + this.board.boardPlayfieldTileLayer.origin.y
                    );
                    this.gameLayer.doMouseZoom(
                        detail ? (detail * -120) : wheelDelta,
                        timeStamp,
                        pageX + this.board.boardShipTileLayer.origin.x,
                        pageY + this.board.boardShipTileLayer.origin.y
                   );
                }  
                // handle mouse click
                if (actionName === 'draggrab') {
                    if (event.target.tagName.match(/input|textarea|select/i)) {
                        return;
                    }
                    this.board.boardShipTileLayer.scroller.doTouchStart([{
                        pageX: pageX + this.board.boardShipTileLayer.origin.x,
                        pageY: pageY + this.board.boardShipTileLayer.origin.y
                    }], timeStamp);

                    this.board.boardPlayfieldTileLayer.scroller.doTouchStart([{
                        pageX: pageX + this.board.boardPlayfieldTileLayer.origin.x,
                        pageY: pageY + this.board.boardPlayfieldTileLayer.origin.y
                    }], timeStamp);
                    this.gameLayer.doTouchStart([{
                        pageX: pageX + this.board.boardShipTileLayer.origin.x,
                        pageY: pageY + this.board.boardShipTileLayer.origin.y
                    }], timeStamp);

                    this.mouseDown = true;
                    

                    if (this.playerUI.playerHandIconEntity.getComponent('spatial').getScreenRect().containsPointUpscaled(pos)) {
                        // clicked on hand icon
                        this.playerHandZone.toggleOpenClose();
                        this.opponentHandZone.toggleOpenClose();
                    }
                   
                }
                // handle mouse move
                if (actionName === 'dragmove') {
                    if (!this.mouseDown) {
                        return;
                    }                    
                    this.board.boardShipTileLayer.scroller.doTouchMove([{
                        pageX: pageX + this.board.boardShipTileLayer.origin.x,
                        pageY: pageY + this.board.boardShipTileLayer.origin.y
                    }], timeStamp);

                    this.board.boardPlayfieldTileLayer.scroller.doTouchMove([{
                        pageX: pageX + this.board.boardPlayfieldTileLayer.origin.x,
                        pageY: pageY + this.board.boardPlayfieldTileLayer.origin.y
                    }], timeStamp);
                    this.gameLayer.doTouchMove([{
                        pageX: pageX + this.board.boardShipTileLayer.origin.x,
                        pageY: pageY + this.board.boardShipTileLayer.origin.y
                    }], timeStamp);
                    this.mouseDown = true;
                }
                // handle mouse drop
                if (actionName === 'dragletgo') {
                    if (!this.mouseDown) {
                        return;
                    }

                    this.board.boardShipTileLayer.scroller.doTouchEnd(timeStamp);
                    this.board.boardPlayfieldTileLayer.scroller.doTouchEnd(timeStamp);
                    this.gameLayer.doTouchEnd(timeStamp);
                    this.mouseDown = false;
                }
                if (actionName === 'zoomtouchstart') {
                    if (touches[0] && touches[0].target && touches[0].target.tagName.match(/input|textarea|select/i)) {
                        return;
                    }
                    
                this.board.boardShipTileLayer.scroller.doTouchStart(touches, timeStamp);
                this.board.boardPlayfieldTileLayer.scroller.doTouchStart(touches, timeStamp);
                this.gameLayer.doTouchStart(touches, timeStamp);
                var point = new pc.Point(touches[0].screenX, touches[0].screenY);

                
                if (touches.length === 1) {
                    if (this.playerUI.playerHandIconEntity.getComponent('spatial').getScreenRect().containsPointUpscaled(point)) {
                        // clicked on player portrait

                        this.playerHandZone.toggleOpenClose();
                        this.opponentHandZone.toggleOpenClose();
                    } else {
                        //this.diceZone.roll({
                        //    numDice: 3
                        //});
                        return;
                    }
                }
                else {
                    //this.diceZone.clear();
                    return;
                }
                    
                }
                if (actionName === 'zoomtouchmove') {
                    var timeStamp = timeStamp;
                    this.board.boardShipTileLayer.scroller.doTouchMove(touches, timeStamp, scale);
                    this.board.boardPlayfieldTileLayer.scroller.doTouchMove(touches, timeStamp, scale);
                    this.gameLayer.doTouchMove(touches, timeStamp, scale);

                }
                if (actionName === 'zoomtouchend') {

                    this.board.boardShipTileLayer.scroller.doTouchEnd(timeStamp);
                    this.board.boardPlayfieldTileLayer.scroller.doTouchEnd(timeStamp);
                    this.gameLayer.doTouchEnd(timeStamp);
                }
        },     
        process: function () {
            // clear the background
           // pc.device.ctx.clearRect(0, 0, pc.device.canvasWidth, pc.device.canvasHeight);       
            // always call the super
            this._super();
        }        

    });