pn.layer.FormBackgroundLayer = pc.EntityLayer.extend('pn.layer.FormBackgroundLayer',
{},
{	
	settings:null,
	init:function(name, zIndex){
		this._super(name, zIndex);
		this.settings = pc.device.game.gameDeviceSettings;
		this.zIndex = 0;
	},
	draw: function(){
		this._super();
		// default background
		 pc.device.ctx.fillStyle = pc.device.game.__constants.get('BACKGROUND_COLOR');
		 //'#cedbd2';
         pc.device.ctx.fillRect(0, 0, this.settings.getBaseDeviceWidth(), this.settings.getBaseDeviceHeight());
	}	
});