﻿/**
 *   Project Neptune - (C)2015 Eddie Games Inc.
 *   Author: Marc D. Arbesman
 */

/**
 *  Class: pn.util.pnHttpClient
 *  Extends: pc.HttpClient
 *  
 *  Description: Facilitates handling refresh tokens and resumes prior request.  Provides a default loading layer
 *  
 */

pn.util.pnHttpClient = pc.HttpClient.extend('pn.util.pnHttpClient',
{},
{    
    init: function (options) {
        this._super(options);

    },
    config: function (options) {
        this._super(options);
      
        // the unauth request that will get called after a refresh token is issued.
        if (pc.checked(options.unAuthRequest)) {
            this._xhr._unauthRequest = options.unAuthRequest;
        }
    
        
    },
    
    _unauthorize: function (self) {
        /// <summary>
        /// Gets called when bearer token expires
        /// </summary>
        /// <param name="self">XMLHTTPRequest object</param>
        //remove prior request's loading layer first
        self = self;
        self.callingObject.setLayerInactive(self.callingObject._loadingLayer);
        // attempt to refresh login with refresh token
        
        var client = new pn.util.pnHttpClient({
            cache: true,
            onError: self.onRefreshError,
            onSuccess: self.onRefreshSuccess,            
            callingObject: self.callingObject,
            unAuthRequest: self

        });
        client._onBeforeSend = function (self) {
            self.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            self.setRequestHeader('Accept', 'application/json');
        };
        var formData = {};
        formData['grant_type'] = 'refresh_token';
        formData['client_id'] = 7;
        formData['refresh_token'] = pc.LocalStore.getJSONObject(pc.device.game.__constants.get('SYS_STORAGE_PLAYER_AUTH')).refresh_token;
        client.postFormAsync({
            url: pc.device.game.__constants.get('SYS_API_DOMAIN') + pc.device.game.__constants.get('SYS_API_TOKEN'),
            formData: formData
        });
       
    },
    _onBeforeSend: function (self) {
        /// <summary>
        /// setting the headers to include the bearer token.  All requests will include these headers by default
        /// </summary>
        /// <param name="self">XMLHTTPRequest object</param>
        self.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        self.setRequestHeader('Accept', 'application/json');
        self.setRequestHeader('Authorization', 'Bearer ' + pc.LocalStore.getJSONObject(pc.device.game.__constants.get('SYS_STORAGE_PLAYER_AUTH')).access_token);
        
    },
    onRefreshError: function (errMsg, self) {
        /// <summary>
        /// Overrides the client's default error handler for refresh token requests.  
        /// If the refresh token fails, this means accesss has been revoked.  onRefreshError handle
        /// transporting the user back to the log in screen. 
        /// </summary>
        /// <param name="errMsg">Error from the server</param>
        /// <param name="self">XMLHTTPRequest object</param>

        // if refresh attemp fails, show login screen
        self.callingObject.setLayerInactive(self.callingObject.loadingLayer);
        var node = pc.device.game.activeScenes.first;
        while (node) {
            pc.device.game.deactivateScene(node);
            node = node.next();
        }
        pc.LocalStore.removeObject(pc.device.game.__constants.get('SYS_STORAGE_PLAYER_AUTH'));
        pc.LocalStore.removeObject("playerProfile");

        //deactivate all actives scenes

        pc.device.game.deactivateScene(self.callingObject);

        // activate the login scene
        pc.device.game.activateScene(pc.device.game.loginScene);
    },
    onRefreshSuccess: function (res, self) {
        /// <summary>
        /// Overrides the default client's success handlers for refresh tokens
        /// When a refresh token is received, resumes the originally request
        /// </summary>
        /// <param name="res"></param>
        /// <param name="self"></param>
        self.callingObject.setLayerInactive(self.callingObject.loadingLayer);
        pc.LocalStore.loadJSONObject({ obj: res, storeName: pc.device.game.__constants.get('SYS_STORAGE_PLAYER_AUTH') });
        self._xhr._unauthRequest._sendAjax();
    },
    _error: function (error, self) {
        /// <summary>
        /// default error for all requests issued by this client
        /// </summary>
        /// <param name="error">Error Message</param>
        /// <param name="self">XMLHTTPRequest object</param>
        self.callingObject.setLayerInactive(self.callingObject.loadingLayer);
        console.log(error);
    },
    _loading: function (self) {
        /// <summary>
        /// the default loading screen for the game
        /// </summary>
        /// <param name="self"></param>
        
        // deactivate inputs for all other layers
        var next = self.callingObject.layers.first;
        while (next) {
            if((next.obj.uniqueId.indexOf('pc.EntityLayer') === 0) && (next.obj.name !== "loadingLayer")){
                    next.obj.deactivateInputs();                                    
            }                
            next = next.next();
        }
        // show loading sprite        
        self.callingObject._loadingLayer.setActive();

    },
    _complete: function (self) {
        /// <summary>
        /// handles the complete event of the request
        /// </summary>
        /// <param name="self">XMLHTTPRequest object</param>
        
        // reactivate all inputs for entity layer
        var next = self.callingObject.layers.first;
        while (next) {
            if((next.obj.uniqueId.indexOf('pc.EntityLayer') === 0) && (next.obj.name !== "loadingLayer")){
                next.obj.activateInputs();                    
            }
            next = next.next();
        }
        // remove the loading layer if exists
        if(self.callingObject._loadingLayer.isActive()){
                 // self.callingObject.setLayerInactive(self.callingObject.loadingLayer);      
                 self.callingObject._loadingLayer.setInactive();
        }
        
    },
    logout: function(){
        // public metthod to send refresh token to web api call

        self = this;
    
        self.delAsync({
            url: pc.device.game.__constants.get('SYS_API_DOMAIN') + pc.device.game.__constants.get('SYS_API_LOGOUT') + '?tokenid=' + pc.LocalStore.getJSONObject(pc.device.game.__constants.get('SYS_STORAGE_PLAYER_AUTH')).refresh_token,            
            headers: [
                {
                    header: 'Authorization',
                    value: 'Bearer ' + pc.LocalStore.getJSONObject(pc.device.game.__constants.get('SYS_STORAGE_PLAYER_AUTH')).access_token
                }
            ]

        });
    }
});