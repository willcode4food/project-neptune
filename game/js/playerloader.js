﻿/**
 * Project Neptune  - (C)2015 Eddie Games Inc.
 * Marc Arbesman
 */

/**
 * Class: PlayerLoader
 * Extends: pc.Base
 * Description:  loads JSON data that represents the each individual player and stores them 
 * into local storage.  This object is called during the asset loading in the main Game object.
 * 
 */

pn.PlayerLoader = pc.Base.extend('PlayerLoader',
{
    load: function (url) {
      /// <summary>Initializes the AJAX request and passes the returned JSON data to gather subsequent images</summary>
      /// <param name="url" type="Object">The URL of locate of the deck's JSON data</param>
      /// <param name="deckName" type="Object">A unique identifier for the deck</param>

        var url = url,
            jsonData = pc.Ajax.requestJSON(url);
        return this.loadData(jsonData);
    },
    //loadIntoLocalStorage: function (jsonData, playerNumber) {
    //    /// <summary></summary>
    //    /// <param name="jsonData" type="Object">JSON formatted player data</param>
    //    /// <param name="playerName" type="Object">the player name</param>

    //    return pc.LocalStore.loadJSONObject({ obj: jsonData, storeName: playerName });
    //},
    loadData: function (JSONdata) {
        var players = [];

        for (var i = 0; i < JSONdata.length; i++) {
            var obj = JSONdata[i];
            var player = new Player();
            player.id = obj.id;
            player.name = obj.name;
            player.imageFile = obj.imageFile;
            //for (var j = 0; j < obj.counters.length; j++) {
            //    player.counters.push(obj);
            //}
            player.counters = obj.counters;
            player.isOpponent = obj.isOpponent;
            this.loadPortraitImage(player.id, player.imageFile);
            players.push(player);            
        }
        
        return players;
    },
    loadPortraitImage: function (playerID, imageFile) {
        pc.device.loader.add(new pc.Image('PlayerPortrait' + playerID, pc.device.game.__constants.get('GAME_IMAGES_CDN_DOMAIN') + pc.device.game.__constants.get('IMAGE_CDN_URL') + pc.device.game.gameDeviceSettings.getUIImageURL() + imageFile));
    }   
},
{
});