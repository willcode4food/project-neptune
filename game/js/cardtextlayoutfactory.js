﻿pn.CardTextLayoutFactory = pc.Base.extend('CardTextLayoutFactory',
{},
{
    getSmallLayout: null,
    getLargeLayout: null,
    setLargeStatsText: null,
    setAttributesText: null,
    yTextCursor: 0,
    init: function (cardType) {
        this.settings = pc.device.game.gameDeviceSettings;
        this.constants = pc.device.game.__constants;
        if (!pc.valid(cardType)) {
            throw 'card type is not spefied';
        }
        if (cardType === 'Compartment') {
            this.shipCardLayout();
        }
        else if(cardType === 'Ordnance') {
            this.ordnanceCardLayout();
        }
        else if (cardType === 'Event') {
            this.eventCardLayout();
        }
        else if (cardType === 'Order') {
            this.orderCardLayout();
        }            
        else if (cardType === 'Misfortune') {
            this.misfortuneCardLayout();
        }
        else if (cardType === 'Attribute') {
            this.attributeCardLayout();
        }
        else if (cardType === 'Support Craft') {
            this.supportCraftCardLayout();
        }
    },
    shipCardLayout: function () {
        this.getSmallLayout = function (shieldStatus, damageStatus, armorValue, structureValue) {

            //Shield Status
            return [{
                text: shieldStatus,
                color: this.settings.getShipShieldTypeColor(),
                strokeColor: this.settings.getShipShieldTypeStrokeColor(),
                lineWidth: this.settings.getShipShieldTypeLineWidth(),
                fontHeight: this.settings.getShipShieldTypeHeight(),
                font: this.settings.getShipShieldTypeFont(),
                offset: { x: this.settings.getShipShieldTypeOffsetX(), y: this.settings.getShipShieldTypeOffsetY() }
            },
            // Damage Status
            {
                text: damageStatus,
                color: this.settings.getShipDamageTypeColor(),
                strokeColor: this.settings.getShipDamageTypeStrokeColor(),
                lineWidth: this.settings.getShipDamageTypeLineWidth(),
                fontHeight: this.settings.getShipDamageTypeHeight(),
                font: this.settings.getShipDamageTypeFont(),
                offset: { x: this.settings.getShipDamageTypeOffsetX(), y: this.settings.getShipDamageTypeOffsetY() }
            },
            // Armor Value
            {
                text: armorValue,
                color: this.settings.getShipArmorTypeColor(),
                strokeColor: this.settings.getShipArmorTypeStrokeColor(),
                lineWidth: this.settings.getShipArmorTypeLineWidth(),
                fontHeight: this.settings.getShipArmorTypeHeight(),
                font: this.settings.getShipArmorTypeFont(),
                offset: { x: this.settings.getShipArmorTypeOffsetX(), y: this.settings.getShipArmorTypeOffsetY() }
            },
            // Structure Value
            {
                text: structureValue,
                color: this.settings.getShipStructureTypeColor(),
                strokeColor: this.settings.getShipStructureTypeStrokeColor(),
                lineWidth: this.settings.getShipStructureTypeLineWidth(),
                fontHeight: this.settings.getShipStructureTypeHeight(),
                font: this.settings.getShipStructureTypeFont(),
                offset: { x: this.settings.getShipStructureTypeOffsetX(), y: this.settings.getShipStructureTypeOffsetY() }
            }];
        };
        this.getMinLayout = function (shieldStatus, damageStatus, armorValue, structureValue) {
            return [];
        };
        this.getLargeLayout = function (options) {
            var textLayout = [];
            this.setLargeStatsText(options.shieldStatus, options.damageStatus, options.armor, options.structure, textLayout);

            this.setTitleText(options.cardName,
                this.settings.getShipTitleLargeSectionWidth(),
                this.settings.getShipStructureTypeLineWidth(),
                '#FFF',
                '#FFF',
                '',
                this.settings.getShipTitleLargeFontSize(),
                this.settings.getShipTitleLargeFontFamily(),
                this.settings.getShipTitleLargeStartX(),
                -300,
                textLayout);

            var subTitleWidth = pc.device.game.util.getTextWidth({
                fontSize: this.settings.getShipSubTitleLargeFontSize(),
                text: options.cardClass,
                fontFamily: this.settings.getShipSubTitleLargeFontFamily()
            });
            // set subtitle
            this.yTextCursor -= 10;

            this.setTitleText(options.cardClass,
                this.settings.getShipTitleLargeSectionWidth(),
                this.settings.getShipStructureTypeLineWidth(),
                '#f0f9a7',
                '#f0f9a7',
                '',
                this.settings.getShipSubTitleLargeFontSize(),
                this.settings.getShipSubTitleLargeFontFamily(),
                this.settings.getShipTitleLargeStartX(),
                this.yTextCursor,
                textLayout);

            this.setTitleText(options.subtitle,
                 this.settings.getShipTitleLargeSectionWidth(),
                 this.settings.getShipStructureTypeLineWidth(),
                 '#f0f9a7',
                 '#f0f9a7',
                 '',
                 this.settings.getShipSubTitleLargeFontSize() - 5,
                 'Government Agent BB Ital',
                 this.settings.getShipTitleLargeStartX(),
                 this.yTextCursor,
                 textLayout);

            this.yTextCursor += 10;
            this.setAttributesText(options.rangeMin, options.rangeMax, options.accuracy, options.damage, options.power, options.restPower, textLayout);

            // set status effect text if we have one
            if (options.statusEffect !== '') {
                this.wrapText(options.statusEffect,
                    this.settings.getShipTitleLargeSectionWidth(),
                    this.settings.getShipStructureTypeLineWidth(),
                    '#FFF',
                    '#f0f9a7',
                    '',
                    15,
                    'Arial',
                    this.settings.getShipTitleLargeStartX() + 10,
                    this.yTextCursor + 25,
                    textLayout);
            }
            return textLayout;
        };
        this.setLargeStatsText = function (shieldStatus, damageStatus, armor, structure, textLayout) {
            textLayout.push(
            {
                text: shieldStatus,
                color: this.settings.getShipShieldTypeColor(),
                strokeColor: this.settings.getShipShieldTypeStrokeColor(),
                lineWidth: this.settings.getShipShieldTypeLineWidth(),
                fontHeight: this.settings.getShipShieldTypeLargeFontHeight(),
                font: this.settings.getShipShieldTypeLargeFont(),
                offset: { x: this.settings.getShipShieldTypeLargeOffsetX(), y: -20  }

            },
            {
                text: damageStatus,
                color: this.settings.getShipDamageTypeColor(),
                strokeColor: this.settings.getShipDamageTypeStrokeColor(),
                lineWidth: this.settings.getShipDamageTypeLineWidth(),
                fontHeight: this.settings.getShipDamageTypeLargeFontHeight(),
                font: this.settings.getShipDamageTypeLargeFont(),
                offset: { x: this.settings.getShipDamageTypeLargeOffsetX(), y: -20 }
            },
            {
                text: armor,
                color: this.settings.getShipArmorTypeColor(),
                strokeColor: this.settings.getShipArmorTypeStrokeColor(),
                lineWidth: this.settings.getShipArmorTypeLineWidth(),
                fontHeight: this.settings.getShipArmorTypeLargeFontHeight(),
                font: this.settings.getShipArmorTypeLargeFont(),
                offset: { x: this.settings.getShipArmorTypeLargeOffsetX(), y: -310 }
            },
            {
                text: structure,
                color: this.settings.getShipStructureTypeColor(),
                strokeColor: this.settings.getShipStructureTypeStrokeColor(),
                lineWidth: this.settings.getShipStructureTypeLineWidth(),
                fontHeight: this.settings.getShipStructureTypeLargeFontHeight(),
                font: this.settings.getShipStructureTypeLargeFont(),
                offset: { x: 165, y: -310 }
            });
        };
        this.setAttributesText = function (rangeMin, rangeMax, accuracy, damage, power, restPower, textLayout) {
            if (rangeMin > 0 && rangeMax > 0) {
                //set range 
                textLayout.push(
                {
                    text: 'Range:  ' + rangeMin + ' - ' + rangeMin,
                    color: '#FFF',
                    strokeColor: '#FFF',
                    lineWidth: this.settings.getShipShieldTypeLineWidth(),
                    fontHeight: 20,
                    font: 'Government Agent BB',
                    offset: { x: this.settings.getShipTitleLargeStartX() + 50, y: this.yTextCursor }
                });
                this.yTextCursor += 20;
            }

            if (accuracy > 0) {
                //set accuracy
                textLayout.push(
                {
                    text: 'Accuracy:  ' + accuracy,
                    color: '#FFF',
                    strokeColor: '#FFF',
                    lineWidth: this.settings.getShipShieldTypeLineWidth(),
                    fontHeight: 20,
                    font: 'Government Agent BB',
                    offset: { x: this.settings.getShipTitleLargeStartX() + 50, y: this.yTextCursor }
                });
                this.yTextCursor += 20;
            }
            if (damage > 0) {
                //set accuracy
                textLayout.push(
                {
                    text: 'Damage:  ' + damage,
                    color: '#FFF',
                    strokeColor: '#FFF',
                    lineWidth: this.settings.getShipShieldTypeLineWidth(),
                    fontHeight: 20,
                    font: 'Government Agent BB',
                    offset: { x: this.settings.getShipTitleLargeStartX() + 50, y: this.yTextCursor }
                });
                this.yTextCursor += 20;
            }
            if (power > 0) {
                //set accuracy
                textLayout.push(
                {
                    text: 'Requires:  ' + power + ' power',
                    color: '#FFF',
                    strokeColor: '#FFF',
                    lineWidth: this.settings.getShipShieldTypeLineWidth(),
                    fontHeight: 20,
                    font: 'Government Agent BB',
                    offset: { x: this.settings.getShipTitleLargeStartX() + 50, y: this.yTextCursor }
                });
                this.yTextCursor += 20;
            }
            if (restPower > 0) {
                //set accuracy
                textLayout.push(
                {
                    text: 'Provides:  ' + restPower + ' power',
                    color: '#FFF',
                    strokeColor: '#FFF',
                    lineWidth: this.settings.getShipShieldTypeLineWidth(),
                    fontHeight: 20,
                    font: 'Government Agent BB',
                    offset: { x: this.settings.getShipTitleLargeStartX() + 50, y: this.yTextCursor }
                });
                this.yTextCursor += 20;
            }
        };
    },
    ordnanceCardLayout: function () {
        this.getSmallLayout = function (shieldStatus, damageStatus, armorValue, structureValue) {

            return [{
                text: shieldStatus,
                color: this.settings.getShipShieldTypeColor(),
                strokeColor: this.settings.getShipShieldTypeStrokeColor(),
                lineWidth: this.settings.getShipShieldTypeLineWidth(),
                fontHeight: this.settings.getShipShieldTypeHeight(),
                font: this.settings.getShipShieldTypeFont(),
                offset: { x: this.settings.getConflictShieldStatusX(), y: this.settings.getConflictShieldStatusY() }
            },
            {
                text: damageStatus,
                color: this.settings.getShipDamageTypeColor(),
                strokeColor: this.settings.getShipDamageTypeStrokeColor(),
                lineWidth: this.settings.getShipDamageTypeLineWidth(),
                fontHeight: this.settings.getShipDamageTypeHeight(),
                font: this.settings.getShipDamageTypeFont(),
                offset: { x: this.settings.getConflictDamageStatusX(), y: this.settings.getConflictDamageStatusY() }
            },
            {
                text: armorValue,
                color: this.settings.getShipArmorTypeColor(),
                strokeColor: this.settings.getShipArmorTypeStrokeColor(),
                lineWidth: this.settings.getShipArmorTypeLineWidth(),
                fontHeight: this.settings.getShipArmorTypeHeight(),
                font: this.settings.getShipArmorTypeFont(),
                offset: { x: this.settings.getConflictArmorStatusX(), y: this.settings.getConflictArmorStatusY() }
            },
            {
                text: structureValue,
                color: this.settings.getShipStructureTypeColor(),
                strokeColor: this.settings.getShipStructureTypeStrokeColor(),
                lineWidth: this.settings.getShipStructureTypeLineWidth(),
                fontHeight: this.settings.getShipStructureTypeHeight(),
                font: this.settings.getShipStructureTypeFont(),
                offset: { x: this.settings.getConflictStructureStatusX(), y: this.settings.getConflictStructureStatusY() }
            }];
        };
        this.getMinLayout = function (shieldStatus, damageStatus, armorValue, structureValue) {

            return [{
                text: shieldStatus,
                color: this.settings.getShipShieldTypeColor(),
                strokeColor: this.settings.getShipShieldTypeStrokeColor(),
                lineWidth: this.settings.getShipShieldTypeLineWidth(),
                fontHeight: this.settings.getShipShieldTypeHeight(),
                font: this.settings.getShipShieldTypeFont(),
                offset: {
                    x: this.constants.get('DECK_CONFLICT_SHIELD_TYPE_MIN_START_X_MOBILE'),
                    y: this.constants.get('DECK_CONFLICT_SHIELD_TYPE_MIN_START_Y_MOBILE')
                }
            },
            {
                text: damageStatus,
                color: this.settings.getShipDamageTypeColor(),
                strokeColor: this.settings.getShipDamageTypeStrokeColor(),
                lineWidth: this.settings.getShipDamageTypeLineWidth(),
                fontHeight: this.settings.getShipDamageTypeHeight(),
                font: this.settings.getShipDamageTypeFont(),
                offset: {
                    x: this.constants.get('DECK_CONFLICT_DAMAGE_TYPE_MIN_START_X_MOBILE'),
                    y: this.constants.get('DECK_CONFLICT_DAMAGE_TYPE_MIN_START_Y_MOBILE')
                }
            },
            {
                text: armorValue,
                color: this.settings.getShipArmorTypeColor(),
                strokeColor: this.settings.getShipArmorTypeStrokeColor(),
                lineWidth: this.settings.getShipArmorTypeLineWidth(),
                fontHeight: this.settings.getShipArmorTypeHeight(),
                font: this.settings.getShipArmorTypeFont(),
                offset: {
                    x: this.constants.get('DECK_CONFLICT_ARMOR_TYPE_MIN_START_X_MOBILE'),
                    y: this.constants.get('DECK_CONFLICT_ARMOR_TYPE_MIN_START_Y_MOBILE')
                }
            },
            {
                text: structureValue,
                color: this.settings.getShipStructureTypeColor(),
                strokeColor: this.settings.getShipStructureTypeStrokeColor(),
                lineWidth: this.settings.getShipStructureTypeLineWidth(),
                fontHeight: this.settings.getShipStructureTypeHeight(),
                font: this.settings.getShipStructureTypeFont(),
                offset: {
                    x: this.constants.get('DECK_CONFLICT_STRUCTURE_TYPE_MIN_START_X_MOBILE'),
                    y: this.constants.get('DECK_CONFLICT_STRUCTURE_TYPE_MIN_START_Y_MOBILE')
                }
            }];
        };
        this.getLargeLayout = function (options) {
            var textLayout = [];
            this.setLargeStatsText(options.shieldStatus, options.damageStatus, options.armor, options.structure, textLayout);

            this.setTitleText(options.cardName,
                this.settings.getCardWidthZoomed(),
                this.settings.getShipStructureTypeLineWidth(),
                '#FFF',
                '#FFF',
                '',
                this.settings.getShipTitleLargeFontSize(),
                this.settings.getShipTitleLargeFontFamily(),
                0,
                -200,
                textLayout);

            var subTitleWidth = pc.device.game.util.getTextWidth({
                fontSize: this.settings.getShipSubTitleLargeFontSize(),
                text: options.cardClass,
                fontFamily: this.settings.getShipSubTitleLargeFontFamily()
            });
            // set subtitle
            this.yTextCursor -= 10;

            this.setTitleText(options.cardClass,
                this.settings.getCardWidthZoomed(),
                this.settings.getShipStructureTypeLineWidth(),
                '#f0f9a7',
                '#f0f9a7',
                '',
                this.settings.getShipSubTitleLargeFontSize(),
                this.settings.getShipSubTitleLargeFontFamily(),
                0,
                this.yTextCursor,
                textLayout);

            this.setTitleText(options.subtitle,
                 this.settings.getCardWidthZoomed,
                 this.settings.getShipStructureTypeLineWidth(),
                 '#f0f9a7',
                 '#f0f9a7',
                 '',
                 this.settings.getShipSubTitleLargeFontSize() - 5,
                 'Government Agent BB Ital',
                 0,
                 this.yTextCursor,
                 textLayout);

            this.yTextCursor += 10;
            this.setAttributesText(
                options.accuracy,
                options.damage,
                options.power,
                options.restPower, textLayout);

            // set status effect text if we have one
            if (options.statusEffect !== '') {
                this.wrapText(options.statusEffect,
                    this.settings.getShipTitleLargeSectionWidth(),
                    this.settings.getShipStructureTypeLineWidth(),
                    '#FFF',
                    '#f0f9a7',
                    '',
                    15,
                    'Arial',
                    70,
                    this.yTextCursor + 5,
                    textLayout);
            }
            return textLayout;
        };
        this.setLargeStatsText = function (shieldStatus, damageStatus, armor, structure, textLayout) {
            textLayout.push(
            {
                text: shieldStatus,
                color: this.settings.getShipShieldTypeColor(),
                strokeColor: this.settings.getShipShieldTypeStrokeColor(),
                lineWidth: this.settings.getShipShieldTypeLineWidth(),
                fontHeight: this.settings.getShipShieldTypeLargeFontHeight(),
                font: this.settings.getShipShieldTypeLargeFont(),
                offset: { x: 5, y: -15 }

            },
            {
                text: damageStatus,
                color: this.settings.getShipDamageTypeColor(),
                strokeColor: this.settings.getShipDamageTypeStrokeColor(),
                lineWidth: this.settings.getShipDamageTypeLineWidth(),
                fontHeight: this.settings.getShipDamageTypeLargeFontHeight(),
                font: this.settings.getShipDamageTypeLargeFont(),
                offset: { x: 300, y: -15 }
            },
            {
                text: armor,
                color: this.settings.getShipArmorTypeColor(),
                strokeColor: this.settings.getShipArmorTypeStrokeColor(),
                lineWidth: this.settings.getShipArmorTypeLineWidth(),
                fontHeight: this.settings.getShipArmorTypeLargeFontHeight(),
                font: this.settings.getShipArmorTypeLargeFont(),
                offset: { x: 5, y: -450 }
            },
            {
                text: structure,
                color: this.settings.getShipStructureTypeColor(),
                strokeColor: this.settings.getShipStructureTypeStrokeColor(),
                lineWidth: this.settings.getShipStructureTypeLineWidth(),
                fontHeight: this.settings.getShipStructureTypeLargeFontHeight(),
                font: this.settings.getShipStructureTypeLargeFont(),
                offset: { x: 300, y: -450 }
            });
        };
        this.setAttributesText = function (accuracy, damage, power, restPower, textLayout) {

            if (accuracy > 0) {
                //set accuracy
                textLayout.push(
                {
                    text: 'Accuracy:  ' + accuracy,
                    color: '#FFF',
                    strokeColor: '#FFF',
                    lineWidth: this.settings.getShipShieldTypeLineWidth(),
                    fontHeight: 20,
                    font: 'Government Agent BB',
                    offset: { x: 100, y: this.yTextCursor }
                });
                this.yTextCursor += 20;
            }
            if (damage > 0) {
                //set accuracy
                textLayout.push(
                {
                    text: 'Damage:  ' + damage,
                    color: '#FFF',
                    strokeColor: '#FFF',
                    lineWidth: this.settings.getShipShieldTypeLineWidth(),
                    fontHeight: 20,
                    font: 'Government Agent BB',
                    offset: { x: 100, y: this.yTextCursor }
                });
                this.yTextCursor += 20;
            }
            if (power > 0) {
                //set accuracy
                textLayout.push(
                {
                    text: 'Requires:  ' + power + ' power',
                    color: '#FFF',
                    strokeColor: '#FFF',
                    lineWidth: this.settings.getShipShieldTypeLineWidth(),
                    fontHeight: 20,
                    font: 'Government Agent BB',
                    offset: { x: 100, y: this.yTextCursor }
                });
                this.yTextCursor += 20;
            }
        };
    },
    eventCardLayout: function () {
        this.getSmallLayout = function (shieldStatus, damageStatus, armorValue, structureValue) {
            return [];
        };
        this.getMinLayout = function (shieldStatus, damageStatus, armorValue, structureValue) {
            return [];
        };
        this.getLargeLayout = function (options) {
            var textLayout = [];

            this.setTitleText(options.cardClass,
               this.settings.getCardWidthZoomed(),
               this.settings.getShipStructureTypeLineWidth(),
               '#f0f9a7',
               '#f0f9a7',
               '',
               this.settings.getShipTitleLargeFontSize(),
               this.settings.getShipSubTitleLargeFontFamily(),
               0,
               -200,
               textLayout);

            this.wrapText(options.statusEffect,
                 this.settings.getCardWidthZoomed() - 20,
                 this.settings.getShipStructureTypeLineWidth(),
                 '#FFF',
                 '#f0f9a7',
                 '',
                 20,
                 'Arial',
                 20,
                 -160,
                 textLayout);
            return textLayout;
        };
    },
    orderCardLayout: function () {
        this.getSmallLayout = function (shieldStatus, damageStatus, armorValue, structureValue) {
            return [];
        };
        this.getMinLayout = function (shieldStatus, damageStatus, armorValue, structureValue) {
            return [];
        };
        this.getLargeLayout = function (options) {
            var textLayout = [];

            this.setTitleText(options.cardClass,
               this.settings.getCardWidthZoomed(),
               this.settings.getShipStructureTypeLineWidth(),
               '#f0f9a7',
               '#f0f9a7',
               '',
               this.settings.getShipTitleLargeFontSize(),
               this.settings.getShipSubTitleLargeFontFamily(),
               0,
               -200,
               textLayout);

            this.wrapText(options.statusEffect,
                 this.settings.getCardWidthZoomed() - 20,
                 this.settings.getShipStructureTypeLineWidth(),
                 '#FFF',
                 '#f0f9a7',
                 '',
                 20,
                 'Arial',
                 20,
                 -160,
                 textLayout);
            return textLayout;
        };
    },
    attributeCardLayout: function () {
        this.getSmallLayout = function (shieldStatus, damageStatus, armorValue, structureValue) {
            return [];
        };
        this.getMinLayout = function (shieldStatus, damageStatus, armorValue, structureValue) {
            return [];
        };
        this.getLargeLayout = function (options) {
            var textLayout = [];

            this.setTitleText(options.cardName,
               this.settings.getCardWidthZoomed(),
               this.settings.getShipStructureTypeLineWidth(),
               '#f0f9a7',
               '#f0f9a7',
               '',
               this.settings.getShipTitleLargeFontSize(),
               this.settings.getShipSubTitleLargeFontFamily(),
               0,
               -200,
               textLayout);
            var subTitleWidth = pc.device.game.util.getTextWidth({
                fontSize: this.settings.getShipSubTitleLargeFontSize(),
                text: options.cardClass,
                fontFamily: this.settings.getShipSubTitleLargeFontFamily()
            });
            // set subtitle
            this.yTextCursor -= 10;

            this.setTitleText(options.cardClass,
                this.settings.getCardWidthZoomed(),
                this.settings.getShipStructureTypeLineWidth(),
                '#FFF',
                '#FFF',
                '',
                this.settings.getShipSubTitleLargeFontSize(),
                this.settings.getShipSubTitleLargeFontFamily(),
                0,
                this.yTextCursor,
                textLayout);

            this.yTextCursor += 10;
            this.wrapText(options.statusEffect,
                 this.settings.getCardWidthZoomed() - 20,
                 this.settings.getShipStructureTypeLineWidth(),
                 '#FFF',
                 '#f0f9a7',
                 '',
                 15,
                 'Arial',
                 20,
                 this.yTextCursor,
                 textLayout);
            this.yTextCursor += 50;
            if (pc.valid(options.description)) {
                this.wrapText(options.description,
                this.settings.getCardWidthZoomed() - 20,
                this.settings.getShipStructureTypeLineWidth(),
                '#FFF',
                '#f0f9a7',
                'Italic',
                15,
                'Arial',
                20,
                this.yTextCursor,
                textLayout);
            }
            if (pc.valid(options.leadership) && options.leadership !== 0) {
                this.setTitleText(
                        'Leadership: ' + options.leadership,
                        this.settings.getCardWidthZoomed(),
                        this.settings.getShipStructureTypeLineWidth(),
                        '#f0f9a7',
                        '#f0f9a7',
                        '',
                        18,
                        this.settings.getShipSubTitleLargeFontFamily(),
                        -110,
                        -20,
                        textLayout);                           
            }
            if (pc.valid(options.fleetCommand) && options.fleetCommand !== 0) {
                this.setTitleText(
                        'Fleet Command: ' + options.fleetCommand,
                        this.settings.getCardWidthZoomed(),
                        this.settings.getShipStructureTypeLineWidth(),
                        '#f0f9a7',
                        '#f0f9a7',
                        '',
                        18,
                        this.settings.getShipSubTitleLargeFontFamily(),
                        95,
                        -20,
                        textLayout);
               
            }
            return textLayout;
        };
    },    
    misfortuneCardLayout: function () {
        this.getSmallLayout = function (shieldStatus, damageStatus, armorValue, structureValue) {
            return [];
        };
        this.getMinLayout = function (shieldStatus, damageStatus, armorValue, structureValue) {
            return [];
        };
        this.getLargeLayout = function (options) {
            var textLayout = [];

            this.setTitleText(options.cardClass,
               this.settings.getCardWidthZoomed(),
               this.settings.getShipStructureTypeLineWidth(),
               '#f0f9a7',
               '#f0f9a7',
               '',
               this.settings.getShipTitleLargeFontSize(),
               this.settings.getShipSubTitleLargeFontFamily(),
               0,
               -200,
               textLayout);

            this.wrapText(options.statusEffect,
                 this.settings.getCardWidthZoomed() - 20,
                 this.settings.getShipStructureTypeLineWidth(),
                 '#FFF',
                 '#f0f9a7',
                 '',
                 20,
                 'Arial',
                 20,
                 -160,
                 textLayout);
            return textLayout;
        };
    },
    supportCraftCardLayout: function () {
        this.getSmallLayout = function (shieldStatus, damageStatus, armorValue, structureValue) {

            return [{
                text: shieldStatus,
                color: this.settings.getShipShieldTypeColor(),
                strokeColor: this.settings.getShipShieldTypeStrokeColor(),
                lineWidth: this.settings.getShipShieldTypeLineWidth(),
                fontHeight: this.settings.getShipShieldTypeHeight(),
                font: this.settings.getShipShieldTypeFont(),
                offset: { x: this.settings.getConflictShieldStatusX(), y: this.settings.getConflictShieldStatusY() }
            },
             {
                 text: damageStatus,
                 color: this.settings.getShipDamageTypeColor(),
                 strokeColor: this.settings.getShipDamageTypeStrokeColor(),
                 lineWidth: this.settings.getShipDamageTypeLineWidth(),
                 fontHeight: this.settings.getShipDamageTypeHeight(),
                 font: this.settings.getShipDamageTypeFont(),
                 offset: { x: this.settings.getConflictDamageStatusX(), y: this.settings.getConflictDamageStatusY() }
             },
             {
                 text: armorValue,
                 color: this.settings.getShipArmorTypeColor(),
                 strokeColor: this.settings.getShipArmorTypeStrokeColor(),
                 lineWidth: this.settings.getShipArmorTypeLineWidth(),
                 fontHeight: this.settings.getShipArmorTypeHeight(),
                 font: this.settings.getShipArmorTypeFont(),
                 offset: { x: this.settings.getConflictArmorStatusX(), y: this.settings.getConflictArmorStatusY() }
             },
             {
                 text: structureValue,
                 color: this.settings.getShipStructureTypeColor(),
                 strokeColor: this.settings.getShipStructureTypeStrokeColor(),
                 lineWidth: this.settings.getShipStructureTypeLineWidth(),
                 fontHeight: this.settings.getShipStructureTypeHeight(),
                 font: this.settings.getShipStructureTypeFont(),
                 offset: { x: this.settings.getConflictStructureStatusX(), y: this.settings.getConflictStructureStatusY() }
             }];
        };
        this.getMinLayout = function (shieldStatus, damageStatus, armorValue, structureValue) {

            return [{
                text: shieldStatus,
                color: this.settings.getShipShieldTypeColor(),
                strokeColor: this.settings.getShipShieldTypeStrokeColor(),
                lineWidth: this.settings.getShipShieldTypeLineWidth(),
                fontHeight: this.settings.getShipShieldTypeHeight(),
                font: this.settings.getShipShieldTypeFont(),
                offset: {
                    x: this.constants.get('DECK_CONFLICT_SHIELD_TYPE_MIN_START_X_MOBILE'),
                    y: this.constants.get('DECK_CONFLICT_SHIELD_TYPE_MIN_START_Y_MOBILE')
                }
            },
            {
                text: damageStatus,
                color: this.settings.getShipDamageTypeColor(),
                strokeColor: this.settings.getShipDamageTypeStrokeColor(),
                lineWidth: this.settings.getShipDamageTypeLineWidth(),
                fontHeight: this.settings.getShipDamageTypeHeight(),
                font: this.settings.getShipDamageTypeFont(),
                offset: {
                    x: this.constants.get('DECK_CONFLICT_DAMAGE_TYPE_MIN_START_X_MOBILE'),
                    y: this.constants.get('DECK_CONFLICT_DAMAGE_TYPE_MIN_START_Y_MOBILE')
                }
            },
            {
                text: armorValue,
                color: this.settings.getShipArmorTypeColor(),
                strokeColor: this.settings.getShipArmorTypeStrokeColor(),
                lineWidth: this.settings.getShipArmorTypeLineWidth(),
                fontHeight: this.settings.getShipArmorTypeHeight(),
                font: this.settings.getShipArmorTypeFont(),
                offset: {
                    x: this.constants.get('DECK_CONFLICT_ARMOR_TYPE_MIN_START_X_MOBILE'),
                    y: this.constants.get('DECK_CONFLICT_ARMOR_TYPE_MIN_START_Y_MOBILE')
                }
            },
            {
                text: structureValue,
                color: this.settings.getShipStructureTypeColor(),
                strokeColor: this.settings.getShipStructureTypeStrokeColor(),
                lineWidth: this.settings.getShipStructureTypeLineWidth(),
                fontHeight: this.settings.getShipStructureTypeHeight(),
                font: this.settings.getShipStructureTypeFont(),
                offset: {
                    x: this.constants.get('DECK_CONFLICT_STRUCTURE_TYPE_MIN_START_X_MOBILE'),
                    y: this.constants.get('DECK_CONFLICT_STRUCTURE_TYPE_MIN_START_Y_MOBILE')
                }
            }];
        };
        this.getLargeLayout = function (options) {
            var textLayout = [];
            this.setLargeStatsText(options.shieldStatus, options.damageStatus, options.armor, options.structure, textLayout);

            this.setTitleText(options.cardName,
                this.settings.getCardWidthZoomed(),
                this.settings.getShipStructureTypeLineWidth(),
                '#FFF',
                '#FFF',
                '',
                this.settings.getShipTitleLargeFontSize(),
                this.settings.getShipTitleLargeFontFamily(),
                0,
                -200,
                textLayout);

            var subTitleWidth = pc.device.game.util.getTextWidth({
                fontSize: this.settings.getShipSubTitleLargeFontSize(),
                text: options.cardClass,
                fontFamily: this.settings.getShipSubTitleLargeFontFamily()
            });
            // set subtitle
            this.yTextCursor -= 10;

            this.setTitleText(options.cardClass,
                this.settings.getCardWidthZoomed(),
                this.settings.getShipStructureTypeLineWidth(),
                '#f0f9a7',
                '#f0f9a7',
                '',
                this.settings.getShipSubTitleLargeFontSize(),
                this.settings.getShipSubTitleLargeFontFamily(),
                0,
                this.yTextCursor,
                textLayout);

            this.setTitleText(options.subtitle,
                 this.settings.getCardWidthZoomed(),
                 this.settings.getShipStructureTypeLineWidth(),
                 '#f0f9a7',
                 '#f0f9a7',
                 '',
                 this.settings.getShipSubTitleLargeFontSize() - 5,
                 'Government Agent BB Ital',
                 0,
                 this.yTextCursor,
                 textLayout);

            this.yTextCursor += 10;
            this.setAttributesText(
                options.accuracy,
                options.damage,
                options.power, 
                options.restPower, textLayout);

            // set status effect text if we have one
            if (options.statusEffect !== '') {
                this.wrapText(options.statusEffect,
                    this.settings.getShipTitleLargeSectionWidth(),
                    this.settings.getShipStructureTypeLineWidth(),
                    '#FFF',
                    '#f0f9a7',
                    '',
                    15,
                    'Arial',
                    70,
                    this.yTextCursor + 5,
                    textLayout);
            }
            return textLayout;
        };
        this.setLargeStatsText = function (shieldStatus, damageStatus, armor, structure, textLayout) {
            textLayout.push(
            {
                text: shieldStatus,
                color: this.settings.getShipShieldTypeColor(),
                strokeColor: this.settings.getShipShieldTypeStrokeColor(),
                lineWidth: this.settings.getShipShieldTypeLineWidth(),
                fontHeight: this.settings.getShipShieldTypeLargeFontHeight(),
                font: this.settings.getShipShieldTypeLargeFont(),
                offset: { x: 5, y: -15 }

            },
            {
                text: damageStatus,
                color: this.settings.getShipDamageTypeColor(),
                strokeColor: this.settings.getShipDamageTypeStrokeColor(),
                lineWidth: this.settings.getShipDamageTypeLineWidth(),
                fontHeight: this.settings.getShipDamageTypeLargeFontHeight(),
                font: this.settings.getShipDamageTypeLargeFont(),
                offset: { x: 300, y: -15 }
            },
            {
                text: armor,
                color: this.settings.getShipArmorTypeColor(),
                strokeColor: this.settings.getShipArmorTypeStrokeColor(),
                lineWidth: this.settings.getShipArmorTypeLineWidth(),
                fontHeight: this.settings.getShipArmorTypeLargeFontHeight(),
                font: this.settings.getShipArmorTypeLargeFont(),
                offset: { x: 5, y: -450 }
            },
            {
                text: structure,
                color: this.settings.getShipStructureTypeColor(),
                strokeColor: this.settings.getShipStructureTypeStrokeColor(),
                lineWidth: this.settings.getShipStructureTypeLineWidth(),
                fontHeight: this.settings.getShipStructureTypeLargeFontHeight(),
                font: this.settings.getShipStructureTypeLargeFont(),
                offset: { x: 300, y: -450 }
            });
        };
        this.setAttributesText = function (accuracy, damage, power, restPower, textLayout) {
            
            if (accuracy > 0) {
                //set accuracy
                textLayout.push(
                {
                    text: 'Accuracy:  ' + accuracy,
                    color: '#FFF',
                    strokeColor: '#FFF',
                    lineWidth: this.settings.getShipShieldTypeLineWidth(),
                    fontHeight: 20,
                    font: 'Government Agent BB',
                    offset: { x: 100, y: this.yTextCursor }
                });
                this.yTextCursor += 20;
            }
            if (damage > 0) {
                //set accuracy
                textLayout.push(
                {
                    text: 'Damage:  ' + damage,
                    color: '#FFF',
                    strokeColor: '#FFF',
                    lineWidth: this.settings.getShipShieldTypeLineWidth(),
                    fontHeight: 20,
                    font: 'Government Agent BB',
                    offset: { x: 100, y: this.yTextCursor }
                });
                this.yTextCursor += 20;
            }
            if (power > 0) {
                //set accuracy
                textLayout.push(
                {
                    text: 'Requires:  ' + power + ' power',
                    color: '#FFF',
                    strokeColor: '#FFF',
                    lineWidth: this.settings.getShipShieldTypeLineWidth(),
                    fontHeight: 20,
                    font: 'Government Agent BB',
                    offset: { x: 100, y: this.yTextCursor }
                });
                this.yTextCursor += 20;
            }            
        };
    },
   
    // global functions for all zone types
    setTitleText: function (text, maxWidth, lineWidth, color, strokeColor, fontStyle, fontHeight, font, startX, y, layout) {
        this.yTextCursor = y;
        var words = text.split(' '),
            line = '',
            lineCount = 0,
            i,
            test,
            metrics,
            yPos,
            spacer = 30;
        //check if first word would fit if it is smaller
        if (words.length === 1) {
            metrics = pc.device.game.util.getTextWidth({
                fontSize: fontHeight,
                text: words[0],
                fontFamily: font
            });
            if (metrics > maxWidth) {
                metrics = pc.device.game.util.getTextWidth({
                    fontSize: fontHeight - 7,
                    text: words[0],
                    fontFamily: font
                });
                var x = (startX + (maxWidth / 2)) - (metrics / 2);
                layout.push(
                    {
                        text: words[0],
                        color: color,
                        strokeColor: strokeColor,
                        lineWidth: lineWidth,
                        fontStyle: fontStyle,
                        fontHeight: fontHeight - 7,
                        font: font,
                        offset: { x: x, y: y }
                    }
                );
                this.yTextCursor += fontHeight;
                return;
            }
            else {
                // title is one word and will fit with the font we passed in.
                var x = (startX + (maxWidth / 2)) - (metrics / 2);
                layout.push(
                    {
                        text: words[0],
                        color: color,
                        strokeColor: strokeColor,
                        lineWidth: lineWidth,
                        fontStyle: fontStyle,
                        fontHeight: fontHeight,
                        font: font,
                        offset: { x: x, y: y }
                    }
                );
                this.yTextCursor += fontHeight;
                return;
            }
        }
        else if (words.length > 1) {
            //title is two words
            var text = words[0] + ' ' + words[1];
            if (words.length === 3) {
                text += ' ' + words[2];
            }
            // check for the first two words
            metrics = pc.device.game.util.getTextWidth({
                fontSize: fontHeight,
                text: text,
                fontFamily: font
            });
            // first two words will not fit on one line
            if (metrics > maxWidth) {
                // try a smaller font of only the first two words
                text = words[0] + ' ' + words[1];
                metrics = pc.device.game.util.getTextWidth({
                    fontSize: fontHeight - 10,
                    text: text,
                    fontFamily: font
                });
                var x = (startX + (maxWidth / 2)) - (metrics / 2);
                layout.push(
                    {
                        text: text,
                        color: color,
                        strokeColor: strokeColor,
                        lineWidth: lineWidth,
                        fontStyle: fontStyle,
                        fontHeight: fontHeight - 10,
                        font: font,
                        offset: { x: x, y: y }
                    }
                );
                this.yTextCursor += fontHeight;
            }
            else {
                // title is two words and fits without resizing the font
                var x = (startX + (maxWidth / 2)) - (metrics / 2);
                layout.push(
                    {
                        text: text,
                        color: color,
                        strokeColor: strokeColor,
                        lineWidth: lineWidth,
                        fontStyle: fontStyle,
                        fontHeight: fontHeight,
                        font: font,
                        offset: { x: x, y: y }
                    }
                );
                this.yTextCursor += fontHeight;
            }           
        }
    },
    wrapText: function (text, maxWidth, lineWidth, color, strokeColor, fontStyle, fontHeight, font, x, y, layout) {


        var words = text.split(' '),
            line = '',
            lineCount = 0,
            i,
            test,
            metrics,
            spacer = 0;

        for (i = 0; i < words.length; i++) {
            test = words[i];
            metrics = pc.device.game.util.getTextWidth({
                fontSize: fontHeight,
                text: test,
                fontFamily: font
            });
            //metrics = context.measureText(test);
            while (metrics > maxWidth) {
                // Determine how much of the word will fit
                test = test.substring(0, test.length - 1);
                metrics = pc.device.game.util.getTextWidth({
                    fontSize: fontHeight,
                    text: test,
                    fontFamily: font
                });
            }

            if (words[i] != test) {
                words.splice(i + 1, 0, words[i].substr(test.length))
                words[i] = test;
            }

            test = line + words[i] + ' ';
            metrics = pc.device.game.util.getTextWidth({
                fontSize: fontHeight,
                text: test,
                fontFamily: font
            });

            if (metrics > maxWidth && i > 0) {
                //context.fillText(line, x, y);
                //  var x = (startX + (maxWidth / 2)) - (metrics / 2);
                layout.push(
                             {
                                 text: line,
                                 color: color,
                                 strokeColor: strokeColor,
                                 lineWidth: lineWidth,
                                 fontStyle: fontStyle,
                                 fontHeight: fontHeight,
                                 font: font,
                                 offset: { x: x, y: y + spacer }
                             }
                       );
                line = words[i] + ' ';
                //y += lineWidth;
                spacer += 20;
                lineCount++;
            }
            else {
                line = test;
            }
        }

        //context.fillText(line, x, y);
        //var x = (startX + (maxWidth / 2)) - (metrics / 2);
        layout.push(
                     {
                         text: line,
                         color: color,
                         strokeColor: strokeColor,
                         lineWidth: lineWidth,
                         fontStyle: fontStyle,
                         fontHeight: fontHeight,
                         font: font,
                         offset: { x: x, y: y + spacer }
                     }
               );
    },
    //getTextWidth: function (options) {
    //    if (!pc.valid(options.fontSize))
    //        throw 'No Font Size Specified.';
    //    if (!pc.valid(options.text))
    //        throw 'No Text Specified.';
    //    if (!pc.valid(options.fontFamily))
    //        throw 'No Font Family Specified.';
    //    pc.device.ctx.font = options.fontSize + 'px ' + options.fontFamily;
    //    return pc.device.ctx.measureText(options.text).width;
    //}
    
});