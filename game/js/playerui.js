﻿/**
 * Project Neptune - (C)2015 Eddie Games Inc.
 * Author: Marc D. Arbesman
 */

/**
 *  Class: PlayerUI
 *  Extends: pc.Pooled
 *  Description: draws all the player and opponents imagery and stats.  
 *  ensures the updates to the display of the score are handled only in here.
 *  Currently pooling this object as I think it would be give a performance increase
 *  Many of the methods and members are not used and currently only stubbed out
 */

pn.ui.PlayerUI = pc.Base.extend('PlayerUI',
    {},
    {
        //private members
        UILayer: null,
        opponentUIEntity: null,
        playerUIEntity: null,        
        opponentCrewStat: null,
        opponentPowerStat: null,
        opponentLeadershipStat: null,
        playerCrewStat: null,
        playerPowerStat: null,
        playerLeadershipStat: null,
        constants: null,
        settings: null,        
        player: null,
        playerCrewEntity: null,
        playerPowerEntity: null,
        playerLeadershipEntity: null,
        playerHandIconEntity: null,
        opponent: null,
        opponentCrewEntity: null,
        opponentPowerEntity: null,
        opponentLeadershipEntity: null,
        portraitWidth: null,
        portraitHeight: null,
        crewIconWidth: null,
        crewIconHeight: null,
        powerIconWidth: null,
        powerIconHeight: null,
        leadIconWidth: null,
        leadIconHeight: null,
        //methods
        //Initialzie data from server data source for both player and opponent
        init: function (scene) {
            if (!pc.valid(scene), false) {
                throw "Not a valid scene"
            }           
            
            this.config(scene);
            this.drawPlayerPanels();
        },
        //get player data
        config: function (scene) {
            this.constants = pc.device.game.__constants;
            this.settings = pc.device.game.gameDeviceSettings;
            this.scene = scene;            
            this.UILayer = new pc.EntityLayer('UI layer', 10000, 2);
            this.UILayer.zIndex = 7;
            this.scene.addLayer(this.UILayer);
            
            this.userPlayer = pc.device.game.getUserPlayer();
            this.opponent = pc.device.game.players[1];
            this.userPlayerCrewStat = pc.device.game.getCounterByName(this.userPlayer,'Crew').Start;
            this.userPlayerLeadershipStat = pc.device.game.getCounterByName(this.userPlayer, 'Leadership').Start;
            this.userPlayerPowerStat = pc.device.game.getCounterByName(this.userPlayer, 'Power').Start;
            this.opponentCrewStat = pc.device.game.getCounterByName(this.opponent, 'Crew').Start;
            this.opponentLeadershipStat = pc.device.game.getCounterByName(this.opponent, 'Leadership').Start;
            this.opponentPowerStat = pc.device.game.getCounterByName(this.opponent, 'Power').Start;
            this.portraitHeight = this.settings.getUIPlayerPortraitHeight();
            this.portraitWidth = this.settings.getUIPlayerPortraitWidth();
        },
        drawPlayerPanels: function () {            
            this.UILayer.addSystem(new pc.systems.Render());
            this.UILayer.addSystem(new pc.systems.Effects());
            this.UILayer.addSystem(new pc.systems.Layout());

            // draw the opponent's panel to hold the key stats of game play

            // draw player hand button
            this.playerHandIconEntity = pc.Entity.create(this.UILayer);
            var handImage = pc.device.loader.get('showHandIcon').resource,
                spriteSheet = new pc.SpriteSheet({
                    image: handImage,
                    frameWidth: this.settings.getUIHandIconWidth(),
                    frameHeight: this.settings.getUIHandIconHeight()
                }),
                sprite = new pc.Sprite(spriteSheet);
            this.playerHandIconEntity.addComponent(pc.components.Spatial.create({
                x: this.settings.getUIHandIconX(),
                y: this.settings.getUIHandIconY(),
                w: this.settings.getUIHandIconWidth() +10,
                h: this.settings.getUIHandIconHeight()+10
            }));
            this.playerHandIconEntity.addComponent(pc.components.Sprite.create(sprite));
            this.playerHandIconEntity.addTag('nozoom');
            // opponent portrait
            this.opponentUIEntity = pc.Entity.create(this.UILayer);
            //load portrait image based off of data so we cannot do this in the initial load on game init
            var portraitImage = new pc.Image('player_' + this.opponent.ID,
                pc.device.game.__constants.get('GAME_IMAGES_CDN_DOMAIN') +
                pc.device.game.__constants.get('IMAGE_CDN_URL') +
                pc.device.game.gameDeviceSettings.getUIImageURL() + this.opponent.imageName);
            portraitImage.load(this.renderOppponentUI, this.onError);
            // draw the player's panel to hold the key stats of game play
            this.userPlayerUIEntity = pc.Entity.create(this.UILayer);

            //var portraitImage = pc.device.loader.get('player_' + this.userPlayer.ID).resource,
            var portraitImage = new pc.Image('player_' + this.opponent.ID,
                pc.device.game.__constants.get('GAME_IMAGES_CDN_DOMAIN') +
                pc.device.game.__constants.get('IMAGE_CDN_URL') +
                pc.device.game.gameDeviceSettings.getUIImageURL() + this.opponent.imageName);
            portraitImage.load(this.renderPlayerUI, this.onError);
        },
        renderOpponentUI: function () {

           var  spriteSheet = new pc.SpriteSheet({
                image: portraitImage.image,
                frameWidth: this.portraitWidth,
                frameHeight: this.portraitHeight
            }),
            sprite = new pc.Sprite(spriteSheet);
            this.opponentUIEntity.addComponent(pc.components.Spatial.create({
                x: this.settings.getBaseDeviceWidth() - this.portraitWidth,
                y: 0,
                w: this.portraitWidth,
                h: this.portraitHeight
            }));
            this.opponentUIEntity.addComponent(pc.components.Sprite.create(sprite));

            // opponent leadership
            this.opponentLeadershipEntity = pc.Entity.create(this.UILayer);
            var image = pc.device.loader.get('leadershipIcon').resource,
                spriteSheet = new pc.SpriteSheet({
                    image: image,
                    frameWidth: this.settings.getUILeadIconWidth(),
                    frameHeight: this.settings.getUILeadIconHeight()
                }),
            sprite = new pc.Sprite(spriteSheet);
            this.opponentLeadershipEntity.addComponent(pc.components.Spatial.create({
                x: this.settings.getUILeadIconOpponentX(),
                y: this.settings.getUILeadIconOpponentY(),
                w: this.settings.getUILeadIconWidth(),
                h: this.settings.getUILeadIconHeight()
            }));
            this.opponentLeadershipEntity.addComponent(pc.components.Sprite.create(sprite));
            this.setText(
                    this.opponentLeadershipEntity,
                    this.opponentLeadershipStat,
                    this.settings.getUILeadCountLineWidth(),
                    this.settings.getUILeadCountFillColor(),
                    this.settings.getUILeadCountStrokeColor(),
                    this.settings.getUILeadCountFontSize(),
                    this.settings.getUILeadCountFont(),
                    {
                        x: this.settings.getUILeadCountOpponentX(),
                        y: this.settings.getUILeadCountOpponentY()
                    });

            // opponent power
            this.opponentPowerEntity = pc.Entity.create(this.UILayer);
            var image = pc.device.loader.get('powerIcon').resource,
                spriteSheet = new pc.SpriteSheet({
                    image: image,
                    frameWidth: this.settings.getUIPowerIconWidth(),
                    frameHeight: this.settings.getUIPowerIconHeight(),
                }),
            sprite = new pc.Sprite(spriteSheet);
            this.opponentPowerEntity.addComponent(pc.components.Spatial.create({
                x: this.settings.getUIPowerIconOpponentX(),
                y: this.settings.getUIPowerIconOpponentY(),
                w: this.settings.getUIPowerIconWidth(),
                h: this.settings.getUIPowerIconHeight()
            }));

            this.opponentPowerEntity.addComponent(pc.components.Sprite.create(sprite));
            this.setText(
                    this.opponentPowerEntity,
                    this.opponentPowerStat,
                    this.settings.getUIPowerCountLineWidth(),
                    this.settings.getUIPowerCountFillColor(),
                    this.settings.getUIPowerCountStrokeColor(),
                    this.settings.getUIPowerCountFontSize(),
                    this.settings.getUIPowerCountFont(),
                    {
                        x: this.settings.getUIPowerCountOpponentX(),
                        y: this.settings.getUIPowerCountOpponentY()
                    });


            //opponent crew
            this.opponentCrewEntity = pc.Entity.create(this.UILayer);
            var image = pc.device.loader.get('crewIcon').resource,
                spriteSheet = new pc.SpriteSheet({
                    image: image,
                    frameWidth: this.settings.getUICrewIconWidth(),
                    frameHeight: this.settings.getUICrewIconHeight()
                }),
            sprite = new pc.Sprite(spriteSheet);
            this.opponentCrewEntity.addComponent(pc.components.Spatial.create({
                x: this.settings.getUICrewIconOpponentX(),
                y: this.settings.getUICrewIconOpponentY(),
                w: this.settings.getUICrewIconWidth(),
                h: this.settings.getUICrewIconHeight()
            }));

            this.opponentCrewEntity.addComponent(pc.components.Sprite.create(sprite));
            this.setText(
                    this.opponentCrewEntity,
                    this.opponentCrewStat,
                    this.settings.getUICrewCountLineWidth(),
                    this.settings.getUICrewCountFillColor(),
                    this.settings.getUICrewCountStrokeColor(),
                    this.settings.getUICrewCountFontSize(),
                    this.settings.getUICrewCountFont(),
                    {
                        x: this.settings.getUICrewCountOpponentX(),
                        y: this.settings.getUICrewCountOpponentY()
                    });
            // draw the player's panel to hold the key stats of game play

        },
        renderPlayerUI: function(){

            var   spriteSheet = new pc.SpriteSheet({
                image: this.image,
                frameWidth: this.width,
                frameHeight: this.height
            }),
                sprite = new pc.Sprite(spriteSheet);
            this.userPlayerUIEntity.addComponent(pc.components.Spatial.create({
                x: 0,
                y: pc.device.game.gameDeviceSettings.getBaseDeviceHeight() - this.height,
                w: this.width,
                h: this.height
            }));
            this.userPlayerUIEntity.addComponent(pc.components.Sprite.create(sprite));

            // player leadership
            this.userPlayerLeadershipEntity = pc.Entity.create(this.UILayer);
            var image = pc.device.loader.get('leadershipIcon').resource,
                spriteSheet = new pc.SpriteSheet({
                    image: image,
                    frameWidth: this.settings.getUILeadIconWidth(),
                    frameHeight: this.settings.getUILeadIconHeight()
                }),
            sprite = new pc.Sprite(spriteSheet);
            this.userPlayerLeadershipEntity.addComponent(pc.components.Spatial.create({
                x: this.settings.getUILeadIconPlayerX(),
                y: this.settings.getUILeadIconPlayerY(),
                w: this.settings.getUILeadIconWidth(),
                h: this.settings.getUILeadIconHeight()
            }));
            
            this.userPlayerLeadershipEntity.addComponent(pc.components.Sprite.create(sprite));
            this.setText(
                    this.userPlayerLeadershipEntity,
                    this.userPlayerLeadershipStat,
                    this.settings.getUILeadCountLineWidth(),
                    this.settings.getUILeadCountFillColor(),
                    this.settings.getUILeadCountStrokeColor(),
                    this.settings.getUILeadCountFontSize(),
                    this.settings.getUILeadCountFont(),
                    {
                        x: this.settings.getUILeadCountPlayerX(),
                        y: this.settings.getUILeadCountPlayerY()

                    });
            // player power
            this.userPlayerPowerEntity = pc.Entity.create(this.UILayer);
            var image = pc.device.loader.get('powerIcon').resource,
                spriteSheet = new pc.SpriteSheet({
                    image: image,
                    frameWidth: this.settings.getUIPowerIconWidth(),
                    frameHeight: this.settings.getUIPowerIconHeight()
                }),
            sprite = new pc.Sprite(spriteSheet);
            this.userPlayerPowerEntity.addComponent(pc.components.Spatial.create({
                x: this.settings.getUIPowerIconPlayerX(),
                y: this.settings.getUIPowerIconPlayerY(),
                w: this.settings.getUIPowerIconWidth(),
                h: this.settings.getUIPowerIconHeight()
            }));
            
            this.userPlayerPowerEntity.addComponent(pc.components.Sprite.create(sprite));
            this.setText(
                    this.userPlayerPowerEntity,
                    this.userPlayerPowerStat,
                    this.settings.getUIPowerCountLineWidth(),
                    this.settings.getUIPowerCountFillColor(),
                    this.settings.getUIPowerCountStrokeColor(),
                    this.settings.getUIPowerCountFontSize(),
                    this.settings.getUIPowerCountFont(),
                    {
                        x: this.settings.getUIPowerCountPlayerX(),
                        y: this.settings.getUIPowerCountPlayerY()
                    });
            
            //player crew
            this.userPlayerCrewEntity = pc.Entity.create(this.UILayer);
            var image = pc.device.loader.get('crewIcon').resource,
                spriteSheet = new pc.SpriteSheet({
                    image: image,
                    frameWidth: this.settings.getUICrewIconWidth(),
                    frameHeight: this.settings.getUICrewIconHeight()
                }),
            sprite = new pc.Sprite(spriteSheet);
            this.userPlayerCrewEntity.addComponent(pc.components.Spatial.create({
                x: this.settings.getUICrewIconPlayerX(),
                y: this.settings.getUICrewIconPlayerY(),
                w: this.settings.getUICrewIconWidth(),
                h: this.settings.getUICrewIconHeight()
            }));
            this.userPlayerCrewEntity.addComponent(pc.components.Sprite.create(sprite));
            this.setText(
                    this.userPlayerCrewEntity,
                    this.userPlayerCrewStat,
                    this.settings.getUICrewCountLineWidth(),
                    this.settings.getUICrewCountFillColor(),
                    this.settings.getUICrewCountStrokeColor(),
                    this.settings.getUICrewCountFontSize(),
                    this.settings.getUICrewCountFont(),
                    {
                        x: this.settings.getUICrewCountPlayerX(),
                        y: this.settings.getUICrewCountPlayerY()
                    });
        },
        onError: function(error){
            console.log(error);
        },
        updateOpponentCrew: function (val) {
            if (val === null || val === undefined) {
                val = 0;
            }
            this.opponentCrewStat += val;
            this.setText(
                    this.opponentCrewEntity,
                    this.opponentCrewStat,
                    this.settings.getUICrewCountLineWidth(),
                    this.settings.getUICrewCountFillColor(),
                    this.settings.getUICrewCountStrokeColor(),
                    this.settings.getUICrewCountFontSize(),
                    this.settings.getUICrewCountFont(),
                    {
                        x: this.settings.getUICrewCountOpponentX(),
                        y: this.settings.getUICrewCountOpponentY()
                    });

        },
        updateOpponentLeadership: function (val) {
            if (val === null || val === undefined) {
                val = 0;
            }
            this.opponentLeadershipStat = this.opponentLeadershipStat + val;
            this.setText(
                    this.opponentLeadershipEntity,
                    this.opponentLeadershipStat,
                    this.settings.getUILeadCountLineWidth(),
                    this.settings.getUILeadCountFillColor(),
                    this.settings.getUILeadCountStrokeColor(),
                    this.settings.getUILeadCountFontSize(),
                    this.settings.getUILeadCountFont(),
                    {
                        x: this.settings.getUILeadCountOpponentX(),
                        y: this.settings.getUILeadCountOpponentY()
                    });
        },
        updateOpponentPower: function (val) {
            if (val === null || val === undefined) {
                val = 0;
            }
            this.opponentPowerStat += val;
            this.setText(
                    this.opponentPowerEntity,
                    this.opponentPowerStat,
                    this.settings.getUICrewCountLineWidth(),
                    this.settings.getUIPowerCountFillColor(),
                    this.settings.getUIPowerCountStrokeColor(),
                    this.settings.getUIPowerCountFontSize(),
                    this.settings.getUIPowerCountFont(),
                    {
                        x: this.settings.getUIPowerCountOpponentX(),
                        y: this.settings.getUIPowerCountOpponentY()
                    });
        },
        updatePlayerCrew: function (val) {
            if (val === null || val === undefined) {
                val = 0;
            }
            this.userPlayerCrewStat += val;
            this.setText(
                this.userPlayerCrewEntity,
                this.userPlayerCrewStat,
                this.settings.getUICrewCountLineWidth(),
                this.settings.getUICrewCountFillColor(),
                this.settings.getUICrewCountStrokeColor(),
                this.settings.getUICrewCountFontSize(),
                this.settings.getUICrewCountFont(),
                {
                    x: this.settings.getUICrewCountPlayerX(),
                    y: this.settings.getUICrewCountPlayerY()
                });
        },
        updatePlayerLeadership: function (val) {
            if (val === null || val === undefined) {
                val = 0;
            }
            this.userPlayerLeadershipStat += val;
            this.setText(
                    this.userPlayerLeadershipEntity,
                    this.userPlayerLeadershipStat,
                    this.settings.getUILeadCountLineWidth(),
                    this.settings.getUILeadCountFillColor(),
                    this.settings.getUILeadCountStrokeColor(),
                    this.settings.getUILeadCountFontSize(),
                    this.settings.getUILeadCountFont(),
                    {
                        x: this.settings.getUILeadCountPlayerX(),
                        y: this.settings.getUILeadCountPlayerY()
                    });
        },
        updatePlayerPower: function (val) {
            if (val === null || val === undefined) {
                val = 0;
            }
            this.userPlayerPowerStat += val;
            this.setText(
                    this.userPlayerPowerEntity,
                    this.userPlayerPowerStat,
                    this.settings.getUIPowerCountLineWidth(),
                    this.settings.getUIPowerCountFillColor(),
                    this.settings.getUIPowerCountStrokeColor(),
                    this.settings.getUIPowerCountFontSize(),
                    this.settings.getUIPowerCountFont(),
                    {
                        x: this.settings.getUIPowerCountPlayerX(),
                        y: this.settings.getUIPowerCountPlayerY()
                    });
        },
        setText: function (entity, text, lineWidth, color, strokeColor, fontHeight, font, offset) {
            if (entity.hasComponentOfType('text')) {
                entity.removeComponentByType('text');
            }
            entity.addComponent(pc.components.Text.create({
                text: [text],
                lineWidth: lineWidth,
                color: color,
                strokeColor: strokeColor, 
                fontHeight: fontHeight,
                font: font,
                offset: offset,

            }));
         }
    });