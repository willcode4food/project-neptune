﻿pn.Zone = pc.Base.extend('Zone',
{

},
{
    xCardPos: null,
    yCardPos: null,
    startPosX: null,
    startPosY: null,
    cardWidth: null,
    cardWidthZoomed: null,
    cardHeight: null,
    cardHeightZoomed: null,
    cardConstants: null,
    cardsJSON: null,
    orientation: null,
    cards: null,
    boardTiles: null,
    settings: null,
    size: null,
    player: null,
    zone: null,
    init: function (player, zone) {
        // load standard cardects 
        this.config(player, zone);
    },
    config: function (player, zone) {
        this.cardConstants = pc.device.game.__constants;
        this.settings = pc.device.game.gameDeviceSettings;
        this.zone = zone;
        this.player = player;
        this.cards = [];
        this.numCards = this.zone.Cards.length;

    },
    load: function (boardTiles, layer, startX, startY, orientation) {
        /// <summary>Loads the deck information from local storage.  Creates the entities and applies their components</summary>
        /// <param name="startX" type="cardect">The deck's X position pre-dealing (if cards need to be dealt)</param>
        /// <param name="startY" type="cardect">The deck's Y position pre-dealing (if cards need to be dealt)</param>
        /// <param name="orientation" type="cardect">Cards can be landscape or portrait 1 for landscape and 0 for portrait</param>
        /// <param name="numCards" type="cardect"></param>
        this.startPosX = startX;
        this.startPosY = startY;
        this.orientation = orientation;
        this.boardTiles = boardTiles;

        // utilize the custom board tiles cardect.  This is a custom collection of tile map tiles that are in use as part of the game board
        // This collection is the definative list of spaces on the game board
        if (!pc.valid(boardTiles, false)) {
            throw 'No Board was defined';
        }

        //We are assigning cards to tiles
        //this.boardTiles = this.scene.board.boardShipMap.boardTiles;
        // Construct the array that will store all the card entities
        // Consider making this a linked list for efficiency


        // Determine the orientation of the deck and set widths and heights appropriately.
        // Some decks are horizontal like compartments, and some are vertical like locker decks
        if (this.orientation === this.cardConstants.get('DECK_ORIENT_LANDSCAPE')) {
            this.cardWidth = this.settings.getCardHeight();
            this.cardHeight = this.settings.getCardWidth();
            this.cardWidthZoomed = this.settings.getCardHeightZoomed();
            this.cardHeightZoomed = this.settings.getCardWidthZoomed();
        }
        else {
            // default to portrait
            this.cardHeight = this.settings.getCardHeight();
            this.cardWidth = this.settings.getCardWidth();
            this.cardHeightZoomed = this.settings.getCardHeightZoomed();
            this.cardWidthZoomed = this.settings.getCardWidthZoomed();
        }


        layer.addSystem(new pc.systems.Effects(this));
        layer.addSystem(new pc.systems.Render());
        layer.addSystem(new pc.systems.CardControls(this));
        layer.addSystem(new pc.systems.Mover());

        for (var i = 0; i < this.numCards; i++) {
            //this zone should only have a certain number of cards on it.
            var card = this.zone.Cards[i];
            if (!pc.valid(this.zone.Cards[i].SmallImg, false)) {
                throw 'Small image data does not exist in JSON';
            }
            if (!pc.valid(this.zone.Cards[i].LargeImg, false)) {
                throw 'Large image data does not eexist in JSON';
            }
         

            var cardImage = pc.device.game.util.getImageIdentifier(card.SmallImg.toString(), card.ID.toString(), this.player.ID),
                cardImageZoomed = pc.device.game.util.getImageIdentifier(card.LargeImg.toString(), card.ID.toString(), this.player.ID),
                cardImageMin = pc.device.game.util.getImageIdentifier(card.MinImg.toString(), card.ID.toString(), this.player.ID);




            e = pc.Entity.create(layer);


            e.addComponent(CardData.create(
                {
                    ID: this.zone.Cards[i].ID,
                    cardClass: this.zone.Cards[i].CardClass,
                    name: this.zone.Cards[i].Name,
                    subtitle: this.zone.Cards[i].Subtitle,
                    cardType: this.zone.Cards[i].CardType,
                    statusEffect: this.zone.Cards[i].StatusEffects,
                    description: pc.checked(this.zone.Cards[i].Description, ''),
                    leadership: pc.checked(this.zone.Cards[i].Leadership, 0),
                    fleetCommand: pc.checked(this.zone.Cards[i].FleetCommand, 0),
                    armor: this.zone.Cards[i].Armor,
                    structure: this.zone.Cards[i].Structure,
                    power: this.zone.Cards[i].Power,
                    accuracy: this.zone.Cards[i].Accuracy,
                    damage: this.zone.Cards[i].Damage,
                    rangeMin: this.zone.Cards[i].RangeMin,
                    rangeMax: this.zone.Cards[i].RangeMax,
                    shield: this.zone.Cards[i].Shield,
                    restPower: this.zone.Cards[i].Rest,
                    special: this.zone.Cards[i].Special,
                    position: this.zone.Cards[i].Position,
                    shieldStatus: 0,
                    damageStatus: 0,
                    crewCost: this.zone.Cards[i].CrewCost,
                    speed: this.zone.Cards[i].Speed,
                    image: cardImage,
                    imageZoomed: cardImageZoomed,
                    imageMin: cardImageMin,
                    pos: {
                        x: 0,
                        y: 0
                    },
                    cardWidth: this.cardWidth,
                    cardHeight: this.cardHeight,
                    cardWidthZoomed: this.cardWidthZoomed,
                    cardHeightZoomed: this.cardHeightZoomed,
                    boardTilePosition: this.zone.Cards[i].Position,
                    cardPosXZoomed: (pc.device.game.centerPosX - (this.cardWidthZoomed / 2)) - 7,
                    cardPosYZoomed: pc.device.game.centerPosY - (this.cardHeightZoomed / 2)
                }));

            if ((pc.device.isiPhone || pc.device.isiPhone4 || pc.device.isiPad || pc.device.isAndroid) && e.getComponent('carddata').imageMin !== '') {
                imageID = e.getComponent('carddata').imageMin;
            }
            else {
                imageID = e.getComponent('carddata').image;
            }

            e.addComponent(pc.components.Spatial.create(
                {
                    x: this.startPosX,
                    y: this.startPosY,
                    w: pc.device.loader.get(imageID).resource.width,
                    h: pc.device.loader.get(imageID).resource.height,
                    originX: this.settings.getShipOriginXPos(),
                    originY: this.settings.getShipOriginYPos()

                }));

            e.addComponent(pc.components.Input.create(
                {
                    // input state used by drag and drop
                    states: [
                         [this.cardConstants.get('DD_STATE_MOUSE_CLICK'), ['MOUSE_BUTTON_LEFT_DOWN'], false],
                         [this.cardConstants.get('DD_STATE_MOUSE_MOVE'), ['MOUSE_MOVE']],
                         [this.cardConstants.get('DD_STATE_MOUSE_OUT'), ['MOUSE_OUT']],
                         [this.cardConstants.get('DD_STATE_MOUSE_OVER'), ['MOUSE_OVER']],
                         [this.cardConstants.get('DD_STATE_TOUCH'), ['TOUCH']]
                    ],
                    actions: [
                            [this.cardConstants.get('DD_ACTION_MOUSE_LEFT_BUTTON_UP'), ['MOUSE_BUTTON_LEFT_UP']],
                            [this.cardConstants.get('DD_ACTION_MOUSE_LEFT_BUTTON_DOWN'), ['MOUSE_BUTTON_LEFT_DOWN']],
                            [this.cardConstants.get('DD_ACTION_MOUSE_LEFT_BUTTON_DBLCLICK'), ['MOUSE_BUTTON_LEFT_DBLCICK']],
                            [this.cardConstants.get('DD_ACTION_MOUSE_MOVE'), ['MOUSE_MOVE'], true],
                            [this.cardConstants.get('DD_ACTION_MOUSE_OUT'), ['MOUSE_OUT'], true],
                            [this.cardConstants.get('DD_ACTION_MOUSE_OVER'), ['MOUSE_OVER'], true],
                            [this.cardConstants.get('DD_ACTION_MOUSE_WHEEL_UP'), ['MOUSE_WHEEL_UP'], true],
                            [this.cardConstants.get('DD_ACTION_MOUSE_WHEEL_DOWN'), ['MOUSE_WHEEL_DOWN'], true],
                            [this.cardConstants.get('DD_ACTION_TOUCH'), ['TOUCH']],
                            [this.cardConstants.get('DD_ACTION_TOUCH_END'), ['TOUCH_END']],
                            [this.cardConstants.get('DD_ACTION_TOUCH_MOVE'), ['TOUCH_MOVE'], true]

                    ]
                }));
            e.addComponent(pc.components.Physics.create(
                {
                    collisionCategory: CollisionType.BOARD,
                    collisionMask: CollisionType.TILEOVER,
                    collisionGroup: 1,
                    sensorOnly: true,

                }));            
                    
            this.cards.push(e);
            delete layout;
        }
    },
    loadCardSprites: function () {
        for (i = 0; i < this.cards.length; i++) {
            var imageID = '',
                carddata = this.cards[i].getComponent('carddata');

            // only switch out a min sized image if we have one and in our case on for this game, we are on mobile
            if ((pc.device.isiPhone || pc.device.isiPhone4 || pc.device.isiPad || pc.device.isAndroid) && this.cards[i].getComponent('carddata').imageMin !== '') {
                imageID = carddata.imageMin;
            }
            else {
                imageID = carddata.image;
            }

            spriteSheet = new pc.SpriteSheet({
                image: pc.device.loader.get(imageID).resource,
                frameWidth: pc.device.loader.get(imageID).resource.width,
                frameHeight: pc.device.loader.get(imageID).resource.height              
            }),
            sprite = new pc.Sprite(spriteSheet);
            if (!this.cards[i].hasComponentOfType('sprite')) {
                this.cards[i].addComponent(pc.components.Sprite.create(sprite));
            }


        }
    },
    dealCards: function () {

    }
});