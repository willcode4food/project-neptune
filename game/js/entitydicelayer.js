﻿EntityDiceLayer = pc.EntityLayer.extend('EntityDiceLayer',
{},
{
    onAnimationComplete: null,
    hasEventFired: null,
    init: function (options) {
        var entityFactory = null, onAnimationEndCallback = null;
        if (!pc.valid(options.name))
        {
            throw ('Layer name not specified');
        }
        if (!pc.valid(options.worldSizeX)) {
            throw ('World X Size not specified');
        }
        if (!pc.valid(options.worldSizeY)) {
            throw ('World Y Size not specified');
        }
        if (pc.checked(options.entityFactory)) {
            entityFactory = options.entityFactory;
        }
        if (pc.checked(options.onAnimationEndCallback)) {
            onAnimationEndCallback = options.onAnimationEndCallback;
        }
        
        this._super(options.name, options.worldSizeX, options.worldSizeY, entityFactory);
        this.onAnimationEndCallback = onAnimationEndCallback;
        this.hasEventFired = false;
    },
    process: function () {
        this._super();

        // Logic to detect when the sprite first animation has finished;
        if (this.onAnimationEndCallback !== null) {
            var node = this.entityManager.entities.first;
            while (node) {
                if (node.obj.hasComponentOfType('alpha')) {
                    if (node.obj.getComponent('alpha').level === 0) {
                        node.obj.remove();
                    }
                }
                if (node.obj.hasComponentOfType('sprite')) {
                    if (node.obj.hasComponentOfType('dicedata')) {
                        if (!node.obj.getComponent('dicedata').isRollComplete) {
                            var numLoops = node.obj.getComponent('sprite').sprite.spriteSheet.animations.entries()[0][1].loops,
                                numFrames = node.obj.getComponent('sprite').sprite.spriteSheet.animations.entries()[0][1].frames.length,
                                currentFrame = node.obj.getComponent('sprite').sprite.currentFrame, totalFrames = (numLoops * numFrames);

                            if (totalFrames > 1) {
                                if (currentFrame >= (totalFrames - 1)) {
                                    // todo figure out how to fire only once.  


                                    this.onAnimationEndCallback(node.obj);
                                    node.obj.getComponent('dicedata').isRollComplete = true;

                                }
                            }
                        }
                    }                    
                }
                node = node.next();
            }
        }
    }

});