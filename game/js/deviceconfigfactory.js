/**
 *  DeviceConfigFactory
 *
 *  Factory to get Game Configurations based on the type of resolutions. (i.e. PC, iPad, etc.)
 *  currently mobile is handled generically, but future plans should be to support mutliple resolutions per device
 *
 *  this object is a pattern defined in the book JavaScript Patterns.  Its structure could  not be replicated 
 *  in the Class structure that comes with the playcraft game engine.  So it was adapted
*/
pn.DeviceConfigFactory = pc.Base.extend('pn.DeviceConfigFactory',
    {},
    {
        init: function () {
            // Mobile's aspect ratio is a little different so we are painting the board in a different location for mobile.
            this.factoryConstants = pc.device.game.__constants;
            if (pc.device.game.gameDeviceDetection.isiPhone || pc.device.game.gameDeviceDetection.isiPhone4 || pc.device.game.gameDeviceDetection.isiPad || pc.device.game.gameDeviceDetection.isiPad3 || pc.device.game.gameDeviceDetection.isAndroid || pc.device.game.gameDeviceDetection.isModernUI) {                    
                this.iPad();
            }
            else {                    
                this.PC();
            }
        },
        factoryConstants: null,

        getBoardShipTileWidth: function () {
            return this._boardShipTileWidth;
        },
        getBoardShipTileHeight: function () {
            return this._boardShipTileHeight;
        },
        getBoardShipTileImage: function () {
            return this._boardShipTileImage;
        },
        getPlayfieldTileWidth: function () {
            return this._playfieldTileWidth;
        },
        getPlayfieldTileHeight: function () {
            return this._playfieldTileHeight;
        },      
        getShipStartXPos: function () {
            return this._shipStartXPos;
        },
        getShipStartYPos: function () {
            return this._shipStartYPos;
        },
        getShipOriginXPos: function(){
            return this._shipOriginX;
        },
        getShipOriginYPos: function () {
            return this._shipOriginY;
        },
        getShip2StartXPos: function () {
            return this._ship2StartXPos;
        },
        getShip2StartYPos: function () {
            return this._ship2StartYPos;
        },
        getPlayfieldTileImage: function () {
            return this._playfieldTileImage;
        },
        getPlayfieldStartXPos: function () {
            return this._playfieldStartXPos;
        },
        getPlayfieldStartYPos: function () {
            return this._playfieldStartYPos;
        },
        getPlayfieldOriginX: function(){
            return this._playfieldOriginX;
        },
        getPlayfieldOriginY: function () {
            return this._playfieldOriginY;
        },
        getBaseDeviceWidth: function () {
            return this._baseDeviceWidth;
        },
        getBaseDeviceHeight: function () {
            return this._baseDeviceHeight;
        },          
        getCardWidth: function(){
            return this._cardWidth;
        },
        getCardHeight: function() {
            return this._cardHeight;
        },
        getCardWidthZoomed: function () {
            return this._cardWidthZoomed;
        },
        getCardHeightZoomed: function () {
            return this._cardHeightZoomed;
        },
        isTabletDevice: function() {
            return this._isTabletDevice;
        },
        getCardZoomedPositionX: function () {
            return this._cardZoomedPositionX;
        },
        getCardZoomedPositionY: function () {
            return this._cardZoomedPositionY;
        },
        getCardImageRoot: function() {
            return this._cardImageRoot;
        },
        getPlayerHandWidth: function () {
            return this._playerHandWidth;
        },
        getPlayerHandHeight: function () {
            return this._playerHandHeight;
        },
    
        getBackgroundImage: function () {
            return this._backgroundImage;
        },
        getBackGroundPaintCoords: function(){
            return this._backgroundPaintCoords;
        },
        getShipTileMapColsAddOn: function(){
            return this._shipColsAddOn;
        },
        getShipTileMapRowsAddOn: function () {
            return this._shipRowsAddOn;
        },
        getCardScaleGrowX: function(){
            return this._cardScaleGrowX;
        },
        getCardScaleGrowY: function () {
            return this._cardScaleGrowY;
        },
        getCardScaleMaxX: function (){
            return this._cardScaleMaxX;
        },
        getCardScaleMaxY: function () {
            return this._cardScaleMaxY;
        },
        getGameZoomMin: function(){
            return this._gameZoomMin;
        },
        getGameZoomMax: function () {
            return this._gameZoomMax;
        },
        getCardZoomPaddingX: function(){
            return this._cardZoomPaddingX;
        },
        getUIAttributeSpaceX: function(){
            return this._UIAttributeSpaceX;
        },
        getUIAttributeSpaceY: function () {
            return this._UIAttributeSpaceY;
        },
        getUIImageURL: function(){
            return this._UIImageURL;
        },
        getUIPlayerPortraitWidth: function(){
            return this._UIPlayerPortraitWidth;
        },
        getUIPlayerPortraitHeight: function () {
            return this._UIPlayerPortraitHeight;
        },
        getUICrewIconWidth: function () {
            return this._UICrewIconWidth;
        },
        getUICrewIconHeight: function () {
            return this._UICrewIconHeight;
        },
        getUIOpponentHandStartX: function () {
            return this._UIOpponentHandStartX
        },
        getUIOpponentHandStartY: function () {
            return this._UIOpponentHandStartY
        },
        getUIOpponentHandMoveX: function () {
            return this._UIOpponentHandMoveX
        },
        getUIOpponentHandMoveY: function () {
            return this._UIOpponentHandMoveY
        },
        getUIPlayerAttributeStartX: function () {
            return this._UIPlayerAttributeStartX;
        },
        getUIPlayerAttributeStartY: function () {
            return this._UIPlayerAttributeStartY;
        },
        getUIOpponentAttributeStartX: function () {
            return this._UIOpponentAttributeStartX;
        },
        getUIOpponentAttributeStartY: function () {
            return this._UIOpponentAttributeStartY;
        },
        getUIOpponentHandPadding: function () {
            return this._UIOpponentHandPadding
        },
        getUIPlayerHandStartX: function (){
            return this._UIPlayerHandStartX
        },
        getUIPlayerHandStartY: function () {
            return this._UIPlayerHandStartY
        },
        getUIPlayerHandMoveX: function () {
            return this._UIPlayerHandMoveX
        },
        getUIPlayerHandMoveY: function () {
            return this._UIPlayerHandMoveY
        },
        getUIPlayerHandPadding: function () {
            return this._UIPlayerHandPadding
        },
        getUIPowerIconWidth: function () {
            return this._UIPowerIconWidth;
        },
        getUIPowerIconHeight: function () {
            return this._UIPowerIconHeight;
        },
        getUIPowerIconOpponentX: function () {
            return this._UIPowerIconOpponentX
        },
        getUIPowerIconOpponentY: function () {
            return this._UIPowerIconOpponentY
        },
        getUIPowerIconPlayerX: function () {
            return this._UIPowerIconPlayerX
        },
        getUIPowerIconPlayerY: function () {
            return this._UIPowerIconPlayerY
        },
        getUIPowerCountPlayerX: function () {
            return this._UIPowerCountPlayerX;
        },
        getUIPowerCountPlayerY: function () {
            return this._UIPowerCountPlayerY;
        },
        getUIPowerCountOpponentX: function () {
            return this._UIPowerCountOpponentX;
        },
        getUIPowerCountOpponentY: function () {
            return this._UIPowerCountOpponentY;
        },
        getUIPowerCountFontSize: function () {
            return this._UIPowerCountFontSize;
        },
        getUIPowerCountLineWidth: function () {
            return this._UIPowerCountLineWidth;
        },
        getUIPowerCountFont: function () {
            return this._UIPowerCountFont;
        },
        getUIPowerCountFillColor: function () {
            return this._UIPowerCountFillColor;
        },
        getUIPowerCountStrokeColor: function () {
            return this._UIPowerCountStrokeColor;
        },
        getUIHandIconWidth :function (){
            return this._UIHandIconWidth;
        },
        getUIHandIconHeight: function () {
            return this._UIHandIconHeight;
        },
        getUIHandIconX: function () {
            return this._UIHandIconX;
        },
        getUIHandIconY: function () {
            return this._UIHandIconY;
        },
        getUILeadIconWidth: function () {
            return this._UILeadIconWidth;
        },
        getUILeadIconHeight: function () {
            return this._UILeadIconHeight;
        },        
        getUILeadIconOpponentX: function(){
            return this._UILeadIconOpponentX;
        },
        getUILeadIconOpponentY: function () {
            return this._UILeadIconOpponentY;
        },
        getUILeadIconPlayerX: function () {
            return this._UILeadIconPlayerX;
        },
        getUILeadIconPlayerY: function () {
            return this._UILeadIconPlayerY;
        },        
        getUILeadCountPlayerX: function () {
            return this._UILeadCountPlayerX;
        },
        getUILeadCountPlayerY: function () {
            return this._UILeadCountPlayerY;
        },
        getUILeadCountOpponentX: function () {
            return this._UILeadCountOpponentX;
        },
        getUILeadCountOpponentY: function () {
            return this._UILeadCountOpponentY;
        },
        getUILeadCountFontSize: function () {
            return this._UILeadCountFontSize;
        },
        getUILeadCountLineWidth: function () {
            return this._UILeadCountLineWidth;
        },
        getUILeadCountFont: function () {
            return this._UILeadCountFont;
        },
        getUILeadCountFillColor: function () {
            return this._UILeadCountFillColor;
        },
        getUILeadCountStrokeColor: function () {
            return this._UILeadCountStrokeColor;
        },
        getUICrewIconOpponentX: function () {
            return this._UICrewIconOpponentX
        },
        getUICrewIconOpponentY: function () {
            return this._UICrewIconOpponentY
        },
        getUICrewIconPlayerX: function () {
            return this._UICrewIconPlayerX
        },
        getUICrewIconPlayerY: function () {
            return this._UICrewIconPlayerY
        },
        getUICrewCountPlayerX: function () {
            return this._UICrewCountPlayerX;
        },
        getUICrewCountPlayerY: function () {
            return this._UICrewCountPlayerY;
        },
        getUICrewCountOpponentX: function () {
            return this._UICrewCountOpponentX;
        },
        getUICrewCountOpponentY: function () {
            return this._UICrewCountOpponentY;
        },
        getUICrewCountFontSize: function () {
            return this._UICrewCountFontSize;
        },
        getUICrewCountLineWidth: function () {
            return this._UICrewCountLineWidth;
        },
        getUICrewCountFont: function(){
            return this._UICrewCountFont;
        },
        getUICrewCountFillColor: function(){
            return this._UICrewCountFillColor;        
        },
        getUICrewCountStrokeColor: function () {
            return this._UICrewCountStrokeColor;
        },
        /*   Font styles for ship deck */
        getShipShieldTypeLineWidth: function(){
            return this._shipShieldTypeLineWidth;
        },
        getShipShieldTypeColor: function(){
            return this._shipShieldTypeColor;
        },
        getShipShieldTypeStrokeColor: function () {
            return this._shipShieldTypeStrokeColor;
        },
        getShipShieldTypeHeight: function () {
            return this._shipShieldTypeHeight;
        },
        getShipShieldTypeFont: function () {
            return this._shipShieldTypeFont;
        },
        getShipShieldTypeOffsetX: function () {
            return this._shipShieldTypeOffsetX;
        },
        getShipShieldTypeOffsetY: function () {
            return this._shipShieldTypeOffsetY;
        },
        getShipShieldTypeLargeFontHeight: function(){
            return this._shipShieldTypeLargeFontHeight;
        },
        getShipShieldTypeLargeFont: function(){
            return this._shipShieldTypeLargeFont;
        },
        getShipShieldTypeLargeOffsetX: function () {
            return this._shipShieldTypeLargeOffsetX;
        },
        getShipShieldTypeLargeOffsetY: function () {
            return this._shipShieldTypeLargeOffsetY;
        },
        getShipDamageTypeLineWidth: function () {
            return this._shipDamageTypeLineWidth;
        },
        getShipDamageTypeColor: function () {
            return this._shipDamageTypeColor;
        },
        getShipDamageTypeStrokeColor: function () {
            return this._shipDamageTypeStrokeColor;
        },
        getShipDamageTypeHeight: function () {
            return this._shipDamageTypeHeight;
        },
        getShipDamageTypeFont: function () {
            return this._shipDamageTypeFont;
        },
        getShipDamageTypeOffsetX: function () {
            return this._shipDamageTypeOffsetX;
        },
        getShipDamageTypeOffsetY: function () {
            return this._shipDamageTypeOffsetY;
        },
        getShipDamageTypeLargeFontHeight: function(){
            return this._shipDamageTypeLargeFontHeight;
        },
        getShipDamageTypeLargeFont: function(){
            return this._shipDamageTypeLargeFont;
        },
        getShipDamageTypeLargeOffsetX: function () {
            return this._shipDamageTypeLargeOffsetX;
        },
        getShipDamageTypeLargeOffsetY: function () {
            return this._shipDamageTypeLargeOffsetY;
        },
        getShipArmorTypeLineWidth: function () {
            return this._shipArmorTypeLineWidth;
        },
        getShipArmorTypeColor: function () {
            return this._shipArmorTypeColor;
        },
        getShipArmorTypeStrokeColor: function () {
            return this._shipArmorTypeStrokeColor;
        },
        getShipArmorTypeHeight: function () {
            return this._shipArmorTypeHeight;
        },
        getShipArmorTypeFont: function () {
            return this._shipArmorTypeFont;
        },
        getShipArmorTypeOffsetX: function () {
            return this._shipArmorTypeOffsetX;
        },
        getShipArmorTypeOffsetY: function () {
            return this._shipArmorTypeOffsetY;
        },
        getShipArmorTypeLargeFontHeight: function () {
            return this._shipArmorTypeLargeFontHeight;
        },
        getShipArmorTypeLargeFont: function () {
            return this._shipArmorTypeLargeFont;
        },
        getShipArmorTypeLargeOffsetX: function () {
            return this._shipArmorTypeLargeOffsetX;
        },
        getShipArmorTypeLargeOffsetY: function () {
            return this._shipArmorTypeLargeOffsetY;
        },
        getShipStructureTypeLineWidth: function () {
            return this._shipStructureTypeLineWidth;
        },
        getShipStructureTypeColor: function () {
            return this._shipStructureTypeColor;
        },
        getShipStructureTypeStrokeColor: function () {
            return this._shipStructureTypeStrokeColor;
        },
        getShipStructureTypeHeight: function () {
            return this._shipStructureTypeHeight;
        },
        getShipStructureTypeFont: function () {
            return this._shipStructureTypeFont;
        },
        getShipStructureTypeOffsetX: function () {
            return this._shipStructureTypeOffsetX;
        },
        getShipStructureTypeOffsetY: function () {
            return this._shipStructureTypeOffsetY;
        },
        getShipStructureTypeLargeFontHeight: function () {
            return this._shipStructureTypeLargeFontHeight;
        },
        getShipStructureTypeLargeFont: function () {
            return this._shipStructureTypeLargeFont;
        },
        getShipStructureTypeLargeOffsetX: function () {
            return this._shipStructureTypeLargeOffsetX;
        },
        getShipStructureTypeLargeOffsetY: function () {
            return this._shipStructureTypeLargeOffsetY;
        },
        getShipTitleLargeSectionWidth: function(){
            return this._shipLargeTitleSectionWidth;
        },
        getShipTitleLargeFontSize: function(){
            return this._shipTitleLargeFontSize;
        },
        getShipTitleLargeFontFamily: function(){
            return this._shipTitleLargeFontFamily;
        },
        getShipTitleLargeStartX: function(){
            return this._shipTitleLargeStartX;
        },
        getShipTitleLargeStartY: function () {
            return this._shipTitleLargeStartY;
        },
        getShipSubTitleLargeFontSize: function (){
            return this._shipSubTitleLargFontSize;
        },
        getShipSubTitleLargeFontFamily: function(){
            return this._shipSubTitleLargeFontFamily;
        },
        getConflictShieldStatusX: function (){
            return this._conflictShieldStatusX;
        },
        getConflictShieldStatusY: function () {
            return this._conflictShieldStatusY;
        },
        getConflictDamageStatusX: function () {
            return this._conflictDamageStatusX;
        },
        getConflictDamageStatusY: function () {
            return this._conflictDamageStatusY;
        },
        getConflictArmorStatusX: function () {
            return this._conflictArmorStatusX
        },
        getConflictArmorStatusY: function () {
            return this._conflictArmorStatusY;
        },
        getConflictStructureStatusX: function () {
            return this._conflictStructureStatusX
        },
        getConflictStructureStatusY: function () {
            return this._conflictStructureStatusY;
        },
        getLoginFacebookButtonPosX: function(){
            return this._loginFacebookButtonPosX;
        },
        getLoginFacebookButtonPosY: function(){
            return this._loginFacebookButtonPosY;
        },
        getLoginGoogleButtonPosX: function(){
            return this._loginGoogleButtonPosX;
        },
        getLoginGoogleButtonPosY: function(){
            return this._loginGoogleButtonPosY;
        },
        getLoginFacebookButtonWidth: function(){
            return this._loginFacebookButtonWidth;
        },
        getLoginFacebookButtonHeight: function(){
            return this._loginFacebookButtonHeight;
        },
        getLoginFacebookButtonLabelX: function(){
            return this._loginFacebookButtonLabelX;
        },
        getLoginFacebookButtonLabelY: function(){
            return this._loginFacebookButtonLabelY;
        },
        getLoginFacebookFontHeight: function (){
            return this._loginFacebookFontHeight;
        },
        getLoginGoogleButtonWidth: function(){
            return this._loginGoogleButtonWidth;
        },
        getLoginGoogleButtonHeight: function(){
            return this._loginGoogleButtonHeight;
        },
        getLoginGoogleButtonLabelX: function(){
            return this._loginGoogleButtonLabelX;
        },
        getLoginGoogleButtonLabelY: function(){
            return this._loginGoogleButtonLabelY;
        },
        getLoginGoogleFontHeight: function (){
            return this._loginGoogleFontHeight;
        },
        getLoginTitle1FontHeight: function (){
            return this._loginTitle1FontHeight;
        },
        getLoginTitle1PosX: function(){
            return this._loginTitle1PosX;
        },
        getLoginTitle1PosY: function(){
            return this._loginTitle1PosY;
        },
        getLoginButtonPosX: function(){
            return this._loginButtonPosX;
        },
        getLoginErrorPosX: function(){
            return this._loginErrorPosX;
        },
        getLoginButtonPosY: function(){
            return this._loginButtonPosY;
        },
        getLoginErrorPosY: function(){
            return this._loginErrorPosY;
        },
        getLoginErrorWidth:function(){
            return this._loginErrorWidth;
        },
        getLoginErrorHeight:function(){
            return this._loginErrorHeight;
        },
        getLoginErrorFontHeight:function(){
            return this._loginErrorFontHeight;
        },
        getLoginButtonLabelX: function(){
            return this._loginButtonLabelX;
        },
        getLoginButtonLabelY: function(){
            return this._loginButtonLabelY;
        },
        getLoginButtonFontHeight: function (){
            return this._loginButtonFontHeight;
        },
        getLoginFacebookButtonLabelX: function(){
            return this._loginFacebookButtonLabelX;
        },
        getLoginFacebookButtonLabelY: function(){
            return this._loginFacebookButtonLabelY;
        },
        getLoginFacebookFontHeight: function (){
            return this._loginFacebookFontHeight;
        },
        getLoginRegisterButtonPosX: function(){
            return this._loginRegisterButtonPosX;
        },
        getLoginRegisterButtonPosY: function(){
            return this._loginRegisterButtonPosY;
        },
        getLoginRegisterButtonWidth: function(){
            return this._loginRegisterButtonWidth;
        },
        getLoginRegisterButtonHeight: function(){
            return this._loginRegisterButtonHeight;
        },
        getLoginRegisterButtonLabelX: function(){
            return this._loginRegisterButtonLabelX;
        },
        getLoginRegisterButtonLabelY: function(){
            return this._loginRegisterButtonLabelY;
        },
        getLoginRegisterButtonFontHeight: function (){
            return this._loginRegisterButtonFontHeight;
        },
        getLoginButtonWidth:function(){
            return this._loginButtonWidth;
        },
        getLoginButtonHeight:function(){
            return this._loginButtonHeight;
        },
        getPasswordTextboxWidth:function(){
            return this._passwordTextboxWidth;
        },
        getPasswordTextboxHeight:function(){
            return this._passwordTextboxHeight;
        },
        getPasswordTextboxPosX: function(){
            return this._passwordTextboxPosX;
        },
        getPasswordTextboxPosY: function(){
            return this._passwordTextboxPosY;
        },
        getUsernameTextboxPosX: function(){
            return this._usernameTextboxPosX;
        },
        getUsernameTextboxPosY: function(){
            return this._usernameTextboxPosY;
        },
        getUsernameTextboxWidth:function(){
            return this._usernameTextboxWidth;
        },
        getUsernameTextboxHeight:function(){
            return this._usernameTextboxHeight;
        },
        getUsernameTextboxFontHeight:function() {
            return this._usernameTextboxFontHeight;
        },
        getPasswordTextboxFontHeight:function() {
            return this._passwordTextboxFontHeight;
        },
        getMenuNewButtonPosX: function(){
            return this._menuNewButtonPosX;
        },
        getMenuNewButtonPosY: function(){
            return this._menuNewButtonPosY;
        },
        getMenuLogoutButtonPosX: function(){
            return this._menuLogoutButtonPosX;
        },
        getMenuLogoutButtonPosY: function(){
            return this._menuLogoutButtonPosY;
        },
        getMenuDeckButtonPosX: function(){
            return this._menuDeckButtonPosX;
        },
        getMenuDeckButtonPosY: function(){
            return this._menuDeckButtonPosY;
        },
        getMenuNewFontHeight: function(){
            return this._menuNewFontHeight;
        },
        getMenuDeckFontHeight: function(){
            return this._menuDeckFontHeight;
        },
        getMenuLogoutFontHeight: function(){
            return this._menuLogoutFontHeight;
        },
        getHubChallengePosX: function(){
            return this._hubChallengePosX;
        },
        getHubChallengePosY: function(){
            return this._hubChallengePosY;
        },
        getHubChallengeWidth: function(){
            return this._hubChallengeWidth;
        },
        getHubChallengeHeight: function(){
            return this._hubChallengeHeight;
        },
        getHubFBInvitePosX: function(){
            return this._hubFBInvitePosX;
        },
        getHubFBInvitePosY: function(){
            return this._hubFBInvitePosY;
        },
        getHubFBInviteWidth: function(){
            return this._hubFBInviteWidth;
        },
        getHubFBInviteHeight: function(){
            return this._hubFBInviteHeight;
        },
        getHubNewGamePosX: function(){
            return this._hubNewGamePosX;
        },
        getHubNewGamePosY: function(){
            return this._hubNewGamePosY;
        },
        getHubNewGameWidth: function(){
            return this._hubNewGameWidth;
        },
        getHubNewGameHeight: function(){
            return this._hubNewGameHeight;
        },
        getHubGotoMenuPosX: function(){
            return this._hubGotoMenuPosX;
        },
        getHubGotoMenuLabelPosY: function(){
            return this._hubGotoMenuLabelPosY;
        },
        getHubHeaderFontHeight: function(){
            return this._hubHeaderFontHeight;
        },
        getHubHeaderPlayerLabelPosX: function(){
            return this._hubHeaderPlayerLabelPosX;
        },
        getHubHeaderPlayerLabelPosY: function(){
            return this._hubHeaderPlayerLabelPosY;
        },
        getHubGotoMenuPosY: function(){
            return this._hubGotoMenuPosY;
        },
        getHubGotoMenuWidth: function(){
            return this._hubGotoMenuWidth;
        },
        getHubGotoMenuHeight: function(){
            return this._hubGotoMenuHeight;
        },
        getHubGotoMenuFontHeight: function(){
            return this._hubGotoMenuFontHeight;
        },
        getHubHeaderHeight: function(){
            return this._hubHeaderHeight;
        },
        getHubListItemWidth: function(){
            return this._hubListItemWidth;
        },
        getHubListItemHeight: function(){
            return this._hubListItemHeight;
        },
        getHubListItemPosX: function(){
            return this._hubListItemPosX;
        },
        getHubListItemPosY: function(){
            return this._hubListItemPosY;
        },
        getHubListItemAlertWidth: function(){
            return this._hubListItemAlertWidth;
        },
        getHubListItemAlertHeight: function(){
            return this._hubListItemAlertHeight;
        },
        getHubListItemAlertFontHeight: function(){
            return this._hubListItemAlertFontHeight;
        },
        getHubListItemAlertPosX: function(){
            return this._hubListItemAlertPosX;
        },
        getHubListItemAlertPosY: function(){
            return this._hubListItemAlertPosY;
        },
        getHubListItemAlertLabelPosX: function(){
            return this._hubListItemAlertLabelPosX;
        },
        getHubListItemAlertLabelPosY: function(){
            return this._hubListItemAlertLabelPosY;
        },
        getHubListItemLabel1PosX: function(){
            return this._hubListItemLabel1PosX
        },
        getHubListItemLabel1PosY: function(){
            return this._hubListItemLabel1PosY
        },
        getHubListItemLabel2PosX: function(){
            return this._hubListItemLabel2PosX
        },
        getHubListItemLabel2PosY: function(){
            return this._hubListItemLabel2PosY
        },
        getHubListItemLabel3PosX: function(){
            return this._hubListItemLabel3PosX
        },
        getHubListItemLabel3PosY: function(){
            return this._hubListItemLabel3PosY
        },
        getHubListItemLabel4PosX: function(){
            return this._hubListItemLabel4PosX
        },
        getHubListItemLabel4PosY: function(){
            return this._hubListItemLabel4PosY
        },
        getHubListItemLabel5PosX: function(){
            return this._hubListItemLabel5PosX
        },
        getHubListItemLabel5PosY: function(){
            return this._hubListItemLabel5PosY
        },
        getHubListItemLabel6PosX: function(){
            return this._hubListItemLabel6PosX
        },
        getHubListItemLabel6PosY: function(){
            return this._hubListItemLabel6PosY
        },
        getHubListItemLabel1FontHeight: function(){
            return this._hubListItemLabel1FontHeight;
        },
        getHubListItemLabel2FontHeight: function(){
            return this._hubListItemLabel2FontHeight;
        },
        getHubListItemLabel3FontHeight: function(){
            return this._hubListItemLabel3FontHeight;
        },
        getHubListItemLabel4FontHeight: function(){
            return this._hubListItemLabel4FontHeight;
        },
        getHubListItemLabel5FontHeight: function(){
            return this._hubListItemLabel5FontHeight;
        },
        getHubListItemLabel6FontHeight: function(){
            return this._hubListItemLabel6FontHeight;
        },
        getHubListItemActionPosX: function(){
            return this._hubListItemActionPosX;
        },
        getHubListItemActionPosY: function(){
            return this._hubListItemActionPosY;
        },
        getHubListItemActionWidth: function(){
            return this._hubListItemActionWidth;
        },
        getHubListItemActionHeight: function(){
            return this._hubListItemActionHeight;
        },
        getButtonDefaultWidth: function(){
            return this._buttonDefaultWidth;
        },
        getButtonDefaultHeight: function(){
            return this._buttonDefaultHeight;
        },
        iPad: function() {
            this._backgroundImage = this.factoryConstants.get('BACKGROUND_IMG_URL_MOBILE');
            this._boardShipTileHeight = this.factoryConstants.get('BOARD_SHIP_TILE_HEIGHT_MOBILE');
            this._boardShipTileWidth = this.factoryConstants.get('BOARD_SHIP_TILE_WIDTH_MOBILE');
            this._boardShipTileImage = this.factoryConstants.get('BOARD_SHIP_TILE_IMAGE_MOBILE');
            this._shipStartXPos = this.factoryConstants.get('BOARD_SHIP_START_X_POS_MOBILE');
            this._shipStartYPos = this.factoryConstants.get('BOARD_SHIP_START_Y_POS_MOBILE');
            this._ship2StartXPos = this.factoryConstants.get('BOARD_SHIP2_START_X_POS_MOBILE');
            this._ship2StartYPos = this.factoryConstants.get('BOARD_SHIP2_START_Y_POS_MOBILE');
            this._shipOriginX = this.factoryConstants.get('BOARD_SHIP_MAP_ORIGIN_X_MOBILE');
            this._shipOriginY = this.factoryConstants.get('BOARD_SHIP_MAP_ORIGIN_Y_MOBILE');
            this._playfieldStartXPos = this.factoryConstants.get('BOARD_PLAYFIELD_START_X_POS_MOBILE');
            this._playfieldStartYPos = this.factoryConstants.get('BOARD_PLAYFIELD_START_Y_POS_MOBILE');
            this._playfieldOriginX = this.factoryConstants.get('BOARD_PLAYFIELD_MAP_ORIGIN_X_MOBILE');
            this._playfieldOriginY = this.factoryConstants.get('BOARD_PLAYFIELD_MAP_ORIGIN_Y_MOBILE');
            this._playfieldTileHeight = this.factoryConstants.get('BOARD_PLAYFIELD_TILE_HEIGHT_MOBILE');
            this._playfieldTileWidth = this.factoryConstants.get('BOARD_PLAYFIELD_TILE_WIDTH_MOBILE');
            this._baseDeviceWidth = this.factoryConstants.get("RESOLUTION_WIDTH_MOBILE");
            this._baseDeviceHeight = this.factoryConstants.get("RESOLUTION_HEIGHT_MOBILE");
            this._cardWidth = this.factoryConstants.get('CARD_WIDTH_MOBILE');
            this._cardHeight = this.factoryConstants.get('CARD_HEIGHT_MOBILE');
            this._cardWidthZoomed = this.factoryConstants.get('CARD_WIDTH_ZOOMED_MOBILE');
            this._cardHeightZoomed = this.factoryConstants.get('CARD_HEIGHT_ZOOMED_MOBILE');
            this._isTabletDevice = 'true';
            this._cardZoomedPositionX = this.factoryConstants.get('CARD_ZOOMED_POS_X_MOBILE');
            this._cardZoomedPositionY = this.factoryConstants.get('CARD_ZOOMED_POS_Y_MOBILE');
            this._cardImageRoot = this.factoryConstants.get('IMAGE_CARD_ROOT_MOBILE');            
            this._playerHandWidth = this.factoryConstants.get('PLAYER_HAND_POS_WIDTH_MOBILE');
            this._playerHandHeight = this.factoryConstants.get('PLAYER_HAND_POS_HEIGHT_MOBILE');
            this._playfieldTileImage = this.factoryConstants.get('BOARD_PLAYFIELD_TILE_IMAGE_MOBILE');
            this._backgroundPaintCoords = this.factoryConstants.get('BOARD_BACKGROUND_PAINT_MOBILE');
            this._shipColsAddOn = this.factoryConstants.get('BOARD_SHIP_TILE_COLS_ADD_MOBILE');
            this._shipRowsAddOn = this.factoryConstants.get('BOARD_SHIP_TILE_ROWS_ADD_MOBILE');
            this._cardScaleGrowX = this.factoryConstants.get('CARD_SCALE_GROW_X_MOBILE');
            this._cardScaleGrowY = this.factoryConstants.get('CARD_SCALE_GROW_Y_MOBILE');
            this._cardScaleMaxX = this.factoryConstants.get('CARD_SCALE_MAX_X_MOBILE');
            this._cardScaleMaxY = this.factoryConstants.get('CARD_SCALE_MAX_Y_MOBILE');
            this._gameZoomMin = this.factoryConstants.get('GAME_ZOOM_MIN_MOBILE');
            this._gameZoomMax = this.factoryConstants.get('GAME_ZOOM_MAX_MOBILE');
            this._cardZoomPaddingX = this.factoryConstants.get('CARD_ZOOMED_PADDING_X_MOBILE');
            this._UIImageURL = this.factoryConstants.get('UI_IMAGE_URL_MOBILE');
            this._UIAttributeSpaceX = this.factoryConstants.get('UI_ATTRIBUTE_SPACE_X_MOBILE');
            this._UIAttributeSpaceY = this.factoryConstants.get('UI_ATTRIBUTE_SPACE_Y_MOBILE');
            this._UIPlayerPortraitWidth = this.factoryConstants.get('UI_PLAYER_PORTRAIT_WIDTH_MOBILE');
            this._UIPlayerPortraitHeight = this.factoryConstants.get('UI_PLAYER_PORTRAIT_HEIGHT_MOBILE');
            this._UICrewIconWidth = this.factoryConstants.get('UI_CREW_ICON_WIDTH_MOBILE');
            this._UICrewIconHeight = this.factoryConstants.get('UI_CREW_ICON_HEIGHT_MOBILE');
            this._UIPowerIconWidth = this.factoryConstants.get('UI_POWER_ICON_WIDHT_MOBILE');
            this._UIPowerIconHeight = this.factoryConstants.get('UI_POWER_ICON_HEIGHT_MOBILE');
            this._UILeadIconWidth = this.factoryConstants.get('UI_LEAD_ICON_WIDTH_MOBILE');
            this._UILeadIconHeight = this.factoryConstants.get('UI_LEAD_ICON_HEIGHT_MOBILE');            
            this._UILeadIconOpponentX = this.factoryConstants.get('UI_LEAD_ICON_OPPONENT_X_MOBILE');
            this._UILeadIconOpponentY = this.factoryConstants.get('UI_LEAD_ICON_OPPONENT_Y_MOBILE');
            this._UILeadIconPlayerX = this.factoryConstants.get('UI_LEAD_ICON_PLAYER_X_MOBILE');
            this._UILeadIconPlayerY = this.factoryConstants.get('UI_LEAD_ICON_PLAYER_Y_MOBILE');
            this._UIHandIconWidth = this.factoryConstants.get('UI_HAND_ICON_WIDTH_MOBILE');
            this._UIHandIconHeight = this.factoryConstants.get('UI_HAND_ICON_HEIGHT_MOBILE');
            this._UIHandIconX = this.factoryConstants.get('UI_HAND_ICON_X_MOBILE');
            this._UIHandIconY = this.factoryConstants.get('UI_HAND_ICON_Y_MOBILE');
            this._UIPowerIconOpponentX = this.factoryConstants.get('UI_POWER_ICON_OPPONENT_X_MOBILE');
            this._UIPowerIconOpponentY = this.factoryConstants.get('UI_POWER_ICON_OPPONENT_Y_MOBILE');
            this._UIPowerIconPlayerX = this.factoryConstants.get('UI_POWER_ICON_PLAYER_X_MOBILE');
            this._UIPowerIconPlayerY = this.factoryConstants.get('UI_POWER_ICON_PLAYER_Y_MOBILE');
            this._UICrewIconOpponentX = this.factoryConstants.get('UI_CREW_ICON_OPPONENT_X_MOBILE');
            this._UICrewIconOpponentY = this.factoryConstants.get('UI_CREW_ICON_OPPONENT_Y_MOBILE');
            this._UICrewIconPlayerX = this.factoryConstants.get('UI_CREW_ICON_PLAYER_X_MOBILE');
            this._UICrewIconPlayerY = this.factoryConstants.get('UI_CREW_ICON_PLAYER_Y_MOBILE');
            this._UICrewCountPlayerX = this.factoryConstants.get('UI_CREW_COUNT_PLAYER_X_MOBILE');
            this._UICrewCountPlayerY = this.factoryConstants.get('UI_CREW_COUNT_PLAYER_Y_MOBILE');
            this._UICrewCountOpponentX = this.factoryConstants.get('UI_CREW_COUNT_OPPONENT_X_MOBILE');
            this._UICrewCountOpponentY = this.factoryConstants.get('UI_CREW_COUNT_OPPONENT_Y_MOBILE');
            this._UICrewCountFontSize = this.factoryConstants.get('UI_CREW_COUNT_FONT_SIZE_MOBILE');
            this._UICrewCountLineWidth = this.factoryConstants.get('UI_CREW_COUNT_LINEWIDTH_MOBILE');
            this._UICrewCountFont = this.factoryConstants.get('UI_CREW_COUNT_FONT');
            this._UICrewCountFillColor = this.factoryConstants.get('UI_CREW_COUNT_COLOR_FILL');
            this._UICrewCountStrokeColor = this.factoryConstants.get('UI_CREW_COUNT_COLOR_STROKE');
            this._UIOpponentHandStartX = this.factoryConstants.get('UI_OPPONENT_HAND_START_X_MOBILE');
            this._UIOpponentHandStartY = this.factoryConstants.get('UI_OPPONENT_HAND_START_Y_MOBILE');
            this._UIOpponentHandMoveX = this.factoryConstants.get('UI_OPPONENT_HAND_MOVE_X_MOBILE');
            this._UIOpponentHandMoveY = this.factoryConstants.get('UI_OPPONENT_HAND_MOVE_Y_MOBILE');
            this._UIOpponentHandPadding = this.factoryConstants.get('UI_OPPONENT_HAND_PADDING_MOBILE');
            this._UIOpponentAttributeStartX = this.factoryConstants.get('UI_OPPONENT_ATTRIBUTE_START_X_MOBILE');
            this._UIOpponentAttributeStartY = this.factoryConstants.get('UI_OPPONENT_ATTRIBUTE_START_Y_MOBILE');
            this._UIPlayerAttributeStartX = this.factoryConstants.get('UI_PLAYER_ATTRIBUTE_START_X_MOBILE');
            this._UIPlayerAttributeStartY = this.factoryConstants.get('UI_PLAYER_ATTRIBUTE_START_Y_MOBILE');            
            this._UIPlayerHandStartX = this.factoryConstants.get('UI_PLAYER_HAND_START_X_MOBILE');
            this._UIPlayerHandStartY = this.factoryConstants.get('UI_PLAYER_HAND_START_Y_MOBILE');
            this._UIPlayerHandMoveX = this.factoryConstants.get('UI_PLAYER_HAND_MOVE_X_MOBILE');
            this._UIPlayerHandMoveY = this.factoryConstants.get('UI_PLAYER_HAND_MOVE_Y_MOBILE');
            this._UIPlayerHandPadding = this.factoryConstants.get('UI_PLAYER_HAND_PADDING_MOBILE');
            this._UIPowerCountPlayerX = this.factoryConstants.get('UI_POWER_COUNT_PLAYER_X_MOBILE');
            this._UIPowerCountPlayerY = this.factoryConstants.get('UI_POWER_COUNT_PLAYER_Y_MOBILE');
            this._UIPowerCountOpponentX = this.factoryConstants.get('UI_POWER_COUNT_OPPONENT_X_MOBILE');
            this._UIPowerCountOpponentY = this.factoryConstants.get('UI_POWER_COUNT_OPPONENT_Y_MOBILE');
            this._UIPowerCountFontSize = this.factoryConstants.get('UI_POWER_COUNT_FONT_SIZE_MOBILE');
            this._UIPowerCountLineWidth = this.factoryConstants.get('UI_POWER_COUNT_LINEWIDTH_MOBILE');
            this._UIPowerCountFont = this.factoryConstants.get('UI_POWER_COUNT_FONT');
            this._UIPowerCountFillColor = this.factoryConstants.get('UI_POWER_COUNT_COLOR_FILL');
            this._UIPowerCountStrokeColor = this.factoryConstants.get('UI_POWER_COUNT_COLOR_STROKE');
            this._UILeadCountPlayerX = this.factoryConstants.get('UI_LEAD_COUNT_PLAYER_X_MOBILE');
            this._UILeadCountPlayerY = this.factoryConstants.get('UI_LEAD_COUNT_PLAYER_Y_MOBILE');
            this._UILeadCountOpponentX = this.factoryConstants.get('UI_LEAD_COUNT_OPPONENT_X_MOBILE');
            this._UILeadCountOpponentY = this.factoryConstants.get('UI_LEAD_COUNT_OPPONENT_Y_MOBILE');
            this._UILeadCountFontSize = this.factoryConstants.get('UI_LEAD_COUNT_FONT_SIZE_MOBILE');
            this._UILeadCountLineWidth = this.factoryConstants.get('UI_LEAD_COUNT_LINEWIDTH_MOBILE');
            this._UILeadCountFont = this.factoryConstants.get('UI_LEAD_COUNT_FONT');
            this._UILeadCountFillColor = this.factoryConstants.get('UI_LEAD_COUNT_COLOR_FILL');
            this._UILeadCountStrokeColor = this.factoryConstants.get('UI_LEAD_COUNT_COLOR_STROKE');
            this._shipShieldTypeLineWidth = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_LINEWIDTH_MOBILE');
            this._shipShieldTypeColor = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_COLOR_MOBILE');            
            this._shipShieldTypeStrokeColor = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_STROKECOLOR_MOBILE');
            this._shipShieldTypeHeight = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_FONT_HEIGHT_MOBILE');
            this._shipShieldTypeFont = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_FONT_MOBILE');
            this._shipShieldTypeOffsetX = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_OFFSET_X_MOBILE');
            this._shipShieldTypeOffsetY = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_OFFSET_Y_MOBILE');
            this._shipDamageTypeLineWidth = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_LINEWIDTH_MOBILE');
            this._shipDamageTypeColor = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_COLOR_MOBILE');
            this._shipDamageTypeStrokeColor = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_STROKECOLOR_MOBILE');
            this._shipDamageTypeHeight = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_FONT_HEIGHT_MOBILE');
            this._shipDamageTypeFont = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_FONT_MOBILE');
            this._shipDamageTypeOffsetX = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_OFFSET_X_MOBILE');
            this._shipDamageTypeOffsetY = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_OFFSET_Y_MOBILE');
            this._shipArmorTypeLineWidth = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_LINEWIDTH_MOBILE');
            this._shipArmorTypeColor = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_COLOR_MOBILE');
            this._shipArmorTypeStrokeColor = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_STROKECOLOR_MOBILE');
            this._shipArmorTypeHeight = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_FONT_HEIGHT_MOBILE');
            this._shipArmorTypeFont = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_FONT_MOBILE');
            this._shipArmorTypeOffsetX = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_OFFSET_X_MOBILE');
            this._shipArmorTypeOffsetY = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_OFFSET_Y_MOBILE');
            this._shipStructureTypeLineWidth = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_LINEWIDTH_MOBILE');
            this._shipStructureTypeColor = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_COLOR_MOBILE');
            this._shipStructureTypeStrokeColor = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_STROKECOLOR_MOBILE');
            this._shipStructureTypeHeight = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_FONT_HEIGHT_MOBILE');
            this._shipStructureTypeFont = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_FONT_MOBILE');
            this._shipStructureTypeOffsetX = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_OFFSET_X_MOBILE');
            this._shipStructureTypeOffsetY = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_OFFSET_Y_MOBILE');
            this._shipArmorTypeLargeFontHeight = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_LARGE_FONT_HEIGHT_MOBILE');
            this._shipArmorTypeLargeFont = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_LARGE_FONT_MOBILE');
            this._shipArmorTypeLargeOffsetX = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_LARGE_OFFSET_X_MOBILE');
            this._shipArmorTypeLargeOffsetY = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_LARGE_OFFSET_Y_MOBILE');
            this._shipShieldTypeLargeFontHeight = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_LARGE_FONT_HEIGHT_MOBILE');
            this._shipShieldTypeLargeFont = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_LARGE_FONT_MOBILE');
            this._shipShieldTypeLargeOffsetX = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_LARGE_OFFSET_X_MOBILE');
            this._shipShieldTypeLargeOffsetY = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_LARGE_OFFSET_Y_MOBILE');
            this._shipDamageTypeLargeFontHeight = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_LARGE_FONT_HEIGHT_MOBILE');
            this._shipDamageTypeLargeFont = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_LARGE_FONT_MOBILE');
            this._shipDamageTypeLargeOffsetX = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_LARGE_OFFSET_X_MOBILE');
            this._shipDamageTypeLargeOffsetY = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_LARGE_OFFSET_Y_MOBILE');
            this._shipStructureTypeLargeFontHeight = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_LARGE_FONT_HEIGHT_MOBILE');
            this._shipStructureTypeLargeFont = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_LARGE_FONT_MOBILE');
            this._shipStructureTypeLargeOffsetX = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_LARGE_OFFSET_X_MOBILE');
            this._shipStructureTypeLargeOffsetY = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_LARGE_OFFSET_Y_MOBILE');
            this._shipLargeTitleSectionWidth = this.factoryConstants.get('DECK_COMPARTMENT_TITLE_LARGE_SECTION_WIDTH_MOBILE');
            this._shipTitleLargeFontSize = this.factoryConstants.get('DECK_COMPARTMENT_TITLE_LARGE_FONT_SIZE_MOBILE');
            this._shipTitleLargeFontFamily = this.factoryConstants.get('DECK_COMPARTMENT_TITLE_LARGE_FONT_FAMILY_MOBILE');
            this._shipTitleLargeStartX = this.factoryConstants.get('DECK_COMPARTMENT_TITLE_LARGE_START_X_MOBILE');
            this._shipTitleLargeStartY = this.factoryConstants.get('DECK_COMPARTMENT_TITLE_LARGE_START_Y_MOBILE');
            this._shipSubTitleLargFontSize = this.factoryConstants.get('DECK_COMPARTMENT_SUBTITLE_LARGE_FONT_SIZE_MOBILE');
            this._shipSubTitleLargeFontFamily = this.factoryConstants.get('DECK_COMPARTMENT_SUBTITLE_LARGE_FONT_FAMILY_MOBILE');
            this._conflictShieldStatusX = this.factoryConstants.get('DECK_CONFLICT_SHIELD_TYPE_SMALL_START_X_MOBILE');
            this._conflictShieldStatusY = this.factoryConstants.get('DECK_CONFLICT_SHIELD_TYPE_SMALL_START_Y_MOBILE');
            this._conflictDamageStatusX = this.factoryConstants.get('DECK_CONFLICT_DAMAGE_TYPE_SMALL_START_X_MOBILE');
            this._conflictDamageStatusY = this.factoryConstants.get('DECK_CONFLICT_DAMAGE_TYPE_SMALL_START_Y_MOBILE');
            this._conflictArmorStatusX = this.factoryConstants.get('DECK_CONFLICT_ARMOR_TYPE_SMALL_START_X_MOBILE');
            this._conflictArmorStatusY = this.factoryConstants.get('DECK_CONFLICT_ARMOR_TYPE_SMALL_START_Y_MOBILE');
            this._conflictStructureStatusX = this.factoryConstants.get('DECK_CONFLICT_STRUCTURE_TYPE_SMALL_START_X_MOBILE');
            this._conflictStructureStatusY = this.factoryConstants.get('DECK_CONFLICT_STRUCTURE_TYPE_SMALL_START_Y_MOBILE');
            this._loginFacebookButtonPosX = this.factoryConstants.get('LOGIN_FACEBOOK_BUTTON_POS_X_MOBILE');
            this._loginFacebookButtonPosY = this.factoryConstants.get('LOGIN_FACEBOOK_BUTTON_POS_Y_MOBILE');
            this._loginGoogleButtonPosX = this.factoryConstants.get('LOGIN_GOOGLE_BUTTON_POS_X_MOBILE');
            this._loginGoogleButtonPosY = this.factoryConstants.get('LOGIN_GOOGLE_BUTTON_POS_Y_MOBILE');
            this._loginGoogleButtonWidth = this.factoryConstants.get('LOGIN_GOOGLE_BUTTON_WIDTH_MOBILE');
            this._loginGoogleButtonHeight = this.factoryConstants.get('LOGIN_GOOGLE_BUTTON_HEIGHT_MOBILE');
            this._loginGoogleButtonLabelX = this.factoryConstants.get('LOGIN_GOOGLE_BUTTON_LABEL_X_MOBILE');
            this._loginGoogleButtonLabelY = this.factoryConstants.get('LOGIN_GOOGLE_BUTTON_LABEL_Y_MOBILE');
            this._loginGoogleFontHeight = this.factoryConstants.get('LOGIN_GOOGLE_BUTTON_FONT_HEIGHT_MOBILE');
            this._loginGoogleButtonHeight = this.factoryConstants.get('LOGIN_GOOGLE_BUTTON_HEIGHT_MOBILE');
            this._loginFacebookButtonLabelX = this.factoryConstants.get('LOGIN_FACEBOOK_BUTTON_LABEL_X_MOBILE');
            this._loginFacebookButtonLabelY = this.factoryConstants.get('LOGIN_FACEBOOK_BUTTON_LABEL_Y_MOBILE');
            this._loginFacebookFontHeight = this.factoryConstants.get('LOGIN_FACEBOOK_BUTTON_FONT_HEIGHT_MOBILE');
            this._loginFacebookButtonWidth = this.factoryConstants.get('LOGIN_FACEBOOK_BUTTON_WIDTH_MOBILE');
            this._loginFacebookButtonHeight = this.factoryConstants.get('LOGIN_FACEBOOK_BUTTON_HEIGHT_MOBILE');
            this._loginTitle1FontHeight = this.factoryConstants.get('LOGIN_TITLE1_FONT_HEIGHT_MOBILE');
            this._loginTitle1PosX = this.factoryConstants.get('LOGIN_TITLE1_POS_X_MOBILE');
            this._loginTitle1PosY = this.factoryConstants.get('LOGIN_TITLE1_POS_Y_MOBILE');
            this._loginButtonPosX = this.factoryConstants.get('LOGIN_BUTTON_POS_X_MOBILE');
            this._loginButtonPosY = this.factoryConstants.get('LOGIN_BUTTON_POS_Y_MOBILE');
            this._loginButtonWidth = this.factoryConstants.get('LOGIN_BUTTON_WIDTH_MOBILE');
            this._loginButtonHeight = this.factoryConstants.get('LOGIN_BUTTON_HEIGHT_MOBILE');
            this._loginErrorPosX = this.factoryConstants.get('LOGIN_ERROR_POS_X_MOBILE');
            this._loginErrorPosY = this.factoryConstants.get('LOGIN_ERROR_POS_Y_MOBILE');
            this._loginErrorWidth = this.factoryConstants.get('LOGIN_ERROR_WIDTH_MOBILE');
            this._loginErrorHeight = this.factoryConstants.get('LOGIN_ERROR_HEIGHT_MOBILE');
            this._loginErrorFontHeight = this.factoryConstants.get('LOGIN_ERROR_FONT_HEIGHT_MOBILE');
            this._loginButtonLabelX = this.factoryConstants.get('LOGIN_BUTTON_LABEL_X_MOBILE');
            this._loginButtonLabelY = this.factoryConstants.get('LOGIN_BUTTON_LABEL_Y_MOBILE');
            this._loginButtonFontHeight = this.factoryConstants.get('LOGIN_BUTTON_FONT_HEIGHT_MOBILE');
            this._loginRegisterButtonLabelX = this.factoryConstants.get('LOGIN_REGISTER_BUTTON_LABEL_X_MOBILE');
            this._loginRegisterButtonLabelY = this.factoryConstants.get('LOGIN_REGISTER_BUTTON_LABEL_Y_MOBILE');
            this._loginRegisterButtonFontHeight = this.factoryConstants.get('LOGIN_REGISTER_BUTTON_FONT_HEIGHT_MOBILE');
            this._loginRegisterButtonPosX = this.factoryConstants.get('LOGIN_REGISTER_BUTTON_POS_X_MOBILE');
            this._loginRegisterButtonPosY = this.factoryConstants.get('LOGIN_REGISTER_BUTTON_POS_Y_MOBILE');
            this._loginRegisterButtonWidth = this.factoryConstants.get('LOGIN_REGISTER_BUTTON_WIDTH_MOBILE');
            this._loginRegisterButtonHeight = this.factoryConstants.get('LOGIN_REGISTER_BUTTON_HEIGHT_MOBILE');
            this._usernameTextboxPosX = this.factoryConstants.get('LOGIN_USERNAME_TEXTBOX_POS_X_MOBILE');
            this._usernameTextboxPosY = this.factoryConstants.get('LOGIN_USERNAME_TEXTBOX_POS_Y_MOBILE');            
            this._usernameTextboxWidth = this.factoryConstants.get('LOGIN_USERNAME_TEXTBOX_WIDTH_MOBILE');
            this._usernameTextboxHeight = this.factoryConstants.get('LOGIN_USERNAME_TEXTBOX_HEIGHT_MOBILE');
            this._usernameTextboxFontHeight = this.factoryConstants.get('LOGIN_USERNAME_TEXTBOX_FONT_HEIGHT_MOBILE');
            this._passwordTextboxPosX = this.factoryConstants.get('LOGIN_PASSWORD_TEXTBOX_POS_X_MOBILE');
            this._passwordTextboxPosY = this.factoryConstants.get('LOGIN_PASSWORD_TEXTBOX_POS_Y_MOBILE');
            this._passwordTextboxWidth = this.factoryConstants.get('LOGIN_PASSWORD_TEXTBOX_WIDTH_MOBILE');
            this._passwordTextboxHeight = this.factoryConstants.get('LOGIN_PASSWORD_TEXTBOX_HEIGHT_MOBILE');
            this._passwordTextboxFontHeight = this.factoryConstants.get('LOGIN_PASSWORD_TEXTBOX_FONT_HEIGHT_MOBILE');
            this._menuNewButtonPosY = this.factoryConstants.get('MENU_NEW_BUTTON_POS_Y_MOBILE');
            this._menuNewButtonPosX = this.factoryConstants.get('MENU_NEW_BUTTON_POS_X_MOBILE');
            this._menuNewFontHeight = this.factoryConstants.get('MENU_NEW_FONT_HEIGHT_MOBILE');
            this._menuLogoutButtonPosY = this.factoryConstants.get('MENU_LOGOUT_BUTTON_POS_Y_MOBILE');
            this._menuLogoutButtonPosX = this.factoryConstants.get('MENU_LOGOUT_BUTTON_POS_X_MOBILE');
            this._menuLogoutFontHeight = this.factoryConstants.get('MENU_LOGOUT_FONT_HEIGHT_MOBILE');
            this._menuDeckButtonPosY = this.factoryConstants.get('MENU_DECK_BUTTON_POS_Y_MOBILE');
            this._menuDeckButtonPosX = this.factoryConstants.get('MENU_DECK_BUTTON_POS_X_MOBILE');
            this._menuDeckFontHeight = this.factoryConstants.get('MENU_DECK_FONT_HEIGHT_MOBILE');
            this._hubHeaderHeight = this.factoryConstants.get('HUB_HEADER_HEIGHT_MOBILE');
            this._hubHeaderFontHeight = this.factoryConstants.get('HUB_HEADER_FONT_HEIGHT_MOBILE');
            this._hubHeaderPlayerLabelPosX = this.factoryConstants.get('HUB_HEADER_PLAYER_LABEL_POSX_MOBILE');
            this._hubHeaderPlayerLabelPosY = this.factoryConstants.get('HUB_HEADER_PLAYER_LABEL_POSY_MOBILE');
            this._hubListItemHeight = this.factoryConstants.get('HUB_LIST_ITEM_HEIGHT_MOBILE');
            this._hubListItemWidth = this.factoryConstants.get('HUB_LIST_ITEM_WIDTH_MOBILE');
            this._hubListItemPosX = this.factoryConstants.get('HUB_LIST_ITEM_POS_X_MOBILE');
            this._hubListItemPosY = this.factoryConstants.get('HUB_LIST_ITEM_POS_Y_MOBILE');
            this._hubListItemAlertHeight = this.factoryConstants.get('HUB_LIST_ITEM_ALERT_HEIGHT_MOBILE');
            this._hubListItemAlertWidth = this.factoryConstants.get('HUB_LIST_ITEM_ALERT_WIDTH_MOBILE');
            this._hubListItemAlertPosX = this.factoryConstants.get('HUB_LIST_ITEM_ALERT_POS_X_MOBILE');
            this._hubListItemAlertPosY = this.factoryConstants.get('HUB_LIST_ITEM_ALERT_POS_Y_MOBILE'); 
            this._hubListItemAlertLabelPosX = this.factoryConstants.get('HUB_LIST_ITEM_ALERT_LABEL_POS_X_MOBILE');
            this._hubListItemAlertLabelPosY = this.factoryConstants.get('HUB_LIST_ITEM_ALERT_LABEL_POS_Y_MOBILE'); 
            this._hubListItemAlertFontHeight = this.factoryConstants.get('HUB_LIST_ITEM_ALERT_FONT_HEIGHT_MOBILE');
            this._hubChallengePosX = this.factoryConstants.get('HUB_CHALLENGE_POSX_MOBILE');
            this._hubChallengePosY = this.factoryConstants.get('HUB_CHALLENGE_POSY_MOBILE');
            this._hubChallengeWidth = this.factoryConstants.get('HUB_CHALLENGE_WIDTH_MOBILE');
            this._hubFBInviteHeight = this.factoryConstants.get('HUB_CHALLENGE_HEIGHT_MOBILE');
            this._hubFBInvitePosX = this.factoryConstants.get('HUB_FBINVITE_POSX_MOBILE');
            this._hubFBInvitePosY = this.factoryConstants.get('HUB_FBINVITE_POSY_MOBILE');
            this._hubFBInviteWidth = this.factoryConstants.get('HUB_FBINVITE_WIDTH_MOBILE');
            this._hubFBInviteHeight = this.factoryConstants.get('HUB_FBINVITE_HEIGHT_MOBILE');
            this._hubNewGamePosX = this.factoryConstants.get('HUB_NEWGAME_POSX_MOBILE');
            this._hubNewGamePosY = this.factoryConstants.get('HUB_NEWGAME_POSY_MOBILE');
            this._hubNewGameWidth = this.factoryConstants.get('HUB_NEWGAME_WIDTH_MOBILE');
            this._hubNewGameHeight = this.factoryConstants.get('HUB_NEWGAME_HEIGHT_MOBILE');
            this._hubGotoMenuPosX = this.factoryConstants.get('HUB_GOTOMENU_POSX_MOBILE');
            this._hubGotoMenuPosY = this.factoryConstants.get('HUB_GOTOMENU_POSY_MOBILE');
            this._hubGotoMenuWidth = this.factoryConstants.get('HUB_GOTOMENU_WIDTH_MOBILE');
            this._hubGotoMenuHeight = this.factoryConstants.get('HUB_GOTOMENU_HEIGHT_MOBILE');
            this._hubGotoMenuFontHeight = this.factoryConstants.get('HUB_GOTOMENU_FONT_HEIGHT_MOBILE');
            this._hubGotoMenuLabelPosY = this.factoryConstants.get('HUB_GOTOMENU_LABEL_POSY_MOBILE');
            this._hubListItemLabel1PosX = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_1_POS_X_MOBILE');
            this._hubListItemLabel1PosY = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_1_POS_Y_MOBILE');
            this._hubListItemLabel2PosX = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_2_POS_X_MOBILE');
            this._hubListItemLabel2PosY = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_2_POS_Y_MOBILE');
            this._hubListItemLabel3PosX = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_3_POS_X_MOBILE');
            this._hubListItemLabel3PosY = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_3_POS_Y_MOBILE');
            this._hubListItemLabel4PosX = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_4_POS_X_MOBILE');
            this._hubListItemLabel4PosY = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_4_POS_Y_MOBILE');
            this._hubListItemLabel5PosX = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_5_POS_X_MOBILE');
            this._hubListItemLabel5PosY = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_5_POS_Y_MOBILE');
            this._hubListItemLabel6PosX = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_6_POS_X_MOBILE');
            this._hubListItemLabel6PosY = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_6_POS_Y_MOBILE');
            this._hubListItemLabel1FontHeight = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_1_FONT_HEIGHT_MOBILE');
            this._hubListItemLabel2FontHeight = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_2_FONT_HEIGHT_MOBILE');
            this._hubListItemLabel3FontHeight = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_3_FONT_HEIGHT_MOBILE');
            this._hubListItemLabel4FontHeight = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_4_FONT_HEIGHT_MOBILE');
            this._hubListItemLabel5FontHeight = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_5_FONT_HEIGHT_MOBILE');
            this._hubListItemLabel6FontHeight = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_6_FONT_HEIGHT_MOBILE');
            this._hubListItemActionPosX = this.factoryConstants.get('HUB_LIST_ITEM_ACTION_POS_X_MOBILE');
            this._hubListItemActionPosY = this.factoryConstants.get('HUB_LIST_ITEM_ACTION_POS_Y_MOBILE');
            this._hubListItemActionWidth = this.factoryConstants.get('HUB_LIST_ITEM_ACTION_WIDTH_MOBILE');
            this._hubListItemActionHeight = this.factoryConstants.get('HUB_LIST_ITEM_ACTION_HEIGHT_MOBILE');
            this._buttonDefaultWidth = this.factoryConstants.get('BUTTON_DEFAULT_WIDTH_MOBILE');
            this._buttonDefaultHeight = this.factoryConstants.get('BUTTON_DEFAULT_HEIGHT_MOBILE');
        },
        PC: function () {
            this._backgroundImage = this.factoryConstants.get('BACKGROUND_IMG_URL_PC');            
            this._boardShipTileHeight = this.factoryConstants.get('BOARD_SHIP_TILE_HEIGHT_PC');
            this._boardShipTileWidth = this.factoryConstants.get('BOARD_SHIP_TILE_WIDTH_PC');
            this._boardShipTileImage = this.factoryConstants.get('BOARD_SHIP_TILE_IMAGE_PC');
            this._shipStartXPos = this.factoryConstants.get('BOARD_SHIP_START_X_POS_PC');
            this._shipStartYPos = this.factoryConstants.get('BOARD_SHIP_START_Y_POS_PC');            
            this._ship2StartXPos = this.factoryConstants.get('BOARD_SHIP2_START_X_POS_PC');
            this._ship2StartYPos = this.factoryConstants.get('BOARD_SHIP2_START_Y_POS_PC');
            this._shipOriginX = this.factoryConstants.get('BOARD_SHIP_MAP_ORIGIN_X_PC');
            this._shipOriginY = this.factoryConstants.get('BOARD_SHIP_MAP_ORIGIN_Y_PC');
            this._playfieldStartXPos = this.factoryConstants.get('BOARD_PLAYFIELD_START_X_POS_PC');
            this._playfieldStartYPos = this.factoryConstants.get('BOARD_PLAYFIELD_START_Y_POS_PC');
            this._playfieldOriginX = this.factoryConstants.get('BOARD_PLAYFIELD_MAP_ORIGIN_X_PC');
            this._playfieldOriginY = this.factoryConstants.get('BOARD_PLAYFIELD_MAP_ORIGIN_Y_PC');            
            this._playfieldTileHeight = this.factoryConstants.get('BOARD_PLAYFIELD_TILE_HEIGHT_PC');
            this._playfieldTileWidth = this.factoryConstants.get('BOARD_PLAYFIELD_TILE_WIDTH_PC');
            this._baseDeviceWidth = this.factoryConstants.get("RESOLUTION_WIDTH_PC");
            this._baseDeviceHeight = this.factoryConstants.get("RESOLUTION_HEIGHT_PC");                        
            this._cardWidth = this.factoryConstants.get('CARD_WIDTH_PC');
            this._cardHeight = this.factoryConstants.get('CARD_HEIGHT_PC');
            this._cardWidthZoomed = this.factoryConstants.get('CARD_WIDTH_ZOOMED_PC');
            this._cardHeightZoomed = this.factoryConstants.get('CARD_HEIGHT_ZOOMED_PC');
            this._isTabletDevice = 'false';
            this._cardZoomedPositionX = this.factoryConstants.get('CARD_ZOOMED_POS_X_PC');
            this._cardZoomedPositionY = this.factoryConstants.get('CARD_ZOOMED_POS_Y_PC');
            this._cardImageRoot = this.factoryConstants.get('IMAGE_CARD_ROOT_PC');
            this._playerHandWidth = this.factoryConstants.get('PLAYER_HAND_POS_WIDTH_PC');
            this._playerHandHeight = this.factoryConstants.get('PLAYER_HAND_POS_HEIGHT_PC');
            this._playfieldTileImage = this.factoryConstants.get('BOARD_PLAYFIELD_TILE_IMAGE_PC');
            this._backgroundPaintCoords = this.factoryConstants.get('BOARD_BACKGROUND_PAINT_PC');
            this._shipColsAddOn = this.factoryConstants.get('BOARD_SHIP_TILE_COLS_ADD_PC');
            this._shipRowsAddOn = this.factoryConstants.get('BOARD_SHIP_TILE_ROWS_ADD_PC');
            this._cardScaleGrowX = this.factoryConstants.get('CARD_SCALE_GROW_X_PC');
            this._cardScaleGrowY = this.factoryConstants.get('CARD_SCALE_GROW_Y_PC');
            this._cardScaleMaxX = this.factoryConstants.get('CARD_SCALE_MAX_X_PC');
            this._cardScaleMaxY = this.factoryConstants.get('CARD_SCALE_MAX_Y_PC');
            this._gameZoomMin = this.factoryConstants.get('GAME_ZOOM_MIN_PC');
            this._gameZoomMax = this.factoryConstants.get('GAME_ZOOM_MAX_PC');
            this._cardZoomPaddingX = this.factoryConstants.get('CARD_ZOOMED_PADDING_X_PC');
            this._UIImageURL = this.factoryConstants.get('UI_IMAGE_URL_PC');
            this._UIAttributeSpaceX = this.factoryConstants.get('UI_ATTRIBUTE_SPACE_X_PC');
            this._UIAttributeSpaceY = this.factoryConstants.get('UI_ATTRIBUTE_SPACE_Y_PC');
            this._UIPlayerPortraitWidth = this.factoryConstants.get('UI_PLAYER_PORTRAIT_WIDTH_PC');
            this._UIPlayerPortraitHeight = this.factoryConstants.get('UI_PLAYER_PORTRAIT_HEIGHT_PC');
            this._UICrewIconWidth = this.factoryConstants.get('UI_CREW_ICON_WIDTH_PC');
            this._UICrewIconHeight = this.factoryConstants.get('UI_CREW_ICON_HEIGHT_PC');
            this._UIPowerIconWidth = this.factoryConstants.get('UI_POWER_ICON_WIDHT_PC');
            this._UIPowerIconHeight = this.factoryConstants.get('UI_POWER_ICON_HEIGHT_PC');
            this._UIHandIconWidth = this.factoryConstants.get('UI_HAND_ICON_WIDTH_PC');
            this._UIHandIconHeight = this.factoryConstants.get('UI_HAND_ICON_HEIGHT_PC');
            this._UIHandIconX = this.factoryConstants.get('UI_HAND_ICON_X_PC');
            this._UIHandIconY = this.factoryConstants.get('UI_HAND_ICON_Y_PC');
            this._UILeadIconWidth = this.factoryConstants.get('UI_LEAD_ICON_WIDTH_PC');
            this._UILeadIconHeight = this.factoryConstants.get('UI_LEAD_ICON_HEIGHT_PC');            
            this._UILeadIconOpponentX = this.factoryConstants.get('UI_LEAD_ICON_OPPONENT_X_PC');
            this._UILeadIconOpponentY = this.factoryConstants.get('UI_LEAD_ICON_OPPONENT_Y_PC');
            this._UILeadIconPlayerX = this.factoryConstants.get('UI_LEAD_ICON_PLAYER_X_PC');
            this._UILeadIconPlayerY = this.factoryConstants.get('UI_LEAD_ICON_PLAYER_Y_PC');
            this._UIPowerIconOpponentX = this.factoryConstants.get('UI_POWER_ICON_OPPONENT_X_PC');
            this._UIPowerIconOpponentY = this.factoryConstants.get('UI_POWER_ICON_OPPONENT_Y_PC');
            this._UIPowerIconPlayerX = this.factoryConstants.get('UI_POWER_ICON_PLAYER_X_PC');
            this._UIPowerIconPlayerY = this.factoryConstants.get('UI_POWER_ICON_PLAYER_Y_PC');
            this._UICrewIconOpponentX = this.factoryConstants.get('UI_CREW_ICON_OPPONENT_X_PC');
            this._UICrewIconOpponentY = this.factoryConstants.get('UI_CREW_ICON_OPPONENT_Y_PC');
            this._UICrewIconPlayerX = this.factoryConstants.get('UI_CREW_ICON_PLAYER_X_PC');
            this._UICrewIconPlayerY = this.factoryConstants.get('UI_CREW_ICON_PLAYER_Y_PC');
            this._UICrewCountPlayerX = this.factoryConstants.get('UI_CREW_COUNT_PLAYER_X_PC');
            this._UICrewCountPlayerY = this.factoryConstants.get('UI_CREW_COUNT_PLAYER_Y_PC');
            this._UICrewCountOpponentX = this.factoryConstants.get('UI_CREW_COUNT_OPPONENT_X_PC');
            this._UICrewCountOpponentY = this.factoryConstants.get('UI_CREW_COUNT_OPPONENT_Y_PC');
            this._UICrewCountFontSize = this.factoryConstants.get('UI_CREW_COUNT_FONT_SIZE_PC');
            this._UICrewCountLineWidth = this.factoryConstants.get('UI_CREW_COUNT_LINEWIDTH_PC');
            this._UICrewCountFont = this.factoryConstants.get('UI_CREW_COUNT_FONT');
            this._UICrewCountFillColor = this.factoryConstants.get('UI_CREW_COUNT_COLOR_FILL');
            this._UICrewCountStrokeColor = this.factoryConstants.get('UI_CREW_COUNT_COLOR_STROKE');
            this._UIOpponentHandStartX = this.factoryConstants.get('UI_OPPONENT_HAND_START_X_PC');
            this._UIOpponentHandStartY = this.factoryConstants.get('UI_OPPONENT_HAND_START_Y_PC');
            this._UIOpponentHandMoveX = this.factoryConstants.get('UI_OPPONENT_HAND_MOVE_X_PC');
            this._UIOpponentHandMoveY = this.factoryConstants.get('UI_OPPONENT_HAND_MOVE_Y_PC');
            this._UIOpponentHandPadding = this.factoryConstants.get('UI_OPPONENT_HAND_PADDING_PC');
            this._UIPlayerAttributeStartX = this.factoryConstants.get('UI_PLAYER_ATTRIBUTE_START_X_PC');
            this._UIPlayerAttributeStartY = this.factoryConstants.get('UI_PLAYER_ATTRIBUTE_START_Y_PC');
            this._UIOpponentAttributeStartX = this.factoryConstants.get('UI_OPPONENT_ATTRIBUTE_START_X_PC');
            this._UIOpponentAttributeStartY = this.factoryConstants.get('UI_OPPONENT_ATTRIBUTE_START_Y_PC');
            this._UIPlayerHandStartX = this.factoryConstants.get('UI_PLAYER_HAND_START_X_PC');
            this._UIPlayerHandStartY = this.factoryConstants.get('UI_PLAYER_HAND_START_Y_PC');
            this._UIPlayerHandMoveX = this.factoryConstants.get('UI_PLAYER_HAND_MOVE_X_PC');
            this._UIPlayerHandMoveY = this.factoryConstants.get('UI_PLAYER_HAND_MOVE_Y_PC');
            this._UIPlayerHandPadding = this.factoryConstants.get('UI_PLAYER_HAND_PADDING_PC');
            this._UIPowerCountPlayerX = this.factoryConstants.get('UI_POWER_COUNT_PLAYER_X_PC');
            this._UIPowerCountPlayerY = this.factoryConstants.get('UI_POWER_COUNT_PLAYER_Y_PC');
            this._UIPowerCountOpponentX = this.factoryConstants.get('UI_POWER_COUNT_OPPONENT_X_PC');
            this._UIPowerCountOpponentY = this.factoryConstants.get('UI_POWER_COUNT_OPPONENT_Y_PC');
            this._UIPowerCountFontSize = this.factoryConstants.get('UI_POWER_COUNT_FONT_SIZE_PC');
            this._UIPowerCountLineWidth = this.factoryConstants.get('UI_POWER_COUNT_LINEWIDTH_PC');
            this._UIPowerCountFont = this.factoryConstants.get('UI_POWER_COUNT_FONT');
            this._UIPowerCountFillColor = this.factoryConstants.get('UI_POWER_COUNT_COLOR_FILL');
            this._UIPowerCountStrokeColor = this.factoryConstants.get('UI_POWER_COUNT_COLOR_STROKE');
            this._UILeadCountPlayerX = this.factoryConstants.get('UI_LEAD_COUNT_PLAYER_X_PC');
            this._UILeadCountPlayerY = this.factoryConstants.get('UI_LEAD_COUNT_PLAYER_Y_PC');
            this._UILeadCountOpponentX = this.factoryConstants.get('UI_LEAD_COUNT_OPPONENT_X_PC');
            this._UILeadCountOpponentY = this.factoryConstants.get('UI_LEAD_COUNT_OPPONENT_Y_PC');
            this._UILeadCountFontSize = this.factoryConstants.get('UI_LEAD_COUNT_FONT_SIZE_PC');
            this._UILeadCountLineWidth = this.factoryConstants.get('UI_LEAD_COUNT_LINEWIDTH_PC');
            this._UILeadCountFont = this.factoryConstants.get('UI_LEAD_COUNT_FONT');
            this._UILeadCountFillColor = this.factoryConstants.get('UI_LEAD_COUNT_COLOR_FILL');
            this._UILeadCountStrokeColor = this.factoryConstants.get('UI_LEAD_COUNT_COLOR_STROKE');
            this._shipShieldTypeLineWidth = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_LINEWIDTH_PC');
            this._shipShieldTypeColor = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_COLOR_PC');            
            this._shipShieldTypeStrokeColor = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_STROKECOLOR_PC');
            this._shipShieldTypeHeight = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_FONT_HEIGHT_PC');
            this._shipShieldTypeFont = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_FONT_PC');
            this._shipShieldTypeOffsetX = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_OFFSET_X_PC');
            this._shipShieldTypeOffsetY = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_OFFSET_Y_PC');
            this._shipDamageTypeLineWidth = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_LINEWIDTH_PC');
            this._shipDamageTypeColor = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_COLOR_PC');
            this._shipDamageTypeStrokeColor = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_STROKECOLOR_PC');
            this._shipDamageTypeHeight = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_FONT_HEIGHT_PC');
            this._shipDamageTypeFont = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_FONT_PC');
            this._shipDamageTypeOffsetX = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_OFFSET_X_PC');
            this._shipDamageTypeOffsetY = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_OFFSET_Y_PC');
            this._shipArmorTypeLineWidth = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_LINEWIDTH_PC');
            this._shipArmorTypeColor = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_COLOR_PC');
            this._shipArmorTypeStrokeColor = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_STROKECOLOR_PC');
            this._shipArmorTypeHeight = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_FONT_HEIGHT_PC');
            this._shipArmorTypeFont = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_FONT_PC');
            this._shipArmorTypeOffsetX = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_OFFSET_X_PC');
            this._shipArmorTypeOffsetY = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_OFFSET_Y_PC');
            this._shipStructureTypeLineWidth = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_LINEWIDTH_PC');
            this._shipStructureTypeColor = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_COLOR_PC');
            this._shipStructureTypeStrokeColor = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_STROKECOLOR_PC');
            this._shipStructureTypeHeight = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_FONT_HEIGHT_PC');
            this._shipStructureTypeFont = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_FONT_PC');
            this._shipStructureTypeOffsetX = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_OFFSET_X_PC');
            this._shipStructureTypeOffsetY = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_OFFSET_Y_PC');
            this._shipArmorTypeLargeFontHeight = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_LARGE_FONT_HEIGHT_PC');
            this._shipArmorTypeLargeFont = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_LARGE_FONT_PC');
            this._shipArmorTypeLargeOffsetX = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_LARGE_OFFSET_X_PC');
            this._shipArmorTypeLargeOffsetY = this.factoryConstants.get('DECK_COMPARTMENT_ARMOR_TYPE_LARGE_OFFSET_Y_PC');
            this._shipShieldTypeLargeFontHeight = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_LARGE_FONT_HEIGHT_PC');
            this._shipShieldTypeLargeFont = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_LARGE_FONT_PC');
            this._shipShieldTypeLargeOffsetX = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_LARGE_OFFSET_X_PC');
            this._shipShieldTypeLargeOffsetY = this.factoryConstants.get('DECK_COMPARTMENT_SHIELD_TYPE_LARGE_OFFSET_Y_PC');
            this._shipDamageTypeLargeFontHeight = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_LARGE_FONT_HEIGHT_PC');
            this._shipDamageTypeLargeFont = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_LARGE_FONT_PC');
            this._shipDamageTypeLargeOffsetX = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_LARGE_OFFSET_X_PC');
            this._shipDamageTypeLargeOffsetY = this.factoryConstants.get('DECK_COMPARTMENT_DAMAGE_TYPE_LARGE_OFFSET_Y_PC');
            this._shipStructureTypeLargeFontHeight = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_LARGE_FONT_HEIGHT_PC');
            this._shipStructureTypeLargeFont = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_LARGE_FONT_PC');
            this._shipStructureTypeLargeOffsetX = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_LARGE_OFFSET_X_PC');
            this._shipStructureTypeLargeOffsetY = this.factoryConstants.get('DECK_COMPARTMENT_STRUCTURE_TYPE_LARGE_OFFSET_Y_PC');
            this._shipLargeTitleSectionWidth = this.factoryConstants.get('DECK_COMPARTMENT_TITLE_LARGE_SECTION_WIDTH_PC');
            this._shipTitleLargeFontSize = this.factoryConstants.get('DECK_COMPARTMENT_TITLE_LARGE_FONT_SIZE_PC');
            this._shipTitleLargeFontFamily = this.factoryConstants.get('DECK_COMPARTMENT_TITLE_LARGE_FONT_FAMILY_PC');
            this._shipTitleLargeStartX = this.factoryConstants.get('DECK_COMPARTMENT_TITLE_LARGE_START_X_PC');
            this._shipTitleLargeStartY = this.factoryConstants.get('DECK_COMPARTMENT_TITLE_LARGE_START_Y_PC');
            this._shipSubTitleLargFontSize = this.factoryConstants.get('DECK_COMPARTMENT_SUBTITLE_LARGE_FONT_SIZE_PC');
            this._shipSubTitleLargeFontFamily = this.factoryConstants.get('DECK_COMPARTMENT_SUBTITLE_LARGE_FONT_FAMILY_PC');
            this._conflictShieldStatusX = this.factoryConstants.get('DECK_CONFLICT_SHIELD_TYPE_SMALL_START_X_PC');
            this._conflictShieldStatusY = this.factoryConstants.get('DECK_CONFLICT_SHIELD_TYPE_SMALL_START_Y_PC');
            this._conflictDamageStatusX = this.factoryConstants.get('DECK_CONFLICT_DAMAGE_TYPE_SMALL_START_X_PC');
            this._conflictDamageStatusY = this.factoryConstants.get('DECK_CONFLICT_DAMAGE_TYPE_SMALL_START_Y_PC');
            this._conflictArmorStatusX = this.factoryConstants.get('DECK_CONFLICT_ARMOR_TYPE_SMALL_START_X_PC');
            this._conflictArmorStatusY = this.factoryConstants.get('DECK_CONFLICT_ARMOR_TYPE_SMALL_START_Y_PC');
            this._conflictStructureStatusX = this.factoryConstants.get('DECK_CONFLICT_STRUCTURE_TYPE_SMALL_START_X_PC');
            this._conflictStructureStatusY = this.factoryConstants.get('DECK_CONFLICT_STRUCTURE_TYPE_SMALL_START_Y_PC');
            this._loginFacebookButtonPosX = this.factoryConstants.get('LOGIN_FACEBOOK_BUTTON_POS_X_PC');
            this._loginFacebookButtonPosY = this.factoryConstants.get('LOGIN_FACEBOOK_BUTTON_POS_Y_PC');
            this._loginGoogleButtonPosX = this.factoryConstants.get('LOGIN_GOOGLE_BUTTON_POS_X_PC');
            this._loginGoogleButtonPosY = this.factoryConstants.get('LOGIN_GOOGLE_BUTTON_POS_Y_PC');
            this._loginGoogleButtonWidth = this.factoryConstants.get('LOGIN_GOOGLE_BUTTON_WIDTH_PC');
            this._loginGoogleButtonHeight = this.factoryConstants.get('LOGIN_GOOGLE_BUTTON_HEIGHT_PC');
            this._loginGoogleButtonLabelX = this.factoryConstants.get('LOGIN_GOOGLE_BUTTON_LABEL_X_PC');
            this._loginGoogleButtonLabelY = this.factoryConstants.get('LOGIN_GOOGLE_BUTTON_LABEL_Y_PC');
            this._loginGoogleFontHeight = this.factoryConstants.get('LOGIN_GOOGLE_BUTTON_FONT_HEIGHT_PC');
            this._loginFacebookButtonLabelX = this.factoryConstants.get('LOGIN_FACEBOOK_BUTTON_LABEL_X_PC');
            this._loginFacebookButtonLabelY = this.factoryConstants.get('LOGIN_FACEBOOK_BUTTON_LABEL_Y_PC');
            this._loginFacebookFontHeight = this.factoryConstants.get('LOGIN_FACEBOOK_BUTTON_FONT_HEIGHT_PC');
            this._loginFacebookButtonWidth = this.factoryConstants.get('LOGIN_FACEBOOK_BUTTON_WIDTH_PC');
            this._loginFacebookButtonHeight = this.factoryConstants.get('LOGIN_FACEBOOK_BUTTON_HEIGHT_PC');
            this._loginTitle1FontHeight = this.factoryConstants.get('LOGIN_TITLE1_FONT_HEIGHT_PC');
            this._loginTitle1PosX = this.factoryConstants.get('LOGIN_TITLE1_POS_X_PC');
            this._loginTitle1PosY = this.factoryConstants.get('LOGIN_TITLE1_POS_Y_PC');
            this._loginButtonPosX = this.factoryConstants.get('LOGIN_BUTTON_POS_X_PC');
            this._loginButtonPosY = this.factoryConstants.get('LOGIN_BUTTON_POS_Y_PC');
            this._loginButtonWidth = this.factoryConstants.get('LOGIN_BUTTON_WIDTH_PC');
            this._loginButtonHeight = this.factoryConstants.get('LOGIN_BUTTON_HEIGHT_PC');
            this._loginErrorPosX = this.factoryConstants.get('LOGIN_ERROR_POS_X_PC');
            this._loginErrorPosY = this.factoryConstants.get('LOGIN_ERROR_POS_Y_PC');
            this._loginErrorWidth = this.factoryConstants.get('LOGIN_ERROR_WIDTH_PC');
            this._loginErrorHeight = this.factoryConstants.get('LOGIN_ERROR_HEIGHT_PC');
            this._loginErrorFontHeight = this.factoryConstants.get('LOGIN_ERROR_FONT_HEIGHT_PC');
            this._loginButtonLabelX = this.factoryConstants.get('LOGIN_BUTTON_LABEL_X_PC');
            this._loginButtonLabelY = this.factoryConstants.get('LOGIN_BUTTON_LABEL_Y_PC');
            this._loginButtonFontHeight = this.factoryConstants.get('LOGIN_BUTTON_FONT_HEIGHT_PC');
            this._loginRegisterButtonPosX = this.factoryConstants.get('LOGIN_REGISTER_BUTTON_POS_X_PC');
            this._loginRegisterButtonPosY = this.factoryConstants.get('LOGIN_REGISTER_BUTTON_POS_Y_PC');
            this._loginRegisterButtonLabelX = this.factoryConstants.get('LOGIN_REGISTER_BUTTON_LABEL_X_PC');
            this._loginRegisterButtonLabelY = this.factoryConstants.get('LOGIN_REGISTER_BUTTON_LABEL_Y_PC');
            this._loginRegisterButtonFontHeight = this.factoryConstants.get('LOGIN_REGISTER_BUTTON_FONT_HEIGHT_PC');
            this._usernameTextboxWidth = this.factoryConstants.get('LOGIN_USERNAME_TEXTBOX_WIDTH_PC');
            this._usernameTextboxHeight = this.factoryConstants.get('LOGIN_USERNAME_TEXTBOX_HEIGHT_PC');
            this._usernameTextboxPosX = this.factoryConstants.get('LOGIN_USERNAME_TEXTBOX_POS_X_PC');
            this._usernameTextboxPosY = this.factoryConstants.get('LOGIN_USERNAME_TEXTBOX_POS_Y_PC');
            this._usernameTextboxFontHeight = this.factoryConstants.get('LOGIN_USERNAME_TEXTBOX_FONT_HEIGHT_PC');
            this._passwordTextboxWidth = this.factoryConstants.get('LOGIN_PASSWORD_TEXTBOX_WIDTH_PC');
            this._passwordTextboxFontHeight = this.factoryConstants.get('LOGIN_PASSWORD_TEXTBOX_FONT_HEIGHT_PC');
            this._passwordTextboxHeight = this.factoryConstants.get('LOGIN_PASSWORD_TEXTBOX_HEIGHT_PC');
            this._passwordTextboxPosX = this.factoryConstants.get('LOGIN_PASSWORD_TEXTBOX_POS_X_PC');
            this._passwordTextboxPosY = this.factoryConstants.get('LOGIN_PASSWORD_TEXTBOX_POS_Y_PC');
            this._loginRegisterButtonWidth = this.factoryConstants.get('LOGIN_REGISTER_BUTTON_WIDTH_PC');
            this._loginRegisterButtonHeight = this.factoryConstants.get('LOGIN_REGISTER_BUTTON_HEIGHT_PC');
            this._menuNewButtonPosY = this.factoryConstants.get('MENU_NEW_BUTTON_POS_Y_PC');
            this._menuNewButtonPosX = this.factoryConstants.get('MENU_NEW_BUTTON_POS_X_PC');
            this._menuNewFontHeight = this.factoryConstants.get('MENU_NEW_FONT_HEIGHT_PC');
            this._menuLogoutButtonPosY = this.factoryConstants.get('MENU_LOGOUT_BUTTON_POS_Y_PC');
            this._menuLogoutButtonPosX = this.factoryConstants.get('MENU_LOGOUT_BUTTON_POS_X_PC');
            this._menuLogoutFontHeight = this.factoryConstants.get('MENU_LOGOUT_FONT_HEIGHT_PC');
            this._menuDeckButtonPosY = this.factoryConstants.get('MENU_DECK_BUTTON_POS_Y_PC');
            this._menuDeckButtonPosX = this.factoryConstants.get('MENU_DECK_BUTTON_POS_X_PC');
            this._menuDeckFontHeight = this.factoryConstants.get('MENU_DECK_FONT_HEIGHT_PC');            
            this._hubChallengePosX = this.factoryConstants.get('HUB_CHALLENGE_POSX_PC');
            this._hubChallengePosY = this.factoryConstants.get('HUB_CHALLENGE_POSY_PC');
            this._hubChallengeWidth = this.factoryConstants.get('HUB_CHALLENGE_WIDTH_PC');
            this._hubChallengeHeight = this.factoryConstants.get('HUB_CHALLENGE_HEIGHT_PC');
            this._hubFBInvitePosX = this.factoryConstants.get('HUB_FBINVITE_POSX_PC');
            this._hubFBInvitePosY = this.factoryConstants.get('HUB_FBINVITE_POSY_PC');
            this._hubFBInviteWidth = this.factoryConstants.get('HUB_FBINVITE_WIDTH_PC');
            this._hubFBInviteHeight = this.factoryConstants.get('HUB_FBINVITE_HEIGHT_PC');
            this._hubNewGamePosX = this.factoryConstants.get('HUB_NEWGAME_POSX_PC');
            this._hubNewGamePosY = this.factoryConstants.get('HUB_NEWGAME_POSY_PC');
            this._hubNewGameWidth = this.factoryConstants.get('HUB_NEWGAME_WIDTH_PC');
            this._hubNewGameHeight = this.factoryConstants.get('HUB_NEWGAME_HEIGHT_PC');
            this._hubGotoMenuPosX = this.factoryConstants.get('HUB_GOTOMENU_POSX_PC');
            this._hubGotoMenuPosY = this.factoryConstants.get('HUB_GOTOMENU_POSY_PC');
            this._hubGotoMenuWidth = this.factoryConstants.get('HUB_GOTOMENU_WIDTH_PC');
            this._hubGotoMenuHeight = this.factoryConstants.get('HUB_GOTOMENU_HEIGHT_PC');
            this._hubGotoMenuLabelPosY = this.factoryConstants.get('HUB_GOTOMENU_LABEL_POSY_PC');
            this._hubGotoMenuFontHeight = this.factoryConstants.get('HUB_GOTOMENU_FONT_HEIGHT_PC');
            this._hubHeaderHeight = this.factoryConstants.get('HUB_HEADER_HEIGHT_PC');
            this._hubHeaderFontHeight = this.factoryConstants.get('HUB_HEADER_FONT_HEIGHT_PC');
            this._hubHeaderPlayerLabelPosX = this.factoryConstants.get('HUB_HEADER_PLAYER_LABEL_POSX_PC');
            this._hubHeaderPlayerLabelPosY = this.factoryConstants.get('HUB_HEADER_PLAYER_LABEL_POSY_PC');
            this._hubListItemHeight = this.factoryConstants.get('HUB_LIST_ITEM_HEIGHT_PC');
            this._hubListItemWidth = this.factoryConstants.get('HUB_LIST_ITEM_WIDTH_PC');
            this._hubListItemPosX = this.factoryConstants.get('HUB_LIST_ITEM_POS_X_PC');
            this._hubListItemPosY = this.factoryConstants.get('HUB_LIST_ITEM_POS_Y_PC');
            this._hubListItemAlertHeight = this.factoryConstants.get('HUB_LIST_ITEM_ALERT_HEIGHT_PC');
            this._hubListItemAlertWidth = this.factoryConstants.get('HUB_LIST_ITEM_ALERT_WIDTH_PC');
            this._hubListItemAlertPosX = this.factoryConstants.get('HUB_LIST_ITEM_ALERT_POS_X_PC');
            this._hubListItemAlertPosY = this.factoryConstants.get('HUB_LIST_ITEM_ALERT_POS_Y_PC'); 
            this._hubListItemAlertFontHeight = this.factoryConstants.get('HUB_LIST_ITEM_ALERT_FONT_HEIGHT_PC');
            this._hubListItemAlertLabelPosX = this.factoryConstants.get('HUB_LIST_ITEM_ALERT_LABEL_POS_X_PC');
            this._hubListItemAlertLabelPosY = this.factoryConstants.get('HUB_LIST_ITEM_ALERT_LABEL_POS_Y_PC'); 
            this._hubListItemLabel1PosX = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_1_POS_X_PC');
            this._hubListItemLabel1PosY = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_1_POS_Y_PC');
            this._hubListItemLabel2PosX = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_2_POS_X_PC');
            this._hubListItemLabel2PosY = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_2_POS_Y_PC');
            this._hubListItemLabel3PosX = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_3_POS_X_PC');
            this._hubListItemLabel3PosY = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_3_POS_Y_PC');
            this._hubListItemLabel4PosX = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_4_POS_X_PC');
            this._hubListItemLabel4PosY = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_4_POS_Y_PC');
            this._hubListItemLabel5PosX = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_5_POS_X_PC');
            this._hubListItemLabel5PosY = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_5_POS_Y_PC');
            this._hubListItemLabel6PosX = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_6_POS_X_PC');
            this._hubListItemLabel6PosY = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_6_POS_Y_PC');
            this._hubListItemLabel1FontHeight = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_1_FONT_HEIGHT_PC');
            this._hubListItemLabel2FontHeight = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_2_FONT_HEIGHT_PC');
            this._hubListItemLabel3FontHeight = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_3_FONT_HEIGHT_PC');
            this._hubListItemLabel4FontHeight = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_4_FONT_HEIGHT_PC');
            this._hubListItemLabel5FontHeight = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_5_FONT_HEIGHT_PC');
            this._hubListItemLabel6FontHeight = this.factoryConstants.get('HUB_LIST_ITEM_LABEL_6_FONT_HEIGHT_PC');
            this._hubListItemActionPosX = this.factoryConstants.get('HUB_LIST_ITEM_ACTION_POS_X_PC');
            this._hubListItemActionPosY = this.factoryConstants.get('HUB_LIST_ITEM_ACTION_POS_Y_PC');
            this._hubListItemActionWidth = this.factoryConstants.get('HUB_LIST_ITEM_ACTION_WIDTH_PC');
            this._hubListItemActionHeight = this.factoryConstants.get('HUB_LIST_ITEM_ACTION_HEIGHT_PC');
            this._buttonDefaultWidth = this.factoryConstants.get('BUTTON_DEFAULT_WIDTH_PC');
            this._buttonDefaultHeight = this.factoryConstants.get('BUTTON_DEFAULT_HEIGHT_PC');
        }
    });


