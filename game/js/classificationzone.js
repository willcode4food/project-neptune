﻿pn.ClassificationZone = pn.Zone.extend('pn.ClassificationZone',
{

},
{
    shipClassIconEntity: null,
    init: function (player, zone) {
        this._super(player, zone);
    },
    load: function (board, layer, startX, startY,orientation) {
        this._super(board, layer, startX, startY, orientation);

        this.loadCardSprites();
        this.dealCards();
    },
    dealCards: function () {
        for (var i = 0; i < this.cards.length; i++) {
            this.cards[i].getComponent('spatial').pos.x += (i * this.settings.getUIAttributeSpaceX());
            this.cards[i].getComponent('spatial').pos.y += (i * this.settings.getUIAttributeSpaceY());
            this.cards[i].addTag('nozoom');
            this.cards[i].addTag('nodragdrop');
        }
    }
});