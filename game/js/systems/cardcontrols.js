﻿pn.systems.CardControls = pc.systems.DragAndDrop.extend('pn.systems.CardControls',
{},
{
    _isDoubleClick: false,
    thisClick: null,
    lastClick: null,
    unZoomedCardPosY: null,
    doubleClickThreshold: null,    
    settings: null,
    zoomedEntity: null,
    isPaused: false,
    timeoutID: 0,
    init: function () {
        this._super('[input]');
        this.constants = pc.device.game.__constants;
        this.settings = pc.device.game.gameDeviceSettings;
        
        this.doubleClickThreshold = this.constants.get('CARD_DOUBLE_CLICK_THRESHOLD');
    },
    onAction: function (actionName, event, pos, uiTarget) {        
        // Detect if drag and drop is disabled for this entity
        if (uiTarget !== undefined) {
            if (uiTarget.getEntity().hasTag('nodragdrop')) {
                // we want to disable the drag and drop ability on compartment cards.  
                // they are static and should never be moved
                this.dragdisabled = true;
            }
        }
        else {            
            return;
        }
        
        this._super(actionName, event, pos, uiTarget);        
        // check global to see if the game is zoomed.  Don't drag and drop if game is zoomed
        if (pc.device.game.isGameZoomed) {
            this.dragdisabled = true;
        }
        else {
            this.dragdisabled = false;
        }                

        /***********************************************************************************************
        *
        * Event Handlers
        *
        ************************************************************************************************/

        /*************  Left Mouse Click ***************************************************************/

        if (actionName === this.constants.get('DD_ACTION_MOUSE_LEFT_BUTTON_DOWN')) {
            // cancel delayed zoomed card view if we click the button to drag and drop
            if (pc.isValid(this.timeoutID)) {
                clearTimeout(this.timeoutID);
                this.timeoutID = 0;
            }
        }

        /*************  Mouse Wheel Down *************************************************************/

        if (actionName === this.constants.get('DD_ACTION_MOUSE_WHEEL_DOWN')) {
            // remove entity if we scroll zoom the entire game
            if (pc.isValid(this.zoomedEntity)) {
                this.zoomedEntity.remove();
                this.zoomedEntity = null;
            }
            this.isPaused = false;
        }
        
        /*************  Touch If Game is NOT Zoomed **************************************************/

        if (actionName === this.constants.get('DD_ACTION_MOUSE_LEFT_BUTTON_DOWN') && !pc.device.game.isGameZoomed) {
            // first check if card is zoomed in            
            //if we click on a card while one is not zoomed in, then we are attempting to drag and drop it.            
            clearTimeout(this.timeoutID);
            this.timeoutID = 0;
            this.isPaused = false;
            if (this.entity.hasTag('setonboard') && !this.isPaused) {
                this.isPaused = true;
                var that = this;
                this.timeoutID = setTimeout(function () { that.zoomForDisplay(that.entity) }, 1500);
            }        

            this.entity.addTag('clicking');
            // The 'clicking' tag is checked in the boardPhysics to identify a card that is being clicked on            
            if (this.entity.hasTag('setonboard')) {
                this.entity.removeTag('setonboard');
            }


        }

        /*************  Touch ***********************************************************************/
        if (actionName === this.constants.get('DD_ACTION_TOUCH')) {
            if (pc.isValid(this.timeoutID)) {
                clearTimeout(this.timeoutID);
                this.timeoutID = 0;
            }
            if (this.entity.hasTag('setonboard') && !this.isPaused) {
                this.isPaused = true;
                var that = this;
                this.timeoutID = setTimeout(function () { that.zoomForDisplay(that.entity) }, 1500);                
            }
            else if (!this.isPaused && !pc.device.game.isGameZoomed) {                
                this.zoomForDisplay(this.entity);
            }
        }
        if (actionName === this.constants.get('DD_ACTION_TOUCH_MOVE')) {
            this.entity.addTag('clicking');
            // The 'clicking' tag is checked in the boardPhysics to identify a card that is being clicked on            
            if (this.entity.hasTag('setonboard')) {
                this.entity.removeTag('setonboard');
            }
            this.entity.getComponent('carddata').pos.x = pos.x;
            this.entity.getComponent('carddata').pos.y = pos.y;
            if (this.timeoutID !== 0) {
                clearTimeout(this.timeoutID);
                this.timeoutID = 0;
                this.isPaused = false;
            }
            if(this.zoomedEntity !== null) {
                // on touch only want zoomed cleared if not a compartment card
                if (this.entity.getComponent('carddata').cardType !== 'Compartment') {
                    if (this.entity.getComponent('carddata').ID == this.zoomedEntity.getComponent('carddata').ID) {
                        this.zoomedEntity.remove();
                        this.zoomedEntity = null;
                        }
                    }
            }


        }
        if (actionName === this.constants.get('DD_ACTION_TOUCH_END')) {      
            if (!this.entity.hasTag('hide')) {
                this.resetBoardTile();
                if (this.entity.hasTag('clicking')) {
                    this.entity.removeTag('clicking');
                }
                if (!this.entity.hasTag('setonboard') && !this.entity.hasTag('nodragdrop')) {
                    this.setCardOnTile();
                    }
                if (!pc.valid(this.entity.getComponent('carddata').tileOver) && this.entity.getComponent('carddata').cardType !== 'Compartment') {
                    this.entity.addTag('nozoom');
                    }

                if(this.zoomedEntity !== null) {

                    if (this.entity.getComponent('carddata').ID == this.zoomedEntity.getComponent('carddata').ID) {
                            this.zoomedEntity.remove();
                            this.zoomedEntity = null;                                                }
                    }
                this.isPaused = false;
                }
        }

        
       /*************  Touch End or Mouse Button Up If Game is NOT Zoomed ****************************/

       if (actionName === this.constants.get('DD_ACTION_MOUSE_LEFT_BUTTON_UP') && !pc.device.game.isGameZoomed) {
           // Communicate with other systems that we are not clicking the card            
           if (!this.entity.hasTag('hide')) {
                this.resetBoardTile();
                if (this.entity.hasTag('clicking')) {
                    this.entity.removeTag('clicking');
                }            
                if (!this.entity.hasTag('setonboard') && !this.entity.hasTag('nodragdrop')) {
                    this.setCardOnTile();
                }
                if (!pc.valid(this.entity.getComponent('carddata').tileOver) && this.entity.getComponent('carddata').cardType !== 'Compartment') {
                    this.entity.addTag('nozoom');
                }
                
                if (this.zoomedEntity !== null) {
                    
                    if (this.entity.getComponent('carddata').ID == this.zoomedEntity.getComponent('carddata').ID) {                        
                            this.zoomedEntity.remove();
                            this.zoomedEntity = null;                        
                    }
                }
                this.isPaused = false;
           }
       }
        
       /*************  Mouse or Touch Move (e.g. dragging) *******************************************/

       if (actionName === this.constants.get('DD_ACTION_MOUSE_MOVE') && !pc.device.game.isGameZoomed) {
           //attach mouse position to entity.  Do this for the boardhysics system to flip tiles      
                this.entity.getComponent('carddata').pos.x = pos.x;
                this.entity.getComponent('carddata').pos.y = pos.y;
                if (this.timeoutID !== 0) {
                    clearTimeout(this.timeoutID);
                    this.timeoutID = 0;
                    this.isPaused = false;
                }
                if (this.zoomedEntity !== null) {
                    // on touch only want zoomed cleared if not a compartment card
                    if (this.entity.getComponent('carddata').cardType !== 'Compartment') {
                        if (this.entity.getComponent('carddata').ID == this.zoomedEntity.getComponent('carddata').ID) {
                            this.zoomedEntity.remove();
                            this.zoomedEntity = null;                            
                        }
                    }
                    
                }
                
       }


       /*************  Mouse Over **********************************************/

       if (actionName === this.constants.get('DD_ACTION_MOUSE_OVER')) {
           if (!this.isInputState(this.entity, this.constants.get('DD_STATE_MOUSE_CLICK'))) {
               if (this.entity.hasTag('setonboard') && !this.isPaused) {
                           this.isPaused = true;
                           var that = this;
                           this.timeoutID = setTimeout(function () { that.zoomForDisplay(that.entity) }, 1500);
               }
               else if(!this.isPaused && !pc.device.game.isGameZoomed) {
                   this.zoomForDisplay(this.entity);
               }
           }
       }

       /*************  Mouse Out  ********************************************************************/

       if (actionName === this.constants.get('DD_ACTION_MOUSE_OUT')) {
           if ((!pc.device.isiPhone && !pc.device.isiPhone4 && !pc.device.isiPad && !pc.device.isAndroid)) {
               if (this.zoomedEntity !== null) {
                   if (this.entity.getComponent('carddata').ID == this.zoomedEntity.getComponent('carddata').ID) {                       
                       this.zoomedEntity.remove();
                       this.zoomedEntity = null;
                   }
                   this.isPaused = false;
               }
           }          
       }       
    },

    setCardOnTile: function () {        
        if (this.entity.getComponent('carddata').tileOver !== null && this.entity.getComponent('carddata').tileOver !== undefined) {
            var xOffset = (this.settings.getPlayfieldTileWidth() - this.entity.getComponent('carddata').cardWidth) / 2;
            this.entity.getComponent('spatial').pos.x = this.entity.getComponent('carddata').tileOver.screenXpos + xOffset;
            this.entity.getComponent('spatial').pos.y = this.entity.getComponent('carddata').tileOver.screenYpos;
            this.entity.addTag('setonboard');
            this.entity.removeTag('nozoom');
            if ((pc.device.isiPhone || pc.device.isiPhone4 || pc.device.isiPad || pc.device.isAndroid) && this.entity.getComponent('carddata').imageMin !== '') {
                this.setSmallSprite();
                this.setSmallText();
            }
        }
        else {
            this.moveToHomeZone();
            if ((pc.device.isiPhone || pc.device.isiPhone4 || pc.device.isiPad || pc.device.isAndroid) && this.entity.getComponent('carddata').imageMin !== '') {
                this.setMinSprite();
                this.setMinText();
            }
        }
       
    },
    resetBoardTile: function () {
        if (this.entity.getComponent('carddata').tileOver !== null && this.entity.getComponent('carddata').tileOver !== undefined && this.entity.getComponent('carddata').playFieldMap !== null) {
            this.entity.getComponent('carddata').playFieldMap.setTile(this.entity.getComponent('carddata').tileOver.x, this.entity.getComponent('carddata').tileOver.y, 0);
            this.entity.getComponent('carddata').playFieldMap = null;
            if (!this.entity.hasTag('nozoom')) {
                this.entity.addTag('nozoom')
            }
        }
    },   
    zoomForDisplay: function (entity) {
        // removing and re-adding the scale component
        // this seems to be the only way to reproduce the same scal effect over and over.
        if (pc.valid(entity)) {
            if (!entity.hasTag('hide')) {
                //if (this.zoomedEntity === null && !pc.device.game.isGameZoomed && this.isInputState(entity, this.constants.get('DD_STATE_MOUSE_MOVE'))) {
                
                if (this.zoomedEntity === null && !pc.device.game.isGameZoomed) {

                    this.zoomedEntity = pc.Entity.create(entity.layer);
                    this.zoomedEntity.addTag('nozoom');
                    this.setLargeSprite(entity);
                    this.setLargeText(this.zoomedEntity);
                    this.isPaused = false;
                }
            }
        }
    },   
    setLargeSprite: function (entity) {
        // Uses the cardData component to get the imageZoomed image file 
        var spriteSheetZoomed = new pc.SpriteSheet({
            image: pc.device.loader.get(this.entity.getComponent('carddata').imageZoomed).resource,
            frameWidth: pc.device.loader.get(this.entity.getComponent('carddata').imageZoomed).resource.width,
            frameHeight: pc.device.loader.get(this.entity.getComponent('carddata').imageZoomed).resource.height
        }), spriteZoomed = new pc.Sprite(spriteSheetZoomed), x = 0, y = 0;

        //this.zoomedEntity.removeComponentByType('sprite');
        this.zoomedEntity.addComponent(pc.components.Sprite.create(spriteZoomed));
        if (entity.hasTag('setonboard')) {
            x = entity.getComponent('spatial').pos.x + entity.getComponent('spatial').dim.x + 5;
            y = (entity.getComponent('spatial').pos.y - (entity.getComponent('carddata').cardHeightZoomed / 2)) + (entity.getComponent('spatial').dim.y / 2);
            if (y < 0) {
                y += 80;
            }
        }
        else {
            x = entity.getComponent('carddata').cardPosXZoomed;
            y = entity.getComponent('carddata').cardPosYZoomed;
        }
        this.zoomedEntity.addComponent(pc.components.Spatial.create({
            x: x,
            y: y,
            w: entity.getComponent('carddata').cardWidthZoomed,
            h: entity.getComponent('carddata').cardHeightZoomed,
            zoom: 1
        }));

        this.zoomedEntity.addComponent(entity.getComponent('carddata'));
    },
    isDoubleClick: function () {
        // checks timing between clicks to see if we have a double click
        // uses a milisecond threshhold constants that can be tweeked
        this.thisClick = new Date().getTime();
        this._isDoubleClick = this.thisClick - this.lastClick < this.doubleClickThreshold;
        this.lastClick = this.thisClick;
        return this._isDoubleClick;  
    },
    setMinSprite: function () {
        var spriteSheet = new pc.SpriteSheetScroller({
            image: pc.device.loader.get(this.entity.getComponent('carddata').imageMin).resource,
            frameWidth: pc.device.loader.get(this.entity.getComponent('carddata').imageMin).resource.width,
            frameHeight: pc.device.loader.get(this.entity.getComponent('carddata').imageMin).resource.height,
            originX: this.settings.getShipOriginXPos(),
            originY: this.settings.getShipOriginYPos(),
            contentHeight: this.settings.getBaseDeviceHeight(),
            contentWidth: this.settings.getBaseDeviceWidth()
        }),
        sprite = new pc.Sprite(spriteSheet);
        if (this.entity.hasComponentOfType('sprite')) {
            this.entity.removeComponentByType('sprite');
        }
        this.entity.addComponent(pc.components.Sprite.create(sprite));
        this.entity.getComponent('spatial').dim.x = pc.device.loader.get(this.entity.getComponent('carddata').imageMin).resource.width;
        this.entity.getComponent('spatial').dim.y = pc.device.loader.get(this.entity.getComponent('carddata').imageMin).resource.height;

    },
    setMinText: function () {
        if (this.entity.hasComponentOfType('textLayout')) {
            this.entity.removeComponentByType('textLayout');
        }
        var layout = new CardTextLayoutFactory(this.entity.getComponent('carddata').cardType);
        this.entity.addComponent(pc.components.TextLayout.create({
            layout: layout.getMinLayout(
                0,
                0,
                this.entity.getComponent('carddata').armor,
                this.entity.getComponent('carddata').structure)
        }));
        delete layout;
    },
    setSmallSprite: function () {
        var spriteSheet = new pc.SpriteSheetScroller({
            image: pc.device.loader.get(this.entity.getComponent('carddata').image).resource,
            frameWidth: pc.device.loader.get(this.entity.getComponent('carddata').image).resource.width,
            frameHeight: pc.device.loader.get(this.entity.getComponent('carddata').image).resource.height,
            originX: this.settings.getShipOriginXPos(),
            originY: this.settings.getShipOriginYPos(),
            contentHeight: this.settings.getBaseDeviceHeight(),
            contentWidth: this.settings.getBaseDeviceWidth()
        }),
        sprite = new pc.Sprite(spriteSheet);
        if (this.entity.hasComponentOfType('sprite')) {
            this.entity.removeComponentByType('sprite');
        }
        this.entity.addComponent(pc.components.Sprite.create(sprite));
        this.entity.getComponent('spatial').dim.x = pc.device.loader.get(this.entity.getComponent('carddata').image).resource.width;
        this.entity.getComponent('spatial').dim.y = pc.device.loader.get(this.entity.getComponent('carddata').image).resource.height;

    },
    setSmallText: function () {
        if (this.entity.hasComponentOfType('textLayout')) {
            this.entity.removeComponentByType('textLayout');
        }
        var layout = new CardTextLayoutFactory(this.entity.getComponent('carddata').cardType);
        this.entity.addComponent(pc.components.TextLayout.create({
            layout: layout.getSmallLayout(
                0,
                0,
                this.entity.getComponent('carddata').armor,
                this.entity.getComponent('carddata').structure)
        }));
        delete layout;
     },
    setLargeText: function (entity) {
        if (entity.hasComponentOfType('textLayout')) {
            entity.removeComponentByType('textLayout');
        }
        var layout = new CardTextLayoutFactory(entity.getComponent('carddata').cardType);
        entity.addComponent(pc.components.TextLayout.create({
            layout: layout.getLargeLayout({
                cardName:  entity.getComponent('carddata').name,
                cardClass:  entity.getComponent('carddata').cardClass,
                subtitle:  entity.getComponent('carddata').subtitle,
                statusEffect: entity.getComponent('carddata').statusEffect,
                leadership: entity.getComponent('carddata').leadership,
                fleetCommand: entity.getComponent('carddata').fleetCommand,
                description: entity.getComponent('carddata').description,
                shieldStatus:  entity.getComponent('carddata').shieldStatus,
                damageStatus:  entity.getComponent('carddata').damageStatus,
                armor:  entity.getComponent('carddata').armor,
                structure:  entity.getComponent('carddata').structure,
                rangeMin:  entity.getComponent('carddata').rangeMin,
                rangeMax:  entity.getComponent('carddata').rangeMax,
                accuracy:  entity.getComponent('carddata').accuracy,
                damage:  entity.getComponent('carddata').damage,
                power:  entity.getComponent('carddata').power,
                restPower:  entity.getComponent('carddata').restPower
            })
        }));
        delete layout;
    },
    moveToHomeZone: function(){
        if(this.entity.hasComponentOfType('mover'))
        {
            this.entity.removeComponentByType('mover');
        }
        if (!this.entity.hasTag('hide')) {
            
            this.entity.addTag('hide');
            
        }
        this.entity.addComponent(pc.components.Mover.create(
                    {
                        targetPos: {
                            x: this.entity.getComponent('carddata').zonePosX,
                            y: this.entity.getComponent('carddata').zonePosY
                        },
                        easing: pc.Easing.EXPONENTIAL_OUT,
                        duration: 300,
                        onComplete: this.resetZonePos(this.entity)
                    }));
    },
    resetZonePos: function (entity) {
        if (entity.hasTag('hide')) {
            // slight delay while card is moving
            // the onComplete seems to fire too quickly
            setTimeout(function () { entity.removeTag('hide') }, 200);
        }
        
    },
    process: function(entity) {
        this._super(entity);      
    }
});