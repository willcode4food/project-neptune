﻿
pn.systems.BoardPhysics = pc.systems.Physics.extend('BoardPhysics',
    {},
    {
        playFieldMap: null,
        settings: null,
        tileOver: null,

        init: function (options) {
            this._super(options);
            this.playFieldMap = options.tileCollisionMap.tileMap;
            this.settings = pc.device.game.gameDeviceSettings;
            
            this.tileOver = {
                tileType: 0,
                x: this.settings.getPlayfieldStartXPos(),
                y: this.settings.getPlayfieldStartYPos(),                

            };
            var screenPos = this.getPositionFromTile(this.tileOver);
            this.tileOver.screenXpos = screenPos.x
            this.tileOver.screenYpos = screenPos.y;            
        },

        onCollision: function (aType, bType, entityA, entityB, force,
           fixtureAType, fixtureBType, contact) {   
        },

        onCollisionStart: function (aType, bType, entityA, entityB,
            fixtureAType, fixtureBType, contact) {
            if (entityA.hasTag('clicking')) {
                
                this.highlightPlayfieldTile(
                {
                    position: {
                        x: entityA.getComponent('carddata').pos.x,
                        y: entityA.getComponent('carddata').pos.y,
                    },
                    entity: entityA
                });
                return;
            }
        },

        onCollisionEnd: function (aType, bType, entityA, entityB,
            fixtureAType, fixtureBType, contact) {            
            if (entityA.hasTag('clicking')) {
                this.highlightPlayfieldTile(
                {
                    position: {
                        x: entityA.getComponent('carddata').pos.x,
                        y: entityA.getComponent('carddata').pos.y,
                    },
                    entity: entityA
                });
                return;
            }
        },
        addTileCollisionMap: function (tileMap, collisionGroup, collisionCategory, collisionMask) {
            // Generate a set of rectangles (polys) for the tiles. To make things more efficient
            // we pack tiles horizontally across to reduce the total number of physics fixtures being
            // added.

            // Changed this paradigm of efficiency because we want to use physics as a sensor only
            // And we want to track which tile the pointer is over.  So to do this, we lay a fixture on every tile

            for (var ty = 0; ty < tileMap.tilesHigh; ty++) {
                // new row, start again
                var x = 0;
                var w = 0;

                for (var tx = 0; tx < tileMap.tilesWide; tx++) {
                    if (tileMap.tiles[ty][tx] >= 0) {
                         w += tileMap.tileWidth;
                    }
                        // we found a gap, so create the physics body for his horizontal tile set
                        if (w > 0) {
                            this.createStaticBody(x - pc.device.game.gameDeviceSettings.getPlayfieldOriginX(), ty * tileMap.tileHeight - pc.device.game.gameDeviceSettings.getPlayfieldOriginY(), w,
                                tileMap.tileWidth, collisionGroup, collisionCategory, collisionMask);
                            w = 0;                        
                        }
                        // set the starting x position for the next rectangle
                        x = ((tx + 1) * tileMap.tileWidth);
                    }
                }       
        },
        highlightPlayfieldTile: function (options) {
            
            var xPos = pc.Math.getDescaledPosition(options.position.x, 'x'),
            yPos = pc.Math.getDescaledPosition(options.position.y, 'y'),
            tilePos = this.getTileFromPosition(xPos + this.settings.getPlayfieldOriginX(), yPos + this.settings.getPlayfieldOriginY()),
            tile = this.playFieldMap.getTile(tilePos.x, tilePos.y),
            tilePixelCoords = null;
            // we are touching a tile in the sprite sheet with a sprite associated
            if (!options.entity.hasTag('zoomedin')) {
                if (tile >= 0) {
                    
                        // the mouse is over a playfield tile, as it has our property                                        
                        // Set the tile to a new sprite in the tile.  Assumes the new sprite is 30 tiles away
                        this.playFieldMap.setTile(tilePos.x, tilePos.y, 1);
                        if (((this.tileOver.x !== tilePos.x) && (this.tileOver.x >= 0)) || ((this.tileOver.y !== tilePos.y) && (this.tileOver.y >= 0))) {
                            //  change it back to its previous sprite when we move the touch off the tile
                            this.playFieldMap.setTile(this.tileOver.x, this.tileOver.y, 0);
                        }
                        //store the tile we previously were on so we can reset it later
                        this.tileOver = { x: tilePos.x, y: tilePos.y };
                        tilePixelCoords = this.getPositionFromTile(this.tileOver);
                        this.tileOver.screenXpos = tilePixelCoords.x;
                        this.tileOver.screenYpos = tilePixelCoords.y;
                        options.entity.getComponent('carddata').tileOver = this.tileOver;
                        options.entity.getComponent('carddata').playFieldMap = this.playFieldMap;
                }
                else {                    
                   // reset the position.  This gets fired only when you move the entity off the board
                   if (((this.tileOver.x !== tilePos.x) && (this.tileOver.x >= 0)) || ((this.tileOver.y !== tilePos.y) && (this.tileOver.y >= 0))) {
                        this.playFieldMap.setTile(this.tileOver.x, this.tileOver.y, 0);
                        options.entity.getComponent('carddata').tileOver = null;
                   }
                }
            }
        },        
        getTileFromPosition: function (x, y) {
            // reusable function to get the x, y coordinates of the tile in the sprite sheet, 
            // translates screen x, y coordinants to spritesheet x,y coordinants.
            var tileXpos = Math.floor(x / this.playFieldMap.tileWidth),
                tileYpos = Math.floor(y / this.playFieldMap.tileHeight);
            
            return { x: tileXpos, y: tileYpos };
        },
        getPositionFromTile: function (tile) {
            var pixelXpos = Math.floor(tile.x * this.playFieldMap.tileWidth + (this.settings.getPlayfieldOriginX() * -1)),
                pixelYpos = Math.floor(tile.y * this.playFieldMap.tileHeight + this.settings.getPlayfieldOriginY() * -1);
            return { x: pixelXpos, y: pixelYpos };
        },
        process: function (entity) {
            this._super(entity);           
        }
    });