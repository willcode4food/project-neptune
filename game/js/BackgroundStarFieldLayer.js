﻿/**
 *   Project Neptune - (C)2013 ThrottleNet Inc.
 *   Author: Marc D. Arbesman
 */

/**
 * Class: BackgroundStarFieldLayer
 * Extends: pc.TileLayer
 * Description:  Creates the parallax star field simulation.  Based on the Martin Well's
 * example, found at http://forum.playcraftlabs.com/viewtopic.php?f=4&t=58. 
 * Generates a random star field based on sprite sheet images 
 */

BackgroundStarFieldLayer = pc.TileLayer.extend('BackgroundStarFieldLayer',
    {   
    },
    {
        starFieldCanvas: null,
        type: null,
        bgWidth: null,        
        width: null,
        height: null,
        move: null,
        top: 0,
        left: 0,
        zoom: 1,
        zoomRows: 0,
        zoomCols: 0,
        zoomLastCols: 0,
        zoomLastRows: 0,
        startLeft: null,
        startTop: null,
        lastZoom: 1,
        lastLeft: 0,
        arrOverFlowTop: null,
        speed: null,


        init: function (options) {
            /// <summary>Initialization, and generation of the background layer</summary>
            /// <param name="type" type="integer">The type of layer for the star simulationn, related to the sprite sheet 1,2,3 </param>
            /// <param name="width" type="integer">Background Width</param>
            /// <param name="height" type="integer">Background Height></param>
            /// <param name="speed" type="decimal">Number of Tiles Wide</param>
            if (!pc.valid(options.type)) {
                throw 'The type of the background has not been specified';
            }
            if (!pc.valid(options.width)) {
                throw 'Width of the background has not been specified';
            }
            if (!pc.valid(options.height)) {
                throw 'Height of the background has not been specified';
            }
            if (!pc.valid(options.speed)) {
                throw 'Scrolling speed has not been specified';
            }
            // generate a starfield
            this.type = options.type;
            this.width = options.width;
            this.height = options.height;
            var tileSize = 512;   // big fat space tile width
            var spaceTiles = this.generateStarFieldTiles({
                                                          tileWidth: tileSize,
                                                          tileHeight: tileSize,
                                                          tilesWide: 4,
                                                          tilesHigh: 1
                                                        });
            var spaceSpriteSheet = new pc.SpriteSheet({
                image: spaceTiles, frameWidth: tileSize, frameHeight: tileSize,
                framesWide: 4, framesHigh: 1
            });

            // we need enough tiles to cover the entire map (plus some edges)
            var tilesWide = Math.ceil(this.width / tileSize) + 4;
            var tilesHigh = Math.ceil(this.height / tileSize) + 4;

            this.bgWidth = tilesWide * 512;
            var tileSet = new pc.TileSet(spaceSpriteSheet)
            var tileMap = new pc.TileMap([tileSet], tilesWide, tilesHigh, tileSize, tileSize);
            tileMap.generate(-1);

            for (var ty = 0; ty < tilesHigh; ty++)
                for (var tx = 0; tx < tilesWide; tx++)
                    tileMap.setTile(tx, ty, pc.Math.rand(0, 3));

            this._super('starfield', false, tileMap, tileSet);
            //this.left = 0;
            //this.top = 0;
            //this.zoom = 0;
            // the overflow array when the tile map is moved towards the top of the screen.
            this.arrOverFlowTop = [];
            this.speed = options.speed;
            this.zIndex = -1;
        },

        generateStarFieldTiles: function (options) {
            /// <summary>Creates the tiles from the sprite sheets</summary>
            /// <param name="type" type="Object">The type of layer for the star simulation</param>
            /// <param name="tileWidth" type="Object">Tile Width</param>
            /// <param name="tileHeight" type="Object"Tile Height></param>
            /// <param name="tilesWide" type="Object">Number of Tiles Wide</param>
            /// <param name="tilesHigh" type="Object">Number of Tiles High</param>
            if (!pc.valid(options.tileWidth)) {
                throw exception('Tile width is not specified');
            }
            if (!pc.valid(options.tileHeight)) {
                throw exception('Tile height is not specified');
            }
            if (!pc.valid(options.tilesWide)) {
                throw exception('Number of tiles wide is not specified');
            }
            if (!pc.valid(options.tilesHigh)) {
                throw exception('Number of tiles high is not specified');
            }
            var stars1 = pc.device.loader.get('stars1').resource;
            var stars2 = pc.device.loader.get('stars2').resource;
            var stars3 = pc.device.loader.get('stars3').resource;

            var starSpriteSheet1 = new pc.SpriteSheet({ image: stars1, frameWidth: 20, frameHeight: 20, framesWide: 2, framesHigh: 3 });
            var starSpriteSheet2 = new pc.SpriteSheet({ image: stars2, frameWidth: 20, frameHeight: 20, framesWide: 4, framesHigh: 3 });
            var starSpriteSheet3 = new pc.SpriteSheet({ image: stars3, frameWidth: 20, frameHeight: 20, framesWide: 2, framesHigh: 3 });
            
            this.starFieldCanvas = document.createElement('canvas');

            this.starFieldCanvas.width = options.tileWidth * options.tilesWide;
            this.starFieldCanvas.height = options.tileHeight * options.tilesHigh;
     

            var ctx = this.starFieldCanvas.getContext('2d');
       
            if (this.type == 0) {
                
                
                ctx.fillStyle = '#3d2669';
                ctx.fillRect(0, 0, this.starFieldCanvas.width, this.starFieldCanvas.height);
            }

            for (var ty = 0; ty < options.tilesHigh; ty++) {
                for (var tx = 0; tx < options.tilesWide; tx++) {
                    var originX = tx * options.tileWidth;
                    var originY = ty * options.tileHeight;

                    // create a first layer of dense but distant (smaller and faded) stars
                    switch (this.type) {
                        case 0:
                            this.generateImageField(ctx, originX, originY, options.tileWidth, options.tileHeight,
                               starSpriteSheet1, 10, 0.1, 1.0, 0);
                            break;

                        case 1:
                            this.generateImageField(ctx, originX, originY, options.tileWidth, options.tileHeight,
                                starSpriteSheet2, 20, 0.25, 0.5, 0);                            
                            break;
                        case 2:
                            this.generateImageField(ctx, originX, originY, options.tileWidth, options.tileHeight,                                
                                starSpriteSheet3, 30, 0.24, 0.5, 0);
                            break;
                    }
                }
            }

            return new pc.CanvasImage(this.starFieldCanvas);
        },

        generateImageField: function (ctx, originX, originY, width, height, spriteSheet, spread, alphaLow, alphaHigh, leapDistance) {
            var nextIncX = 1;
            var nextIncY = 1;

            for (var y = originY; y < originY + height; y += nextIncY) {
                for (var x = originX; x < originX + width; x += nextIncX) {
                    ctx.globalAlpha = pc.Math.randFloat(alphaLow, alphaHigh);

                    var px = x + pc.Math.rand(0, spread);
                    var py = y + pc.Math.rand(0, spread);
                    var fx = pc.Math.rand(0, spriteSheet.framesWide - 1);
                    var fy = pc.Math.rand(0, spriteSheet.framesHigh - 1);

                    // make sure we don't draw something over the edge of the canvas (it'll get cutoff
                    // when tiled and look choppy, not nice and seamless and beautiful, like my girlfriend)
                    if (pc.Math.isRectInRect(px, py, spriteSheet.frameWidth, spriteSheet.frameHeight,
                        originX, originY, width, height))
                        spriteSheet.drawFrame(ctx, fx, fy, px, py);

                    nextIncX = pc.Math.rand(spread - (spread / 2), spread + (spread / 2));
                    if (pc.Math.rand(leapDistance / 2, leapDistance) < leapDistance / 4)
                        nextIncX += leapDistance;
                }
                nextIncY = pc.Math.rand(spread - (spread / 2), spread + (spread / 2));
                if (pc.Math.rand(0, leapDistance) < leapDistance / 2)
                    nextIncY += leapDistance;
            }
        },
        checkToMoveBehind: function () {
            if (this.origin.x >= this.bgWidth) {
                this.setOrigin((-1 * this.bgWidth + 50), 0);
                
            }            
        },
        drawTiled: function () {
            var tileWidth = this.tileMap.tileWidth,
           tileHeight = this.tileMap.tileHeight ,
           // the properties currCanvasWidth and currCanvasHeight are custom to my game.
           // This will need to be changed to pc.device.game.canvasWidth and pc.device.game.canvasHeight
           // adding support for resizing the browser.
           contentWidth = pc.device.canvas.width,
           contentHeight = pc.device.canvas.height,
           clientWidth = pc.device.game.currCanvasWidth,
           clientHeight = pc.device.game.currCanvasHeight,
           maxRows = this.tileMap.tilesHigh,
           maxCols = this.tileMap.tilesWide;
           

            // Compute starting rows/columns and support out of range scroll positions
            this.startRow = Math.max(Math.floor(this.top / tileHeight), 0);
            this.startCol = Math.max(Math.floor(this.left / tileWidth), 0);
            // Compute maximum rows/columns to render for content size      


            // Compute initial render offsets
            // 1. Positive scroll position: We match the starting rows/tile first so we
            //    just need to take care that the half-visible tile is fully rendered
            //    and placed partly outside.
            // 2. Negative scroll position: We shift the whole render context
            //    (ignoring the tile dimensions) and effectively reduce the render
            //    dimensions by the scroll amount.
            this.startTop = 0;
            this.startLeft = 0;
            this.startTop = this.top >= 0 ? -this.top % tileHeight : -this.top;
            this.startLeft = this.left >= 0 ? -this.left % tileWidth : -this.left;
            this.zoomLastCols = this.zoomCols;
            this.zoomLastRows = this.zoomRows;

            if (this.debugTileMap) {
                pc.device.ctx.fillText('Zoom Rows: ' + this.zoomRows + ' Last Zoom: ' + this.zoomLastRows, 50, 510);
                pc.device.ctx.fillText('Zoom Cols: ' + this.zoomCols + ' Last Zoom: ' + this.zoomLastCols, 50, 520);
                pc.device.ctx.fillText('start top: ' + this.startTop, 50, 530);
                pc.device.ctx.fillText('start left: ' + this.startLeft, 50, 540);
                pc.device.ctx.fillText('left: ' + this.left, 50, 550);
                pc.device.ctx.fillText('top: ' + this.top, 50, 560);
                pc.device.ctx.fillText('zoom: ' + this.zoom, 50, 570);
                pc.device.ctx.fillText('shifted up: ' + this.shiftedUp, 50, 580);
                pc.device.ctx.fillText('shifted left: ' + this.shiftedLeft, 50, 590);
            }
            
            // Compute number of rows to render            
            this.zoomRows = this.tileMap.tilesHigh;

            if ((this.top % tileHeight) > 0) {
                this.zoomRows += 1;
            }

            if ((this.startTop + (this.zoomRows * tileHeight)) < clientHeight) {
                this.zoomRows += 1;
            }

            // Compute number of columns to render            
            this.zoomCols = this.tileMap.tilesWide;

            if ((this.left % tileWidth) > 0) {
                this.zoomCols += 1;
            }

            if ((this.startLeft + (this.zoomCols * tileWidth)) < clientWidth) {
                this.zoomCols += 1;
            }

            // Limit rows/columns to maximum numbers
            this.zoomRows = Math.min(this.zoomRows, maxRows - this.startRow);
            this.zoomCols = Math.min(this.zoomCols, maxCols - this.startCol);

            if (this.zoomCols < this.zoomLastCols) {
                this.shiftTileMapLeft(1);
            }
            if (this.zoomCols > this.zoomLastCols && this.zoomLastCols !== 0) {
                this.shiftTileMapRight(1);
            }
            if (this.zoomRows < this.zoomLastRows) {
                this.shiftTileMapUp(1);
            }
            if (this.zoomRows > this.zoomLastRows) {
                this.shiftTileMapDown(1);

            }

            if (this.debugTileMap) {
                this.drawMapDebug();
            }
            // figure out which tiles are on screen
            var tx = Math.floor(this.origin.x / this.tileMap.tileWidth);
            if (tx < 0) tx = 0;
            var ty = Math.floor(this.origin.y / this.tileMap.tileHeight);
            if (ty < 0) ty = 0;

            var tw = (Math.ceil((this.origin.x + this.scene.viewPort.w) / this.tileMap.tileWidth) - tx) + 2;
            if (tx + tw >= this.tileMap.tilesWide - 1) tw = this.tileMap.tilesWide - 1 - tx;
            var th = (Math.ceil((this.origin.y + this.scene.viewPort.h) / this.tileMap.tileHeight) - ty) + 2;
            if (ty + th >= this.tileMap.tilesHigh - 1) th = this.tileMap.tilesHigh - 1 - ty;

            for (var y = ty, c = ty + th; y < c + 1; y++) {
                var ypos = this.screenY(y * this.tileMap.tileHeight);

                for (var x = tx, d = tx + tw; x < d; x++) {
                    this.tileMap.drawTileTo(
                        pc.device.ctx, x, y,
                        this.screenX(x * this.tileMap.tileWidth + this.startLeft), ypos);
                    

                    if (this.debugShowGrid) {
                        pc.device.ctx.save();
                        pc.device.ctx.strokeStyle = '#222222';
                        pc.device.ctx.strokeRect(this.screenX(x * this.tileMap.tileWidth), this.screenY(y * this.tileMap.tileHeight),
                            this.tileMap.tileWidth, this.tileMap.tileHeight);
                        pc.device.ctx.restore();
                    }
                }
            }
            
        },
        shiftTileMapLeft: function (shiftLeft) {
            for (var i = 0; i < this.tileMap.tiles.length; i++) {
                this.tileMap.tiles[i] = this.tileMap.tiles[i].concat(this.tileMap.tiles[i].splice(0, shiftLeft));
            }
            if (this.arrOverFlowTop.length > 0) {
                for (var i = 0; i < this.arrOverFlowTop.length; i++) {
                    this.arrOverFlowTop[i] = this.arrOverFlowTop[i].concat(this.arrOverFlowTop[i].splice(0, shiftLeft));
                }
            }
            this.shiftedLeft += shiftLeft;
        },
        shiftTileMapRight: function (shiftRight) {
            for (var i = 0; i < this.tileMap.tiles.length; i++) {
                this.tileMap.tiles[i] = this.tileMap.tiles[i].concat(this.tileMap.tiles[i].splice(0, this.tileMap.tiles[i].length - shiftRight));
            }
            if (this.arrOverFlowTop.length > 0) {
                for (var i = 0; i < this.arrOverFlowTop.length; i++) {
                    this.arrOverFlowTop[i] = this.arrOverFlowTop[i].concat(this.arrOverFlowTop[i].splice(0, this.arrOverFlowTop[i].length - shiftRight));
                }
            }
            this.shiftedLeft -= shiftRight;

        },
        shiftTileMapUp: function (shiftUp) {
            this.arrOverFlowTop = this.arrOverFlowTop.concat(this.tileMap.tiles.splice(0, shiftUp));

            for (i = 0; i < shiftUp; i++) {
                this.tileMap.tiles.push(this.createBlankTileRow(this.tileMap.tiles));
                this.shiftedUp += 1;
            }
        },
        shiftTileMapDown: function (shiftDown) {

            if (this.arrOverFlowTop.length > 0) {
                for (i = 0; i < shiftDown; i++) {
                    this.tileMap.tiles.unshift(this.arrOverFlowTop.pop());
                    this.tileMap.tiles.pop();
                    this.shiftedUp -= 1;
                }
            }
        },
        createBlankTileRow: function (tileArray) {


            //find lenght of tile map row
            var len = tileArray[0].length;
            var arrBlanks = [];
            for (i = 0; i < len; i++) {
                arrBlanks.push(-1);
            }
            return arrBlanks;
        },
        drawMapDebug: function () {
            var overflowX = 0, overflowY = 0;
            if (this.arrOverFlowTop.length > 0) {
                overflowX = this.arrOverFlowTop[0].length;
                overflowY = this.arrOverFlowTop.length;
                for (var i = 0; i < this.arrOverFlowTop.length; i++) {
                    for (var j = 0; j < this.arrOverFlowTop[i].length; j++) {
                        var text = this.arrOverFlowTop[i][j];
                        if (text === 0) {
                            pc.device.ctx.fillStyle = 'red';
                        }
                        else {
                            pc.device.ctx.fillStyle = 'yellow';
                        }
                        pc.device.ctx.fillText(this.arrOverFlowTop[i][j], j * 20, (i + 1) * 20);
                    }
                }
            }

            for (var i = 0; i < this.tileMap.tiles.length; i++) {
                for (var j = 0; j < this.tileMap.tiles[i].length; j++) {
                    var text = this.tileMap.tiles[i][j];
                    if (text === 0) {
                        pc.device.ctx.fillStyle = 'white';
                    }
                    else {
                        pc.device.ctx.fillStyle = 'blue';
                    }
                    pc.device.ctx.fillText(this.tileMap.tiles[i][j], j * 20, (i + overflowY + 1) * 20);
                }
            }

        },
        process: function () {
            this.left += this.speed;
            
        }
       
    });