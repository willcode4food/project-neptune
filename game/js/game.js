﻿/**
 *   Project Neptune - (C)2015 Eddie Games Inc.
 *   Author: Marc D. Arbesman
 */

/**
 * Class: ProjectNeptune
 * Extends: pc.Game
 * Description: Facilitates loading the initial game state.  All global variables for the game 
 * are stored in this class.  Game scenes and assets are loaded here.    Dynamic resizing also occurs in 
 * this class.
 */

pn.ProjectNeptune = pc.Game.extend('pn.ProjectNeptune',
    {},
    {
        
        __constants: null,
        
        gameScene: null,
        hubScene: null,
        loginScene: null,
        gameDeviceDetection: null,
        gameDeviceSettings: null,
        resolutionWidth: null,
        resolutionHeight: null,
        currCanvasHeight: null,
        currCanvasWidth: null,
        optimalRation: null,        
        util: null,
        isGameZoomed: false,
        players: [],
        sessionId: null,
        cardTextLayouts: null,
        centerPosX: 0,
        centerPosY: 0,
        getSecurityInfo: null,

        onReady: function () {
            /// <summary>Event that gets fired once the bootstrapping system for files is completed.</summary>

            this._super();            
            this.__constants = new pn.Constant();
            this.loginScene = pc.device.game.__constants.get('GAME_LOGIN_SCENE');
            
            this.gameDeviceDetection = new pn.DeviceDetection();
            this.gameDeviceSettings = new pn.DeviceConfigFactory();
            this.util = pn.util.Util;
            // map all ascii chars to keycodes from events
            this.util.initKeyMap();
            this.sessionId = 'ee6cd3bf-b361-4b3c-99a5-d0c7b2d579e6';
            pc.device.devMode = false;
            pc.device.showDebug = true;
            pc.device.ctx.imageSmoothingEnabled = false;
            pc.device.ctx.mozImageSmoothingEnabled = false;
            // disable caching when developing
            if (pc.device.devMode) {

                pc.device.loader.setDisableCache();
                document.getElementById('stats').style.display = 'table-row';
            }
            else {
                document.getElementById('stats').style.display = 'none';
            }
            //var stateLoader = new GameStateLoader(this.sessionId);
            //stateLoader.load(pc.device.game.getGameDataURL(this.__constants.get('GAME_STATE_DATA_URL')), this.sessionId);
            ////this.players = PlayerLoader.load(pc.device.game.getGameDataURL(this.__constants.get('PLAYER_DATA_URL')))
            //pc.device.loader.add(new pc.DataResource('GameState',pc.device.game.getGameDataURL(this.__constants.get('GAME_STATE_DATA_URL'))));
            //pc.device.loader.add(new pc.Image('playfield_cell', this.__constants.get('GAME_IMAGES_CDN_DOMAIN') + this.gameDeviceSettings.getPlayfieldTileImage()));
            //pc.device.loader.add(new pc.Image('ship_cell', this.__constants.get('GAME_IMAGES_CDN_DOMAIN') + this.gameDeviceSettings.getBoardShipTileImage()));
            //pc.device.loader.add(new pc.Image('stars1', this.__constants.get('GAME_IMAGES_CDN_DOMAIN') + this.__constants.get('IMAGE_CDN_URL') + '/board/stars1.png'));
            //pc.device.loader.add(new pc.Image('stars2', this.__constants.get('GAME_IMAGES_CDN_DOMAIN') + this.__constants.get('IMAGE_CDN_URL') + '/board/stars2.png'));
            //pc.device.loader.add(new pc.Image('stars3', this.__constants.get('GAME_IMAGES_CDN_DOMAIN') + this.__constants.get('IMAGE_CDN_URL') + '/board/stars3.png'));
            //pc.device.loader.add(new pc.Image('dice', this.__constants.get('GAME_IMAGES_CDN_DOMAIN') + this.__constants.get('IMAGE_CDN_URL') + '/dice/pc/spritesheet.png'));
            //pc.device.loader.add(new pc.Image('crewIcon', pc.device.game.__constants.get('GAME_IMAGES_CDN_DOMAIN') + pc.device.game.__constants.get('IMAGE_CDN_URL') + this.gameDeviceSettings.getUIImageURL() + 'crew-icon.png'));
            //pc.device.loader.add(new pc.Image('leadershipIcon', pc.device.game.__constants.get('GAME_IMAGES_CDN_DOMAIN') + pc.device.game.__constants.get('IMAGE_CDN_URL') + this.gameDeviceSettings.getUIImageURL() + 'leadership-icon.png'));
            //pc.device.loader.add(new pc.Image('powerIcon', pc.device.game.__constants.get('GAME_IMAGES_CDN_DOMAIN') + pc.device.game.__constants.get('IMAGE_CDN_URL') + this.gameDeviceSettings.getUIImageURL() + 'power-icon.png'));
            //pc.device.loader.add(new pc.Image('showHandIcon', pc.device.game.__constants.get('GAME_IMAGES_CDN_DOMAIN') + pc.device.game.__constants.get('IMAGE_CDN_URL') + this.gameDeviceSettings.getUIImageURL() + 'hand-icon.png'));
            //pc.device.loader.add(new pc.Image('shipClassificationIcon', pc.device.game.__constants.get('GAME_IMAGES_CDN_DOMAIN') + pc.device.game.__constants.get('IMAGE_CDN_URL') + this.gameDeviceSettings.getUIImageURL() + 'ship-classification-icon.png'))
            //pc.device.loader.add(new pc.Image('specialistIcon', pc.device.game.__constants.get('GAME_IMAGES_CDN_DOMAIN') + pc.device.game.__constants.get('IMAGE_CDN_URL') + this.gameDeviceSettings.getUIImageURL() + 'specialists-icon.png'))
            pc.device.loader.add(new pc.Image('loadingIcon', pc.device.game.__constants.get('GAME_IMAGES_CDN_DOMAIN') + pc.device.game.__constants.get('IMAGE_CDN_URL') + this.gameDeviceSettings.getUIImageURL() + 'loading.png'))
            pc.device.loader.add(new pc.Image('facebookInviteButton', pc.device.game.__constants.get('GAME_IMAGES_CDN_DOMAIN') + pc.device.game.__constants.get('IMAGE_CDN_URL') + this.gameDeviceSettings.getUIImageURL() + 'facebook_invite.png'))
            pc.device.loader.add(new pc.Image('twitterInviteButton', pc.device.game.__constants.get('GAME_IMAGES_CDN_DOMAIN') + pc.device.game.__constants.get('IMAGE_CDN_URL') + this.gameDeviceSettings.getUIImageURL() + 'twitter_invite.png'))
            // pc.device.loader.add(new pc.Image('facebookLoginButton', pc.device.game.__constants.get('GAME_IMAGES_CDN_DOMAIN') + pc.device.game.__constants.get('IMAGE_CDN_URL') + this.gameDeviceSettings.getUIImageURL() + 'fb_login_btn.png'))
           
            /*
            if (pc.device.soundEnabled)
                pc.device.loader.add(new pc.Sound('fire', 'sounds/fire', ['ogg', 'mp3'], 15));
                */
            // fire up the loader
            pc.device.loader.start(this.onLoading.bind(this), this.onLoaded.bind(this));
            //this.initGame();

            this.resolutionHeight = this.gameDeviceSettings.getBaseDeviceHeight();
            this.resolutionWidth = this.gameDeviceSettings.getBaseDeviceWidth();

            //initiate the resize routine to scale based on window size.
            window.addEventListener('orientationchange', this.checkOrientation, false);
            this.onResize();
        },

        onLoading: function (percentageComplete) {
            /// <summary>Handles loading screen animations</summary>
            /// <param name="percentageComplete" type="Object">Displays the percentage completed from the loader class.</param>

            // draw title screen -- with loading bar
        },
        onLoaded: function () {
            /// <summary>resources are all ready, start the main game scene.  (or a menu if you have one of those)</summary>
            //GameStateLoader.loadState(this.sessionId);

            //for (k = 0; k < this.players.length; k++) {
            //    GameStateLoader.loadPlayerImages(this.players[k], this.initGame, this.onError);
            //}
            this.initGame();
        
        },
        initGame: function(){
            //this.gameScene = new pn.scene.GameScene();
            //this.addScene(this.gameScene, false);
            this.menuScene = new pn.scene.MenuScene();
            this.hubScene = new pn.scene.HubScene();
            this.loginScene = new pn.scene.LoginScene();


            this.addScene(this.menuScene, false);
            this.addScene(this.hubScene,false); 
            this.addScene(this.loginScene, false);
                 
            if (this.hasPreviouslyLoggedIn()) {                
                this.activateMenuScene();
                // this.activateHubScene();
            }
            else {
                this.activateLoginScene(this.loginScene);
            }
          
        },    
        activateMenuScene:function(){
            this.deactivateAllScenes();
            this.activateScene(this.menuScene);

        },
        activateLoginScene: function () {
            this.deactivateAllScenes();
            this.activateScene(this.loginScene);

        },
        activateHubScene: function(){
            this.deactivateAllScenes();
            this.activateScene(this.hubScene);
        },
        deactivateAllScenes:function(){
            var node = this.activeScenes.first;
            while (node) {
                this.deactivateScene(node.obj);
                node = node.next();
            }
        },
        hasPreviouslyLoggedIn: function(){
            //get authentication object from localstorage
            if (pc.LocalStore.exists(pc.device.game.__constants.get('SYS_STORAGE_PLAYER_AUTH'))) {
                return true;
            }
            else {
                return false;
            }
        },
        refreshLogin: function (self) {
            // attempt to refresh login with refresh token
            var authObj = pc.LocalStore.getJSONObject(pc.device.game.__constants.get('SYS_STORAGE_PLAYER_AUTH'));
            var client = new pc.HttpClient({
                cache: true,
                onError: self.onRefreshError,
                onComplete: self.onComplete,
                onSuccess: self.onRefreshSuccess,
                onLoading: self.onLoading,
                callingObject: self

            });
            var formData = {};
            formData['grant_type'] = 'refresh_token';
            formData['client_id'] = pc.device.game.__constants.get('SYS_CLIENT_ID');
            formData['refresh_token'] = authObj.refresh_token;
            client.postFormAsync({
                url: pc.device.game.__constants.get('SYS_API_DOMAIN') + pc.device.game.__constants.get('SYS_API_TOKEN'),
                formData: formData
            });

        },
        onRefreshError: function(){
            // if refresh attemp fails, show login screen
            this.activateLoginScene();
        },
        onRefreshSuccess: function (res, self) {

        },
        onError: function (error) {

            // todo
            throw 'Error in refresh request';
        },
        onResize: function () {
            /// <summary>Handles the resize event, and checks for swith to lanscape first if in portrait (which the correct orientation to play in) the next methods handle scaling the canvas when a new resolution is available.</summary>
            
            this.checkOrientation();
            this.resizeGame();
            this.resizeEngine();
            if(this.gameScene !== null){
                this.gameScene.zoomCount = 0;

            }
           

        },
        getUserPlayer: function(){
            for(i=0;i<this.players.length;i++){
                if (this.players[i].isUser) {
                    return this.players[i];
                }
            }
        },
        getOpponentPlayer:function(){
            for (i = 0; i < this.players.length; i++) {
                if (!this.players[i].isUser) {
                    return this.players[i];
                }
            }
        },
        getZoneByName: function (player, name) {
            if (!pc.valid(player)) {
                //refactor to use built in debugging
                console.log('player object not available');
                return;
            }
            if (!pc.valid(name)) {
                //refactor to use built in debugging
                console.log('type string not available');
                return;
            }

            for (i = 0; i < player.Zones.length; i++) {
                if (player.Zones[i].Name === name) {
                    return player.Zones[i];
                }
            }
        },
        getCounterByName: function (player, name) {
            if (!pc.valid(player)) {
                //refactor to use built in debugging
                console.log('player object not available');
                return;
            }
            if (!pc.valid(name)) {
                //refactor to use built in debugging
                console.log('type string not available');
                return;
            }

            for (i = 0; i < player.Counters.length; i++) {
                if (player.Counters[i].Name === name) {
                    return player.Counters[i];
                }
            }
        },
        resizeGame: function () {
            /// <summary>determine the ratio based windiw inner width and height when the browser is resized.           This uses CSS, resolutionWidth and height are determined by the device we are playing on.  for desktop we use 1280 X 720, for tablets we use 1024 / 768 These resolutions are our starting point to maintain the correct aspect ratio</summary>
            var gameWidth = window.innerWidth,gameHeight = window.innerHeight, scaleToFitX = gameWidth / this.resolutionWidth
            scaleToFitY = gameHeight / this.resolutionHeight,
            canvas = document.getElementById('gameCanvas'),
            currentScreenRatio = gameWidth / gameHeight,
            optimalRatio = Math.min(scaleToFitX, scaleToFitY);
            
            this.currCanvasWidth = Math.floor(this.resolutionWidth * optimalRatio);
            this.currCanvasHeight = Math.floor(this.resolutionHeight * optimalRatio);
            canvas.style.width = this.currCanvasWidth + "px";
            canvas.style.height = this.currCanvasHeight + "px";
            canvas.width = this.currCanvasWidth;
            canvas.height = this.currCanvasHeight;
            if (pc.device.isiPhone || pc.device.isiPhone4 || pc.device.isiPad || pc.device.isAndroid) {
             document.getElementById('gameArea').style.paddingTop = '24px';
            }
        },
        resizeEngine: function () {
            /// <summary>along with using CSS to resize the physical canvas, we need to resize the Playcraft engine rendering</summary>

            // I have no idea why I need to set these attributes...but you just do.
            pc.device.canvas.width = this.resolutionWidth;
            pc.device.canvas.height = this.resolutionHeight;
           // if the canvas has been resized by CSS then update the pc.device canvas width and height
            if (this.currCanvasHeight >= this.resolutionHeight || this.currCanvasWidth > this.resolutionWidth) {
                pc.device.canvasWidth = this.currCanvasWidth;
                pc.device.canvasHeight = this.currCanvasHeight
            }
            else {
                pc.device.canvasWidth = this.resolutionWidth;
                pc.device.canvasHeight = this.resolutionHeight;
            }                               
            // Handling the debug panel too (if turned on)
            pc.device.debugPanel.onResize(pc.device.canvasWidth, pc.device.canvasHeight);
        },
        checkOrientation: function () {
            // we need to force portrait mode on tablets (phone support is still up in the air).  Portrait mode is more natural for the lengthwise game board 
            // used in project neptune.  So if we see a rotation to landscape mode, show a locking screen with a prompt for switching back to portrait
            if (this.gameDeviceSettings !== null && this.gameDeviceSettings !== undefined) {
                if (this.gameDeviceSettings.isTabletDevice() == 'true') {
                    if (window.innerHeight < window.innerWidth) {
                        this.showPortraitLockWarning();
                    }
                    else {
                        this.hidePortraitLockWarning();
                    }
                }
            }
        },
        showPortraitLockWarning: function () {
            // using simple jQuery to show a red background for now if in landscape mode.            
            document.getElementById('gameCanvas').style.display = 'none';
            document.getElementById('gameArea').style.backgroundColor = 'red';


        },
        hidePortraitLockWarning: function () {
            // hide the locking screen if switched back            
            document.getElementById('gameArea').style.backgroundColor = '#000';
            document.getElementById('gameCanvas').style.display = 'inline';

        },
        // Global methods for retrieving the URLs of the assets and data
        getGameDataURL: function (partialURL) {
            return this.__constants.get('GAME_DATA_DOMAIN') + partialURL;
        },
        getGameImageURL: function (partialURL) {
            return this.__constants.get('GAME_IMAGES_CDN_DOMAIN') + partialURL;
        },
        getGameURL: function (partialURL) {
            return this.__constants.get('GAME_DOMAIN') + partialURL;
        },
        getSecurityInfo: function () {
            return pc.LocalStore.getJSONObject(pc.device.game.__constants.get('SYS_STORAGE_PLAYER_AUTH'));
        },
        externalLoginResponse: function (options) {
            if (!pc.valid(options.provider) || !pc.valid(options.external_access_token) || !pc.valid(options.has_local_account) ||
                !pc.valid(options.external_user_name) || !pc.valid(options.email)) {
                throw 'invalid information sent'; 
            }
          
            if (options.has_local_account === 'false') {
                this._registerExternal({
                    userName: options.email,
                    provider: options.provider,
                    externalAccessToken: options.external_access_token
                });
            }
            else {
                this._optainLocalAccessToken({
                    provider: options.provider,
                    externalAccessToken: options.external_access_token
                });
            }

        },
        _registerExternal: function (options) {
            if (!pc.valid(options.userName) || !pc.valid(options.provider) || !pc.valid(options.externalAccessToken)) {
                throw 'invalid information for external registration';
            }

            var client = new pc.HttpClient({
                cache: true,
                onError: this._regExternalError,
                onComplete: this._regExternalComplete,
                onSuccess: this._regExternalSuccess,
                onLoading: this._regExternalLoading,
                
                callingObject: this.hubScene
            }),
            formData = {};

            formData['userName'] = options.userName;
            formData['provider'] = options.provider;
            formData['externalAccessToken'] = options.externalAccessToken;
            formData['clientId'] = pc.device.game.__constants.get('SYS_CLIENT_ID');

            client._onBeforeSend = function (xhr) {
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.setRequestHeader('Accept', 'application/json');
            }
            client.postAsync({
                url: pc.device.game.__constants.get('SYS_API_DOMAIN') + pc.device.game.__constants.get('SYS_API_EXTERNAL_REG'),
                formData: JSON.stringify(formData)
            });



        },
        _regExternalError: function (response, xhr) {
            var apiResponse = JSON.parse(response);
            console.log(apiResponse.message);
        },
        _regExternalLoading: function(){
            return;
        },
        _regExternalSuccess: function (res, self) {
            pc.LocalStore.loadJSONObject({ obj: res, storeName: pc.device.game.__constants.get('SYS_STORAGE_PLAYER_AUTH') });
            // send user to  menu            
            var client = new pn.util.pnHttpClient({
                cache: true,
                callingObject: self.callingObject,
                onSuccess: function(res, self){
                    pc.LocalStore.loadJSONObject({ obj: res, storeName: pc.device.game.__constants.get('SYS_STORAGE_PLAYER_PROFILE') });
                    pc.device.game.activateMenuScene();
                } 
            });
            client.getAsync({
            url: pc.device.game.__constants.get('SYS_API_DOMAIN') + '/api/Account/Player/' + res.userId,
             headers: [
                {
                    header: 'Authorization',
                    value: 'Bearer ' + res.access_token
                }
            ]
        });
        },
        _regExternalComplete: function () {
            //pc.device.game.activateMenuScene();
        },
        _optainLocalAccessToken: function (options) {
            if (!pc.valid(options.provider) || !pc.valid(options.externalAccessToken)) {
                throw 'invalid information to obtain access token';
            }

            var client = new pc.HttpClient({
                cache: true,
                onError: this._regExternalError,
                onComplete: this._regExternalComplete,
                onSuccess: this._regExternalSuccess,
                onLoading: this._regExternalLoading,
                callingObject: this.hubScene
            });
         
            client.getAsync({
                url: pc.device.game.__constants.get('SYS_API_DOMAIN') + pc.device.game.__constants.get('SYS_API_OBTAIN_LOCAL_TOKEN') + '?provider=' + options.provider + '&externalAccessToken=' +
                     options.externalAccessToken + '&clientId=' + pc.device.game.__constants.get('SYS_CLIENT_ID'),
                headers: [
                   {
                       header: 'Content-Type',
                       value: 'application/json'
                   },
                   {
                       header: 'Accept',
                       value: 'application/json'
                   }
                ]
            });
        }
    });

/**
 * Collision Types
 * 
 */


CollisionType =
{
    NONE: 0x0000,     // BIT MASK
    BOARD: 0x0001,  // 0000001
    TILEOVER: 0x0002,    // 0000010    
};