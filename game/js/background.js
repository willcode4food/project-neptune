﻿/**
 *   Project Neptune - (C)2015 Eddie Games Inc.
 *   Author: Marc D. Arbesman
 */

/**
* Class: Background Class
* Extends: pc.Base
* Description: Background Facilitates the creation of the 4 layers that make up the parallax starfield
*/

pn.Background = pc.Base.extend('Background',
{},
{
    bgLayer1: null,
    bgLayer2: null,
    bgLayer3: null,
    bgConstants: null,
    settings: null,
    bgLayer: null,
    backgroundMap: null,
    init: function(scene){
        this.config(scene);
        this._super();
    },
    config: function (scene) {
        /// <summary>currently using duplicate layers of the "moving" layers
        //   we basically place layers side by side and move them across the screen
        //   then we switch the leading layer behind the layer next to it.  
        //   Martin from Playcraft called this a cheese move.  And he's right.  But it works,
        //   When it comes time to optimize, we review his suggestions to provide a better performing system for 
        //   and endless space effect</summary>
        /// <param name="scene" type="Object">The pc.Scene object</param>

        this.bgConstants = pc.device.game.__constants;
        this.settings = pc.device.game.gameDeviceSettings;

        var bgWidth = this.settings.getBaseDeviceWidth(),
           bgHeight = this.settings.getBaseDeviceHeight();
        

        this.bgLayer1 = scene.addLayer(new BackgroundStarFieldLayer({
                                                                      type: 0,
                                                                      width: bgWidth,
                                                                      height: bgHeight,
                                                                      speed: .5
                                                                    }));
        this.bgLayer2 = scene.addLayer(new BackgroundStarFieldLayer({
                                                                      type: 1,
                                                                      width: bgWidth,
                                                                      height: bgHeight,
                                                                      speed: .9
                                                                    }));
    }
    
});