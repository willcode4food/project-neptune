﻿/**
 * Project Neptune  - (C)2013 Castle One Games.
 * Marc Arbesman - Developer / Producer
 */

/**
 * Class: GameStateLoader
 * Extends: pc.Base
 * Description:  loads JSON data that represents the each individual card and stores them 
 * into local storage.  This object is called during the asset loading in the main Game object.
 * 
 */


pn.GameStateLoader = pc.Base.extend('GameStateLoader',
{
    
    loadState: function (dataKey) {
        /// <summary>Initializes the AJAX request and passes the returned JSON data to gather subsequent images</summary>
        /// <param name="url" type="Object">The URL of locate of the deck's JSON data</param>
        /// <param name="deckName" type="Object">A unique identifier for the deck</param>
        var self = this;
      
        var gameData = JSON.parse(pc.device.loader.get('GameState').resource.data);
         deckStore = this.loadIntoLocalStorage(dataKey, gameData);
            // load ship card images
            for (k = 0; k < gameData.GameState.Players.length; k++) {
                this.loadCardImages(pc.device.game.getZoneByName(gameData.GameState.Players[k], 'Conflict'), gameData.GameState.Players[k].ID);
                this.loadCardImages(pc.device.game.getZoneByName(gameData.GameState.Players[k], 'Ship'), gameData.GameState.Players[k].ID);
                this.loadCardImages(pc.device.game.getZoneByName(gameData.GameState.Players[k], 'Classification'), gameData.GameState.Players[k].ID);
                //this.loadPlayerImages(gameData.GameState.Players[k]);
            }
            pc.device.game.players = gameData.GameState.Players;
        
    },
    loadIntoLocalStorage: function (dataKey, gameData) {
        /// <summary></summary>
        /// <param name="this.jsonData" type="Object">JSON formatted deck data</param>
        /// <param name="deckName" type="Object">the deck name</param>

        pc.LocalStore.loadJSONObject({ obj: gameData, storeName: dataKey });
    },
    loadCardImages: function (zone, playerID) {
        // loop through each node of the JSON data
        // Each node has image information determining the small and large image.

        var resourceID = null, imagePath = pc.device.game.__constants.get('GAME_IMAGES_CDN_DOMAIN') + pc.device.game.__constants.get('IMAGE_CDN_URL') + pc.device.game.gameDeviceSettings.getCardImageRoot();
          
        for (var i = 0; i < zone.Cards.length; i++) {
            var obj = zone.Cards[i];
            for (var key in obj) {
                if (key === pc.device.game.__constants.get('DECK_SMALL_IMAGE_FIELDNAME') || key === pc.device.game.__constants.get('DECK_LARGE_IMAGE_FIELDNAME') || key === pc.device.game.__constants.get('DECK_MIN_IMAGE_FIELDNAME')) {
                    // Loading all images from the deck JSON Data
                    if (!pc.valid(obj.SmallImg, false)) {
                        throw 'Small Image data was not Defined';
                    }
                    if (!pc.valid(obj.LargeImg, false)) {
                        throw 'Large Image data was not Defined';
                    }
                    if (key === pc.device.game.__constants.get('DECK_SMALL_IMAGE_FIELDNAME')) {
                        resourceID = pc.device.game.util.getImageIdentifier(obj.SmallImg.toString(), obj.ID.toString());
                        // load alt image for swapping upon zooming
                        // naming convention for alt images are <Image name minus prefix>_min_alt.png
                        pc.device.loader.add(new pc.Image(pc.device.game.util.getImageIdentifier(
                                                                                    obj.SmallImg.toString(),
                                                                                    obj.ID.toString(), playerID),
                                                            imagePath +
                                                            obj[key].toString()));
                            }
                    else if (key === pc.device.game.__constants.get('DECK_LARGE_IMAGE_FIELDNAME')) {
                        // Naming convention for the image identifier is <LargeImg>_<ID>
                        resourceID = pc.device.game.util.getImageIdentifier(obj.LargeImg.toString(), obj.ID.toString());
                        pc.device.loader.add(new pc.Image(pc.device.game.util.getImageIdentifier(
                                                                                    obj.LargeImg.toString(),
                                                                                    obj.ID.toString(), playerID),
                                                            imagePath +
                                                            obj[key].toString()));
                    }
                    else if (key === pc.device.game.__constants.get('DECK_MIN_IMAGE_FIELDNAME')){
                       resourceID = pc.device.game.util.getImageIdentifier(obj.MinImg.toString(), obj.ID.toString());
                       pc.device.loader.add(new pc.Image(pc.device.game.util.getImageIdentifier(
                                                                                    obj.MinImg.toString(),
                                                                                    obj.ID.toString(), playerID),
                                                            imagePath +
                                                            obj[key].toString()));

                    }
                }
            }
        }
    },
    loadPlayerImages: function (player, onLoadCallback, onErrorCallback) {
        pc.device.loader.add(new pc.Image('player_' + player.ID, pc.device.game.__constants.get('GAME_IMAGES_CDN_DOMAIN') + pc.device.game.__constants.get('IMAGE_CDN_URL') + pc.device.game.gameDeviceSettings.getUIImageURL() + player.imageName, onLoadCallback, onErrorCallback));
        pc.device.loader.start();
    },
    onError:function(error){
        console.log(error);
    }
},
{});