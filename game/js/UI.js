﻿/**
 * Project Neptune - (C)2013 ThrottleNet Inc.
 * Author: Marc D. Arbesman
 */

/**
 *  Class: UI
 *  Extends: pc.Pooled
 *  Description: draws all the player and opponents imagery and stats.  
 *  ensures the updates to the display of the score are handled only in here.
 *  Currently pooling this object as I think it would be give a performance increase
 *  Many of the methods and members are not used and currently only stubbed out
 */

UI = pc.Pooled.extend('UI',
    {},
    {
        //private members
        UILayer: null,
        opponentUIEntity: null,
        playerUIEntity: null,        
        opponentCrewStat: null,
        opponentPowerStat: null,
        opponentLeadershipStat: null,
        playerCrewStat: null,
        playerPowerStat: null,
        playerLeadershipStat: null,
        constants: null,
        settings: null,
        UIMenu:null,
        create: function(){
            var newUI = this._super();
            return newUI;
        },

        //methods
        //Initialzie data from server data source for both player and opponent
        init: function (layer) {
            if (layer !== null &&  layer !== undefined) {
                this.UILayer = layer;
                this.UILayer.zIndex = 3;
                this.config();
                this.drawPlayerPanels();

            }
        },
        //get player data
        config: function () {
            this.constants = pc.device.game.__constants;
            this.settings = pc.device.game.gameDeviceSettings;
            this.opponentCrewStat = 100;
            this.opponentLeadershipStat = 20;
            this.opponentPowerStat = 100;

            this.playerCrewStat = 85;
            this.playerLeadershipStat = 30;
            this.playerPowerStat = 80;

        },
        drawPlayerPanels: function () {            
            this.UILayer.addSystem(new pc.systems.Render());
            this.UILayer.addSystem(new pc.systems.Effects());
            this.UILayer.addSystem(new pc.systems.Layout());
            // draw the opponent's panel to hold the key stats of game play
            this.opponentUIEntity = pc.Entity.create(this.UILayer);
            this.opponentUIEntity.addComponent(pc.components.Rect.create({ color: this.constants.get('COLOR_UI_PANEL_BACKGROUND') }));
            this.opponentUIEntity.addComponent(pc.components.Spatial.create({                
                x: this.settings.getBaseDeviceWidth() - this.constants.get('UI_PLAYER_PANEL_WIDTH') - (this.constants.get('UI_PLAYER_PORTRAIT_WIDTH') / 2),
                y: 0,
                w: this.constants.get('UI_PLAYER_PANEL_WIDTH') ,
                h: this.constants.get('UI_PLAYER_PANEL_HEIGHT')
            }));
            this.opponentUIEntity.addComponent(pc.components.Text.create({
                text: ['C: ' + (this.opponentCrewStat).toString() + '   L: ' + (this.opponentLeadershipStat).toString() + '   P: ' + (this.opponentPowerStat).toString()],
                lineWidth: 0,
                fontHeight: this.constants.get('UI_PLAYER_PANEL_FONT_HEIGHT'),
                offset: { x: 10, y: -10},
                color: this.constants.get('UI_PLAYER_PANEL_FONT_COLOR')

            }));

            //draw the opponent portrait
            this.opponentPortraitEntity = pc.Entity.create(this.UILayer);           
            this.opponentPortraitEntity.addComponent(pc.components.Circle.create({ color: this.constants.get('COLOR_UI_PORTRAIT_BACKGROUND') }));
            this.opponentPortraitEntity.addComponent(pc.components.Spatial.create({
                x: this.settings.getBaseDeviceWidth() - this.constants.get('UI_PLAYER_PORTRAIT_WIDTH'),
                y: this.constants.get('UI_PLAYER_PORTRAIT_YPOS'),
                w: this.constants.get('UI_PLAYER_PORTRAIT_WIDTH'),
                h: 0
            }));
            
            //this.opponentPortraitEntity.addComponent(pc.components.Fade.create({ holdTime:7500, fadeInTime:500, fadeOutTime:500 }));
            // draw the player's panel to hold the key stats of game play
            this.playerUIEntity = pc.Entity.create(this.UILayer);
            this.playerUIEntity.addComponent(pc.components.Rect.create({ color: this.constants.get('COLOR_UI_PANEL_BACKGROUND') }));
            this.playerUIEntity.addComponent(pc.components.Spatial.create({
                x: (this.constants.get('UI_PLAYER_PORTRAIT_WIDTH') / 2),                
                y: this.settings.getBaseDeviceHeight() - this.constants.get('UI_PLAYER_PANEL_HEIGHT'),
                w: this.constants.get('UI_PLAYER_PANEL_WIDTH'),
                h: this.constants.get('UI_PLAYER_PANEL_HEIGHT')
            }));
            this.playerUIEntity.addComponent(pc.components.Text.create({
                text: ['C: ' + (this.playerCrewStat).toString() + '   L: ' + (this.playerLeadershipStat).toString() + '   P: ' + (this.playerPowerStat).toString()],
                lineWidth: 0,
                fontHeight: this.constants.get('UI_PLAYER_PANEL_FONT_HEIGHT'),
                offset: { x: 50, y: -10 },
                color: this.constants.get('UI_PLAYER_PANEL_FONT_COLOR')

            }));
            //draw the player's portrait
            this.playerPortraitEntity = pc.Entity.create(this.UILayer);
            this.playerPortraitEntity.addComponent(pc.components.Circle.create({ color: this.constants.get('COLOR_UI_PORTRAIT_BACKGROUND') }));
            this.playerPortraitEntity.addComponent(pc.components.Spatial.create({
                x: 0,
                y: this.settings.getBaseDeviceHeight() - this.constants.get('UI_PLAYER_PORTRAIT_YPOS'),
                w: this.constants.get('UI_PLAYER_PORTRAIT_WIDTH'),
                h: 0
            }));

        },
        updateOpponentCrew: function (val) {
            if (val === null || val === undefined) {
                val = 0;
            }
            this.opponentCrewStat = this.opponentCrewStat + val;
            this.updateOpponentUIStats();
        },
        updateOpponentLeadership: function (val) {
            if (val === null || val === undefined) {
                val = 0;
            }
            this.opponentLeadershipStat = this.opponentLeadershipStat + val;
            this.updateOpponentUIStats();
        },
        updateOpponentPower: function (val) {
            if (val === null || val === undefined) {
                val = 0;
            }
            this.opponentPowerStat = this.opponentPowerStat + val;
            this.updateOpponentUIStats();
        },
        updatePlayerCrew: function (val) {
            if (val === null || val === undefined) {
                val = 0;
            }
            this.playerCrewStat = this.playerCrewStat + val;
            this.updatePlayerUIStats();
        },
        updatePlayerLeadership: function (val) {
            if (val === null || val === undefined) {
                val = 0;
            }
            this.playerLeadershipStat = this.playerLeadershipStat + val;
            this.updatePlayerUIStats();
        },
        
        updatePlayerPower: function (val) {
            if (val === null || val === undefined) {
                val = 0;
            }
            this.playerPowerStat = this.playerPowerStat + val;
            this.updatePlayerUIStats();
        },
        updateOpponentUIStats: function () {
            this.opponentUIEntity.removeComponentByType('text');
            this.opponentUIEntity.addComponent(pc.components.Text.create({
                text: [
                    'C: ' + (this.opponentCrewStat).toString() +
                    '   L: ' + (this.opponentLeadershipStat).toString() +
                    '   P: ' + (this.opponentPowerStat).toString()
                ],
                lineWidth: 0,
                fontHeight: this.constants.get('UI_PLAYER_PANEL_FONT_HEIGHT'),
                offset: { x: 10, y: -10 },
                color: this.constants.get('UI_PLAYER_PANEL_FONT_COLOR')

            }));
        },
        updatePlayerUIStats: function () {
            this.playerUIEntity.removeComponentByType('text');
            this.playerUIEntity.addComponent(pc.components.Text.create({
                text: [
                    'C: ' + (this.playerCrewStat).toString() +
                    '   L: ' + (this.playerLeadershipStat).toString() +
                    '   P: ' + (this.playerPowerStat).toString()
                ],
                lineWidth: 0,
                fontHeight: this.constants.get('UI_PLAYER_PANEL_FONT_HEIGHT'),
                offset: { x: 10, y: -10 },
                color: this.constants.get('UI_PLAYER_PANEL_FONT_COLOR')

            }));
        },
        UIMenu: function () {


        }
    });