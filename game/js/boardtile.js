﻿/**
 *   Project Neptune - (C)2015 Eddie Games Inc.
 *   Author: Marc D. Arbesman
 */

/**
* Class: BoardTile
* Extends: pc.Base
* 
* Description: The BoardTile object will define the tiles, screen pixel postion in x,y coordinants.
* the type of tile, (Tile Types are defined by the super class TileMap which defines an active space or not in the tileMap matrix)
* the tileMap X,Y position multi-dimensional array.  This will make it easier to access the tile cooresponding tile type from the super class tilemap array, tiles
* Also the tile height and width ar stored and the boardTileId which his board tile space number
* Currently the board space number is tightly bound to the BoardTileMap array's index.  
* This then correlates to the asusmption all board spaces are in sequence of when the BoardTileMap class paints them
* May want to consider changing this to make the space number assignements to the game board spaces more dynamic
*/

pn.BoardTile = pc.Base.extend('BoardTile',
{},
{
    screenPosX: null,
    screenPosY: null,
    type: null,
    tileMapX: null,
    tileMapY: null,
    tileWidth: null,
    tileHeight: null,
    boardTileId: null,
    settings: null,

    init: function (options) {
        /// <summary>Initializes the object and sets all properties</summary>
        /// <param name="tileType" type="Object">The Tile Type (-1 blank, 0 first spritesheet tile, etc.)</param>
        /// <param name="tileMapX" type="Object">The column of the tile is located in</param>
        /// <param name="tileMapY" type="Object">The row the tile is located in</param>
        /// <param name="tileHeight" type="Object">The height of the tile</param>
        /// <param name="tileWidth" type="Object">The width of the tile</param>
        /// <param name="boardTileId" type="Object">The ID of tile.  This cooresponds to the location of the tile on the map as a game board</param>
        if (!pc.valid(options.tileType)) {
            throw 'the tile type has not been specified';
        }
        if (!pc.valid(options.tileMapX)) {
            throw 'TileMapX, the column has not been for the tile has not been specified';
        }
        if (!pc.valid(options.tileMapY)) {
            throw 'TileMapY, the row has not been for the tile has not been specified';
        }
        if (!pc.valid(options.tileHeight)) {
            throw 'Height of the tile has not been specified';
        }
        if (!pc.valid(options.tileWidth)) {
            throw 'Width of the tile has not been specified';
        }
        if (!pc.valid(options.boardTileId)) {
            throw 'ID of the tile has not been specified';
        }
        this._super();
        this.settings = pc.device.game.gameDeviceSettings;
        this.type = options.tileType;
        this.tileMapX = options.tileMapX;
        this.tileMapY = options.tileMapY;
        this.tileHeight = options.tileHeight;
        this.tileWidth = options.tileWidth;
        this.screenPosX = this.getPixelPos(this.tileMapX, 'x');
        this.screenPosY = this.getPixelPos(this.tileMapY, 'y');
        this.boardTileId = options.boardTileId;
    },
    getPixelPos: function (pos, XorY) {
        if (XorY.toUpperCase() === 'X') {
            return (pos * this.tileWidth);
        } else if (XorY.toUpperCase() === 'Y') {
            return (pos * this.tileHeight);
        }
    }
});