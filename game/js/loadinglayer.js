pn.layer.LoadingLayer = pc.EntityLayer.extend('pn.layer.LoadingLayer',
{},
{
	_loadingEntity: null,
  _bgEntity: null,  
  _loadingSprite: null,
  _loadingSpriteSheet: null,
	init:function ()
	{
		this._super("loadingLayer", 10000,1000);
    this.zIndex = 10;
		this.addSystem(new pc.systems.Render());
    this.addSystem(new pc.systems.Effects());
    this._loadingEntity = pc.Entity.create(this);
    this._bgEntity = pc.Entity.create(this);    


    this._loadingSpriteSheet = new pc.SpriteSheet({
      image: pc.device.loader.get('loadingIcon').resource,
      frameWidth: 124,
      frameHeight: 128,
      framesWide: 20,
      framesHigh: 1
    });

    this._loadingSpriteSheet.addAnimation({
        name: 'load',
        frameCount: 20,
        time: 600,
        frameX: 0,
        frameY: 0,
        holdOnEnd: false,
        loops: 0

    });

    this._loadingSprite = new pc.Sprite(this._loadingSpriteSheet);        
	},
      /**
     * Fired when a bound event/action is triggered in the input system. Use bindAction
     * to set one up. Override this in your layer to do something about it.
     * @param {String} actionName The name of the action that happened
     * @param {Object} event Raw event object
     * @param {pc.Point} pos Position, such as a touch input or mouse position
     * @param {pc.Base} uiTarget the uiTarget where the action occurred
     */

  setActive: function(){
     
      if(!this._bgEntity.hasComponentOfType('spatial')){
          this._bgEntity.addComponent(pc.components.Spatial.create({
            x: 0,
            y: 0,
            w: pc.device.game.resolutionWidth,
            h: pc.device.game.resolutionHeight
        }));  
      }
      if(!this._bgEntity.hasComponentOfType('rect')){
         this._bgEntity.addComponent(pc.components.Rect.create({
            color:'#0F1D36'
         }));
      }     

      if(!this._bgEntity.hasComponentOfType('alpha')){
        this._bgEntity.addComponent(pc.components.Alpha.create({
            level:0.9

        }));
      }
      if(!this._loadingEntity.hasComponentOfType('spatial')){
        this._loadingEntity.addComponent(pc.components.Spatial.create({
            x: (pc.device.game.resolutionWidth/2) - 62,
            y: (pc.device.game.resolutionHeight/2) - 64,
            w: 124,
            h: 128
        }));  
      }
      if(!this._loadingEntity.hasComponentOfType('sprite')){
        this._loadingEntity.addComponent(pc.components.Sprite.create(this._loadingSprite));        
      } 

      this._loadingEntity.addTag('loadingIcon');
  },
  setInactive: function(){
    if(this._loadingEntity.hasComponentOfType('spatial')){
      this._loadingEntity.removeComponentByType('spatial');  
    }
    if(this._loadingEntity.hasComponentOfType('sprite')){
      this._loadingEntity.removeComponentByType('sprite');
    }
    if(this._bgEntity.hasComponentOfType('spatial')){
      this._bgEntity.removeComponentByType('spatial');  
    }
    if(this._bgEntity.hasComponentOfType('rect')){
      this._bgEntity.removeComponentByType('rect');
    }
    if(this._bgEntity.hasComponentOfType('alpha')){
      this._bgEntity.removeComponentByType('alpha');
    }

  }
});