﻿/**
 *   Project Neptune - (C)2015 Eddie Games Inc.
 *   Author: Marc D. Arbesman
 */

/**
 * Class: BoardTileMap
 * Extends: pc.TileMap
 * Description: Handles the loading of the TileMap.  But also builds an array to references
 * the tiles as positions on a game board.
 */


pn.BoardTileMap = pn.TNTileMap.extend('pn.BoardTileMap',
{},
{
    tiles: null,
    tileHeight: null,
    tileWidth: null,
    boardTiles: null,
    boardTileId: 0,
    init: function (tileSets, tilesWide, tilesHigh, tileWidth, tileHeight, tiles) {
        this._super(tileSets, tilesWide, tilesHigh, tileWidth, tileHeight, tiles);
        this.boardTiles = [];
    },

    paint: function (options)
    {
        if (!pc.valid(options.x)) {
            throw 'x coordinate not specified';
        }
        if (!pc.valid(options.y)) {
            throw 'y coordinate not specified';
        }
        if (!pc.valid(options.w)) {
            throw 'width not specified';
        }
        if (!pc.valid(options.h)) {
            throw 'height not specified';
        }
        if (!pc.valid(options.tileType)) {
            throw 'the tiletype not specified';
        }
        /// <summary>Populate an area of the tile map with a given tile type</summary>
        /// <param name="x" type="Object">x tile X position to start the paint</param>
        /// <param name="y" type="Object">y tile Y position to start the paint</param>
        /// <param name="w" type="Object">w How wide to paint</param>
        /// <param name="h" type="Object">h How high to paint</param>
        /// <param name="tileType" type="Object">tileType Type of tile to paint</param>

       
        // in the case of a game board, when a tile gets painted, we want to consider that tile
        // space on the game board.  So we track this by creating the array boardTiles that stores
        // all active game board spaces.  The index of the array is considered the space number.  This space number will be used
        // to defined where certain cards are located on the game board upon initially dealing them. 

        /**
         *  A NOTE ON THE BOARDTILES INDEX
         *  Currently, the boardTiles array index idetifies the space number.  We can use this to call up the boardtile informaiton by specifying the 
         *  the space number as the index of the array.  In our case we want to set a card on a specific space when we deal the deck initially.  '
         *  We can define the space number in the deck data and place the card on that deck space.
         *  The issues is that we are assuming that space numbers are assigned to game board spaces in the order they are painted.  
         *  We may want to reconsider this if we ever need to assign board spaces the game board in an unordered fashion.

         */

        for (var aty = options.y; aty < options.y + options.h; aty++)
            for (var atx = options.x; atx < options.x + options.w; atx++) {
                this.tiles[aty][atx] = options.tileType;

                // we want store the array with objects of type BoardTile
                // Consider Rafactoring to not store the whole object, but a reference to the object.
                this.boardTiles.push(new BoardTile({
                    tileType: options.tileType,
                    tileMapX: atx,
                    tileMapY: aty,
                    tileHeight: this.tileHeight,
                    tileWidth: this.tileWidth,
                    boardTileId: this.boardTileId
                }));
                this.boardTileId++;
            }
    }

});

