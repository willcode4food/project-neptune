﻿pn.scene.MenuScene = pc.Scene.extend('MenuScene',
{},
{
    buttonLayer: null,    
    bgLayer: null,
    gameDeviceSettings: null,
    init: function () {
        this._super();
        this.gameDeviceSettings = new pn.DeviceConfigFactory();
        this.bgLayer = this.addLayer(new pn.layer.FormBackgroundLayer('bgLayer'));
        this.buttonLayer = this.addLayer(new pc.EntityLayer('menu item layer'), 10000, 10000);
        this.buttonLayer.addSystem(new pc.systems.Render());
        this.buttonLayer.addSystem(new pc.systems.Effects());
        this.buttonLayer.addSystem(new pc.systems.FormInput());
        this.buttonLayer.addSystem(new pc.systems.Layout());
        
        var logoutButton = pc.ButtonEntity.create(this.buttonLayer);
        logoutButton.initButton({
            label: 'Logout',
            buttonVAlign: 'top',
            buttonHAlign: 'right',
            posX: this.gameDeviceSettings.getMenuLogoutButtonPosX(),
            posY: this.gameDeviceSettings.getMenuLogoutButtonPosY(),
            buttonFontHeight: this.gameDeviceSettings.getMenuLogoutFontHeight(),
            labelOffset:{
                x: 0,
                y: 0
            },
            margin: {
                left: 0,
                bottom: 0,
                top: 10,
                right: 10
            },
            unClickEvent: function  (){
                var self = this;
                pc.device.game.util.doLogout();
                // self.onMouseButtonUp();
                pc.device.game.activateLoginScene();
            }
        });
        var newGameButton = pc.ButtonEntity.create(this.buttonLayer);
        newGameButton.initButton({
            label: 'Play',
            buttonVAlign: 'middle',
            buttonHAlign: 'center',
            posX: this.gameDeviceSettings.getMenuNewButtonPosX(),
            posY: this.gameDeviceSettings.getMenuNewButtonPosY(),
            buttonFontHeight: this.gameDeviceSettings.getMenuNewFontHeight(),
            labelOffset: { x: 0, y: 0 },
            margin: {
                left: 300,
                bottom: 20,
                top: 0,
                right: 0
            },
            unClickEvent:function(){
                pc.device.game.activateHubScene();
            }

        });
        var buildDeckButton = pc.ButtonEntity.create(this.buttonLayer);
        buildDeckButton.initButton({
            label: 'Deck Builder',
            buttonVAlign: 'middle',
            buttonHAlign: 'center',
            posX: this.gameDeviceSettings.getMenuDeckButtonPosX(),
            posY: this.gameDeviceSettings.getMenuDeckButtonPosY(),
            buttonFontHeight: this.gameDeviceSettings.getMenuDeckFontHeight(),
            labelOffset: { x: 0, y: 0},
            margin: {
                left: 300,
                bottom: 20,
                top: 0,
                right: 0
            }
        });

      


    },
    process: function () {
        // clear the background
        pc.device.ctx.clearRect(0, 0, pc.device.canvasWidth, pc.device.canvasHeight);
        // always call the super
        this._super();
    }



});