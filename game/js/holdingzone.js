﻿pn.HoldingZone = pn.Zone.extend('pn.HoldingZone',
{

},
{
    /**
        * Methods
        */
    isOpen: false,
    startX: null,
    startY: null,
    moveX: null,
    moveY: null,
    padding: null,
    init: function (player, zone) {
        this._super(player, zone);
    },
    load: function (board, layer, startX, startY, moveX, moveY, padding, orientation) {

        this._super(board, layer, startX, startY, orientation);
        this.startX = startX;
        this.startY = startY;
        this.moveX = moveX;
        this.moveY = moveY;
        this.padding = padding;
        // hide the cards, the hide tag should prevent the sprite and input components to be added to the entity.
        for (i = 0; i < this.cards.length; i++) {
            if (this.cards[i].getComponent('carddata').cardType !== 'Compartment') {                               
                this.cards[i].addTag('nozoom');
                this.cards[i].addTag('hide');
                this.cards[i].addTag('nodragdrop');
                
            }
            if (!this.cards[i].hasComponentOfType('textLayout')) {
                var layout = new CardTextLayoutFactory(this.cards[i].getComponent('carddata').cardType);
                if ((pc.device.isiPhone || pc.device.isiPhone4 || pc.device.isiPad || pc.device.isAndroid) && this.cards[i].getComponent('carddata').imageMin !== '') {
                    this.cards[i].addComponent(pc.components.TextLayout.create({
                        layout: layout.getMinLayout(0, 0, this.cards[i].getComponent('carddata').armor, this.cards[i].getComponent('carddata').structure)
                    }));
                }
                else {
                    this.cards[i].addComponent(pc.components.TextLayout.create({
                        layout: layout.getSmallLayout(0, 0, this.cards[i].getComponent('carddata').armor, this.cards[i].getComponent('carddata').structure)
                    }));
                }
            }
                   
        }      
    },
    resetZonePos: function (entity, x, y) {
        entity.getComponent('carddata').zonePosX = x;
        entity.getComponent('carddata').zonePosY = y + this.padding;
        
        if (entity.hasComponentOfType('sprite')) {
            setTimeout(function () { entity.removeComponentByType('sprite') }, 195);
        }        
    },
    toggleOpenClose: function () {
        if (!this.isOpen) {
            that = this;
            //open hand
            var moveCardToX = this.moveX, 
                moveCardToY = this.moveY;
            for (i = 0; i < this.cards.length; i++) {
                if (!this.cards[i].hasTag('setonboard')) {
                    if (pc.device.game.gameDeviceDetection.isiPad) {
                        moveCardToX = moveCardToX * (i + 1);
                        moveCardToX += this.startX;
                    }
                    else {
                        moveCardToY = moveCardToY * (i + 1);
                    }
                    if (this.cards[i].hasComponentOfType('mover')) {
                        this.cards[i].removeComponentByType('mover');
                    }
                    this.cards[i].addComponent(pc.components.Mover.create(
                     {
                         targetPos: {
                             x: moveCardToX,
                             y: moveCardToY + this.padding
                         },
                         easing: pc.Easing.EXPONENTIAL_OUT,
                         duration: 500,
                         onComplete: this.resetZonePos(this.cards[i], moveCardToX, moveCardToY)
                     }));
                    moveCardToX = this.moveX;
                    moveCardToY = this.moveY;
                    if (this.cards[i].hasTag('hide')) {
                        this.cards[i].removeTag('hide');
                    }
                    if (!this.cards[i].hasTag('nozoom')) {
                        this.cards[i].addTag('nozoom');
                    }
                    if (this.cards[i].hasTag('nodragdrop')) {
                        this.cards[i].removeTag('nodragdrop');
                    }
                }
              
                
            }
            this.isOpen = true;
            this.loadCardSprites();
            
        }
        else {
            //close hand
            for (i = 0; i < this.cards.length; i++) {
                if (this.cards[i].getComponent('carddata').cardType !== 'Compartment') {
                    if (this.cards[i].hasComponentOfType('mover')) {
                        this.cards[i].removeComponentByType('mover');
                    }
                    // only snap back to holding zone if not placed on the board
                    if (!this.cards[i].hasTag('setonboard')) {                    
                        this.cards[i].addComponent(pc.components.Mover.create(
                        {
                            targetPos: {
                                x: this.startX,
                                y: this.startY + this.padding
                            },
                            easing: pc.Easing.LINEAR,
                            duration: 200,
                            onComplete: this.resetZonePos(this.cards[i], this.startX, this.startY)
                        }));
                        if (!this.cards[i].hasTag('hide')) {
                            this.cards[i].addTag('hide');
                        }
                        if (!this.cards[i].hasTag('nodragdrop')) {
                            this.cards[i].addTag('nodragdrop');
                        }
                    }
                }
            }
            this.isOpen = false;
        }
    },
    dealCards: function () {

    }
});