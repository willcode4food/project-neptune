﻿pn.DiceZone = pc.Base.extend('DiceZone',
{},
{
    diceArray: null,
    diceLayer: null,
    diceEntity: null,
    diceSprite: null,
    diceImage: null,
    scene: null,        
    frames: null,    
    diceEntities:null,
    init: function (options) {
        if (!pc.valid(options.scene)) {
            throw exception('no scene defined')
        }
        this.config(options);
    },
    config: function (options) {
        this.diceImage = pc.device.loader.get('dice').resource;
        this.scene = options.scene;
        

        this.diceLayer = new pc.EntityDiceLayer(
            {
                name: 'Dice Layer',
                worldSizeX: 10002,
                worldSizeY: 2,
                onAnimationEndCallback: this.onAnimationEnd
            });

        this.diceLayer.zIndex = 7;
        this.scene.addLayer(this.diceLayer);

        this.diceLayer.addSystem(new pc.systems.Render());
        this.diceLayer.addSystem(new pc.systems.Effects());
        // this will hold coordinants of the faces within the sprite sheet
        // Assumption:  the spritesheets second row (137 px high) holds the faces in the first ten 
        //              frames
        this.diceEntities = [];
        
    },
    roll: function (options) {
        if (this.diceEntities.length > 0) {
            this.clear();
        }
        this.numDice = options.numDice;

        for (i = 0; i < this.numDice; i++) {
            d = pc.Entity.create(this.diceLayer);
            this.setRollSprite(d);
        }
    },
    onAnimationEnd: function (entity) {

        var faceMap = [
            {
                x: 0,
                y: 1
            },
            {
                x: 1,
                y: 1
            }, {
                x: 2,
                y: 1
            }, {
                x: 3,
                y: 1
            }, {
                x: 4,
                y: 1
            }, {
                x: 5,
                y: 1
            }, {
                x: 6,
                y: 1
            }, {
                x: 7,
                y: 1
            }, {
                x: 8,
                y: 1
            }
        ];

        if (entity.hasComponentOfType('sprite')) {
            entity.removeComponentByType('sprite');
        }
        var faceSpriteSheet = new pc.SpriteSheet(
           {
               image: pc.device.loader.get('dice').resource,
               frameWidth: 115,
               frameHeight: 137,
               framesWide: 55,
               framesHigh: 2

           });
        rolledValue = pc.Math.getRandomInt(0, 8);
        tile = faceMap[rolledValue];

        faceSpriteSheet.addAnimation({
            name: 'land',
            frameCount: 1,
            time: 600,
            frameX:tile.x,                                
            frameY: tile.y,
            holdOnEnd: true,
            loops:1

        });                 
        var diceSprite = new pc.Sprite(faceSpriteSheet);
        entity.addComponent(pc.components.Sprite.create(diceSprite));
       
        if (entity.hasComponentOfType('dicedata')) {
            entity.removeComponentByType('dicedata');
            
        }
        if (rolledValue === 0) {
            rolledValue = 10;
        }
        entity.addComponent(DiceData.create({
            isRollComplete: true,
            value: rolledValue

        }));       
        console.log('You rolled a ' + entity.getComponent('dicedata').value);
    },
    clear: function () {
        for (i = 0; i < this.diceEntities.length; i++) {
            this.diceEntities[i].remove();

        }
        var arrLength = this.diceEntities.length;
        for (j = 0; j <= arrLength; j++) {
            this.diceEntities.pop();
        }
    },
    deactivate: function () {
        for (i = 0; i < this.diceEntities.length; i++) {
            if (this.diceEntities[i].hasComponentOfType('fade')) {
                this.diceEntities[i].removeComponentByType('fade');
            }
            this.diceEntities[i].addComponent(pc.components.Fade.create({ holdTime: 100, fadeOutTime: 1000 }));
        }
     
        
    },
    setRollSprite: function(entity){
        var diceSpriteSheet = new pc.SpriteSheet(
           {
               image: this.diceImage,
               frameWidth: 115,
               frameHeight: 137,
               framesWide: 55,
               framesHigh: 1

           });


        diceSpriteSheet.addAnimation({
            name: 'roll',
            frameCount: pc.Math.getRandomInt(20,54),
            time: 200,
            frameX: pc.Math.getRandomInt(0, 55),
            frameY: 0,
            holdOnEnd: true,
            loops: 1,
            allowDroppedFrames: false

        });

        var diceSprite = new pc.Sprite(diceSpriteSheet),
            centerPosX = pc.device.game.centerPosX - 7
        totalDiceArrayLength = (this.numDice * 115),
        startX = centerPosX - (totalDiceArrayLength / 2)
        diceUnitLength = totalDiceArrayLength / this.numDice,
        //diceArrayLength  =  (pc.device.game.centerPosX - (,(((115 * (i + 1)  + (120 * i))/ 2)) - 7 ) / 2) 
      
        entity.addComponent(pc.components.Spatial.create({
            x: startX + (diceUnitLength * i),
            // x: (totalDiceArrayLength / 2),
            y: 200,
            w: 190,
            h: 190
        }));
        entity.addTag('nozoom');
        entity.addComponent(pc.components.Sprite.create(diceSprite));
        entity.addComponent(DiceData.create({
            value: null,
            isRollComplete: false
        }));
        this.diceEntities.push(entity);
    }


});
