/***
 *   Project Neptune - (C)2015 Eddie Games Inc.
 *   Author: Marc D. Arbesman
 * 
 *  pn.scene.TitleScene
 *  
 *  The title scene serves as a presentation of the entry to the game as well as a log in screen
 * 
 * 
 * 
 */

pn.scene.LoginScene = pc.Scene.extend('LoginScene',
{},
{   
    formLayer: null,
    _loadingLayer: null,
    errMsgEntity: null, 
    bgLayer:null,
    gameDeviceSettings: null,
    init:function() {
        this._super();
        this.gameDeviceSettings = new pn.DeviceConfigFactory();
        this.bgLayer = this.addLayer(new pn.layer.FormBackgroundLayer('bgLayer'));
        this.bgLayer.zIndex = 1;
        //-----------------------------------------------------------------------------
        // title layer
        //-----------------------------------------------------------------------------

        // This uses the Form Layer class to facilitate form constructions
        this.formLayer = this.addLayer(new pc.FormLayer('title layer', 10000, 10000));
        this.formLayer.zIndex = 2;               
        this._loadingLayer = this.addLayer(new pn.layer.LoadingLayer());
        
        
        // title
        var title1 = pc.Entity.create(this.formLayer);
        title1.addComponent(pc.components.Spatial.create({ 
            w: 250, 
            h: 50, 
            x: this.gameDeviceSettings.getLoginTitle1PosX(), 
            y: this.gameDeviceSettings.getLoginTitle1PosY() 
        }));
        title1.addComponent(pc.components.Text.create({ 
            fontHeight: this.gameDeviceSettings.getLoginTitle1FontHeight(), 
            lineWidth: 0, 
            strokeColor: '#dd1c3d', 
            color: '#4CC8FB', 
            text: ['Project Neptune'] 
        }));

        var googleLogin = pc.ButtonEntity.create(this.formLayer);
        googleLogin.initButton({
            posX: this.gameDeviceSettings.getLoginGoogleButtonPosX(),
            posY: this.gameDeviceSettings.getLoginGoogleButtonPosY(),
            buttonWidth: this.gameDeviceSettings.getLoginGoogleButtonWidth(),                        
            buttonHeight: this.gameDeviceSettings.getLoginGoogleButtonHeight(),
            buttonFontHeight: this.gameDeviceSettings.getLoginGoogleFontHeight(),
            buttonColor: '#cf3f2e',
            mouseOverColor: '#cf3f2e',
            mouseClickColor: '#cf3f2e',
            mouseOverFontColor:'#ffffff',
            mouseClickFontColor:'#ffffff',        
            labelOffset: {
                x:0,
                y:0
            },
            label: 'Connect with Google',
            margin: { right: 350, top: 15, bottom: 27 },
            unClickEvent: function () {     
            //use the formlayer's clear message method
                this.layer._clearErrorMsg();                                    
                var externalProviderUrl = pc.device.game.__constants.get('SYS_API_GOOGLE_LOGIN');
                window.open(externalProviderUrl, "Authenticate Account", "location=0,status=0,width=600,height=750");
                // console.log('yo');
            },
            touchEvent: function () {     
            //use the formlayer's clear message method
                this.layer._clearErrorMsg();                                    
                var externalProviderUrl = pc.device.game.__constants.get('SYS_API_GOOGLE_LOGIN');
                window.open(externalProviderUrl, "Authenticate Account", "location=0,status=0,width=600,height=750");
                // console.log('yo');
            }

        });
        var facebookLogin = pc.ButtonEntity.create(this.formLayer);
        facebookLogin.initButton({
            posX: this.gameDeviceSettings.getLoginFacebookButtonPosX(),
            posY: this.gameDeviceSettings.getLoginFacebookButtonPosY(),
            buttonWidth:  this.gameDeviceSettings.getLoginFacebookButtonWidth(),                        
            buttonHeight:  this.gameDeviceSettings.getLoginFacebookButtonHeight(),
            buttonFontHeight: this.gameDeviceSettings.getLoginFacebookFontHeight(),
            buttonColor: '#2a4a8f',
            mouseOverColor: '#2a4a8f',
            mouseClickColor: '#2a4a8f',
            mouseOverFontColor:'#ffffff',
            mouseClickFontColor:'#ffffff',        
            labelOffset: {
                 x:0,
                y:0
            },
            label: 'Connect with Facebook',
            margin: { right: 350, top: 0, bottom: 30 },
            clickEvent: function () { 
                //use the formlayer's clear message method
                this.layer._clearErrorMsg();         
                var externalProviderUrl = pc.device.game.__constants.get('SYS_API_FACEBOOK_LOGIN');
                window.open(externalProviderUrl, "Authenticate Account", "location=0,status=0,width=600,height=750");                
            },
            touchEvent: function () { 
                //use the formlayer's clear message method
                this.layer._clearErrorMsg();         
                var externalProviderUrl = pc.device.game.__constants.get('SYS_API_FACEBOOK_LOGIN');
                window.open(externalProviderUrl, "Authenticate Account", "location=0,status=0,width=600,height=750");                
            }
        });



        // config method requires an array of form item objects, a submit info object, and callbacks
        // for onLoading, onComplete, onLoading,  onSuccess and onError
        // these methods are called back by the XMLHttpRequest based on readystate
        
        this.formLayer.config(
        [{
            id:'username',      
            label: 'Email',
            offset: {
                x: 5,
                y: -12
            },
            posX: this.gameDeviceSettings.getUsernameTextboxPosX(),
            posY: this.gameDeviceSettings.getUsernameTextboxPosY(),
            width: this.gameDeviceSettings.getUsernameTextboxWidth(),
            height: this.gameDeviceSettings.getUsernameTextboxHeight(),
            fontHeight: this.gameDeviceSettings.getUsernameTextboxFontHeight(),
            focusFontHeight: this.gameDeviceSettings.getUsernameTextboxFontHeight(),
            blurFontHeight: this.gameDeviceSettings.getUsernameTextboxFontHeight(),
            offset: {x:10, y:-17},
            isSecure: false,
            value: '',
            hidden: false,
            maxChar: 27,
            tabOrder: 1
        },
        {
            id: 'password',
            label: 'Password',
            offset: {
                x: 5,
                y: -12
            },
            posX: this.gameDeviceSettings.getPasswordTextboxPosX(),
            posY: this.gameDeviceSettings.getPasswordTextboxPosY(),
            width: this.gameDeviceSettings.getPasswordTextboxWidth(),
            height: this.gameDeviceSettings.getPasswordTextboxHeight(),
            fontHeight: this.gameDeviceSettings.getPasswordTextboxFontHeight(),
            focusFontHeight: this.gameDeviceSettings.getPasswordTextboxFontHeight(),
            blurFontHeight: this.gameDeviceSettings.getPasswordTextboxFontHeight(),
            offset: {x:10, y:-17},
            isSecure: true,
            value: '',
            hidden: false,
            maxChar: 27,
            tabOrder: 2
        },
        {
            hidden: true,
            id: 'grant_type',
            value: 'password'
        },
        {
            hidden: true,
            id: 'client_id',
            value: pc.device.game.__constants.get('SYS_CLIENT_ID')

        }],
        {
            method: 'POST',
            url: pc.device.game.__constants.get('SYS_API_DOMAIN') + pc.device.game.__constants.get('SYS_API_TOKEN'),
            label: 'Login',
            buttonWidth: this.gameDeviceSettings.getLoginButtonWidth(),
            buttonHeight: this.gameDeviceSettings.getLoginButtonHeight(),
            posX: this.gameDeviceSettings.getLoginButtonPosX(),
            posY: this.gameDeviceSettings.getLoginButtonPosY(),
            buttonBorderWidth: 2,
            buttonFontHeight: this.gameDeviceSettings.getLoginButtonFontHeight(),
            labelOffset: { 
                x: this.gameDeviceSettings.getLoginButtonLabelX(), 
                y: this.gameDeviceSettings.getLoginButtonLabelY()
            },           
            // margin: {
            //     left: 40,
            //     bottom: 20,
            //     top: 0,
            //     right: 0
            // },


        },
        this.onLoginSuccess,
        this.onLoginComplete,
        this.onAjaxLoading,
        this.onLoginError);
        var registerButton = pc.ButtonEntity.create(this.formLayer);
        registerButton.initButton({
            label: 'Create a Free Account',
            buttonWidth: this.gameDeviceSettings.getLoginRegisterButtonWidth(),
            buttonHeight: this.gameDeviceSettings.getLoginRegisterButtonHeight(),
            posX: this.gameDeviceSettings.getLoginRegisterButtonPosX(),
            posY: this.gameDeviceSettings.getLoginRegisterButtonPosY(),
            buttonFontHeight: this.gameDeviceSettings.getLoginRegisterButtonFontHeight(),
            labelOffset: { 
                x: this.gameDeviceSettings.getLoginRegisterButtonLabelX(), 
                y: this.gameDeviceSettings.getLoginRegisterButtonLabelY() 
            },
            // margin: {
            //     left: 40,
            //     bottom: 50,
            //     top: 0,
            //     right: 0
            // },
            unClickEvent: function () { 
                //use the formlayer's clear message method
                this.layer._clearErrorMsg();         
            }
        });
    },
    /**
     * Call Back event handers from the formLayer
     * 
     */
    onAjaxLoading: function (xhr) {
      
        // show loading sprite
        var self = xhr.callingObject.scene;
        self._loadingLayer.setActive();   
        self.formLayer.deactivateInputs();     
    },
    onLoginSuccess: function (res, self) {
        self.callingObject.buttonEntity.removeTag('submitting');
        // handle successful login by setting the local storage object 
            
            pc.LocalStore.loadJSONObject({ obj: res, storeName: pc.device.game.__constants.get('SYS_STORAGE_PLAYER_AUTH') });
        // send user to lobby 
          //  pc.device.game.activateMenuScene();
          var client = new pn.util.pnHttpClient({
            cache: true,
            callingObject: self.callingObject,
            onSuccess: function(res, self){
                pc.LocalStore.loadJSONObject({ obj: res, storeName: pc.device.game.__constants.get('SYS_STORAGE_PLAYER_PROFILE') });
                pc.device.game.activateMenuScene();
                // pc.device.game.activateHubScene();
            }            
        });
       
        client.getAsync({
            url: pc.device.game.__constants.get('SYS_API_DOMAIN') + '/api/Account/Player/' + res.userId,
             headers: [
                {
                    header: 'Authorization',
                    value: 'Bearer ' + res.access_token
                }
            ]
        });
    },

    onLoginComplete: function (xhr) {
        xhr.callingObject.scene._loadingLayer.setInactive();
        xhr.callingObject.scene.formLayer.activateInputs();
        if (xhr.callingObject.buttonEntity.hasTag('submiting')) {
            xhr.callingObject.buttonEntity.removeTag('submiting');
        }
        
        var entityMgr = xhr.callingObject.getEntityManager(),
        node = entityMgr.entities.first;
        while (node) {
            if (node.obj.hasTag('loadingIcon')) {
                    entityMgr.remove(node.obj);
                    
            }
            node = node.next();
        }
    },
    onLoginError: function (response, xhr) {
        xhr.callingObject.scene._loadingLayer.setInactive();
        xhr.callingObject.scene.formLayer.activateInputs();
        xhr.callingObject.buttonEntity.removeTag('submitting');
        // callingObject is the layer, 
        var entityMgr = xhr.callingObject.getEntityManager(),
            node = entityMgr.entities.first,
            gameSettings = xhr.callingObject.scene.gameDeviceSettings,

            errorDesc;
        // blank error_description means the form was submitted but not filled out.
        if (response.error_description === undefined) {
            errorDesc = 'Please Enter a Valid Username and Password.';
        }
        else {
            errorDesc = response.error_description;
        }
        while (node) {
            if (node.obj.hasTag('loadingIcon')) {
                entityMgr.remove(node.obj);

            }
            node = node.next();
        }
        //handle failure
        xhr.callingObject.errMsgEntity = new pc.Entity(xhr.callingObject);
        xhr.callingObject.errMsgEntity.addComponent(pc.components.Spatial.create({
            x: gameSettings.getLoginErrorPosX(),
            y: gameSettings.getLoginErrorPosY(),
            w: gameSettings.getLoginErrorWidth(),
            h: gameSettings.getLoginErrorHeight()

        }));
        xhr.callingObject.errMsgEntity.addComponent(pc.components.Text.create({
            fontHeight: gameSettings.getLoginErrorFontHeight(),
            lineWidth: 1,
            strokeColor: '#dd1c3d',
            color: '#dd1c3d',
            text: [errorDesc]
        }));
    },
    // deactivateInputs: function(){

    // },
    // activateInputs: function(){

    // },
    process: function () {
        // clear the background
        pc.device.ctx.clearRect(0, 0, pc.device.canvasWidth, pc.device.canvasHeight);
        // always call the super
        this._super();
    }


});