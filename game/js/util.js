﻿
pn.util.Util = pc.Base.extend('pn.util.Util',
{
    keyMap: [],
    shiftKeyMap: [],

    /*
     * initKeyMap - the keymap is used to translate the keycode passed back by an event into the ascii character of the keyboard input.
     * 
     */

    initKeyMap:function(){
        this.keyMap[48] = 0;
        this.keyMap[49] = 1;
        this.keyMap[50] = 2;
        this.keyMap[51] = 3;
        this.keyMap[52] = 4;
        this.keyMap[53] = 5;
        this.keyMap[54] = 6;
        this.keyMap[55] = 7;
        this.keyMap[56] = 8;
        this.keyMap[57] = 9;
        this.keyMap[65] = 'a';
        this.keyMap[66] = 'b';
        this.keyMap[67] = 'c';
        this.keyMap[68] = 'd';
        this.keyMap[69] = 'e';
        this.keyMap[70] = 'f';
        this.keyMap[71] = 'g';
        this.keyMap[72] = 'h';
        this.keyMap[73] = 'i';
        this.keyMap[74] = 'j';
        this.keyMap[75] = 'k';
        this.keyMap[76] = 'l';
        this.keyMap[77] = 'm';
        this.keyMap[78] = 'n';
        this.keyMap[79] = 'o';
        this.keyMap[80] = 'p';
        this.keyMap[81] = 'q';
        this.keyMap[82] = 'r';
        this.keyMap[83] = 's';
        this.keyMap[84] = 't';
        this.keyMap[85] = 'u';
        this.keyMap[86] = 'v';
        this.keyMap[87] = 'w';
        this.keyMap[88] = 'x';
        this.keyMap[89] = 'y';
        this.keyMap[90] = 'z';
        // num pad numbers
        this.keyMap[96] = 0;
        this.keyMap[97] = 1;
        this.keyMap[98] = 2;
        this.keyMap[99] = 3;
        this.keyMap[100] = 4;
        this.keyMap[101] = 5;
        this.keyMap[102] = 6;
        this.keyMap[103] = 7;
        this.keyMap[104] = 8;
        this.keyMap[105] = 9;
        this.keyMap[190] = '.';
        // handling shift key by adding 100 to lowercase numbers
        this.shiftKeyMap[65] = 'A';
        this.shiftKeyMap[66] = 'B';
        this.shiftKeyMap[67] = 'C';
        this.shiftKeyMap[68] = 'D';
        this.shiftKeyMap[69] = 'E';
        this.shiftKeyMap[70] = 'F';
        this.shiftKeyMap[71] = 'G';
        this.shiftKeyMap[72] = 'H';
        this.shiftKeyMap[73] = 'I';
        this.shiftKeyMap[74] = 'J';
        this.shiftKeyMap[75] = 'K';
        this.shiftKeyMap[76] = 'L';
        this.shiftKeyMap[77] = 'M';
        this.shiftKeyMap[78] = 'N';
        this.shiftKeyMap[79] = 'O';
        this.shiftKeyMap[80] = 'P';
        this.shiftKeyMap[81] = 'Q';
        this.shiftKeyMap[82] = 'R';
        this.shiftKeyMap[83] = 'S';
        this.shiftKeyMap[84] = 'T';
        this.shiftKeyMap[85] = 'U';
        this.shiftKeyMap[86] = 'V';
        this.shiftKeyMap[87] = 'W';
        this.shiftKeyMap[88] = 'X';
        this.shiftKeyMap[89] = 'Y';
        this.shiftKeyMap[90] = 'Z';
        this.shiftKeyMap[48] = ')';
        this.shiftKeyMap[49] = '!';
        this.shiftKeyMap[50] = '@';
        this.shiftKeyMap[51] = '#';
        this.shiftKeyMap[52] = '$';
        this.shiftKeyMap[53] = '%';
        this.shiftKeyMap[54] = '^';
        this.shiftKeyMap[55] = '&';
        this.shiftKeyMap[56] = '*';
        this.shiftKeyMap[57] = '(';
    },
    keyMapToUpper: function (keyCode) {
        return this.shiftKeyMap[keyCode];
    },
    getScaledPosition: function (pos, xOrY) {
        var scaledPos = 0;
        var scaledLength = 0;
        if (xOrY.toUpperCase() === "X") {
            scaledLength = Math.ceil((window.innerWidth - pc.device.game.currCanvasWidth) / 2)
            scaledPos = Math.ceil((pos * pc.device.game.currCanvasWidth) / pc.device.game.resolutionWidth) + scaledLength;

        }
        else if (xOrY.toUpperCase() === "Y") {
            scaledLength = Math.ceil((window.innerHeight - pc.device.game.currCanvasHeight) / 2)
            scaledPos = Math.ceil((pos * pc.device.game.currCanvasHeight) / pc.device.game.resolutionHeight) + scaledLength;
        }
        else {
            throw ('Correct axis not specified');
        }
        return scaledPos;
    },
    getDescaledPosition: function (pos, xOrY) {
        var deScaledPos = 0;
        var deScaledLength = 0;
        if (xOrY.toUpperCase() === "X") {
            deScaledLength = Math.ceil((window.innerWidth - pc.device.game.currCanvasWidth) / 2)
            deScaledPos = Math.ceil((((pos - deScaledLength) * pc.device.game.resolutionWidth) / pc.device.game.currCanvasWidth));
        }
        else if (xOrY.toUpperCase() === "Y") {
            deScaledLength = Math.ceil((window.innerHeight - pc.device.game.currCanvasHeight) / 2)
            deScaledPos = Math.ceil((((pos - deScaledLength) * pc.device.game.resolutionHeight) / pc.device.game.currCanvasHeight));
        }
        else {
            throw ('Correct axis not specified');
        }
        return deScaledPos;
    },
    getScaledDimension: function (dim) {
        var scaledDim = 0;
        var scaledLength = 0;
        var ratio = (pc.device.game.currCanvasWidth / pc.device.game.resolutionWidth);

        scaledDim = Math.ceil(dim * ratio);

        return scaledDim;
    },
    getImageIdentifier: function (imageName, ID, playerID) {
        // Naming convention for the image identifier in the loader object is <LargeImg>_<ID>_large
        if (imageName === '' || ID === '' || playerID === '') {
            return '';
        }

        return imageName.replace('.png', '') + "_" + ID + '_' + playerID;
    },
    getCardCenteredPosX: function (cardWidth) {
        return Math.floor((pc.device.canvas.width / 2) - (cardWidth / 2));
    },
    getCardCenteredPosY: function (cardHeight) {
        return Math.floor((pc.device.canvas.height / 2) - (cardHeight / 2));
    },
    getTextWidth: function (options) {
        if (!pc.valid(options.fontSize))
            throw 'Cannot Measure the Text, No Font Size Specified.';
        if (!pc.valid(options.text))
            throw 'Cannot Measure the Text, No Text Specified.';
        if (!pc.valid(options.fontFamily))
            throw 'Cannot Measure the Text, No Font Family Specified.';
        pc.device.ctx.font = options.fontSize + 'px ' + options.fontFamily;
        return pc.device.ctx.measureText(options.text).width;
    },
    formatDateTime: function(dateNum){
        var dateOptions = {
                weekday: "short", 
                year: "numeric", 
                month: "short",
                day: "numeric", 
                hour: "2-digit",
                minute: "2-digit"
            };
           var formattedDate = Date.parse(dateNum) || 0;
           if(formattedDate > 0){
                    formattedDate = new Date(formattedDate);
                    formattedDate = formattedDate.toLocaleTimeString("en-us", dateOptions);
                    return formattedDate;
            }
            else{
                return 'N/A'
            }
    },
    doLogout: function(){
         var refreshToken = pc.LocalStore.getJSONObject(pc.device.game.__constants.get('SYS_STORAGE_PLAYER_AUTH')).refresh_token;
                if(pc.valid(refreshToken)){
                    var client = new pn.util.pnHttpClient({callingObject: this, cache:false});
                    client.delAsync({
                            url: pc.device.game.__constants.get('SYS_API_DOMAIN') + pc.device.game.__constants.get('SYS_API_LOGOUT') + '?tokenid=' + refreshToken,
                            headers: [
                                {
                                    header: 'Authorization',
                                    value: 'Bearer ' + pc.LocalStore.getJSONObject(pc.device.game.__constants.get('SYS_STORAGE_PLAYER_AUTH')).access_token
                                }
                            ]
                    });   
                }
                pc.LocalStore.removeObject(pc.device.game.__constants.get('SYS_STORAGE_PLAYER_AUTH'));
                pc.LocalStore.removeObject("playerProfile");
        }
    },
{});

