﻿pn.components.DiceData = pc.components.Component.extend('DiceData',    
    {
        ID: null,
        value: null,
        isRollComplete: null,

        create:function (options)
        {
            var n = this._super();
            n.config(options);
            return n;
        }
    },
    {
        init: function (options) {
            this._super('dicedata');
        },
        config: function (options) {
            this.value = options.value;
            this.isRollComplete = options.isRollComplete;
        }
    });
