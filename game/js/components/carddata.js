﻿
pn.components.CardData = pc.components.Component.extend('CardData',
    {
        ID: null,
        cardClass: null,
        name: null,
        subtitle: null,
        cardType: null,
        statusEffect: null,
        description: null,
        leadership: null,
        fleetCommand: null,
        armor: null,
        structure: null,
        power: null,
        accuracy: null,
        damage: null,
        rangeMin: null,
        rangeMax: null,
        shield: null,
        restPower: null,
        special: null,
        smallImg: null,
        largeImg: null,
        position: null,
        shieldStatus: null,
        damageStatus: null,
        crewCost: null,
        speed: null,
        image: null,
        imageMin: null,
        imageZoomed: null,                
        pos: null,
        tileOver: null,
        playFieldMap: null,
        cardWidth: null,
        cardHeight: null,
        cardWidthZoomed: null,
        cardHeightZoomed: null,
        cardPosXZoomed: null,
        cardPosYZoomed: null,
        boardTilePosition: null,
        zonePosX: null,
        zonePosY: null,
        create:function (options)
        {
            var n = this._super();
            n.config(options);
            return n;
        }
    },
    {
        init: function (options) {
            this._super('carddata');
        },
        config: function (options) {
            if (!options.image && !options.imageZoomed)
                throw 'CardData requires at least an image or image zoomed set';
            
            this.ID = options.ID;
            this.cardClass = options.cardClass;
            this.name = options.name;
            this.subtitle = options.subtitle;
            this.cardType = options.cardType;
            this.description = pc.checked(options.description, '');
            this.leadership = pc.checked(options.leadership, 0);
            this.fleetCommand = pc.checked(options.fleetCommand, 0);
            this.statusEffect = options.cardType;
            this.armor = options.armor;
            this.structure = options.structure;
            this.power = options.power;
            this.accuracy = options.accuracy;
            this.damage = options.damage;
            this.rangeMax = options.rangeMax;
            this.rangeMin = options.rangeMin;
            this.shield = options.shield;
            this.restPower = options.restPower;
            this.special = options.special;
            this.position = options.position;
            this.shieldStatus = options.shieldStatus;
            this.damageStatus = options.damageStatus;
            this.crewCost = options.crewcost;
            this.speed = options.speed;
            this.statusEffect = options.statusEffect;
            this.image = options.image;
            this.imageMin = options.imageMin;
            this.imageZoomed = options.imageZoomed;                        
            this.pos = options.pos;
            this.tileOver = options.tileOver;
            this.playFieldMap = options.playFieldMap;
            this.cardWidth = options.cardWidth;
            this.cardHeight = options.cardHeight;
            this.cardWidthZoomed = options.cardWidthZoomed;
            this.cardHeightZoomed = options.cardHeightZoomed;
            this.cardPosXZoomed = options.cardPosXZoomed;
            this.cardPosYZoomed = options.cardPosYZoomed;
            this.boardTilePosition = options.boardTilePosition;
        }
    });
