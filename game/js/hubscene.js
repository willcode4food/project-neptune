/***
 *   Project Neptune - (C)2015 Eddie Games Inc.
 *   Author: Marc D. Arbesman
 * 
 *  pn.scene.HubScene
 *  
 *  The hub scene serves as a presentation of the list of active games and access to other features
 * 
 * 
 * 
 */

pn.scene.HubScene = pc.Scene.extend('HubScene',
{},
{   
    sessionLayer: null,
    headerLayer: null,
    _loadingLayer: null,
    menuLayer: null,    
    access_token: null,
    httpClient: null,
    gameSessions: null,
    sessionButtonLayer: null,
    sessionLayer: null,
    bgLayer: null,
    playerEntity: null,
    gameDeviceSettings: null,
    util: null,
    init: function(){
        this.gameDeviceSettings = new pn.DeviceConfigFactory();
        this.util = pn.util.Util; 
        this._super();      
        this.bgLayer = this.addLayer(new pn.layer.FormBackgroundLayer('bgLayer'));
        this._loadingLayer = this.addLayer(new pn.layer.LoadingLayer());
        this._initMenu();        
        this._initHeader(); 
       
    },
    onActivated: function(){
        this._initSecurityInfo();          
        this._setPlayerInfo();
        this._getGameSessions();
        this._super();

    },
    _initLayers: function(){
        this.sessionLayer = this.addLayer(new pc.FormListViewLayer('sessions layer', 5000, 5000));
    },
   
    _initMenu:  function(){
        this.menuLayer = this.addLayer(new pc.EntityLayer('hub menu layer', 1000,1000));
        this.menuLayer.zIndex = 2;
        this.menuLayer.addSystem(new pc.systems.Render());
        this.menuLayer.addSystem(new pc.systems.Effects());
        this.menuLayer.addSystem(new pc.systems.FormInput());
        this.menuLayer.addSystem(new pc.systems.Layout());
        var bgEntity = pc.Entity.create(this.menuLayer);
     
            bgEntity.addComponent(pc.components.Spatial.create({
                    x:0,
                    y:0,
                    w: this.gameDeviceSettings.getBaseDeviceWidth(),
                    h: this.gameDeviceSettings.getHubHeaderHeight()
            }));
            bgEntity.addComponent(pc.components.Rect.create({
                color: '#0F1D36'
            }));
            bgEntity.addComponent(pc.components.Shadow.create({
              offSetY: 3,
              offSetX: 0,
              blur: 6,
              color: '#000000',
        })),
        menuButton = pc.ButtonEntity.create(this.menuLayer),
        facebookButton = pc.ButtonEntity.create(this.menuLayer),
        twitterButton = pc.ButtonEntity.create(this.menuLayer),
        newGameButton = pc.ButtonEntity.create(this.menuLayer);
        menuButton.initButton({
            label: 'Menu',
            buttonWidth: this.gameDeviceSettings.getHubGotoMenuWidth(),
            buttonHeight: this.gameDeviceSettings.getHubGotoMenuHeight(),
            buttonFontHeight: this.gameDeviceSettings.getHubGotoMenuFontHeight(),
            posX: this.gameDeviceSettings.getHubGotoMenuPosX(),
            posY: this.gameDeviceSettings.getHubGotoMenuPosY(),
            labelOffset: { x: 0, y: this.gameDeviceSettings.getHubGotoMenuLabelPosY() },
            unClickEvent: function () {
                var self = this;
                pc.device.game.activateMenuScene();
            },

        });
        newGameButton.initButton({
                label: 'New Game',
                buttonWidth: this.gameDeviceSettings.getHubNewGameWidth(),
                buttonHeight: this.gameDeviceSettings.getHubNewGameHeight(),
                posX: this.gameDeviceSettings.getHubNewGamePosX(),
                posY: this.gameDeviceSettings.getHubNewGamePosY(),
                buttonFontHeight: this.gameDeviceSettings.getHubGotoMenuFontHeight(),
                labelOffset: { x: 0, y: this.gameDeviceSettings.getHubGotoMenuLabelPosY()},
                margin: {
                    left: 0,
                    bottom: 0,
                    top: 10,
                    right: -10
                },
                unClickEvent: function () {
                    var self = this;
                // pc.device.game.activateMenuScene();
                return;
                },
        });
        facebookButton.initButton({
            label: '',
            imageResource: pc.device.loader.get('facebookInviteButton'),
            posX: this.gameDeviceSettings.getHubFBInvitePosX(),
            posY: this.gameDeviceSettings.getHubFBInvitePosY(),
            buttonWidth: this.gameDeviceSettings.getHubFBInviteWidth(),
            buttonHeight: this.gameDeviceSettings.getHubFBInviteHeight(),
            buttonFontHeight: 25,
            labelOffset: { x: 0, y: 0 },
            shadowColor: '#000000',
            shadowOffSetX: 1,
            shadowOffSetY: 1.75,
            shadowBlur: 10,
            unClickEvent: function () {
                var self = this;
                // pc.device.game.activateMenuScene();
                return;
            },

        });
        twitterButton.initButton({
            label: '',
            imageResource: pc.device.loader.get('twitterInviteButton'),
            buttonWidth: this.gameDeviceSettings.getHubChallengeWidth(),
            buttonHeight: this.gameDeviceSettings.getHubChallengeHeight(),
            posX: this.gameDeviceSettings.getHubChallengePosX(),
            posY: this.gameDeviceSettings.getHubChallengePosY(),
            buttonFontHeight: 25,
            labelOffset: { x: 0, y: 0 },
            margin: {
                left: 0,
                bottom: 0,
                top: 10,
                right: 10
            },
            shadowColor: '#000000',
            shadowOffSetX: 1,
            shadowOffSetY: 1.75,
            shadowBlur: 10,
            unClickEvent: function () {
                // var self = this;
                // pc.device.game.activateMenuScene();
                return;
            },
        });
    },
    _initHeader: function(){     
            this.playerEntity = pc.Entity.create(this.menuLayer);
            this.playerEntity.addComponent(pc.components.Spatial.create({
                    x:this.gameDeviceSettings.getHubHeaderPlayerLabelPosX(),
                    y:this.gameDeviceSettings.getHubHeaderPlayerLabelPosY(),
                    w: this.gameDeviceSettings.getBaseDeviceWidth(),
                    h: 80
            }));
            this.playerEntity.addComponent(pc.components.Rect.create({
                color: ''
            }));       
    },
 
    _setPlayerInfo: function(){
        if(this.playerEntity.hasComponentOfType('textlayout')){
            this.playerEntity.removeComponent(this.playerEntity.getComponent('textlayout'));    
        }        
        this.playerEntity.addComponent(pc.components.TextLayout.create({
                layout:[{
                        text: 'Welcome back ' + pc.LocalStore.getJSONObject(pc.device.game.__constants.get('SYS_STORAGE_PLAYER_PROFILE')).nickName,
                        color: '#ffffff',
                        strokeColor: '#382262',
                        lineWidth: 0,
                        fontHeight: this.gameDeviceSettings.getHubHeaderFontHeight(),
                        font: 'Government Agent BB',
                        offset: { x: 17, y: -35 }
                }]                    
        }));
    },
    _initSecurityInfo: function(){
        this.access_token = pc.LocalStore.getJSONObject(pc.device.game.__constants.get('SYS_STORAGE_PLAYER_AUTH')).access_token;
    },
    _getGameSessions: function(){
        this._initHttpClient({
            callingObject: this,
            onSuccess: this._showSessions,
            onLoading: this._onLoading

        });
        this.httpClient.getAsync({
            url: pc.device.game.__constants.get('SYS_API_DOMAIN') + pc.device.game.__constants.get('SYS_API_GAME_SESSIONS'),
            headers: [
                {
                    header: 'Authorization',
                    value: 'Bearer ' + pc.LocalStore.getJSONObject(pc.device.game.__constants.get('SYS_STORAGE_PLAYER_AUTH')).access_token
                }
            ]

        });
    },
    _initHttpClient: function(options){
        this.httpClient = new pn.util.pnHttpClient({
            callingObject: this,            
            cache: pc.checked(options.cache, true),            
            onSuccess: pc.checked(options.onSuccess, this.onSuccess),
 
        });
    },
    /**
     * Call Back event handers from the formLayer
     * 
     */
    _showSessions:function(gameSessions, xhr){
        var self = xhr.callingObject, opponentName, playerId = pc.LocalStore.getJSONObject('playerProfile').idPlayer, 
        startTime, lastTurn, options
       
        listViewItems = [];
        self.gameSessions = gameSessions;
        self.menuLayer.activateInputs();
        self._initLayers();

        if(self.gameSessions.length > 0){
                options = {
                    itemHeight: self.gameDeviceSettings.getHubListItemHeight(),
                    itemWidth: self.gameDeviceSettings.getHubListItemWidth(),
                    itemStartX: self.gameDeviceSettings.getHubListItemPosX(),
                    itemStartY: self.gameDeviceSettings.getHubListItemPosY(),
                    // itemPadding: 20,
                     itemColor: '#0F1D36',
                    // itemBorderColor: '#4CC8FB',
                    // itemBorderWidth: 0,
                    scrollOptions:{
                        zIndex: 0,
                        zooming: false,
                        bouncing: false,
                        animating: true,
                        animationDuration: 500,
                        snapping: false,
                        paging: false,
                        scrollingX: false,
                        scrollingY: true,
                        locking: false,
                        pageing: false,
                        deviceWidth: self.gameDeviceSettings.getBaseDeviceWidth(),
                        deviceHeight: self.gameDeviceSettings.getBaseDeviceHeight()
                    }
                };
            for(var x in self.gameSessions){
                // create listItems
                if(self.gameSessions[x].idPlayer_1 === playerId){
                    opponentName = self.gameSessions[x].player_2;
                }
                else{
                     opponentName = self.gameSessions[x].player_1;
                }

                // startTime = Date.parse(self.gameSessions[x].startTime) || 0;
                // lastTurn = Date.parse(self.gameSessions[x].lastTurn) || 0;
                // console.log(lastTurn);
                listViewItems.push({
                    textLayouts:[
                        {
                            text: '.VS',
                            color: '#c50638',
                            strokeColor: '#A2E4FC',
                            lineWidth: 0,
                            fontHeight: self.gameDeviceSettings.getHubListItemLabel1FontHeight(),
                            font: 'Government Agent BB',
                            offset: { 
                                x: self.gameDeviceSettings.getHubListItemLabel1PosX(), 
                                y: self.gameDeviceSettings.getHubListItemLabel1PosY()
                            }
                        },
                        {
                            text:  opponentName,
                            color: '#A2E4FC',
                            strokeColor: '#ffffff',
                            lineWidth: 0,
                            fontHeight: self.gameDeviceSettings.getHubListItemLabel2FontHeight(),
                            font: 'Government Agent BB',
                            offset: { 
                                x: self.gameDeviceSettings.getHubListItemLabel2PosX(), 
                                y: self.gameDeviceSettings.getHubListItemLabel2PosY()
                            }
                        },
                        {
                            text:  'Game Started: ',
                            color: '#a8ffd1',
                            strokeColor: '#382262',
                            lineWidth: 0,
                            fontHeight: self.gameDeviceSettings.getHubListItemLabel3FontHeight(),
                            font: 'PT Sans',
                            fontStyle: 'bold',
                            offset: { 
                                x: self.gameDeviceSettings.getHubListItemLabel3PosX(), 
                                y: self.gameDeviceSettings.getHubListItemLabel3PosY()
                            }
                        },
                        {
                            text: self.util.formatDateTime(self.gameSessions[x].startTime),
                            color: '#a8ffd1',
                            strokeColor: '#5F5259',
                            lineWidth: 0,
                            fontHeight: self.gameDeviceSettings.getHubListItemLabel4FontHeight(),
                            font: 'PT Sans',
                            fontStyle: 'bold',
                            offset: { 
                                x: self.gameDeviceSettings.getHubListItemLabel4PosX(), 
                                y: self.gameDeviceSettings.getHubListItemLabel4PosY()
                            }
                        },
                        {
                            text:  'Last Turn: ',
                            color: '#a8ffd1',
                            strokeColor: '#382262',
                            lineWidth: 0,
                            fontHeight: self.gameDeviceSettings.getHubListItemLabel5FontHeight(),
                            font: 'PT Sans',
                            fontStyle: 'bold',
                            offset: { 
                                x: self.gameDeviceSettings.getHubListItemLabel5PosX(), 
                                y: self.gameDeviceSettings.getHubListItemLabel5PosY()
                            }
                        },
                           {
                            text:  self.util.formatDateTime(self.gameSessions[x].lastTurn) !== 'N/A' ? self.util.formatDateTime(self.gameSessions[x].lastTurn) : 'No Turn Taken' ,
                            color: '#a8ffd1',
                            strokeColor: '#5F5259',
                            lineWidth: 0,
                            fontHeight: self.gameDeviceSettings.getHubListItemLabel6FontHeight(),
                            font: 'PT Sans',
                            fontStyle: 'bold',
                           offset: { 
                                x: self.gameDeviceSettings.getHubListItemLabel6PosX(), 
                                y: self.gameDeviceSettings.getHubListItemLabel6PosY()
                            }
                        }



                    ], //array,
                    action:{
                        label: 'Continue',                                 
                        posX: self.gameDeviceSettings.getHubListItemActionPosX(),
                        posY: self.gameDeviceSettings.getHubListItemActionPosY(),
                        buttonWidth: self.gameDeviceSettings.getHubListItemActionWidth(), 
                        buttonHeight: self.gameDeviceSettings.getHubListItemActionHeight(),
                        buttonFontHeight: self.gameDeviceSettings.getHubGotoMenuFontHeight(),
                        labelOffset: {
                            x:0,
                            y:-20
                        },    
                        unClickEvent: function () {
                            var self = this;
                            pc.device.game.activateMenuScene();
                        },
                    }

                });              
            }  
           
        }
        else{
            options = {
                    itemHeight: self.gameDeviceSettings.getHubListItemAlertHeight(),
                    itemWidth: self.gameDeviceSettings.getHubListItemAlertWidth(),
                    itemStartX: self.gameDeviceSettings.getHubListItemAlertPosX(),
                    itemStartY: self.gameDeviceSettings.getHubListItemAlertPosY(),
                    // itemPadding: 20,
                    itemColor: '#0F1D36',
                    itemBorderColor: '#c50638',
                    itemBorderWidth: 2,
                    scrollOptions:{
                        zIndex: 0,
                        zooming: false,
                        bouncing: false,
                        animating: true,
                        animationDuration: 500,
                        snapping: false,
                        paging: false,
                        scrollingX: false,
                        scrollingY: true,
                        locking: false,
                        pageing: false,
                        deviceWidth: self.gameDeviceSettings.getBaseDeviceWidth(),
                        deviceHeight: self.gameDeviceSettings.getBaseDeviceHeight()
                    }
            };
            listViewItems.push({
                    textLayouts:[
                        {
                            text: 'No Games Currently in Session',
                            color: '#A2E4FC',
                            strokeColor: '#A2E4FC',
                            lineWidth: 0,
                            fontHeight: self.gameDeviceSettings.getHubListItemAlertFontHeight(),
                            font: 'Government Agent BB',
                            offset: { 
                                x: self.gameDeviceSettings.getHubListItemAlertLabelPosX(), 
                                y: self.gameDeviceSettings.getHubListItemAlertLabelPosY()
                            }
                        }],
                        action:{
                            visible:false
                        }
            });

        }
         options.listViewItems = listViewItems;
            self.sessionLayer.create(options);  
    },
    process: function () {
        // clear the background
        pc.device.ctx.clearRect(0, 0, pc.device.canvasWidth, pc.device.canvasHeight);
        // always call the super
        this._super();
    }
});