﻿/**
 *   Project Neptune - (C)2015 Eddie Games Inc.
 *   Author: Marc D. Arbesman
 */

/**
 * Class: DeviceDetecion
 * Extends: pc.Base
 * Description:  Needed more control over device detection to support multiple devices
 *
 */

pn.DeviceDetection = pc.Base.extend('pn.DeviceDetection',
    {
        pixelRatio: 0,
        isiPhone: false,
        isiPhone4: false,
        isiPad: false,
        isiPod: false,
        isAndroid: false,
        isTouch: false,
        isFirefox: false,
        isChrome: false,
        isOpera: false,
        isIE: false,
        ieVersion: 0,

        init: function () {
            this.pixelRatio = window.devicePixelRatio || 1;
            this.isiPhone = navigator.userAgent.toLowerCase().indexOf('iphone') != -1;
            this.isiPod = navigator.userAgent.toLowerCase().indexOf('ipod') != -1;
            this.isiPhone4 = (this.pixelRatio == 2 && this.isiPhone);
            this.isiPad = (navigator.userAgent.toLowerCase().indexOf('ipad') != -1);
            this.isiPad3 = (this.pixelRatio == 2 && this.isiPad);
            this.isAndroid = navigator.userAgent.toLowerCase().indexOf('android') != -1;
            this.isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') != -1;
            this.isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') != -1;
            this.isOpera = navigator.userAgent.toLowerCase().indexOf('opera') != -1;
            this.isTouch = window.ontouchstart !== 'undefined';
            this.isiOS = (this.isiPhone || this.iPad || this.isiPod);
            this.isModernUI = (!this.isActiveXEnabled() && this.isWin64());

            if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
                this.ieVersion = new Number(RegExp.$1);
                this.isIE = true;
            }
        },
        isActiveXEnabled: function () {
            var supported = null;
            try {
                supported = !!new ActiveXObject("htmlfile");
            } catch (e) {
                supported = false;
            }

            return supported;
        },
        isWin64: function () {
                return navigator.platform == "Win64";
        },
        isFullScreen: function () {
            
            return (window.innerWidth == screen.width && window.innerHeight == screen.height);
        }
    });