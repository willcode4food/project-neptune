﻿/**
 * Project Neptune  - (C)2013 Eddie Games.
 * Marc Arbesman - Developer / Producer
 */

/**
 * Class: ShipZone 
 * Extends: pc.Base
 * Description: Facilitates the interaction of each card entity as a whole.  Handles initial dealing of the cards and management
 */
pn.ShipZone = pn.Zone.extend('pn.Zone',
{
    
},
{
    /**
     * Methods
     */
    init: function(player, zone){
        this._super(player, zone);
    },
    load: function (board, layer, startX, startY, orientation) {
        this._super(board, layer, startX, startY, orientation);
        // remove the sprite and input to make the cards dormant
        for (i = 0; i < this.cards.length; i++) {
            if (this.cards[i].getComponent('carddata').cardType === 'Compartment') {
                this.cards[i].addTag('nodragdrop');
            }
            if (!this.cards[i].hasComponentOfType('textLayout')) {
                var layout = new CardTextLayoutFactory(this.cards[i].getComponent('carddata').cardType);
                if ((pc.device.isiPhone || pc.device.isiPhone4 || pc.device.isiPad || pc.device.isAndroid) && this.cards[i].getComponent('carddata').imageMin !== '') {
                    this.cards[i].addComponent(pc.components.TextLayout.create({
                        layout: layout.getMinLayout(0, 0, this.cards[i].getComponent('carddata').armor, this.cards[i].getComponent('carddata').structure)
                    }));
                }
                else {
                    this.cards[i].addComponent(pc.components.TextLayout.create({
                        layout: layout.getSmallLayout(0, 0, this.cards[i].getComponent('carddata').armor, this.cards[i].getComponent('carddata').structure)
                    }));
                }
            }
        }
        this.loadCardSprites();
    },
    
    dealCards: function () {
        /// <summary>takes starting position and uses the mover component to move 
        ///         the cards into the appropriate spaces.  These spaces are defined by the configuration data
        ///         Send along with the JSON </summary>
        for (var i = 0; i < this.cards.length; i++) {
            var boardTile = this.boardTiles[(this.cards[i].getComponent('carddata').boardTilePosition - 1)];

            this.cards[i].addComponent(pc.components.Mover.create(
            {
                targetPos: {
                    x: boardTile.screenPosX - this.settings.getShipOriginXPos(),
                    y: boardTile.screenPosY - this.settings.getShipOriginYPos()
                },
                easing: pc.Easing.LINEAR,
                duration: 500
            }));
            this.cards[i].getComponent('carddata').zonePosX = boardTile.screenPosX - this.settings.getShipOriginXPos()
            this.cards[i].getComponent('carddata').zonePosY = boardTile.screenPosY - this.settings.getShipOriginYPos();
        }
    }
   });