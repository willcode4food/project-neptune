﻿pn.TNTileMap = pc.TileMap.extend('TNTileMap',
    {},
    {
        init: function (tileSets, tilesWide, tilesHigh, tileWidth, tileHeight, tiles) {
            this._super(tileSets, tilesWide, tilesHigh, tileWidth, tileHeight, tiles);
        },
        /**
     * Say you want to paint a rectangular region on a tileMap with a full image.  This function uses the Playcraft
     * tileMap object to fill a region with tiles from a its sprite sheet in sequential order.  It assumes
     * the "image" is the entire width of the sprite sheet and the total tiles passed in, along with the 
     * specified starting position on the sprite sheet, make up the "image" to be painted.
     * 
     * specify a starting X and Y position on the tileMap, the tile on the sprite sheet you want to start with,
     * the number of tiles to paint (assumes the tiles that make up the image are sequential), and the columns (or width)
     * of the rectangle.  Height of the rectangular region is specified by the total number of tiles to paint (assumes
     * the sprite sheet "image" is a rectangle as well)
     *
     */
        paintTileSkinRect: function (startX, startY, tileCursor, totalTiles, numCols) {
            //save the starting position since this will be our "zero" x starting point
            var xPos = startX;

            //loop on the total tiles to paint and sequentially set the tiles according to the number
            //of columns specified
            for (var numTilesToPaint = 0; numTilesToPaint < totalTiles; numTilesToPaint++) {
                // paint the tiles one at a time.
                this.setTile(startX, startY, tileCursor);

                // checking if we've reached the total number of tiles in the row to paint
                if (startX >= (numCols + xPos) - 1) {
                    // starting a new row (x position is zero based)
                    startX = xPos - 1;
                    startY++;
                }
                // move to the next column
                startX++;
                // move on to the next tile in the sprite sheet "image"
                tileCursor++;
            }

        }

    });