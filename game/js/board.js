﻿/**
 *   Project Neptune - (C)2015 Eddie Games Inc.
 *   Author: Marc D. Arbesman
 */

/**
 *  Class: Board
 *  Extends: pc.Base
 *  
 *  Description: This class handles the drawing of the board.  It also manages game piece location.
 *  The animations that occur on the board will also be handled here.
 *  
 */

pn.Board = pc.Base.extend('Board',
    {},
    {
        /**
         * Private members
         */

        boardShipTileLayer: null,
        boardPlayfieldTileLayer: null,        
        boardShipMap: null,        
        boardPlayfieldMap: null,                
        boardWidth: null,
        boardHeight: null,
        boardShipHeight: null,        
        boardConstants: null,
        settings: null,
        tilePositionMap:null,
        scene: null,
        boardPosX: null,
        boardPosY: null,
        centerPosX: null,
        centerPosY: null,
        /**
         * Methods
         */

        init: function (scene) {
            this.config(scene);
            this._super();
        },
        config: function (scene) {
            /// <summary>Using the tilesheet and tile mapping features of Playcraft, we render and position the board</summary>
            /// <param name="scene" type="Object">The pc.Scene object</param>
            this.boardConstants = pc.device.game.__constants;
            this.settings = pc.device.game.gameDeviceSettings;
            this.scene = scene;            
            this.boardWidth = this.boardConstants.get('BOARD_WIDTH_TILES');
            this.boardHeight = this.boardConstants.get('BOARD_HEIGHT_TILES');
            this.boardShipHeight = this.boardConstants.get('BOARD_SHIP_HEIGHT');
            this.tileOver = { tileType: 0, x: 0, y: 0 };

            this.generateBoard();            
        },
        generateBoard: function () {
            /// <summary>creates the game board from tile maps.  Uses one tile map for the ships on either side.  Uses another tile map to present the game's play field</summary>

            /*------------------- Ship ----------------------------*/
            
            var boardShipSheet = new pc.SpriteSheet({
                image: pc.device.loader.get('ship_cell').resource,
                frameWidth: this.settings.getBoardShipTileWidth(),
                frameHeight: this.settings.getBoardShipTileHeight()
            }),
            bgData = this.settings.getBackGroundPaintCoords();
            
            this.boardShipMap = new BoardTileMap([new pc.TileSet(boardShipSheet)],
                                                        this.getShipSpriteSheetColumns(),
                                                        this.getShipSpriteSheetRows(),
                                                        this.settings.getBoardShipTileWidth(),
                                                        this.settings.getBoardShipTileHeight()
            );         
            this.boardShipMap.generate(-1);
            this.boardShipMap.paint({
                x: this.settings.getShipStartXPos(),
                y: this.settings.getShipStartYPos(),
                w: this.boardWidth,
                h: this.boardShipHeight, 
                tileType: 0
                });
            this.boardShipMap.paint({
                x: this.settings.getShip2StartXPos(),
                y: this.settings.getShip2StartYPos(),
                w: this.boardWidth,
                h:this.boardShipHeight, 
                tileType: 0
            });

            for (var i = 0; i < bgData.length; i++) {
                this.boardShipMap.paintTileSkinRect(bgData[i].startX, bgData[i].startY, bgData[i].tileCursor, bgData[i].totalTiles, bgData[i].numCols);
            }            
            
           this.tilePositionMap = new Array(this.boardShipMap.numTilesPainted);    
           this.boardShipTileLayer = new pc.TileLayerScroller(
               'ship_board',
               true,
               this.boardShipMap,
               [],
               (this.settings.getBaseDeviceWidth()),
               (this.settings.getBaseDeviceHeight())
               );
           this.boardShipTileLayer.zIndex = 2;
           this.boardShipTileLayer.setOrigin(
                   this.settings.getShipOriginXPos(),
                   this.settings.getShipOriginYPos()
           );
           this.boardShipTileLayer.debugTileMap = false;
           //calculate the pixel position of the upper left hand corner of the board
           pc.device.game.centerPosY = ((this.settings.getBaseDeviceHeight() - this.settings.getPlayfieldOriginX()) / 2);
           pc.device.game.centerPosX = ((this.settings.getBaseDeviceWidth() - this.settings.getPlayfieldOriginY()) / 2);
           this.scene.addLayer(this.boardShipTileLayer);

           // ///*------------------- Play Field ----------------------------*/
            var boardPlayfieldSheet = new pc.SpriteSheet({
                image: pc.device.loader.get('playfield_cell').resource,
                frameWidth: this.settings.getPlayfieldTileWidth(),
                frameHeight: this.settings.getPlayfieldTileHeight()
            });
         
            
            this.boardPlayfieldMap = new pc.BoardTileMap([new pc.TileSet(boardPlayfieldSheet)],
                                                  this.getPlayfieldSpriteSheetColumns() + 2,
                                                  this.getPlayfieldSpriteSheetRows()+ 2,
                                                  this.settings.getPlayfieldTileWidth(),
                                                  this.settings.getPlayfieldTileHeight()
            );
            this.boardPlayfieldMap.generate(-1);
            this.boardPlayfieldMap.paint({
                x: this.settings.getPlayfieldStartXPos(),
                y: this.settings.getPlayfieldStartYPos(),
                w: this.boardWidth,
                h: this.boardConstants.get('BOARD_PLAYFIELD_HEIGHT'), 
                tileType: 0
                });
            
           // // trying zooming tile layer
            this.boardPlayfieldTileLayer = new pc.TileLayerScroller('playfield_board',
                false,
                this.boardPlayfieldMap,
                [],
                (this.settings.getBaseDeviceWidth()),
                (this.settings.getBaseDeviceHeight())
                );
                               
            this.boardPlayfieldTileLayer.zIndex = 2;
            this.boardPlayfieldTileLayer.debugTileMap = false;
            this.boardPlayfieldTileLayer.setOrigin(this.settings.getPlayfieldOriginX(),
            this.settings.getPlayfieldOriginY());
            this.scene.gameLayer.addSystem(new pc.systems.BoardPhysics(
                {
                    tileCollisionMap: {
                        tileMap: this.boardPlayfieldMap,
                        collisionCategory: CollisionType.BOARD,
                        collisionMask: CollisionType.TILEOVER,
                        collisionGroup: 1                        
                        
                    }
                }));
        this.scene.addLayer(this.boardPlayfieldTileLayer);
            
        },
        getShipSpriteSheetColumns: function () {
            /// <summary>Get all columns in the ship sprite sheet, not just the ones we display</summary>
            return Math.floor(this.settings.getBaseDeviceWidth() / this.settings.getBoardShipTileWidth()) + this.settings.getShipTileMapColsAddOn();
        },
        getShipSpriteSheetRows: function () {
            /// <summary>Get all rows in the ship sprite sheet, not just the ones we display</summary>
            return Math.floor(this.settings.getBaseDeviceHeight() / this.settings.getBoardShipTileHeight()) + this.settings.getShipTileMapRowsAddOn();
        },
        getPlayfieldSpriteSheetColumns: function () {
            /// <summary>Get all columns in the playfield sprite sheet, not just the ones we display</summary>
            return Math.floor(this.settings.getBaseDeviceWidth() / this.settings.getPlayfieldTileHeight() + 2);
            //return Math.floor(this.settings.getBaseDeviceWidth() / this.settings.getPlayfieldTileHeight());
        },
        getPlayfieldSpriteSheetRows: function () {
            /// <summary>Get all rows in the playfield sprite sheet, not just the ones we display</summary>
            return Math.floor(this.settings.getBaseDeviceWidth() / this.settings.getPlayfieldTileHeight());
            //return Math.floor(this.settings.getBaseDeviceWidth() / this.settings.getPlayfieldTileHeight());
        }

        
    });
