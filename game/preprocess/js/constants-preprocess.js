﻿
/**
 *   Project Neptune - (C)2015 Eddie Games Inc.
 *   Author: Marc D. Arbesman
 */

/**
*  Class: Constants 
*  Extends: pc.Base
   Description: Provides an immuatable value for creating
*  constant values.
*  
*  Usage:
*  
*  define
*  constant.set("MAX_WIDTH", 480);
* 
*  check
*  constant.isDefined("MAX_WIDTH");  returns true
*
*  get
*  constant.get("MAX_WIDTH"); = 480S
*  any attempts to redefine will not work. 
*
*/


 pn.Constant = pc.Base.extend('pn.Constant',
    {},    
    {
        init: function () {
            
            this.set('BACKGROUND_IMG_URL_PC', '/dts-images/board/background_pc.png');
            this.set('BACKGROUND_IMG_URL_MOBILE', '/dts-images/background_mobile.png');
            this.set('BACKGROUND_COLOR','#122341');
            this.set('BOARD_NUMCOLS', 6);
            this.set('BOARD_NUMROWS', 9);
            this.set('BOARD_BACKGROUND_PAINT_PC', [
                 {
                     startX: 0,
                     startY: 0,
                     tileCursor: 5,
                     totalTiles: 65,
                     numCols: 5
                 },
                 {
                     startX: 11,
                     startY: 0,
                     tileCursor: 70,
                     totalTiles: 65,
                     numCols: 5
                 },
                 {
                     startX: 5,
                     startY: 0,
                     tileCursor: 135,
                     totalTiles: 5,
                     numCols: 5
                 },
                 {
                     startX: 10,
                     startY: 0,
                     tileCursor: 145,
                     totalTiles: 1,
                     numCols: 1
                 },
                 {   startX: 5,
                     startY: 12,
                     tileCursor: 140,
                     totalTiles: 5,
                     numCols: 5
                     
                 },
                 {
                     startX: 10,
                     startY: 12,
                     tileCursor: 146,
                     totalTiles: 1,
                     numCols: 1
                 }]);
             this.set('BOARD_BACKGROUND_PAINT_MOBILE', [
                 {
                     startX: 0,
                     startY: 0,
                     tileCursor: 2,
                     totalTiles: 15,
                     numCols: 1
                 },
                 {
                     startX: 1,
                     startY: 0,
                     tileCursor: 17,
                     totalTiles: 6
                 },
                 {
                     startX: 1,
                     startY: 13,
                     tileCursor: 23,
                     totalTiles: 6
                 },
                 {
                     startX: 1,
                     startY: 14,
                     tileCursor: 23,
                     totalTiles: 6
                 },
                 {
                     startX: 7,
                     startY: 0,
                     tileCursor: 2,
                     totalTiles: 15,
                     numCols: 1
                 }
             ]);
             this.set('BOARD_SHIP_HEIGHT', 2);             
             this.set('BOARD_WIDTH_TILES', 6);
             this.set('BOARD_HEIGHT_TILES', 9);
             this.set('BOARD_SHIP_TILE_HEIGHT_MOBILE', 70);
             this.set('BOARD_SHIP_TILE_WIDTH_MOBILE', 112);
             this.set('BOARD_SHIP_TILE_HEIGHT_PC', 60);
             this.set('BOARD_SHIP_TILE_WIDTH_PC', 84);
             this.set('BOARD_SHIP_TILE_IMAGE_PC', '/dts-images/board/pc/ship_cell.png');
             this.set('BOARD_SHIP_TILE_IMAGE_MOBILE', '/dts-images/board/mobile/ship_cell.png');             
             this.set('BOARD_SHIP_TILE_COLS_ADD_PC', 3);
             this.set('BOARD_SHIP_TILE_ROWS_ADD_PC', 3);
             this.set('BOARD_SHIP_TILE_COLS_ADD_MOBILE', 6);
             this.set('BOARD_SHIP_TILE_ROWS_ADD_MOBILE', 6);
             this.set('BOARD_SHIP_MAP_ORIGIN_X_PC', 56);
             this.set('BOARD_SHIP_MAP_ORIGIN_Y_PC', 43);
             this.set('BOARD_SHIP_MAP_ORIGIN_X_MOBILE', 56);
             this.set('BOARD_SHIP_MAP_ORIGIN_Y_MOBILE', -5);
             this.set('BOARD_SHIP_START_X_POS_PC', 5);
             this.set('BOARD_SHIP_START_Y_POS_PC', 1);
             this.set('BOARD_SHIP_START_X_POS_MOBILE', 1);
             this.set('BOARD_SHIP_START_Y_POS_MOBILE', 1);             
             this.set('BOARD_SHIP2_START_X_POS_PC', 5);
             this.set('BOARD_SHIP2_START_Y_POS_PC', 10);
             this.set('BOARD_SHIP2_START_X_POS_MOBILE', 1);
             this.set('BOARD_SHIP2_START_Y_POS_MOBILE', 11);
             this.set('BOARD_PLAYFIELD_NUM_TILES', 30);
             this.set('BOARD_PLAYFIELD_HEIGHT', 5);
             this.set('BOARD_PLAYFIELD_WIDTH', 6);
             this.set('BOARD_PLAYFIELD_MAP_ORIGIN_X_PC', 56);
             this.set('BOARD_PLAYFIELD_MAP_ORIGIN_Y_PC', 31);
             this.set('BOARD_PLAYFIELD_MAP_ORIGIN_X_MOBILE', 56);
             this.set('BOARD_PLAYFIELD_MAP_ORIGIN_Y_MOBILE', 9);
             this.set('BOARD_PLAYFIELD_TILE_HEIGHT_MOBILE', 112);
             this.set('BOARD_PLAYFIELD_TILE_WIDTH_MOBILE', 112);
             this.set('BOARD_PLAYFIELD_TILE_HEIGHT_PC', 84);
             this.set('BOARD_PLAYFIELD_TILE_WIDTH_PC', 84);
             this.set('BOARD_PLAYFIELD_START_X_POS_PC', 5);
             this.set('BOARD_PLAYFIELD_START_Y_POS_PC', 2);
             this.set('BOARD_PLAYFIELD_START_X_POS_MOBILE', 1);
             this.set('BOARD_PLAYFIELD_START_Y_POS_MOBILE', 2);
             this.set('BOARD_PLAYFIELD_TILE_IMAGE_PC', '/dts-images/board/pc/playfield_cell.png');
             this.set('BOARD_PLAYFIELD_TILE_IMAGE_MOBILE', '/dts-images/board/mobile/playfield_cell.png');
             this.set('BOARD_TILE_BORDER', 1);
             this.set('BOARD_TILE_COLOR_SPACE', '#GGGGGG');
             this.set('BOARD_TILE_COLOR_SHIP', '#CCCCCC'); 
             this.set('BUTTON_DEFAULT_WIDTH_PC', 450);
             this.set('BUTTON_DEFAULT_HEIGHT_PC', 80);
             this.set('BUTTON_DEFAULT_WIDTH_MOBILE', 300);   
             this.set('BUTTON_DEFAULT_HEIGHT_MOBILE', 60);                                               
             this.set('COLOR_UI_PORTRAIT_BACKGROUND', '#fffaa7');
             this.set('COLOR_UI_PANEL_BACKGROUND', '#a2defa');             
             this.set('CARD_SCALE_GROW_X_MOBILE',2);
             this.set('CARD_SCALE_GROW_Y_MOBILE', 2);
             this.set('CARD_SCALE_GROW_X_PC',2);
             this.set('CARD_SCALE_GROW_Y_PC',2);
             this.set('CARD_SCALE_MAX_X_PC', 1.5);
             this.set('CARD_SCALE_MAX_Y_PC', 1.5);
             this.set('CARD_SCALE_MAX_X_MOBILE', 1);
             this.set('CARD_SCALE_MAX_Y_MOBILE', 1);
             this.set('CARD_SCALE_SPATIAL_X', 1);
             this.set('CARD_SCALE_SPATIAL_Y', 1);
             this.set('CARD_TYPE_COMPARTMENT', 'Compartment');
             this.set('CARD_DESCALE_GROW_X', 1);
             this.set('CARD_DESCALE_GROW_Y', 1);
             this.set('CARD_DESCALE_MAX_X', 1);
             this.set('CARD_DESCALE_MAX_Y', 1);
             this.set('CARD_DESCALE_SPATIAL_X', 4);
             this.set('CARD_DESCALE_SPATIAL_Y', 4);
             this.set('CARD_WIDTH_PC', 60);
             this.set('CARD_HEIGHT_PC', 84);
             this.set('CARD_WIDTH_MOBILE', 70);
             this.set('CARD_HEIGHT_MOBILE', 112);
             this.set('CARD_WIDTH_ZOOMED_PC', 349);
             this.set('CARD_HEIGHT_ZOOMED_PC', 489);
             this.set('CARD_WIDTH_ZOOMED_MOBILE',349);
             this.set('CARD_HEIGHT_ZOOMED_MOBILE', 489);
             this.set('CARD_DOUBLE_CLICK_THRESHOLD', 200);             
             this.set('CARD_ZOOMED_POS_X_PC', 465);
             this.set('CARD_ZOOMED_POS_Y_PC', 120);
             this.set('CARD_ZOOMED_POS_X_MOBILE', 450);             
             this.set('CARD_ZOOMED_POS_Y_MOBILE', 180);
             this.set('CARD_ZOOMED_PADDING_X_PC', 15);
             this.set('CARD_ZOOMED_PADDING_X_MOBILE', 0);
             this.set('DD_ACTION_TOUCH', 'dragdrop touch');
             this.set('DD_ACTION_TOUCH_MOVE', 'dragdrop touch move');
             this.set('DD_ACTION_TOUCH_END', 'dragdrop touch end');
             this.set('DD_ACTION_MOUSE_LEFT_BUTTON_DOWN', 'dragdrop mouse click');
             this.set('DD_ACTION_MOUSE_LEFT_BUTTON_DBLCLICK', 'dragdrop mouse double click');
             this.set('DD_ACTION_MOUSE_LEFT_BUTTON_UP', 'dragdrop mouse left button');
             this.set('DD_ACTION_MOUSE_MOVE', 'dragdrop mouse move');
             this.set('DD_ACTION_MOUSE_OUT', 'dragdrop mouse out');
             this.set('DD_ACTION_MOUSE_OVER', 'dragdrop mouse over');
             this.set('DD_ACTION_MOUSE_WHEEL_UP', 'dragdrop mouse wheel up');
             this.set('DD_ACTION_MOUSE_WHEEL_DOWN', 'dragdrop mouse wheel down');
             this.set('DD_STATE_MOUSE_MOVE', 'dragdrop mouse moving');
             this.set('DD_STATE_MOUSE_CLICK', 'dragdrop mouse holding');
             this.set('DD_STATE_TOUCH_MOVE', 'dragdrop touch moving');
             this.set('DD_STATE_MOUSE_OUT', 'dragdrop mouse out');
             this.set('DD_STATE_MOUSE_OVER', 'dragdrop mouse over');
             this.set('DD_STATE_TOUCH', 'card touch');
             this.set('DECK_COMPARTMENT_IMAGE_URL_PC', '/dts-images/cards/pc/compartments/');
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_LINEWIDTH_PC', 0);
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_COLOR_PC', '#00f6ff');
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_STROKECOLOR_PC', '#000000');
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_FONT_HEIGHT_PC', '20');
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_FONT_PC', 'Government Agent BB');
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_OFFSET_X_PC', 5);
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_OFFSET_Y_PC', -5);
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_LARGE_FONT_PC', 'Digitek');
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_LARGE_FONT_HEIGHT_PC', 40);
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_LARGE_OFFSET_X_PC', 5);
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_LARGE_OFFSET_Y_PC', 85);
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_LINEWIDTH_MOBILE', 0);
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_COLOR_MOBILE', '#00f6ff');
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_STROKECOLOR_MOBILE', '#000000');
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_FONT_HEIGHT_MOBILE', '25');
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_FONT_MOBILE', 'Government Agent BB');
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_OFFSET_X_MOBILE', 5);
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_OFFSET_Y_MOBILE', -10);
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_LARGE_FONT_MOBILE', 'Digitek');
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_LARGE_FONT_HEIGHT_MOBILE', 40);
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_LARGE_OFFSET_X_MOBILE', 5);
             this.set('DECK_COMPARTMENT_SHIELD_TYPE_LARGE_OFFSET_Y_MOBILE', 155);
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_LINEWIDTH_PC', 0);
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_COLOR_PC', '#ff0013');
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_STROKECOLOR_PC', '#000000');
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_FONT_HEIGHT_PC', '20');
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_FONT_PC', 'Government Agent BB');
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_OFFSET_X_PC', 68);
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_OFFSET_Y_PC', -5);
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_LARGE_FONT_PC', 'Digitek');
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_LARGE_FONT_HEIGHT_PC', 40);
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_LARGE_OFFSET_X_PC', 440);
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_LARGE_OFFSET_Y_PC', 85);
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_LINEWIDTH_MOBILE', 0);
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_COLOR_MOBILE', '#ff0013');
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_STROKECOLOR_MOBILE', '#000000');
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_FONT_HEIGHT_MOBILE', '25');
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_FONT_MOBILE', 'Government Agent BB');
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_OFFSET_X_MOBILE', 90);
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_OFFSET_Y_MOBILE', -10);
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_LARGE_FONT_MOBILE', 'Digitek');
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_LARGE_FONT_HEIGHT_MOBILE', 40);
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_LARGE_OFFSET_X_MOBILE', 440);
             this.set('DECK_COMPARTMENT_DAMAGE_TYPE_LARGE_OFFSET_Y_MOBILE', 155);
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_LINEWIDTH_PC', 0);
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_COLOR_PC', '#ffffff');
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_STROKECOLOR_PC', '#000000');
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_FONT_HEIGHT_PC', '20');
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_FONT_PC', 'Government Agent BB');
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_OFFSET_X_PC', 5);
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_OFFSET_Y_PC', -40);
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_LARGE_FONT_PC', 'Digitek');
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_LARGE_FONT_HEIGHT_PC', 40);
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_LARGE_OFFSET_X_PC', 5);
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_LARGE_OFFSET_Y_PC', -200);
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_LINEWIDTH_MOBILE', 0);
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_COLOR_MOBILE', '#ffffff');
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_STROKECOLOR_MOBILE', '#000000');
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_FONT_HEIGHT_MOBILE', '25');
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_FONT_MOBILE', 'Government Agent BB');
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_OFFSET_X_MOBILE', 5);
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_OFFSET_Y_MOBILE', -47);
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_LARGE_FONT_MOBILE', 'Digitek');
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_LARGE_FONT_HEIGHT_MOBILE', 40);
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_LARGE_OFFSET_X_MOBILE', 15);
             this.set('DECK_COMPARTMENT_ARMOR_TYPE_LARGE_OFFSET_Y_MOBILE', -125);
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_LINEWIDTH_PC', 0);
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_COLOR_PC', '#eff8a7');
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_STROKECOLOR_PC', '#000000');
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_FONT_HEIGHT_PC', '20');
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_FONT_PC', 'Government Agent BB');
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_OFFSET_X_PC', 68);
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_OFFSET_Y_PC', -40);
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_LARGE_FONT_PC', 'Digitek');
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_LARGE_FONT_HEIGHT_PC', 40);
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_LARGE_OFFSET_X_PC', 150);
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_LARGE_OFFSET_Y_PC', -200);
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_LINEWIDTH_MOBILE', 0);
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_COLOR_MOBILE', '#eff8a7');
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_STROKECOLOR_MOBILE', '#000000');
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_FONT_HEIGHT_MOBILE', '25');
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_FONT_MOBILE', 'Government Agent BB');
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_OFFSET_X_MOBILE', 90);
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_OFFSET_Y_MOBILE', -47);
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_LARGE_FONT_MOBILE', 'Digitek');
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_LARGE_FONT_HEIGHT_MOBILE', 40);
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_LARGE_OFFSET_X_MOBILE', 160);
             this.set('DECK_COMPARTMENT_STRUCTURE_TYPE_LARGE_OFFSET_Y_MOBILE', -125);
             this.set('DECK_COMPARTMENT_TITLE_LARGE_SECTION_WIDTH_PC', 230);
             this.set('DECK_COMPARTMENT_TITLE_LARGE_SECTION_WIDTH_MOBILE', 237);
             this.set('DECK_COMPARTMENT_TITLE_LARGE_FONT_SIZE_PC', 40);
             this.set('DECK_COMPARTMENT_TITLE_LARGE_FONT_SIZE_MOBILE', 40);
             this.set('DECK_COMPARTMENT_TITLE_LARGE_FONT_FAMILY_PC', 'Government Agent BB Ital');
             this.set('DECK_COMPARTMENT_TITLE_LARGE_FONT_FAMILY_MOBILE', 'Government Agent BB Ital');
             this.set('DECK_COMPARTMENT_TITLE_LARGE_START_X_PC', 252);
             this.set('DECK_COMPARTMENT_TITLE_LARGE_START_Y_PC', -210);
             this.set('DECK_COMPARTMENT_TITLE_LARGE_START_X_MOBILE', 252);
             this.set('DECK_COMPARTMENT_TITLE_LARGE_START_Y_MOBILE', -140);
             this.set('DECK_COMPARTMENT_SUBTITLE_LARGE_FONT_FAMILY_PC', 'Government Agent BB');
             this.set('DECK_COMPARTMENT_SUBTITLE_LARGE_FONT_FAMILY_MOBILE', 'Government Agent BB');
             this.set('DECK_COMPARTMENT_SUBTITLE_LARGE_FONT_SIZE_PC', 25);
             this.set('DECK_COMPARTMENT_SUBTITLE_LARGE_FONT_SIZE_MOBILE', 25);
             this.set('DECK_CONFLICT_SHIELD_TYPE_SMALL_START_X_PC', 3);
             this.set('DECK_CONFLICT_SHIELD_TYPE_SMALL_START_Y_PC', -2);
             this.set('DECK_CONFLICT_DAMAGE_TYPE_SMALL_START_X_PC', 46);
             this.set('DECK_CONFLICT_DAMAGE_TYPE_SMALL_START_Y_PC', -2);
             this.set('DECK_CONFLICT_ARMOR_TYPE_SMALL_START_X_PC', 3);
             this.set('DECK_CONFLICT_ARMOR_TYPE_SMALL_START_Y_PC', -68);
             this.set('DECK_CONFLICT_STRUCTURE_TYPE_SMALL_START_X_PC', 46);
             this.set('DECK_CONFLICT_STRUCTURE_TYPE_SMALL_START_Y_PC', -68);
             this.set('DECK_CONFLICT_SHIELD_TYPE_SMALL_START_X_MOBILE', 3);
             this.set('DECK_CONFLICT_SHIELD_TYPE_SMALL_START_Y_MOBILE', -2);
             this.set('DECK_CONFLICT_DAMAGE_TYPE_SMALL_START_X_MOBILE', 46);
             this.set('DECK_CONFLICT_DAMAGE_TYPE_SMALL_START_Y_MOBILE', -2);
             this.set('DECK_CONFLICT_ARMOR_TYPE_SMALL_START_X_MOBILE', 3);
             this.set('DECK_CONFLICT_ARMOR_TYPE_SMALL_START_Y_MOBILE', -90);
             this.set('DECK_CONFLICT_STRUCTURE_TYPE_SMALL_START_X_MOBILE', 52);
             this.set('DECK_CONFLICT_STRUCTURE_TYPE_SMALL_START_Y_MOBILE', -90);
             this.set('DECK_CONFLICT_SHIELD_TYPE_MIN_START_X_MOBILE', 3);
             this.set('DECK_CONFLICT_SHIELD_TYPE_MIN_START_Y_MOBILE', -3);
             this.set('DECK_CONFLICT_DAMAGE_TYPE_MIN_START_X_MOBILE', 53);
             this.set('DECK_CONFLICT_DAMAGE_TYPE_MIN_START_Y_MOBILE', -3);
             this.set('DECK_CONFLICT_ARMOR_TYPE_MIN_START_X_MOBILE', 3);
             this.set('DECK_CONFLICT_ARMOR_TYPE_MIN_START_Y_MOBILE', -50);
             this.set('DECK_CONFLICT_STRUCTURE_TYPE_MIN_START_X_MOBILE', 53);
             this.set('DECK_CONFLICT_STRUCTURE_TYPE_MIN_START_Y_MOBILE', -50);
             this.set('DECK_PLAYER1_SHIPDECK', 'Player1Ship');
             this.set('DECK_PLAYER2_SHIPDECK', 'Player2Ship');
             this.set('DECK_ORIENT_LANDSCAPE', 1);
             this.set('DECK_ORIENT_PORTRAIT', 0);
             this.set('DECK_SMALL_IMAGE_FIELDNAME', 'SmallImg');
             this.set('DECK_LARGE_IMAGE_FIELDNAME', 'LargeImg');
             this.set('DECK_MIN_IMAGE_FIELDNAME', 'MinImg');
             this.set('GAME_STATE_DATA_URL', '/dts-images/pn_GAME_STATE.json');
             this.set('GAME_ZOOM_MIN_MOBILE', 1);
             this.set('GAME_ZOOM_MAX_MOBILE', 1.75);
             this.set('GAME_ZOOM_MIN_PC', 1);
             this.set('GAME_ZOOM_MAX_PC', 2);             
             this.set('GAME_DATA_DOMAIN', 'https://projectneptune.blob.core.windows.net');             
             this.set('GAME_IMAGES_CDN_DOMAIN', 'https://projectneptune.blob.core.windows.net');
             // @if NODE_ENV == 'local'
             this.set('GAME_DOMAIN', 'http://local.eddiegames.com');
             // @endif
             // @if NODE_ENV == 'production'
             this.set('GAME_DOMAIN', 'http://www.eddiegames.com');
             // @endif
             this.set('GAME_LOGIN_SCENE', 'TitleScene');
             this.set('HUB_HEADER_HEIGHT_PC', 110);
             this.set('HUB_HEADER_HEIGHT_MOBILE', 140);
             this.set('HUB_HEADER_FONT_HEIGHT_PC',50);
             this.set('HUB_HEADER_FONT_HEIGHT_MOBILE',20);
             this.set('HUB_HEADER_PLAYER_LABEL_POSX_PC',23);
             this.set('HUB_HEADER_PLAYER_LABEL_POSY_PC',23);
             this.set('HUB_HEADER_PLAYER_LABEL_POSX_MOBILE',19);
             this.set('HUB_HEADER_PLAYER_LABEL_POSY_MOBILE',5);
             this.set('HUB_LIST_ITEM_WIDTH_PC', 1800);
             this.set('HUB_LIST_ITEM_WIDTH_MOBILE', 710);
             this.set('HUB_LIST_ITEM_HEIGHT_PC', 180);
             this.set('HUB_LIST_ITEM_HEIGHT_MOBILE', 180);
             this.set('HUB_LIST_ITEM_POS_X_PC', 60);
             this.set('HUB_LIST_ITEM_POS_Y_PC', 140);
             this.set('HUB_LIST_ITEM_POS_X_MOBILE', 30);             
             this.set('HUB_LIST_ITEM_POS_Y_MOBILE', 180);
             this.set('HUB_LIST_ITEM_ALERT_WIDTH_PC', 1800);
             this.set('HUB_LIST_ITEM_ALERT_HEIGHT_PC', 180);
             this.set('HUB_LIST_ITEM_ALERT_WIDTH_MOBILE', 710);
             this.set('HUB_LIST_ITEM_ALERT_HEIGHT_MOBILE', 130);
             this.set('HUB_LIST_ITEM_ALERT_POS_X_PC', 60);
             this.set('HUB_LIST_ITEM_ALERT_POS_Y_PC', 150);
             this.set('HUB_LIST_ITEM_ALERT_POS_X_MOBILE', 30);             
             this.set('HUB_LIST_ITEM_ALERT_POS_Y_MOBILE', 180);
             this.set('HUB_LIST_ITEM_ALERT_FONT_HEIGHT_PC', 67);
             this.set('HUB_LIST_ITEM_ALERT_FONT_HEIGHT_MOBILE', 45);
             this.set('HUB_LIST_ITEM_ALERT_LABEL_POS_X_PC', 400);
             this.set('HUB_LIST_ITEM_ALERT_LABEL_POS_Y_PC', -70);             
             this.set('HUB_LIST_ITEM_ALERT_LABEL_POS_X_MOBILE', 30);
             this.set('HUB_LIST_ITEM_ALERT_LABEL_POS_Y_MOBILE', -50);
             this.set('HUB_LIST_ITEM_LABEL_1_POS_X_PC', 170);
             this.set('HUB_LIST_ITEM_LABEL_1_POS_Y_PC', -60);             
             this.set('HUB_LIST_ITEM_LABEL_1_POS_X_MOBILE', 30);
             this.set('HUB_LIST_ITEM_LABEL_1_POS_Y_MOBILE', -60);
             this.set('HUB_LIST_ITEM_LABEL_1_FONT_HEIGHT_MOBILE', 85);
             this.set('HUB_LIST_ITEM_LABEL_1_FONT_HEIGHT_PC', 110);
             this.set('HUB_LIST_ITEM_LABEL_2_POS_X_PC',370);
             this.set('HUB_LIST_ITEM_LABEL_2_POS_Y_PC', -75);
             this.set('HUB_LIST_ITEM_LABEL_2_POS_X_MOBILE', 175);
             this.set('HUB_LIST_ITEM_LABEL_2_POS_Y_MOBILE', -100);
             this.set('HUB_LIST_ITEM_LABEL_2_FONT_HEIGHT_MOBILE', 30);
             this.set('HUB_LIST_ITEM_LABEL_2_FONT_HEIGHT_PC', 70);
             this.set('HUB_LIST_ITEM_LABEL_3_POS_X_PC', 900);
             this.set('HUB_LIST_ITEM_LABEL_3_POS_Y_PC', -105);
             this.set('HUB_LIST_ITEM_LABEL_3_POS_X_MOBILE', 175);
             this.set('HUB_LIST_ITEM_LABEL_3_POS_Y_MOBILE', -70);
             this.set('HUB_LIST_ITEM_LABEL_3_FONT_HEIGHT_MOBILE', 16);
             this.set('HUB_LIST_ITEM_LABEL_3_FONT_HEIGHT_PC', 30);
             this.set('HUB_LIST_ITEM_LABEL_4_POS_X_PC', 1100);
             this.set('HUB_LIST_ITEM_LABEL_4_POS_Y_PC', -105);
             this.set('HUB_LIST_ITEM_LABEL_4_POS_X_MOBILE', 285);
             this.set('HUB_LIST_ITEM_LABEL_4_POS_Y_MOBILE', -70);
             this.set('HUB_LIST_ITEM_LABEL_4_FONT_HEIGHT_MOBILE', 16);
             this.set('HUB_LIST_ITEM_LABEL_4_FONT_HEIGHT_PC', 30);
             this.set('HUB_LIST_ITEM_LABEL_5_POS_X_PC', 900);
             this.set('HUB_LIST_ITEM_LABEL_5_POS_Y_PC', -60);
             this.set('HUB_LIST_ITEM_LABEL_5_POS_X_MOBILE', 175);
             this.set('HUB_LIST_ITEM_LABEL_5_POS_Y_MOBILE', -45);
             this.set('HUB_LIST_ITEM_LABEL_5_FONT_HEIGHT_MOBILE', 16);
             this.set('HUB_LIST_ITEM_LABEL_5_FONT_HEIGHT_PC', 30);
             this.set('HUB_LIST_ITEM_LABEL_6_POS_X_PC', 1100);
             this.set('HUB_LIST_ITEM_LABEL_6_POS_Y_PC', -60);
             this.set('HUB_LIST_ITEM_LABEL_6_POS_X_MOBILE', 285);
             this.set('HUB_LIST_ITEM_LABEL_6_POS_Y_MOBILE', -45);
             this.set('HUB_LIST_ITEM_LABEL_6_FONT_HEIGHT_MOBILE', 16);
             this.set('HUB_LIST_ITEM_LABEL_6_FONT_HEIGHT_PC', 30);
             this.set('HUB_LIST_ITEM_ACTION_POS_X_PC', 300);
             this.set('HUB_LIST_ITEM_ACTION_POS_Y_PC', 130);
             this.set('HUB_LIST_ITEM_ACTION_POS_X_MOBILE', 190);
             this.set('HUB_LIST_ITEM_ACTION_POS_Y_MOBILE', 117);
             this.set('HUB_LIST_ITEM_ACTION_WIDTH_PC', 260);
             this.set('HUB_LIST_ITEM_ACTION_HEIGHT_PC', 70);
             this.set('HUB_LIST_ITEM_ACTION_WIDTH_MOBILE', 175);
             this.set('HUB_LIST_ITEM_ACTION_HEIGHT_MOBILE', 60);
             this.set('HUB_CHALLENGE_POSX_PC', 740);
             this.set('HUB_CHALLENGE_POSY_PC', 17);
             this.set('HUB_CHALLENGE_POSX_MOBILE', 320);
             this.set('HUB_CHALLENGE_POSY_MOBILE', 17);
             this.set('HUB_CHALLENGE_WIDTH_PC', 260);
             this.set('HUB_CHALLENGE_HEIGHT_PC', 70);
             this.set('HUB_CHALLENGE_WIDTH_MOBILE', 200);
             this.set('HUB_CHALLENGE_HEIGHT_MOBILE', 90);
             this.set('HUB_FBINVITE_POSX_PC', 1040);
             this.set('HUB_FBINVITE_POSY_PC', 18);
             this.set('HUB_FBINVITE_POSX_MOBILE', 320);
             this.set('HUB_FBINVITE_POSY_MOBILE', 77);
             this.set('HUB_FBINVITE_WIDTH_PC', 260);
             this.set('HUB_FBINVITE_HEIGHT_PC', 70);
             this.set('HUB_FBINVITE_WIDTH_MOBILE', 350);
             this.set('HUB_FBINVITE_HEIGHT_MOBILE', 80);
             this.set('HUB_GOTOMENU_POSX_PC', 1640);
             this.set('HUB_GOTOMENU_POSY_PC', 19);
             this.set('HUB_GOTOMENU_LABEL_POSY_PC',-20);
             this.set('HUB_GOTOMENU_LABEL_POSY_MOBILE',0);
             this.set('HUB_GOTOMENU_POSX_MOBILE', 550);
             this.set('HUB_GOTOMENU_POSY_MOBILE', 17);
             this.set('HUB_GOTOMENU_WIDTH_PC', 260);
             this.set('HUB_GOTOMENU_HEIGHT_PC', 70);
             this.set('HUB_GOTOMENU_WIDTH_MOBILE', 200);
             this.set('HUB_GOTOMENU_HEIGHT_MOBILE', 40);
             this.set('HUB_GOTOMENU_FONT_HEIGHT_PC',45);
             this.set('HUB_GOTOMENU_FONT_HEIGHT_MOBILE',30);
             this.set('HUB_NEWGAME_POSX_PC', 1340);
             this.set('HUB_NEWGAME_POSY_PC', 19);
             this.set('HUB_NEWGAME_POSX_MOBILE', 550);
             this.set('HUB_NEWGAME_POSY_MOBILE', 77);
             this.set('HUB_NEWGAME_WIDTH_PC', 260);
             this.set('HUB_NEWGAME_HEIGHT_PC', 70);
             this.set('HUB_NEWGAME_WIDTH_MOBILE', 200);
             this.set('HUB_NEWGAME_HEIGHT_MOBILE', 40);
             this.set('IMAGE_CDN_URL', '/dts-images');
             this.set('IMAGE_CARD_ROOT_PC', '/cards/pc/');
             this.set('IMAGE_CARD_ROOT_MOBILE', '/cards/mobile/');
             this.set('LOGIN_FACEBOOK_BUTTON_POS_X_PC',582);
             this.set('LOGIN_FACEBOOK_BUTTON_POS_Y_PC',600);
             this.set('LOGIN_FACEBOOK_BUTTON_POS_X_MOBILE',190);
             this.set('LOGIN_FACEBOOK_BUTTON_POS_Y_MOBILE',855);
             this.set('LOGIN_FACEBOOK_BUTTON_WIDTH_PC',812);
             this.set('LOGIN_FACEBOOK_BUTTON_HEIGHT_PC',116);
             this.set('LOGIN_FACEBOOK_BUTTON_WIDTH_MOBILE',400);
             this.set('LOGIN_FACEBOOK_BUTTON_HEIGHT_MOBILE',77);
             this.set('LOGIN_FACEBOOK_BUTTON_LABEL_Y_PC',-30);
             this.set('LOGIN_FACEBOOK_BUTTON_LABEL_X_PC',150);
             this.set('LOGIN_FACEBOOK_BUTTON_LABEL_Y_MOBILE',-30);
             this.set('LOGIN_FACEBOOK_BUTTON_LABEL_X_MOBILE',60);
             this.set('LOGIN_FACEBOOK_BUTTON_FONT_HEIGHT_MOBILE',30);
             this.set('LOGIN_FACEBOOK_BUTTON_FONT_HEIGHT_PC',40);             
             this.set('LOGIN_GOOGLE_BUTTON_POS_X_PC',582);
             this.set('LOGIN_GOOGLE_BUTTON_POS_Y_PC',411);
             this.set('LOGIN_GOOGLE_BUTTON_POS_X_MOBILE',190);
             this.set('LOGIN_GOOGLE_BUTTON_POS_Y_MOBILE',760);
             this.set('LOGIN_GOOGLE_BUTTON_WIDTH_PC',812);
             this.set('LOGIN_GOOGLE_BUTTON_HEIGHT_PC',116);
             this.set('LOGIN_GOOGLE_BUTTON_WIDTH_MOBILE',400);
             this.set('LOGIN_GOOGLE_BUTTON_HEIGHT_MOBILE',77);
             this.set('LOGIN_GOOGLE_BUTTON_LABEL_Y_PC',-30);
             this.set('LOGIN_GOOGLE_BUTTON_LABEL_X_PC',150);
             this.set('LOGIN_GOOGLE_BUTTON_LABEL_Y_MOBILE',-30);
             this.set('LOGIN_GOOGLE_BUTTON_LABEL_X_MOBILE',60);
             this.set('LOGIN_GOOGLE_BUTTON_FONT_HEIGHT_MOBILE',30);
             this.set('LOGIN_GOOGLE_BUTTON_FONT_HEIGHT_PC',40);
             this.set('LOGIN_TITLE1_POS_X_PC',53);
             this.set('LOGIN_TITLE1_POS_Y_PC',300);
             this.set('LOGIN_TITLE1_FONT_HEIGHT_PC',60);
             this.set('LOGIN_TITLE1_FONT_HEIGHT_MOBILE',60);
             this.set('LOGIN_TITLE1_POS_X_MOBILE',175);
             this.set('LOGIN_TITLE1_POS_Y_MOBILE',300);
             this.set('LOGIN_ERROR_WIDTH_PC', 124);
             this.set('LOGIN_ERROR_HEIGHT_PC', 128);
             this.set('LOGIN_ERROR_WIDTH_MOBILE', 124);
             this.set('LOGIN_ERROR_HEIGHT_MOBILE', 128);
             this.set('LOGIN_ERROR_POS_X_PC',53);
             this.set('LOGIN_ERROR_POS_Y_PC',700);
             this.set('LOGIN_ERROR_POS_X_MOBILE',137);
             this.set('LOGIN_ERROR_POS_Y_MOBILE',430);      
             this.set('LOGIN_ERROR_FONT_HEIGHT_PC', 30);       
             this.set('LOGIN_ERROR_FONT_HEIGHT_MOBILE', 30);
             this.set('LOGIN_BUTTON_WIDTH_PC', 450);
             this.set('LOGIN_BUTTON_HEIGHT_PC', 60);
             this.set('LOGIN_BUTTON_WIDTH_MOBILE', 400);
             this.set('LOGIN_BUTTON_HEIGHT_MOBILE', 60);
             this.set('LOGIN_BUTTON_POS_X_PC',53);
             this.set('LOGIN_BUTTON_POS_Y_PC',600);
             this.set('LOGIN_BUTTON_POS_X_MOBILE',190);
             this.set('LOGIN_BUTTON_POS_Y_MOBILE',590);
             this.set('LOGIN_BUTTON_LABEL_Y_PC',-15);
             this.set('LOGIN_BUTTON_LABEL_X_PC',0);
             this.set('LOGIN_BUTTON_LABEL_Y_MOBILE',-17);
             this.set('LOGIN_BUTTON_LABEL_X_MOBILE',140);
             this.set('LOGIN_BUTTON_FONT_HEIGHT_MOBILE',40);
             this.set('LOGIN_BUTTON_FONT_HEIGHT_PC',45);
             this.set('LOGIN_REGISTER_BUTTON_POS_X_PC',53);
             this.set('LOGIN_REGISTER_BUTTON_POS_Y_PC',690);
             this.set('LOGIN_REGISTER_BUTTON_POS_X_MOBILE',190);
             this.set('LOGIN_REGISTER_BUTTON_POS_Y_MOBILE',670);
             this.set('LOGIN_REGISTER_BUTTON_WIDTH_PC', 450);
             this.set('LOGIN_REGISTER_BUTTON_HEIGHT_PC', 60);
             this.set('LOGIN_REGISTER_BUTTON_WIDTH_MOBILE', 400);
             this.set('LOGIN_REGISTER_BUTTON_HEIGHT_MOBILE', 60);
             this.set('LOGIN_REGISTER_BUTTON_LABEL_Y_PC',-20);
             this.set('LOGIN_REGISTER_BUTTON_LABEL_X_PC',70);
             this.set('LOGIN_REGISTER_BUTTON_LABEL_Y_MOBILE',-20);
             this.set('LOGIN_REGISTER_BUTTON_LABEL_X_MOBILE',18);
             this.set('LOGIN_REGISTER_BUTTON_FONT_HEIGHT_MOBILE',35);
             this.set('LOGIN_REGISTER_BUTTON_FONT_HEIGHT_PC',30);
             this.set('LOGIN_USERNAME_TEXTBOX_POS_X_PC', 53);
             this.set('LOGIN_USERNAME_TEXTBOX_POS_Y_PC', 411);
             this.set('LOGIN_USERNAME_TEXTBOX_POS_X_MOBILE',190);
             this.set('LOGIN_USERNAME_TEXTBOX_POS_Y_MOBILE',390);
             this.set('LOGIN_USERNAME_TEXTBOX_WIDTH_PC',450);
             this.set('LOGIN_USERNAME_TEXTBOX_HEIGHT_PC',60);
             this.set('LOGIN_USERNAME_TEXTBOX_WIDTH_MOBILE',400);
             this.set('LOGIN_USERNAME_TEXTBOX_HEIGHT_MOBILE',50);
             this.set('LOGIN_USERNAME_TEXTBOX_FONT_HEIGHT_PC',40);
             this.set('LOGIN_USERNAME_TEXTBOX_FONT_HEIGHT_MOBILE',30);
             this.set('LOGIN_PASSWORD_TEXTBOX_POS_X_PC',53);
             this.set('LOGIN_PASSWORD_TEXTBOX_POS_Y_PC',501);             
             this.set('LOGIN_PASSWORD_TEXTBOX_WIDTH_PC',450);
             this.set('LOGIN_PASSWORD_TEXTBOX_HEIGHT_PC',60);
             this.set('LOGIN_PASSWORD_TEXTBOX_POS_X_MOBILE', 190);
             this.set('LOGIN_PASSWORD_TEXTBOX_POS_Y_MOBILE',460);
             this.set('LOGIN_PASSWORD_TEXTBOX_WIDTH_MOBILE',400);
             this.set('LOGIN_PASSWORD_TEXTBOX_HEIGHT_MOBILE',50);
             this.set('LOGIN_PASSWORD_TEXTBOX_FONT_HEIGHT_PC',40);
             this.set('LOGIN_PASSWORD_TEXTBOX_FONT_HEIGHT_MOBILE',30);
             this.set('MENU_NEW_BUTTON_POS_X_PC',735);
             this.set('MENU_NEW_BUTTON_POS_Y_PC',350);
             this.set('MENU_NEW_BUTTON_POS_X_MOBILE',230);
             this.set('MENU_NEW_BUTTON_POS_Y_MOBILE',350);
             this.set('MENU_NEW_FONT_HEIGHT_PC',45);
             this.set('MENU_NEW_FONT_HEIGHT_MOBILE',30);
             this.set('MENU_DECK_BUTTON_POS_X_PC',735);
             this.set('MENU_DECK_BUTTON_POS_Y_PC',500);
             this.set('MENU_DECK_BUTTON_POS_X_MOBILE',230);
             this.set('MENU_DECK_BUTTON_POS_Y_MOBILE',450);
             this.set('MENU_DECK_FONT_HEIGHT_PC',45);
             this.set('MENU_DECK_FONT_HEIGHT_MOBILE',30);
             this.set('MENU_LOGOUT_BUTTON_POS_X_PC',735);
             this.set('MENU_LOGOUT_BUTTON_POS_Y_PC', 650);
             this.set('MENU_LOGOUT_BUTTON_POS_X_MOBILE',230);
             this.set('MENU_LOGOUT_BUTTON_POS_Y_MOBILE',550);
             this.set('MENU_LOGOUT_FONT_HEIGHT_PC',45);
             this.set('MENU_LOGOUT_FONT_HEIGHT_MOBILE',30);
             this.set('PLAYER_HAND_POS_X_PC', -184);
             this.set('PLAYER_HAND_POS_Y_PC', 150);
             this.set('PLAYER_HAND_POS_X_MOBILE', 200);
             this.set('PLAYER_HAND_POS_Y_MOBILE', 100);
             this.set('PLAYER_HAND_POS_WIDTH_PC', 200);
             this.set('PLAYER_HAND_POS_HEIGHT_PC', 400);
             this.set('PLAYER_HAND_POS_WIDTH_MOBILE', 200);
             this.set('PLAYER_HAND_POS_HEIGHT_MOBILE', 300);
             this.set('PLAYER_DATA_URL', '/scripts/json/players.json');
             this.set('RESOLUTION_HEIGHT_MOBILE', 1000);
             this.set('RESOLUTION_WIDTH_MOBILE', 768);
             this.set('RESOLUTION_HEIGHT_PC', 1080);
             this.set('RESOLUTION_WIDTH_PC', 1920);
             this.set('SYS_CLIENT_ID', 7);   
            // @if NODE_ENV == 'local'  
             this.set('SYS_API_DOMAIN', 'http://local-api.zebulon.io');   
             // @endif 
             // @if NODE_ENV == 'prod'      
             this.set('SYS_API_DOMAIN', 'http://api.zebulon.io');   
              // @endif   
             this.set('SYS_API_TOKEN', '/token');
             this.set('SYS_API_GAME_SESSIONS', '/api/game/1/gameSession?active=true');
             this.set('SYS_API_LOGOUT', '/api/refreshTokens');
             this.set('SYS_API_EXTERNAL_REG', '/api/Account/RegisterExternal');
             this.set('SYS_API_OBTAIN_LOCAL_TOKEN', '/api/Account/ObtainLocalAccessToken');
             // @if NODE_ENV == 'prod' 
             this.set('SYS_API_GOOGLE_LOGIN',this.get('SYS_API_DOMAIN') + '/api/Account/ExternalLogin?provider=Google&response_type=token&client_id=' + this.get('SYS_CLIENT_ID') + '&redirect_uri=http://' + window.location.host + '/games/pn/authComplete.html');
             // @endif
             // @if NODE_ENV == 'local'
             this.set('SYS_API_GOOGLE_LOGIN',this.get('SYS_API_DOMAIN') + '/api/Account/ExternalLogin?provider=Google&response_type=token&client_id=' + this.get('SYS_CLIENT_ID') + '&redirect_uri=http://' + window.location.host + '/authComplete.html');
             // @endif 
             // @if NODE_ENV == 'prod'
             this.set('SYS_API_FACEBOOK_LOGIN',this.get('SYS_API_DOMAIN') +'/api/Account/ExternalLogin?provider=Facebook&response_type=token&client_id=' + this.get('SYS_CLIENT_ID') + '&redirect_uri=http://' + window.location.host + '/games/pn/authComplete.html');
             // @endif
             // @if NODE_ENV == 'local'
             this.set('SYS_API_FACEBOOK_LOGIN',this.get('SYS_API_DOMAIN') +'/api/Account/ExternalLogin?provider=Facebook&response_type=token&client_id=' + this.get('SYS_CLIENT_ID') + '&redirect_uri=http://' + window.location.host + '/authComplete.html');
             // @endif 
             this.set('SYS_STORAGE_PLAYER_AUTH', 'playerAuthentication');
             this.set('SYS_STORAGE_PLAYER_PROFILE', 'playerProfile');             
             this.set('UI_ACTION_TOUCH', 'ui touch');
             this.set('UI_ACTION_TOUCH_MOVE', 'ui touch move');
             this.set('UI_ACTION_TOUCH_END', 'ui touch end');
             this.set('UI_ACTION_MOUSE_LEFT_BUTTON_DOWN', 'ui mouse click');
             this.set('UI_ACTION_MOUSE_LEFT_BUTTON_DBLCLICK', 'ui mouse double click');
             this.set('UI_ACTION_MOUSE_LEFT_BUTTON_UP', 'ui mouse left button');
             this.set('UI_ACTION_MOUSE_MOVE', 'ui mouse move');
             this.set('UI_ACTION_MOUSE_OUT', 'ui mouse out');
             this.set('UI_ACTION_MOUSE_OVER', 'ui mouse over');
             this.set('UI_ATTRIBUTE_SPACE_X_PC', 50);
             this.set('UI_ATTRIBUTE_SPACE_Y_PC', 0);
             this.set('UI_ATTRIBUTE_SPACE_X_MOBILE', 0);
             this.set('UI_ATTRIBUTE_SPACE_Y_MOBILE', -50);
             this.set('UI_OPPONENT_HAND_START_X_MOBILE', 480);
             this.set('UI_OPPONENT_HAND_START_Y_MOBILE', 3);
             this.set('UI_OPPONENT_HAND_START_X_PC', 1190);
             this.set('UI_OPPONENT_HAND_START_Y_PC', 45);
             this.set('UI_OPPONENT_HAND_MOVE_X_MOBILE', -80);
             this.set('UI_OPPONENT_HAND_MOVE_Y_MOBILE', 3);
             this.set('UI_OPPONENT_HAND_MOVE_X_PC', 1190);
             this.set('UI_OPPONENT_HAND_MOVE_Y_PC', 90);
             this.set('UI_OPPONENT_HAND_PADDING_PC', 80);
             this.set('UI_OPPONENT_HAND_PADDING_MOBILE', 0);
             this.set('UI_OPPONENT_ATTRIBUTE_START_X_PC', 365);
             this.set('UI_OPPONENT_ATTRIBUTE_START_Y_PC', 683);
             this.set('UI_OPPONENT_ATTRIBUTE_START_X_MOBILE', 260);
             this.set('UI_OPPONENT_ATTRIBUTE_START_Y_MOBILE', 720);
             this.set('UI_PLAYER_ATTRIBUTE_START_X_PC', 365);
             this.set('UI_PLAYER_ATTRIBUTE_START_Y_PC', 683);
             this.set('UI_PLAYER_ATTRIBUTE_START_X_MOBILE', 13);
             this.set('UI_PLAYER_ATTRIBUTE_START_Y_MOBILE', 795);
             this.set('UI_PLAYER_HAND_START_X_MOBILE',260);
             this.set('UI_PLAYER_HAND_START_Y_MOBILE',920);
             this.set('UI_PLAYER_HAND_START_X_PC',30);
             this.set('UI_PLAYER_HAND_START_Y_PC', 610);
             this.set('UI_PLAYER_HAND_MOVE_X_MOBILE', 80);
             this.set('UI_PLAYER_HAND_MOVE_Y_MOBILE', 920);
             this.set('UI_PLAYER_HAND_MOVE_X_PC', 30);
             this.set('UI_PLAYER_HAND_MOVE_Y_PC', 90);
             this.set('UI_PLAYER_PANEL_FONT_COLOR', '#000000');
             this.set('UI_PLAYER_PANEL_FONT_HEIGHT', 12);
             this.set('UI_PLAYER_HAND_PADDING_PC', 0);
             this.set('UI_PLAYER_HAND_PADDING_MOBILE', 0);
             this.set('UI_PLAYER_PANEL_HEIGHT', 30);
             this.set('UI_PLAYER_PANEL_WIDTH', 160);
             this.set('UI_PLAYER_PORTRAIT_WIDTH_PC', 120);
             this.set('UI_PLAYER_PORTRAIT_HEIGHT_PC', 120);
             this.set('UI_PLAYER_PORTRAIT_WIDTH_MOBILE', 70);
             this.set('UI_PLAYER_PORTRAIT_HEIGHT_MOBILE', 70);
             this.set('UI_PLAYER_PORTRAIT_YPOS', 25);
             this.set('UI_HAND_ICON_WIDTH_PC', 40)
             this.set('UI_HAND_ICON_HEIGHT_PC', 40)
             this.set('UI_HAND_ICON_X_PC', 141);
             this.set('UI_HAND_ICON_Y_PC', 555);
             this.set('UI_HAND_ICON_X_MOBILE', 7);
             this.set('UI_HAND_ICON_Y_MOBILE', 860);
             this.set('UI_HAND_ICON_WIDTH_MOBILE', 40);
             this.set('UI_HAND_ICON_HEIGHT_MOBILE', 40);
             this.set('UI_IMAGE_URL_PC', '/UI/pc/');
             this.set('UI_IMAGE_URL_MOBILE', '/UI/mobile/');
             this.set('UI_CREW_ICON_WIDTH_PC', 15);
             this.set('UI_CREW_ICON_HEIGHT_PC', 37);
             this.set('UI_CREW_ICON_WIDTH_MOBILE', 11);
             this.set('UI_CREW_ICON_HEIGHT_MOBILE', 28);
             this.set('UI_CREW_ICON_PLAYER_X_PC', 5);
             this.set('UI_CREW_ICON_PLAYER_Y_PC', 550);
             this.set('UI_CREW_ICON_PLAYER_X_MOBILE', 75);
             this.set('UI_CREW_ICON_PLAYER_Y_MOBILE', 950);
             this.set('UI_CREW_ICON_OPPONENT_X_PC', 1250);
             this.set('UI_CREW_ICON_OPPONENT_Y_PC', 130);
             this.set('UI_CREW_ICON_OPPONENT_X_MOBILE', 680);
             this.set('UI_CREW_ICON_OPPONENT_Y_MOBILE', 23);
             this.set('UI_CREW_COUNT_PLAYER_X_PC', 35);
             this.set('UI_CREW_COUNT_PLAYER_Y_PC', -10);
             this.set('UI_CREW_COUNT_PLAYER_X_MOBILE', 20);
             this.set('UI_CREW_COUNT_PLAYER_Y_MOBILE', -7);
             this.set('UI_CREW_COUNT_OPPONENT_X_PC', -70);
             this.set('UI_CREW_COUNT_OPPONENT_Y_PC' , -10);
             this.set('UI_CREW_COUNT_OPPONENT_X_MOBILE', -45);
             this.set('UI_CREW_COUNT_OPPONENT_Y_MOBILE', -7);
             this.set('UI_CREW_COUNT_FONT_SIZE_PC', 30);
             this.set('UI_CREW_COUNT_FONT_SIZE_MOBILE', 17);
             this.set('UI_CREW_COUNT_LINEWIDTH_PC', .5);
             this.set('UI_CREW_COUNT_LINEWIDTH_MOBILE', .5);
             this.set('UI_CREW_COUNT_FONT', 'Digitek');
             this.set('UI_CREW_COUNT_COLOR_FILL', '#dd1c3d');
             this.set('UI_CREW_COUNT_COLOR_STROKE', '#eaeace');
             this.set('UI_LEAD_ICON_WIDTH_PC', 22);
             this.set('UI_LEAD_ICON_HEIGHT_PC', 40);             
             this.set('UI_LEAD_ICON_WIDTH_MOBILE', 11);
             this.set('UI_LEAD_ICON_HEIGHT_MOBILE', 20);
             this.set('UI_LEAD_ICON_PLAYER_X_PC', 140);
             this.set('UI_LEAD_ICON_PLAYER_Y_PC', 615);
             this.set('UI_LEAD_ICON_PLAYER_X_MOBILE', 220);
             this.set('UI_LEAD_ICON_PLAYER_Y_MOBILE', 955);
             this.set('UI_LEAD_ICON_OPPONENT_X_PC', 1130);
             this.set('UI_LEAD_ICON_OPPONENT_Y_PC', 10);
             this.set('UI_LEAD_ICON_OPPONENT_X_MOBILE', 545);
             this.set('UI_LEAD_ICON_OPPONENT_Y_MOBILE', 28);
             this.set('UI_LEAD_COUNT_PLAYER_X_PC', 35);
             this.set('UI_LEAD_COUNT_PLAYER_Y_PC', -10);
             this.set('UI_LEAD_COUNT_PLAYER_X_MOBILE', 25);
             this.set('UI_LEAD_COUNT_PLAYER_Y_MOBILE', -4);
             this.set('UI_LEAD_COUNT_OPPONENT_X_PC', -80);
             this.set('UI_LEAD_COUNT_OPPONENT_Y_PC', -10);
             this.set('UI_LEAD_COUNT_OPPONENT_X_MOBILE', -40);
             this.set('UI_LEAD_COUNT_OPPONENT_Y_MOBILE', -4);
             this.set('UI_LEAD_COUNT_COLOR_FILL', '#d4923b');
             this.set('UI_LEAD_COUNT_COLOR_STROKE', '#eaeace');             
             this.set('UI_LEAD_COUNT_LINEWIDTH', .5);
             this.set('UI_LEAD_COUNT_FONT_SIZE_PC', 30);
             this.set('UI_LEAD_COUNT_FONT_SIZE_MOBILE', 17);
             this.set('UI_LEAD_COUNT_LINEWIDTH_PC', .5);
             this.set('UI_LEAD_COUNT_LINEWIDTH_MOBILE', .5);
             this.set('UI_LEAD_COUNT_FONT', 'Digitek');
             this.set('UI_POWER_ICON_WIDHT_PC', 19);
             this.set('UI_POWER_ICON_HEIGHT_PC', 30);
             this.set('UI_POWER_ICON_WIDHT_MOBILE', 14);
             this.set('UI_POWER_ICON_HEIGHT_MOBILE', 23);
             this.set('UI_POWER_ICON_PLAYER_X_PC', 140);
             this.set('UI_POWER_ICON_PLAYER_Y_PC', 670);
             this.set('UI_POWER_ICON_PLAYER_X_MOBILE', 150);
             this.set('UI_POWER_ICON_PLAYER_Y_MOBILE', 955);
             this.set('UI_POWER_ICON_OPPONENT_X_PC', 1135);
             this.set('UI_POWER_ICON_OPPONENT_Y_PC', 60);
             this.set('UI_POWER_ICON_OPPONENT_X_MOBILE', 607);
             this.set('UI_POWER_ICON_OPPONENT_Y_MOBILE', 27);
             this.set('UI_POWER_COUNT_PLAYER_X_PC', 35);
             this.set('UI_POWER_COUNT_PLAYER_Y_PC', -10);
             this.set('UI_POWER_COUNT_PLAYER_X_MOBILE', 25);
             this.set('UI_POWER_COUNT_PLAYER_Y_MOBILE', -7);
             this.set('UI_POWER_COUNT_OPPONENT_X_PC', -85);
             this.set('UI_POWER_COUNT_OPPONENT_Y_PC', -7);
             this.set('UI_POWER_COUNT_OPPONENT_X_MOBILE', -39);
             this.set('UI_POWER_COUNT_OPPONENT_Y_MOBILE', -6);
             this.set('UI_POWER_COUNT_FONT_SIZE_PC', 30);
             this.set('UI_POWER_COUNT_FONT_SIZE_MOBILE', 17);
             this.set('UI_POWER_COUNT_LINEWIDTH_PC', .5);
             this.set('UI_POWER_COUNT_LINEWIDTH_MOBILE', .5);
             this.set('UI_POWER_COUNT_FONT', 'Digitek');
             this.set('UI_POWER_COUNT_COLOR_FILL', '#382262');
             this.set('UI_POWER_COUNT_COLOR_STROKE', '#eaeace');
             
        },
        constants: {},
        ownProp: Object.prototype.hasOwnProperty,
        allowed: {
            string: 1,
            number: 1,
            boolean: 1,
            object: 1
        },
        prefix: (Math.random() + "_").slice(2),

        set: function (name, value) {
            if (this.isDefined(name)) {
                return false;
            }
            if (!this.ownProp.call(this.allowed, typeof value)) {
                return false;
            }
            this.constants[this.prefix + name] = value;
            return true;
        },
        isDefined: function (name) {
            return this.ownProp.call(this.constants, this.prefix + name);

        },
        get: function (name) {
            if (this.isDefined(name)) {
                if (this.constants[this.prefix + name] === undefined || this.constants[this.prefix + name] === null) {
                    throw 'Constant is null or undefined';
                }
                return this.constants[this.prefix + name];
            }
            else {
                throw 'Constant Name, '+ name +' Not Defined';
                return null;
            }
            
        }
    });
